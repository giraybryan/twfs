$(document).ready(function(){
	$('#has_variant').change(function(){
		
		if($(this).is(':checked')){
			has_variant = 1;
			$('.variant-btn').removeClass('hide')
		} else {
			has_variant = 0;
			$('.variant-btn').addClass('hide')

		}
		$.ajax({
			type: "get",
			url : $('#baseurl').val()+"merchandises/form_variant",
			data: { has_variant : has_variant, id : $('#id').val() },
			success: function(response){
				$('#variant_wrapper').html(response);
			}
		})
	})

	$(this).on('click','.addvariant',function(e){
		e.preventDefault();
		child = $('#variant_wrapper table tbody tr').length 
		if( $('[name="[variant]['+child+'][variant]"]').length > 0 ){
			child++;
		}

		tr = "<tr>"
		tr += "<td><input type='' name='variant["+child+"][variant]' value='' class='form-control'/></td>"
		tr += "<td><input type='' name='variant["+child+"][cost]' value='' class='form-control'/></td>"
		tr += "<td><input type='' name='variant["+child+"][price]' value='' class='form-control'/></td>"
		tr += "<td><button class='btn-remove-tr btn btn-danger'><i class='fa fa-close'></i></button></td>"
		tr += "</tr>"
		$('#variant_wrapper table tbody').append(tr)
	})
	$(this).on('click','.btn-remove-tr',function(){})
});

