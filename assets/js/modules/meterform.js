$(document).ready(function(){

	
	$(this).on('click','.action-btn', function(){
		action_type = $(this).attr('data-action');
		encode_data = JSON.parse( $(this).closest('td').attr('data-encode'));
		switch(action_type){
			case "edit":
				$('#metermodal').modal('show');
				$.each(encode_data,function(index, value){
					$('[name="'+index+'"]').val(value);
				});
				$('.form_result').html('')
				break;
			case "add" :
				$('#readingmodal').modal('show');
				$.ajax({
					type: 'GET',
					data: {id : encode_data.id},
					url: 'meters/get_option_meters',
					success: function(response){
						$('#meter_id').html(response);
						$('#meter_id').attr('readonly', true);
						$('.datepick').datepicker();
					}
				});
				break;
			case "delete":
				delete_meter(encode_data.id);
				break;
		}
	});

	$('#meterform').submit(function(e){
		e.preventDefault();
		formdata = new FormData( $('#meterform')[0] );
		result_container = $(this).find('.form_result');
		$.ajax({
			type: $(this).attr('method'),
			url: $(this).attr('action'),
			data: formdata,
			dataType: 'json',
			async: false,
			cache: false,
			contentType: false,
			processData: false,
			success: function(response){
				if( response.result == 1 ){
					$('#metermodal').modal('hide');
					$('#module_list').load('meters/paginate');
				} else {
					$(result_container).html('<div class="alert alert-danger">'+response.msg+'</div>')
				}

			}
		});
	});
});

function delete_meter(meter_id){
	if ( confirm("Are you sure you want to delete this meter?") ){
		$.ajax({
			type: 'POST',
			url: 'meters/delete',
			data: {id : meter_id},
			dataType: "json",
			success: function(response){
				if( response.result == 1 ){
					$('#module_list').load('meters/paginate');
				} 
				alert(response.msg)
			}
		});
	}
}