$(document).ready(function(){
	
	$('input[name="daterange"]').daterangepicker();

	$('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
  getby_range(picker.startDate.format('YYYY-MM-DD'), picker.endDate.format('YYYY-MM-DD') )

});
	
});

function getby_range(start_date, end_date){
	$.ajax({ 
		type: 'POST',
		url: $("#baseurl").val()+'reports/sales_expensechart_ajax',
		data: { start_date : start_date, end_date : end_date, type : 'range' },
		success: function(response){
			$('#sales_ex_compare').html(response)
		}
	})
}
