$(document).ready(function(){
	getsales();
	$(this).on('click','.markpaid',function(){
		var r = confirm("Are you sure you want to mark this one as paid?");
		if (r == true) {
		    $.ajax({
		    	type : 'POST',
		    	url : $('#baseurl').val()+"chillers/ajaxupdate_sales",
		    	data: { id : $(this).attr('data-id'), paid : 1},
		    	dataType: 'json',
		    	success: function(response){
		    		alert(response.msg);
		    		if( response.result == 1 ){
		    		getsales();
		    		}
		    	}
		    }); 
		} 
	});

	$(this).on('click','[data-action="delete"]',function(){
		var r = confirm("Are you sure you want to delete this sales? you cannot revert once you confirm");
		if (r == true) {
		    $.ajax({
		    	type : 'POST',
		    	url : $('#baseurl').val()+"chillers/revertsales",
		    	data: { id : $(this).attr('data-id'), paid : 1},
		    	dataType: 'json',
		    	success: function(response){
		    		alert(response.msg);
		    		if( response.result == 1 ){
		    		getsales();
		    		}
		    	}
		    }); 
		} 
	});

	$('#sales_date').change(function(){
		getsales();
	})
	$('#membersearch').keyup(function(){

		$.ajax({
			type: 'POST',
			url: $('#baseurl').val()+"members/search",
			data: { namesearch : $(this).val()},
			success: function(response){
				$('#member_searchresult').html(response)
			}
		});
	});

	$(this).on('click','.memberselect',function(){

			$('#member_id').val($(this).attr('data-id'));
			$('#member_id').trigger('change');
			$('#membersearch').val();
			$('#memberselected').html($(this).attr('data-firstname')+' '+$(this).attr('data-lastname'));
			membershiplist($(this).attr('data-id'));

	});
});

function getsales(){
	$.ajax({ 
		type: 'POST',
		url: $("#baseurl").val()+'chillers/fetchsales',
		data: { date : $('#sales_date').val() },
		success: function(response){
			$('#module_list').html(response)
		}
	})
}

function membershiplist(membership_id){
	$.ajax({
	type: 'GET',
	url : $('#baseurl').val()+"memberships/search_merbershiplog/",
	data : { id  : membership_id },
	success : function(response){
		$('#membership_log_result').html(response)
	}
})
}