$(document).ready(function(){
	
	$('#simple_go').click(function(){
		$('#module_list').html("<div class='alert alert-info'> Loading...</div>")
		$.ajax({
			type: 'POST',
			url: $('#baseurl').val()+"members/search_list",
			data: { namesearch : $('#membersearch').val(), class_type_id : $('#filter_class').val() },
			success: function(response){
				$('#module_list').html(response)
			}
		});
	})
	$('#filter_class').change(function(){
		$('#simple_go').trigger('click');
	});
	$('#membersearch').keyup(function(e){
		var code = e.which; // recommended to use e.which, it's normalized across browsers
    if(code==13){
    	e.preventDefault();
    	$('#simple_go').trigger('click');
    }
		
	});

	$(this).on('click','.memberselect',function(){

			$('#member_id').val($(this).attr('data-id'));
			$('#member_id').trigger('change');
			$('#customername').html($(this).attr('data-firstname')+' '+$(this).attr('data-lastname'));
			membershiplist($(this).attr('data-id'));

	});

	$('#member_id').change(function(){
			
		if( $(this).val() == "") {
			$('.form2').addClass('hide');
		} else {
			$('#membersearch').val(''); $('#member_searchresult').html('')
			$('.form2').removeClass('hide')
		}
	})
});


function membershiplist(membership_id){
	$.ajax({
	type: 'GET',
	url : $('#baseurl').val()+"memberships/search_merbershiplog/",
	data : { id  : membership_id },
	success : function(response){
		$('#membership_log_result').html(response)
	}
})
}