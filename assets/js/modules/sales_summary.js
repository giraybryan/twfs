$(document).ready(function(){
	getsales();
	
	$('#date').change(function(){
		getsales();
	})
	$('input[name="daterange"]').daterangepicker();

	$('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
  getsales_range(picker.startDate.format('YYYY-MM-DD'), picker.endDate.format('YYYY-MM-DD') )

});
	$('#membersearch').keyup(function(){

		$.ajax({
			type: 'POST',
			url: $('#baseurl').val()+"members/search",
			data: { namesearch : $(this).val()},
			success: function(response){
				$('#member_searchresult').html(response)
			}
		});
	});

	$(this).on('click','.memberselect',function(){

			$('#member_id').val($(this).attr('data-id'));
			$('#member_id').trigger('change');
			$('#membersearch').val();
			$('#memberselected').html($(this).attr('data-firstname')+' '+$(this).attr('data-lastname'));
			membershiplist($(this).attr('data-id'));

	});
});

function getsales(){
	$.ajax({ 
		type: 'POST',
		url: $("#baseurl").val()+'sales/salesdata_bydate',
		data: { date : $('#date').val(), type: 'date' },
		success: function(response){
			$('#sales').html(response)
		}
	})
}
function getsales_range(start_date, end_date){
	$.ajax({ 
		type: 'POST',
		url: $("#baseurl").val()+'sales/salesdata_bydate',
		data: { start_date : start_date, end_date : end_date, type : 'range' },
		success: function(response){
			$('#sales').html(response)
		}
	})
}

function membershiplist(membership_id){
	$.ajax({
	type: 'GET',
	url : $('#baseurl').val()+"memberships/search_merbershiplog/",
	data : { id  : membership_id },
	success : function(response){
		$('#membership_log_result').html(response)
	}
})
}