$(document).ready(function(){
	getexpenses();
	
	$('#date').change(function(){
		getexpenses();
	})

	$('input[name="daterange"]').daterangepicker();

		$('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
	  getexpenses_range(picker.startDate.format('YYYY-MM-DD'), picker.endDate.format('YYYY-MM-DD') )

	});
});

function getexpenses(){
	$.ajax({ 
		type: 'POST',
		url: $("#baseurl").val()+'expenses/expensedata_bydate',
		data: { date : $('#date').val() },
		success: function(response){
			$('#expense').html(response)
		}
	})

	$.ajax({ 
		type: 'POST',
		url: $("#baseurl").val()+'sales/salesdata_bydate',
		data: { date : $('#date').val(), type: 'date' },
		success: function(response){
			$('#sales').html(response)
		}
	})
}

function getexpenses_range(start_date, end_date){
	$.ajax({ 
		type: 'POST',
		url: $("#baseurl").val()+'expenses/expensedata_bydate',
		data: { start_date : start_date, end_date : end_date, type : 'range' },
		success: function(response){
			$('#expense').html(response)
		}
	})

	$.ajax({ 
		type: 'POST',
		url: $("#baseurl").val()+'sales/salesdata_bydate',
		data: { start_date : start_date, end_date : end_date, type : 'range' },
		success: function(response){
			$('#sales').html(response)
		}
	})
}

