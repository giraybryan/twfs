ajaxrequest = ""
$(document).ready(function(){
    $('.edit-rate').keyup(function(){
        rate = $(this).val();
        class_type_id = $(this).attr('data-id');
        field = $(this).attr('data-field');
        if( typeof ajaxrequest == "object" ){
            ajaxrequest.abort();
            console.log(ajaxrequest);
        }
        alertify.dismissAll();
        ajaxrequest = $.ajax({
            type: "POST",
            url : $('#baseurl').val()+'walkins/update_rate',
            data : { class_type_id : class_type_id, rate : rate, field : field},
            dataType: "json",
            success: function(response){
                if( response.result == 1){
                    alertify.success(response.msg);
                } else {
                    alertify.error(response.msg);
                }
                ajaxrequest = ""
            }
        })
    });
})