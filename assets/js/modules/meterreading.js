$(document).ready(function(){
	
	$(this).on('click','.read-action-btn',function(){
		action_type = $(this).attr('data-action');
		encode_data = JSON.parse( $(this).closest('td').attr('data-encode'));
		switch(action_type){
			case "edit":
				$('#readingmodal').modal('show');
				$.each(encode_data,function(index, value){
					$('[name="'+index+'"]').val(value);
				});
				$('.form_result').html('')
				break;
			case "delete":
				deleteread(encode_data.id);
				break;
		}
	});
	
	$('#readingform').submit(function(e){
		e.preventDefault();
		formdata = new FormData( $('#readingform')[0] );
		result_container = $(this).find('.form_result');
		$.ajax({
			type: $(this).attr('method'),
			url: $(this).attr('action'),
			data: formdata,
			dataType: 'json',
			async: false,
			cache: false,
			contentType: false,
			processData: false,
			success: function(response){
				if( response.result == 1 ){
					$('#readingmodal').modal('hide');
					$('#module_list').load('meter_reading/paginate');
				} else {
					$(result_container).html('<div class="alert alert-danger">'+response.msg+'</div>')
				}

			}
		});
	});
});

function deleteread(meter_id){
	if ( confirm("Are you sure you want to delete this reading?") ){
		$.ajax({
			type: 'POST',
			url: 'meter_reading/delete',
			data: {id : meter_id},
			dataType: "json",
			success: function(response){
				if( response.result == 1 ){
					$('#module_list').load('meter_reading/paginate');
				} 
				alert(response.msg)
			}
		});
	}
}

