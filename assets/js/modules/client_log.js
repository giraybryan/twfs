$(document).ready(function(){
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          $('#search').trigger('click');
          return false;
        }
      });

	$(this).on('click','.setsess',function(){
        memberid_no = $('#memberid_no').val()
        membership_log_id = $(this).attr('data-membership')
        alertify.prompt( 'Client Password', 'Insert Client Password', ''
                , function(evt, value) { 
                    //validate if password is correct 
                    $.ajax({
                        type : "POST",
                        url : $('#baseurl').val()+'clientlog/confirm_session',
                        data : { memberid_no : memberid_no, membership_log_id : membership_log_id, password : value},
                        dataType : "json",
                        success: function(response){
                            if( response.result == 1 ){
                                alertify.success('Session Counted');
                                $('#search').trigger('click')
                            } else {
                                alertify.error(response.msg) 
                            }
                        }
                    })
                }
                , function() { alertify.error('Cancel') 
        }).set('type', 'password');
    });
    $(this).on('click','.setatten',function(){
        memberid_no = $('#memberid_no').val()
        membership_log_id = $(this).attr('data-membership')
        $.ajax({
            type : "POST",
            url : $('#baseurl').val()+'clientlog/post_attendance',
            data : { memberid_no : memberid_no, membership_log_id : membership_log_id },
            dataType : "json",
            success: function(response){
                if( response.result == 1 ){
                    alertify.success('Attendance Counted');
                    fetch_attendance();
                } else {
                    alertify.error(response.msg) 
                }
            }
        })
    });
    $('#search').click(function(e){
        number_id = $('#memberid_no').val();
        if( number_id != "" ){
            $('#result').html("searching...");
            $.ajax({
                type : "POST",
                url  : $('#baseurl').val()+"clientlog/search_member",
                data : { id : number_id },
                success : function(response ){
                    $('#result').html(response);
                    setTimeout(function(){
                        if( $('.setatten').length > 0 ){
                            $('.setatten').trigger('click');
                        }
                    },500)
                }
            })

            if( number_id == 1366 ){
                $.ajax({
                    type: "POST",
                    url  : $('#baseurl').val()+"clientlog/send_sms",
                    data : { id : number_id },
                     dataType : "json",
                    success : function(response ){
                        if( response.result == 1 ){
                            alertify.success(response.msg);
                        } else {
                            alertify.error(response.msg) 
                        }
                    }
                })
            }
        }
    })
})

function fetch_attendance(){
    $.ajax({
        type: "POST",
        url  : $('#baseurl').val()+"clientlog/get_attendance",
        success : function(response ){
            $('#attedance_wrapper').html(response)
        }
    })
}