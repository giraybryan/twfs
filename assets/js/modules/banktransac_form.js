$(document).ready(function(){
	$('#bankid').change(function(){
		$.ajax({
			type: "get",
			url: $("#baseurl").val()+'banks/getBalance/'+$('#bankid').val(),
			success: function(response){
				$('#current_balance').html('Current Balance : '+response);
			}
		})
	})
	$('#transaction_type, #date').change(function(){
		if( $('#transaction_type').val() == "deposit" ){
			date = $('#date').val()
			var _date = new Date(date);
			month = _date.getMonth()+1
			month = month < 10 ? '0'+month : month
			date = _date.getFullYear()+'-'+month+'-'+_date.getDate();
			$.ajax({
				type: "get",
				url: $("#baseurl").val()+'cashflows/getCOH/'+date,
				success: function(response){
					$('#current_coh').html('COH :'+response)
				}
			})	
		} else {
			$('#current_coh').html('')
		}
		
	}) 
});

