$(document).ready(function(){
    $('#uploadimage').click(function(e){
        e.preventDefault();
        $('#userfile').trigger('click');
    })

    $('#userfile').change(function(){
        $(this).closest('form').submit();
    })
    $('.info-edit').keyup(function(){
        value = $(this).val();
        $.ajax({
            type: 'post',
            url : $('#baseurl').val()+'members/ajax_update',
            data : { value : value , field : $(this).attr('data-field'), id : $('#member_id').val() },
            dataType: "json",
            success: function(response){
                if( response.result == 1){
                    alertify.success(response.msg);
                } else {
                    alertify.error(response.msg);
                }
            }
        });
    });

    $('#update_pass').click(function(){
        msg = "<strong>Required Fields:</strong><br/>";
        error = 0;
        if( $('#old_pass').length > 0 && $('#old_pass').val() == ""){
            msg += "Old password<br/>";
            error++;
        }
        if( $('#new_pass').length > 0 && $('#new_pass').val() == ""){
            msg += "New password<br/>";
            error++;
        }
        if( $('#confirm_pass').length > 0 && $('#confirm_pass').val() == ""){
            msg += "Confirm password<br/>";
            error++;
        }

        if( error == 0 ){
            $.ajax({
                type: "post",
                url : $('#baseurl').val()+'members/update_password',
                data: { id : $('#member_id').val(), confirm_pass : $('#confirm_pass').val(), new_pass : $('#new_pass').val(), old_pass : $('#old_pass').val() },
                dataType: "json",
                success: function(response){
                    if( response.result == 1){
                        alertify.success(response.msg);
                    } else {
                        alertify.error(response.msg);
                    }
                }
            })
        } else {
            alertify.error(msg);
        }
    })

    $(this).on('click','.freeze',function(){
		// alertify.confirm("This is a confirm dialog.",
		// function(){
		// 	alertify.success('Ok');
		// },
		// function(){
		// 	alertify.error('Cancel');
		// });
		$('#freeze_modal').modal('show')
    });
    $(this).on('click','.edit-note',function(){
        $(this).closest('.note_display').addClass('hide')
        $(this).closest('.editnote').find('.note_edit').removeClass('hide');
    })
    $(this).on('click','.save-note',function(){
        newvalue = $(this).closest('.note_edit').find('textarea').val();
        $(this).closest('.note_edit').addClass('hide')
        parent  = $(this).closest('.editnote');
        $(parent).find('.note_display').removeClass('hide');
        $(parent).find('.historynote').html(newvalue)
        
    })
    $('.save_freeze').click(function(){
        alertify.confirm("This is a confirm dialog.",
            function(){
                $.ajax({
                    type: "POST",
                    url : $('#baseurl').val()+'members/freeze_account',
                    data : { start_date : $('#start_date').val(), months : $('#month_freeze').val(), member_id : $('#member_id').val() },
                    dataType: "json",
                    success: function(response){
                        if( response.result == 1 ){
                            alertify.success(response.msg);
                            $('#freeze_modal').modal('hide');
                            window.location.href= '';

                        } else {
                            alertify.error(response.msg);
                        }
                    }
    
                })
            },
            function(){
                alertify.error('Cancel');
            });
        
    });
});