$(document).ready(function(){
    $('#membersearch').keyup(function(e){
		if( $('#membersearch').val().length > 2){
            $('#member_searchresult').html('searching...')
			$.ajax({
				type: 'POST',
				url: $('#baseurl').val()+"members/search",
				data: { namesearch : $(this).val()},
				success: function(response){
					$('#member_searchresult').html(response)
				}
			});
		}
    });
    $("#customername").keypress(function(e){
        e.preventDefault();
        
    })
    $(this).on('click','.memberselect',function(){

        $('#member_id').val($(this).attr('data-id'));
        $('#member_id').trigger('change');
        $('#customername').val($(this).attr('data-firstname')+' '+$(this).attr('data-lastname'));
        membershiplist($(this).attr('data-id'));
        $('#member_searchresult').html('')
        //filter class for the member
        $.ajax({ 
            type: "POST",
            url : $("#baseurl").val()+"memberships/filter_clssass_type",
            data: { member_id : $('#member_id').val() },
            success: function(response){
                $('[name="class_type_id"]').html(response);
            }
        })

    });
    $('#walkin_type').change(function(){
        if( $('[name="class_type_id"]').val() != "" ){
            $('[name="class_type_id"]').trigger('change')
        }
        if( $(this).val() == "member" ){
            $("#member_search").removeClass('hide');
            $('#customername').attr('required',true)
        } else {
            $("#member_search").addClass('hide');
            $('#customername').removeAttr('hide');
            $('#customername').removeAttr('required')

        }
    })
    $('[name="class_type_id"]').change(function(){
		if($(this).val()){
			
			$.ajax({
				type: 'POST',
				url: $('#baseurl').val()+'walkins/walkin_rate',
				data: { class_type_id : $(this).val(), walkin_type : $('#walkin_type').val() },
				success: function(response){
                    if( response != "invalid"){
                        $('[name="amount"]').val(response); 
                        $('[type="submit"]').removeAttr('disabled')
                    } else {
                        $('[name="amount"]').val('');
                        $('[type="submit"]').attr('disabled', true)
                    }
                    
				}
			});
		} else {
			
			$('[name="amount"]').val(0)
		}
	})
})