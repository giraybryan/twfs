$(document).ready(function(){
	
	
	$('#membersearch').keyup(function(){

		$.ajax({
			type: 'POST',
			url: $('#baseurl').val()+"members/search",
			data: { namesearch : $(this).val()},
			success: function(response){
				$('#member_searchresult').html(response)
			}
		});
	});

	$(this).on('click','.memberselect',function(){

			$('#member_id').val($(this).attr('data-id'));
			$('#member_id').trigger('change');
			$('#member_searchresult').html('')
			$('#membersearch').val();
			$('#memberselected').html($(this).attr('data-firstname')+' '+$(this).attr('data-lastname'));
			membershiplist($(this).attr('data-id'));

	});
});


function membershiplist(membership_id){
	$.ajax({
	type: 'GET',
	url : $('#baseurl').val()+"memberships/search_merbershiplog/",
	data : { id  : membership_id },
	success : function(response){
		$('#membership_log_result').html(response)
	}
})
}