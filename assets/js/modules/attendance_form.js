$(document).ready(function(){
	
	$('#attendance_type').change(function(){
		$('.formtype').addClass('hide');
		$('.'+$(this).val()+'form').removeClass("hide");

		$('[name="class_type_id"]').trigger('change')

	})

	$('[name="class_type_id"]').change(function(){
		if($(this).val()  && $('#attendance_type').val() == "walkin"){
			
			$.ajax({
				type: 'POST',
				url: $('#baseurl').val()+'classes/walkin_rate',
				data: { class_type_id : $(this).val() },
				success: function(response){
					$('[name="amount"]').val(response);
				}
			});
		} else {
			
			$('[name="amount"]').val(0)
		}
	})
	$(this).on('click','.reset_start',function(){
		$.ajax({
			type: 'POST',
			url: $('#baseurl').val()+'memberships/reset_start',
			data: { id : $(this).attr('data-id') },
			success: function(response){
				membershiplist($('#member_id').val())
			}
		})
	});
	$('#membersearch').keyup(function(e){
		if( $('#membersearch').val().length > 2){
			$.ajax({
				type: 'POST',
				url: $('#baseurl').val()+"members/search",
				data: { namesearch : $(this).val()},
				success: function(response){
					$('#member_searchresult').html(response)
				}
			});
			}
	});

	$(this).on('click','.memberselect',function(){

			$('#member_id').val($(this).attr('data-id'));
			$('#member_id').trigger('change');
			$('#customername').html($(this).attr('data-firstname')+' '+$(this).attr('data-lastname'));
			membershiplist($(this).attr('data-id'));

			//filter class for the member
			$.ajax({ 
				type: "POST",
				url : $("#baseurl").val()+"memberships/filter_clssass_type",
				data: { member_id : $('#member_id').val() },
				success: function(response){
					$('[name="class_type_id"]').html(response);
				}
			})

	});

	$('#member_id').change(function(){
			
		if( $(this).val() == "") {
			$('.form2').addClass('hide');
		} else {
			$('#membersearch').val(''); $('#member_searchresult').html('')
			$('.form2').removeClass('hide')
		}
	})
});


function membershiplist(membership_id){
	$.ajax({
	type: 'GET',
	url : $('#baseurl').val()+"memberships/search_merbershiplog/",
	data : { id  : membership_id },
	success : function(response){
		$('#membership_log_result').html(response)
	}
})
}