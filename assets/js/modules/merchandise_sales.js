$(document).ready(function(){
    $('#sales_date').datepicker();
    $('#sales_date').change(function(){
        $.ajax({
            type: "POST",
            url : $('#baseurl').val()+'merchandises/daily_sales',
            data : { date : $(this).val() },
            success: function( response ){
                $('#module_list').html(response)
            }
        })
    });

});