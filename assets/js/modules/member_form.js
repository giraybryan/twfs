$(document).ready(function(){
	$('#referralby').keyup(function(){
		$('#member_searchresult').html("")
		if( $(this).val() != ""){
			$.ajax({
				type: 'POST',
				url: $('#baseurl').val()+"members/search",
				data: { namesearch : $(this).val()},
				success: function(response){
					$('#member_searchresult').html(response)
				}
			});	
		}
		
	});

	$('[data-toggle="tooltip"]').tooltip();   

	$(this).on('click','.memberselect',function(){

			$('#referral_id').val($(this).attr('data-id'));
			$('#referral_id').trigger('change');
			$('#customername').html($(this).attr('data-firstname')+' '+$(this).attr('data-lastname'));
			$('#member_searchresult').html("")
			$('#referralby').val('')

	});
});

