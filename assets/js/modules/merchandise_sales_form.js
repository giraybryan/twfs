$(document).ready(function(){
    $('#add_btn').click(function(){
        variant_id = $('#merchandise_item').val();
        
        if( variant_id == '')
        {
            return false;
        }

        jsonstr = $('#merchandise_item option[value="'+variant_id+'"]').attr('data-json')
        jsonobj = JSON.parse(jsonstr)
        if( $('#list').find('tr#item'+jsonobj.id).length == 0  )
        {
            ctr = parseInt($('#list').attr('data-ctr'))

            rowform = "<tr id='item"+jsonobj.id+"'>"
            rowform += "<td>"+jsonobj.item+" ("+jsonobj.stock_var+") <input type='hidden' value='"+jsonobj.id+"' name='item["+ctr+"][variant_id]' /></td>"
            rowform += "<td>"+jsonobj.price_var+"<input type='hidden' value='"+jsonobj.price_var+"' name='item["+ctr+"][cost]' /></td>"
            rowform += "<td><input style='max-width: 80px;' max='"+jsonobj.stock_var+"' data-price='"+jsonobj.price_var+"' min=1 type='number' class='form-control item-qty' value=1 name='item["+ctr+"][qty]'/></td>"
            rowform += "<td class='rowcost text-center'>"+jsonobj.price_var+"</td>"
            rowform += "</tr>"
            
            if( $('#list').html()){
                $('#list').append(rowform)
            } else {
                $('#list').html(rowform);
            }

            $('#list').attr('data-ctr', ctr+1);

            compute_total();
        }
        

    })
    $(this).on('keyup','.item-qty',function(e){

        qty = parseInt($(this).val())
        cost = parseFloat($(this).attr('data-price'))
        $(this).closest('tr').find('.rowcost').html((qty * cost).toFixed(2))
        compute_total();
    })
    $(this).on('change','.item-qty',function(e){

        qty = parseInt($(this).val())
        cost = parseFloat($(this).attr('data-price'))
        $(this).closest('tr').find('.rowcost').html((qty * cost).toFixed(2))
        compute_total();
    })

    $('#cash').keyup(function(){
        compute_change();
    })

    $('#checkout').click(function(e){
        e.preventDefault();
        alertify
        .confirm("Are you sure you want to Checkout?", 
        function(){
            $('form[name="sales"] [type="submit"]').trigger('click');
          },
          function(){
            alertify.error('Cancel');
          }).setHeader('Checking out');
    })
});

function compute_total(){
    total = 0;
    $('#list tr').each(function(){
        qty = parseInt($(this).find('.item-qty').val());
        cost = parseFloat($(this).find('.item-qty').attr('data-price'));
        total = total + ( qty * cost );
    })
    $('#grandtotal').html(total.toFixed(2))
    compute_change();
}

function compute_change(){
    cash = $('#cash').val() == "" ? 0 : parseInt($('#cash').val())
    total =  parseFloat($('#grandtotal').html())
    change = (cash - total).toFixed(2)
    $('#change').html( change )

    if( change < 0 ){
        $('#checkout').attr('disabled', true);
    } else {
        $('#checkout').attr('disabled', false);
    }
}