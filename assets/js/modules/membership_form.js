$(document).ready(function(){
	$('[name="class_type_id"]').change(function(){
		$('[name="membership_type_id"] option').hide();
		$('[name="membership_type_id"] [data-class="'+$(this).val()+'"]').show();

		if( $('[name="membership_type_id"]').val() != "" && $('[name="promo_id"]').val() != "" ){
			$('[name="promo_id"]').trigger('change')
		}
	});
	$('#membershipform').submit(function(){
		$('#membershipform [type="submit"]').attr('disabled', true)
	});
	$('[name="membership_type_id"]').change(function(){
		$('#amount').val($('[name="membership_type_id"] option[value="'+$(this).val()+'"]').attr('data-amount'));

		if( $('[name="class_type_id"]').val() != "" && $('[name="promo_id"]').val() != "" ){
			$('[name="promo_id"]').trigger('change')
		}
	});

	$('[name="promo_id"]').change(function(){
		valid = true;
		if( $('[name="membership_type_id"]').val() == ""){
			valid = false;
			alert("Select Duration");
		}
		if( $('[name="class_type_id"]').val() == "" ){
			alert('Select Class Type');
			valid = false;
		}
		if( valid == true){
			$.ajax({
				type: "POST",
				url: $('#baseurl').val()+'memberships/getpromo_price',
				data: { promo_id : $('[name="promo_id"]').val(), membership_type_id : $('[name="membership_type_id"]').val() , class_type_id : $('[name="class_type_id"]').val()},
				success: function(response){
					$('#amount').val(response);
					if(response == "") {
						$('[type="submit"]').attr('disabled', true);
						setTimeout(function(){alert('Remove Promo / click "Select Promo Type" if the amount is empty '); },1500);
					} else {
						$('[type="submit"]').removeAttr('disabled')
					}
				}
			})
		}
	});
});

