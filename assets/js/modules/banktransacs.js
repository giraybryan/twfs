$(document).ready(function(){
	get_transactions();
	
	$('#date').change(function(){
		get_transactions();
	})
	
	$('input[name="daterange"]').daterangepicker();

	$('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
  gettransactions_range(picker.startDate.format('YYYY-MM-DD'), picker.endDate.format('YYYY-MM-DD') )
});
	
});

function get_transactions(){
	$.ajax({ 
		type: 'POST',
		url: $("#baseurl").val()+'banktransacs/transaction_bydate',
		data: { date : $('#date').val(), type: 'date' },
		success: function(response){
			$('#module_list').html(response)
		}
	})
}
function getsales_range(start_date, end_date){
	$.ajax({ 
		type: 'POST',
		url: $("#baseurl").val()+'sales/salesdata_bydate',
		data: { start_date : start_date, end_date : end_date, type : 'range' },
		success: function(response){
			$('#module_list').html(response)
		}
	})
}

function membershiplist(membership_id){
	$.ajax({
	type: 'GET',
	url : $('#baseurl').val()+"memberships/search_merbershiplog/",
	data : { id  : membership_id },
	success : function(response){
		$('#membership_log_result').html(response)
	}
})
}