<?php

class MY_Controller extends CI_Controller {

	public $userdata = array();
	public $header = "templates/header";
	public $footer = "templates/footer";
	public $branch_id = 1;
	public $session_validate = 0;
	public function __construct(){
		parent::__construct();
		$this->userdata = $this->session->userdata('userdata');

		if( empty($this->userdata) || $this->userdata['logged_in'] === FALSE) {
			
			$this->userdata = array('username' => '', 'logged_in' => FALSE);
			$inital_session = array('userdata' => $this->userdata);
			$this->session->set_userdata($inital_session);
			$this->userdata = $this->session->userdata('userdata');
			//redirect(base_url().'non_users', 'refresh');
			$this->session_validate = 1;
		
		} else {
			$this->userdata = $this->session->userdata('userdata');
		}
		$this->load->model('cashflows_model');
		$this->load->model('users_model');
		$this->load->model('membershiptype_model');
		$this->load->model('walkinrates_model');
		$this->load->model('config_model');
		$this->load->model('branch_model');

	}

	public function debug($arr){
		echo "<pre>";
		print_r($arr);
		die;
	}

	public function template($templatefile, $_data = array()){
		$data = $_data;
		$data['userdata'] = $this->userdata;
		$data['session_validate'] = $this->session_validate;
		$data['access'] = $this->superadmin_validation();
		$this->load->view($this->header, $data);
		$this->load->view($templatefile, $_data);
		$this->load->view($this->footer, $data);
	}

	public function login_template(){

	}

	public function cashflow_audtrail($params){
		$amount = $params['amount'];
		$curr_flow = $this->cashflows_model->get_by_date($params['date']);
		if( empty($curr_flow) || $curr_flow->coh == 0 ){
			$current_coh = $this->get_current_coh($params['date']);
		} else {
			$current_coh = $curr_flow->coh;
			switch( $params['fieldselected'] ){
				case "withdraw":
					$current_amount = $curr_flow->withdraw;
					break;
				case "deposit":
					$current_amount = $curr_flow->deposit;
					break;
				case "equity":
					$current_amount = $curr_flow->equity;
					break;
				case "expenses":	
					$current_amount = $curr_flow->expenses;
					break;
				case "coh":
					$current_amount = $curr_flow->coh;
					break;
			}
		}
		if( empty($curr_flow)){
			$coh = $this->get_current_coh($params['date']);
			switch ($params['fieldselected'] ) {
				case 'deposit':
				case 'equity':
				case 'expenses':
					$coh = $coh - $params['amount'];
					break;
				case 'withdraw':
				case 'coh':
					$coh = $coh + $params['amount'];
					break;
			}
			
			$data = array(
				$params['fieldselected'] =>  $params['amount'],
				'date'	=> $params['date'],
				'coh'	=> $coh

			);
			if( $params['fieldselected'] == 'withdraw'){
				$data['coh'] = $params['amount'];
			} 
			
			$this->cashflows_model->add($data);
		} else{

			$amount =$current_amount +  $params['amount'];

			$data = array(
				$params['fieldselected'] =>  $amount,
				'cashid' => $curr_flow->cashid

			);
			
			if( $params['fieldselected'] == 'withdraw' || $params['fieldselected'] == 'coh'){
				$data['coh'] = $curr_flow->coh + $params['amount'];
			} 
			if( $params['fieldselected'] == 'deposit' || $params['fieldselected'] == 'equity' || $params['fieldselected'] == 'expenses' ){
				$data['coh'] = $curr_flow->coh - $params['amount'];
			}

			if( !empty($data) ){
				$this->cashflows_model->edit($data);
			}
		}
		
		

	}

	public function recussiveUndoCoh($params){
		
		//get current coh based on date 
		$coh = $this->cashflows_model->get_by_date($params['date']);
		//minus value to current coh 
		$new_coh = array('cashid' => $coh->cashid);

		if( !empty($params['expenses']) ){
		  $new_coh['expenses'] = $coh->expenses - $params['expenses'];	
		} 

		switch($params['field']){
			case 'expenses':
				$new_coh['coh'] = $coh->coh  + $params['amount'];
				break;
			case 'coh':
				$new_coh['coh'] = $coh->coh - $params['amount'];
				break;
			case 'deposit':
				$new_coh['coh'] = $coh->coh + $params['amount'];
				break;
			case 'withdraw':
				break;
			case 'equity':
				break;
		}
		//update coh
		$return = $this->cashflows_model->edit($new_coh);
		if( $return ){
			//check the cashflow after the selected date
			$advance_coh = $this->cashflows_model->get_cashflow_abovedate($params['date']);
			if( !empty($advance_coh)){
				$params = array('date' => date('Y-m-d', strtotime($advance_coh->date)), 'field' => $params['field'], 'amount' => $params['amount']);
				$this->recussiveUndoCoh($params);
				return false;
			}
		}
		return false;
	}

	public function get_current_coh($date = ""){
		if( empty($date) ){
			$cashflow = $this->cashflows_model->get_latestcash();
		} else {
			$cashflow =  $this->cashflows_model->get_by_date($date);
			if( empty($cashflow)){
				$cashflow = $this->cashflows_model->get_latestcash();
			}
		}

		if( !empty($cashflow ) ){
			return $cashflow->coh;
		} else {
			return false;
		}

	}

	public function superadmin_access(){

		$session = $this->session->userdata('userdata');
		if( $session['username'] != "SUPERADMINB" ){
			return false;
		}
		$user  = $this->users_model->get($session['username']);
		if( !empty($user) &&  $user->superadmin != 1){
			redirect('');
		}
	}

	public function superadmin_validation(){

		$session = $this->session->userdata('userdata');
		if( $session['username'] == "SUPERADMINB" ){
			return 1;
		}

		$user  = $this->users_model->get($session['username']);
		if( !empty($user) && $user->superadmin != 1 ){
			return 0;
		} else {
			return 1;
		}
	}

	public function get_active_branch(){
		return $this->branch_id;
	}
}