<?php 

function format_currency($value, $symbol=true)
{

	if(!is_numeric($value))
	{
		return;
	}
	
	$CI = &get_instance();
	
	if($value < 0 )
	{
		$neg = '- ';
	} else {
		$neg = ' ';
	}
	
	if($symbol)
	{
		#$formatted	= number_format(abs($value), 2, $CI->config->item('currency_decimal'), $CI->config->item('currency_thousands_separator'));
		$formatted	= number_format(abs($value),2);
		
		if($CI->config->item('currency_symbol_side') == 'left')
		{
			$formatted	= $neg.'<span class="currency_code">'.$CI->config->item('currency_symbol').'</span> '.$formatted;
		}
		else
		{
			$formatted	= $neg.'<span class="currency_code">'.$CI->config->item('currency_symbol').'</span> '.$formatted;
		}
	}
	else
	{
		//traditional number formatting
		#$formatted	= number_format(abs($value), 2, '.', ',');
		$formatted	= number_format(abs($value));
	}
	
	return $formatted;
}

function format_percent($value){
	return round($value, 2);
}



function dateformat($datestring){

	if( empty($datestring))
		return false;
    // $key = '';
    // $keys = array_merge(range(0, 9), range('a', 'z'));

    // for ($i = 0; $i < $length; $i++) {
    //     $key .= $keys[array_rand($keys)];
    // }
	$date = new DateTime($datestring);
	$newdate = date_format($date,'d/m/Y');
    return $newdate;

}


function get_meteroptions(){
	$CI 	 = &get_instance();
	$result = $CI->db->order_by('meter_room_name', 'ASC')->get('meters')->result();
	return $result;
}


function column_sort($targetid, $url){

	echo "event.preventDefault();  load_pageloader('".$targetid."','".$url."');";
}


