<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Membershiptypes extends MY_Controller {
	public $table = "membership_type";
	public $controller = "membershiptypes";
	public function __construct(){
		parent::__construct();
		$this->load->model(array('members_model','classtype_model','membershiptype_model','memberships_model','class_durations_model'));
		//$this->superadmin_access();
	}
	public function index(){

		$result = $this->fetch();
		$js_files = array('modules/delete.js');
		$this->template('membershiptypes/index', array('controller' => $this->controller,'rates' => $result,'js' => $js_files));
	}


	public function paginate($keywords = 0, $sort_by = 'membership_type.id', $sort_order = 'DESC', $limit = 20, $offset = 0){

		$result['members'] = $this->fetch($keywords, $sort_by, $sort_order, $limit, $offset);
		echo $this->load->view('members/list',$result, true);
	}

	public function fetch($keywords = 0, $sort_by = 'class_type.class_title', $sort_order = 'ASC', $limit = 20, $offset = 0){
		
		$params = array(
				'limit'  => $limit,
				'sort_by' => $sort_by, 
				'sort_order' => $sort_order, 
				'offset' => $offset, 
			);

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}
		$result = $this->membershiptype_model->get_membertypes($params);
		return $result;
	}
	public function edit($id){
		if( empty($id) ){
			redirect(base_url('membershiptypes'));
		}

		$this->load->library('form_validation');
		
		$post = $this->input->post();

		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			$rules = array(
		           	'class_type_id' => array(
		                     'field' => 'class_type_id',
		                     'label' => 'Class Type',
		                     'rules' => 'trim|required'
		                     ),
		           	'membership_type_id' => array(
		                     'field' => 'duration_id',
		                     'label' => 'Duration',
		                     'rules' => 'trim|required'
		                     ),
		           	'class_type_id' => array(
		                     'field' => 'price',
		                     'label' => 'Price',
		                     'rules' => 'trim|required'
		                     ),

		           ); 

			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {

				$selected_duration = $this->class_durations_model->get_by_id($post['duration_id']);
				$selected_class_type = $this->classtype_model->get_by_id($post['class_type_id']);
				$post['title'] = $selected_duration->durations.' '.$selected_duration->duration_type.'( '.$selected_class_type->class_title.' )';
				unset($post['userid']);
				$result = $this->membershiptype_model->edit($post);

				if ( $result ){
					redirect(base_url('membershiptypes'));
				} else {
					$response = array('result' => 0, 'msg' => 'Failed to '.$action);
				}
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}
	
		if( !empty($response)){
			$data = $response;
		} else {
			$response = array();
		}

		if( !empty($id) ){
			$rate = $this->membershiptype_model->get_by_id($id);
		} else {
			$rate = array();
		}

		$class_types = $this->classtype_model->get_types();
		$mothly_duration = $this->class_durations_model->get_by_type('monthly');
		$session_duration = $this->class_durations_model->get_by_type('session');
		$class_durations = $this->class_durations_model->get();
		$js_files = array('modules/membershiptypes.js');
	
		$this->template('membershiptypes/form', array('rate' => $rate,'response' => $response,'class_types' => $class_types,'months' => $mothly_duration,'sessions' => $session_duration,'response' => $response,'js' => $js_files ));
	}

	public function add(){

		$this->load->library('form_validation');
		
		$post = $this->input->post();
		
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			$rules = array(
		           	'class_type_id' => array(
		                     'field' => 'class_type_id',
		                     'label' => 'Class Type',
		                     'rules' => 'trim|required'
		                     ),
		           	'class_type_id' => array(
		                     'field' => 'price',
		                     'label' => 'Price',
		                     'rules' => 'trim|required'
		                     ),

				   ); 
			if( empty( $this->input->post('duration_id') ) ) {
				$rules['session_duration_id'] =  array(
					'field' => 'session_duration_id',
					'label' => 'Sessions',
					'rules' => 'trim|required'
				);
			}

			if( empty( $this->input->post('session_duration_id') ) ) {
				$rules['duration_id'] =  array(
					'field' => 'duration_id',
					'label' => 'Monthly Duration',
					'rules' => 'trim|required'
				);
			}

			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				$post['date_created'] = date('Y-m-d');
				
				//CREATING THE MONTHLY SESSION STRING
				$duration_string = '';
				if( !empty($post['duration_id']) ){
					$selected_monthly = $this->class_durations_model->get_by_id($post['duration_id']);
					$duration_string = $selected_monthly->durations.' Month(s)';
				}

				//CREATING THE SESSION STRING
				if( !empty($post['session_duration_id']) ){
					$selected_sess = $this->class_durations_model->get_by_id($post['session_duration_id']);
					if( $duration_string != '' ){
						$duration_string = $duration_string . " w/ ";
					}
					$duration_string = $duration_string .$selected_sess->durations .' Sessions(s)';
				}
				
				$selected_class_type = $this->classtype_model->get_by_id($post['class_type_id']);
				$post['title'] = $selected_class_type->class_title.'( '.$duration_string.' )';
				unset($post['userid']);

				if( empty($post['session_duration_id']) )
					unset($post['session_duration_id'] );

				if( empty($post['duration_id']) )
					unset($post['duration_id'] );
				
				if( empty($_POST['id']) ){
					unset($post['id']);
					$result = $this->membershiptype_model->add($post);
					$membership_id = $this->db->insert_id();
				} else {
					$result = $this->membershiptype_model->edit($post);
					$membership_id = $_POST['id'];
				}
	
				if ( $result ){
					$response = array('result' => 1, 'msg' => 'Successfully '.$action.'d');
					redirect('membershiptypes');
					

				} else {
					$response = array('result' => 0, 'msg' => 'Failed to '.$action);
				}
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}
	
		if( !empty($response) ){
			$data = $response;
		} else {
			$response = array();
		}

		if( !empty($membership_id) ){
			$membership = $this->memberships_model->get_membership($membership_id);
		} else {
			$membership = array();
		}

		$class_types = $this->classtype_model->get_types();
		$mothly_duration = $this->class_durations_model->get_by_type('monthly');
		$session_duration = $this->class_durations_model->get_by_type('session');
		$js_files = array('modules/membershiptypes.js');

		$this->template('membershiptypes/form', array('class_types' => $class_types,'months' => $mothly_duration,'sessions' => $session_duration,'response' => $response,'js' => $js_files ));
		
	}

	public function insert_or($sales_arr  = array()){
		$or  = $this->db->select('*')->where(array('sales_id' => $sales_arr['sales_id']))->get('or_issue')->row_array();
		if (empty($or) ){
			$this->db->insert('or_issue',$sales_arr);
		} else {
			$this->db->where('id', $or['id']); 
        	$this->db->update('or_issue', $sales_arr);
		}
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->membershiptype_model->inactive($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	
	public function update_membership(){
		$data = $this->db->select('membership_log.*,sales.date')->join('membership_log','membership_log.id = sales.membership_log_id','left')->where('membership_log.date_start','2018-12-01')->get('sales')->result();

		foreach ($data as $membership) {
			$_data = array('id' => $membership->id,'date_register' => $membership->date);
			$this->memberships_model->edit($_data);
			print_r($_data);
		}
	}

}