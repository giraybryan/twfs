<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array('banks_model','bank_transactions_model','cashflows_model','sales_model','expense_model','memberships_model','members_model'));
	}
	public function index(){
		$date = date('Y-m-d');
		$data['banks'] = $this->banks_model->gets(
				array(
					'where'=> array(
							array('date' => $date)
					)
				) 
			);
		$data['js'] = array('modules/banktransacs.js');

		$this->template('reports/index', $data );
	}

	public function add(){

		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		$response = array();
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			$rules = array(
           	'bankid' => array(
                     'field' => 'bankid',
                     'label' => 'Bank Type',
                     'rules' => 'trim|required'
                     ),
           	'transaction_type' => array(
                     'field' => 'transaction_type',
                     'label' => 'Transaction Type',
                     'rules' => 'trim|required'
                     ),
           	'amount' => array(
                     'field' => 'amount',
                     'label' => 'Amount',
                     'rules' => 'trim|required'
                     ),

           ); 
			if( empty($post['date'])){
				$post['date'] = date('Y-m-d');
			} else {
				$post['date'] = date('Y-m-d', strtotime($post['date']));
			}
			$this->form_validation->set_rules($rules);
			$insuffecient_error = "";
			switch ($post['transaction_type']) {
				case 'deposit':
					$current_coh = $this->get_current_coh($post['date']);
					$insuffecient_error = "";
					if( empty( $current_coh ) || $current_coh < $post['amount'] ){
						$insuffecient_error = "Insufficient Fund for Cash On Hand.";
					}
					break;
				case 'withdraw':
					$current_balance = $this->get_bank_balance($post['bankid']);
					$insuffecient_error = "";
					if( empty( $current_balance ) || $current_balance < $post['amount'] ){
						$insuffecient_error = "Withdraw Cannot Proceed. Insufficient Fund for ".$this->banks_model->getBankName($post['bankid']);
					}
					break;
				default:

					break;
			}
			

			if ( $this->form_validation->run() && empty($insuffecient_error) ) {
				
				 $result =  $this->bank_transactions_model->add($post);
				 if( empty($post['id']) ){
				 	// adjust the current bank amount
				 	$bank = $this->banks_model->get($post['bankid']);
				 	
				 	switch( $post['transaction_type'] ) {
				 		case "deposit":
				 			$bank->amount = $bank->amount + $post['amount'];
				 		
				 			break;
				 		case "withdraw":
				 			$bank->amount = $bank->amount - $post['amount'];
				 			
				 			break;
				 		case "transfer":
				 			break;
				 	}
				 	$_bank = array('id' => $post['bankid'], 'amount' => $bank->amount);
				 	$this->banks_model->edit($_bank);
				 	//update cashflow
				 	$cashflow = array('date' =>$post['date'],'fieldselected' => $post['transaction_type'],'amount' => $post['amount']);				 
				 	$this->cashflow_audtrail($cashflow);
				 }
				 
 
				 if( $result) {
				 	redirect(base_url('banktransacs'));
				 } else {
				 	die('error');
				 }
				
				
			} else {
				$error = "";
				if( !empty($insuffecient_error ) ){
					$error = $insuffecient_error.' <br/>';
				}
				
				$error .= validation_errors();

				$response = array('result' => 0, 'msg' => $error );
			}	
		}

		if( !empty($response)){
			$data = $response;
		}
		$banktypes = $this->banks_model->gets();
		
		$js_files = array('modules/banktransac_form.js');
		$this->template('banktransacs/form', array('response' => $response,'banktypes' => $banktypes ,'js' => $js_files));
		
	}

	public function edit($id){
		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		$response = "";
		if( !empty($post) ){
			$action = 'edit';
		
		$rules = array(
           	'sales_type_id' => array(
                     'field' => 'sales_type_id',
                     'label' => 'Sales Type',
                     'rules' => 'trim|required'
                     ),
           	'amount' => array(
                     'field' => 'amount',
                     'label' => 'Amount',
                     'rules' => 'trim|required'
                     ),

           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				
				if( empty($post['date'])){
					$post['date'] = date('Y-m-d');
				} else {
					$post['date'] = date('Y-m-d', strtotime($post['date']));
				}
				
				 $result =  $this->sales_model->edit($post);
				 if( $result) {
				 	redirect(base_url('sales'));
				 } else {
				 	die('error');
				 }
			}
		}
		$sales_types = $this->sales_model->get_types();
		$sales = $this->sales_model->get($id);
		$this->template('sales/form', array('response' => $response,'sales' => $sales,'types' => $sales_types));
	}

	

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	public function get_bank_balance($bankid){
		if( empty($bankid)){
			die('Empty Bank ID');
		}
		$bank = $this->banks_model->get($bankid);
		if( !empty($bank))
			return $bank->amount;

		return false;
	}


	public function get_salesexpense_data($date_start, $date_end){

		$sales =  $this->sales_model->gettotal_byrange($date_start,$date_end);
		$expenses=  $this->expense_model->gettotal_byrange($date_start,$date_end);
		$monthly = array();
		$month_category = array();
		foreach($sales as $sale ){
			$month_year = date('M', strtotime($sale->date)).'-'.date('Y', strtotime($sale->date));
			if (!array_key_exists($month_year, $monthly)) {
				$monthly[$month_year] = array('sales' => $sale->sales, 'date' => $sale->date, 'expense' => 0);
			} else {
				$monthly[$month_year]['sales'] = $monthly[$month_year]['sales'] + $sale->sales;
			}

		}
		
		foreach ($expenses as $expense) {
			$month_year = date('M', strtotime($expense->date_paid)).'-'.date('Y', strtotime($expense->date_paid));
			
			if (!array_key_exists($month_year, $monthly)) {
				$monthly[$month_year] = array('sales' => 0,'expense' => $expense->expense, 'date' => date('Y-m-d', strtotime($expense->date_paid)));
			} else {
				$monthly[$month_year]['expense'] = $monthly[$month_year]['expense'] + $expense->expense;
			}
		}
		$sales_expense_string = "";
		

		$highest_sales = array('month' => '', 'total' => 0);
		$lowest_sales = array('month' => '', 'total' => 0);
		$highest_expense = array('month' => '', 'total' => 0);
		$lowest_expense = array('month' => '', 'total' => 0);
		$annual_gross = array();
		$annual_expenses = array();
		$row_year = "";
		foreach ($monthly as $month) {
			$month_year = date('M', strtotime($month['date'])).'-'.date('Y', strtotime($month['date']));
			$year = date('Y', strtotime($month['date']));
			$_month = date('M', strtotime($month['date']));
			if( $sales_expense_string != "" )
				$sales_expense_string .= ",";

			$sales_expense_string .= '{"Month" : "'.$month_year.'", "expense" : '.$month['expense'].', "sales" : '.$month['sales'].' }';

			$month_category[$_month][$year] = array( 'sales' => $month['sales'], 'expense' => $month['expense']);


			if( $month['sales'] > $highest_sales['total']){
				$highest_sales = array( 'month' => $month_year, 'total' => $month['sales']);
			}

			if( $lowest_sales['total'] == 0 || $lowest_sales['total'] > $month['sales'] ){
				$lowest_sales = array('month' => $month_year, 'total' => $month['sales']);
			}

			if( $highest_expense['total'] == 0 || $highest_expense['total'] < $month['expense'] ){
				$highest_expense = array('month' => $month_year, 'total' => $month['expense'] );
			}

			if( $lowest_expense['total'] == 0 || $lowest_expense['total'] > $month['expense'] ){
				$lowest_expense = array('month' => $month_year, 'total' => $month['expense'] );
			}

			if ( $row_year != $year ){
				$row_year = $year;
				$annual_gross[] = array('year' => $year, 'income' => $month['sales'],'expense' => $month['expense']);
			} else {
				foreach ($annual_gross as $yearkey => $annual) {
				
					if( $annual_gross[$yearkey]['year'] == $row_year ){
						$annual_gross[$yearkey]['income'] += $month['sales'];
						$annual_gross[$yearkey]['expense'] += $month['expense'];
					}
				}
			}

		}

		$year_net = array();
		foreach($annual_gross as $annual){
			$net = $annual['income'] - $annual['expense'];
			$year_net[] = array('year'=> $annual['year'], 'income' => $net);
		}

		$data['annual_gross'] = json_encode($annual_gross);
		$data['annual_net'] = json_encode($year_net);

		$data['highest_sales'] = $highest_sales;
		$data['lowest_sales'] = $lowest_sales;
		$data['highest_expense'] = $highest_expense;
		$data['lowest_expense'] = $lowest_expense;

		//sales_breakdown 
		$sales_breakdown = $this->sales_model->get_salesrange($date_start,$date_end);
		$sales_category = array();
		$sales_category_data = array();
		foreach($sales_breakdown as $salesbrk){
			//echo "<pre>"; print_r($salesbrk); die;
			if( !in_array($salesbrk->sales_type, $sales_category) ){
				$sales_category[] = $salesbrk->sales_type;
			} 

			$scategory_key = array_search($salesbrk->sales_type,$sales_category);
			
			if( empty($sales_category_data[$scategory_key])){
				$sales_category_data[$scategory_key] = (float) $salesbrk->amount;
			} else {
				$sales_category_data[$scategory_key] = $sales_category_data[$scategory_key] +  $salesbrk->amount;
			}
		}
		$sales_arr = $this->reorder_per_total($sales_category_data, $sales_category);
		$data['brkdownsales_category'] = $sales_arr['cat_arr'];
		$data['brkdownsales_category_data'] = $sales_arr['total_arr'];
		$data['colorbrkdown'] = array('#E91E63','#30df2e','#b059d3','#ecdd22','#3323ca','#aae230','#4d543c','#e351e8','#957c96','#353035','#af7d18','#dafb28','#4dc9d6','#ea842c','#730887','#fcefc8');
		
		//expense breakdown 
		$expense_breakdown = $this->expense_model->get_byrange($date_start, $date_end);
		$expense_category = array();
		$expense_category_data = array();
		foreach($expense_breakdown as $expbrk){
			//echo "<pre>"; print_r($salesbrk); die;
			if( !in_array($expbrk->expense_type, $expense_category) ){
				$expense_category[] = $expbrk->expense_type;
			} 

			$scategory_key = array_search($expbrk->expense_type,$expense_category);
			
			if( empty($expense_category_data[$scategory_key])){
				$expense_category_data[$scategory_key] = (float) $expbrk->cost;
			} else {
				$expense_category_data[$scategory_key] = $expense_category_data[$scategory_key] +  $expbrk->cost;
			}
		}
		$expense_arr = $this->reorder_per_total($expense_category_data, $expense_category);
		$data['brkdownexpense_category'] = $expense_arr['cat_arr'];
		$data['brkdownexpense_category_data'] = $expense_arr['total_arr'];
		
		$data['monthly'] = $monthly;
		$data['month_category'] = $month_category;
		$data['sales_expense_string'] = $sales_expense_string;

		$data['js'] = array('modules/report_sales_expense.js');
		$data['date_range'] = date('m/d/Y', strtotime($date_start)).' - '.date('m/d/Y',strtotime($date_end));

		return $data;
	}

	public function reorder_per_total($total_arr, $cat_arr){
		
		$old_arr = $total_arr;
		
		array_multisort($total_arr,SORT_DESC);
		$new_cat = array();
	
		foreach($total_arr as $key => $value){

			foreach($old_arr as $old_key => $old_val ){
				if( $old_val == $value ){
					$new_cat[] = $cat_arr[$old_key];
				}
			}
		}
		return array( 'total_arr' => $total_arr, 'cat_arr' => $new_cat);
	}
	public function sales_expensechart(){
		
		$lastyear = date("Y",strtotime("-1 year"));
		$date_start = $lastyear.'-01-01';
		$date_end = date('Y-m-d');

		$data = $this->get_salesexpense_data($date_start, $date_end);
		
		$this->template('reports/sales_ex_index', $data );
	}

	public function sales_expensechart_ajax(){
		$post = $this->input->post();
		$date_start = $post['start_date'];
		$date_end = $post['end_date'];

		$data = $this->get_salesexpense_data($date_start, $date_end);

		echo $this->load->view('reports/sales_expensebar', $data, true, true);
	}

	public function memberschart(){
		
		$lastyear = date("Y",strtotime("-1 year"));
		$date_start = $lastyear.'-01-01';
		$date_end = date('Y-m-d');

		$data = $this->get_memberschart($date_start, $date_end);

		$this->template('reports/memberschart_index', $data );
	}

	public function memberschart_ajax(){
		
		$post = $this->input->post();
		$date_start = $post['start_date'];
		$date_end = $post['end_date'];

		$data = $this->get_memberschart($date_start, $date_end);
		
		echo $this->load->view('reports/memberschart', $data, true, true);
	}
	public function get_memberschart($date_start, $date_end){
		$data = array();
		
		$begin = new DateTime($date_start);
		$end = new DateTime($date_end);

		$interval = DateInterval::createFromDateString('1 month');
		$period = new DatePeriod($begin, $interval, $end);
		$member_data = array();

		$total_new = 0;
		$total_renew = 0;
		foreach ($period as $dt) {

			$first_day_this_month = date('Y-m-01',strtotime($dt->format("Y-m-d"))); 
        	$last_day_this_month  = date('Y-m-t',strtotime($dt->format("Y-m-d")));
        	$_post = array(
				'type' => 'new',
				'date_start' => $first_day_this_month,
				'date_end' => $last_day_this_month
				);
        	$key = date('M',strtotime($first_day_this_month)).'-'.date('Y',strtotime($first_day_this_month));
        	$row = array();
        	$result = $this->memberships_model->get_renewal_newmember_range($_post);
        	$new  = $result->total < 150 ? $result->total : 150;
			
			$_post['type'] = 'renew';
			$result = $this->memberships_model->get_renewal_newmember_range($_post);
			$renew = $result->total < 150 ? $result->total : 150;


			$total_renew += $renew;
			$total_new += $new;
			$row = array(
				'Month' => $key,
				'new' => $new,
				'renew' => $renew
				);
			array_push($member_data, $row);
			
		}
		
		$data['new_renew_string'] = 	json_encode($member_data);
		$data['total_new'] = 	$total_new;
		$data['total_renew'] = 	$total_renew;


		$data['genders']['male'] = $this->memberships_model->get_renewal_gender_newmember_range(array('date_start' => $date_start,'date_end' => $date_end, 'gender' => 'm','type'=>'new')); 
		$data['genders']['female'] = $this->memberships_model->get_renewal_gender_newmember_range(array('date_start' => $date_start,'date_end' => $date_end, 'gender' => 'f','type'=>'new')); 

		$data['renew_gender']['male'] = $this->memberships_model->get_renewal_gender_newmember_range(array('date_start' => $date_start,'date_end' => $date_end, 'gender' => 'm','type'=>'renew')); 
		$data['renew_gender']['female'] = $this->memberships_model->get_renewal_gender_newmember_range(array('date_start' => $date_start,'date_end' => $date_end, 'gender' => 'f','type'=>'renew')); 


		$data['date_range'] = date('m/d/Y', strtotime($date_start)).' - '.date('m/d/Y',strtotime($date_end));
		$data['js'] = array('modules/report_memberschart.js');
		return $data;
	}
	

	public function financial_report(){

		$data = array();
		$this->template('reports/financial_report_index', $data );
	}


}