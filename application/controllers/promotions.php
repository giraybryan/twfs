<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promotions extends MY_Controller {
	public $table = "promotions";
	public function __construct(){
		parent::__construct();
		$this->load->model(array('classtype_model','class_category_model','promos_model','membershiptype_model','branch_model'));
	}
	public function index(){

		$result = $this->fetch();
		$js_files = array('modules/promotion_list.js');
		$this->template('promotions/index', array('promos' => $result,'js' => $js_files));
	}

	public function paginate($keywords = 0, $sort_by = 'class_type.id', $sort_order = 'DESC', $limit = 20, $offset = 0){

		$result['promos'] = $this->fetch($keywords, $sort_by, $sort_order, $limit, $offset);
		echo $this->load->view('promotions/list',$result, true);
	}

	public function fetch($keywords = 0, $sort_by = 'promotions.id', $sort_order = 'DESC', $limit = 20, $offset = 0){
		
		$params = array(
				'limit'  => $limit,
				'sort_by' => $sort_by, 
				'sort_order' => $sort_order, 
				'offset' => $offset, 
			);

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}

		$result = $this->promos_model->get($params);
		return $result;
	}

	public function add(){

		$this->load->library('form_validation');
		
		$post = $this->input->post();
		if( empty($post['id']) ){
			
			$action = 'save';
		} else {
			$action = 'edit';
		}

		$rules = array(
	           	'promo_title' => array(
	                     'field' => 'promo_title',
	                     'label' => 'Promo Title',
	                     'rules' => 'trim|required'
	                     ),
	           	'class_type_id' => array(
	                     'field' => 'class_type_id',
	                     'label' => 'Class Type',
	                     'rules' => 'trim|required'
						 ),
				 'membership_type_id' => array(
	                     'field' => 'membership_type_id',
	                     'label' => 'Membership Duration',
	                     'rules' => 'trim|required'
						 ),
				 'promo_type_id' => array(
	                     'field' => 'promo_type_id',
	                     'label' => 'Promo Type Duration',
	                     'rules' => 'trim|required'
	                     ),
	           ); 
		$this->form_validation->set_rules($rules);
		if ( $this->form_validation->run() ) {
		
			$post['date_start'] = date('Y-m-d',strtotime($post['date_start']) );
			$post['date_end'] = date('Y-m-d',strtotime($post['date_end']) );

			if( $post['promo_type_id'] == 1 ){
				$post['discount_rate'] = $post['amount'];
			} else {
				$post['price_rate'] = $post['amount'];
			}
			unset($post['amount']);

			if( empty($post['id']) ){
				$result = $this->promos_model->add($post);
			} else {
				$result = $this->promos_model->edit($post);
			}

			if ( $result ){
				$response = array('result' => 1, 'msg' => 'Successfully '.$action.'ed');
				redirect(base_url('promotions'));
			} else {
				$response = array('result' => 0, 'msg' => 'Failed to '.$action);
			}
			
		} else {
			$response = array('result' => 0, 'msg' => validation_errors());
		}
		if( !empty($response) )
			$data['response'] = $response;
		if( !empty($_POST) )
			$data['post'] = $_POST;

		$dataresult = array();
		if ( !empty($result) )
			$dataresult = $result;

		$data['class_types'] = $this->classtype_model->get_types();
		$data['membershiptypes'] = $this->membershiptype_model->get_membertypes();
		$data['promo_types'] = $this->promos_model->get_types();
		$data['branch_id'] = $this->branch_id;
		$data['branch_name'] = $this->branch_model->get_branchname($this->branch_id);
		$data['js'] = array('modules/promotion_form.js');
		$this->template('promotions/form', $data);
		
		
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	public function get_option_meters(){

		$data['meters'] = $this->fetch();
		$data['selected'] = $this->input->get('id');
		print_r($data['selected']);
		echo $this->load->view("meter_reading/options", $data, true);
	}

	public function walkin_rate(){

		$post = $this->input->post();

		if(!empty($post['class_type_id'])){
			$rate = $this->walkinrates_model->get_rate($post['class_type_id']);
		} else {
			$rate = 0;
		} 

		echo $rate;
	}

}