<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Walkins extends MY_Controller {
	public $table = "walkin_log";
	public function __construct(){
		parent::__construct();
		$this->load->model(array('walkins_model','classes_model','walkinrates_model','attendance_model','sales_model'));
	}
	public function index(){

		$result = $this->fetch();
		$js_files = array('modules/walkin_list.js');
		$this->template('walkins/index', array('walkins' => $result,'js' => $js_files));
	}

	public function paginate($keywords = 0, $sort_by = 'members.id', $sort_order = 'DESC', $limit = 20, $offset = 0){

		$result['walkins'] = $this->fetch($keywords, $sort_by, $sort_order, $limit, $offset);
		echo $this->load->view('members/list',$result, true);
	}

	public function fetch($keywords = 0, $sort_by = 'members.id', $sort_order = 'DESC', $limit = 20, $offset = 0){
		
		$params = array(
				'limit'  => $limit,
				'sort_by' => $sort_by, 
				'sort_order' => $sort_order, 
				'offset' => $offset, 
			);

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}

		$result = $this->walkinrates_model->get($params);
		return $result;
	}

	public function update_rate()
	{
		$post = $this->input->post();
		if( $post ){
			if( $post['rate'] == ""  || empty($post['rate']) ){
				$post['rate'] = 0;
			}
			$post[$post['field']] = $post['rate'];
			unset($post['field']);
			if( !empty($post['nonmem_rate']) ){
				unset($post['rate']); 
			}
			if( $this->walkinrates_model->get_walkinrate_by_class($post['class_type_id'] ) ){
				$result = $this->walkinrates_model->edit($post);
			} else {
				$result = $this->walkinrates_model->add($post);
			}

			if( $result ){
				$resonse = array('msg' => 'class updated', 'result' => 1);
			} else {
				$resonse = array('msg' => 'changes not saved', 'result' => 0);
			}
			echo json_encode($resonse);
		}
		
	}

	public function walkin_rate(){
		$post = $this->input->post();
		if( $post ){
			$rate = $this->walkinrates_model->get_walkinrate_by_class($post['class_type_id']);
			if( !empty($rate) ){
				if( strtolower($post['walkin_type']) == "member" ){
					echo $rate->rate;
				} else {
					echo $rate->nonmem_rate;
				}
			} else {
				echo "invalid";
			}
			
		}
	}

	public function entry(){

		$post = $this->input->post();
		//get walkin sales type
		$sales_type_id = $this->sales_model->get_idby_code('walkin');
		if( empty($sales_type_id) )
			die('Create Sales Type for Walkin w/ code of walkin');

		if( $post ){
			
			$post['date'] = date('Y-m-d');

			if( $post['walkin_type'] == "member" ){
				$entry_data = array(
					'member_id' 	=> $post['member_id'],
					'amount' 		=> $post['amount'],
					'class_type_id' => $post['class_type_id']
				);
			} else {
				$entry_data = array(
					'description '  => 'nonmember',
					'amount' 		=> $post['amount'],
					'class_type_id' => $post['class_type_id']
				);
			}
			//add walkin log
			$walkin_id = $this->walkins_model->add($entry_data);

			//add daily log attendance
			$attendance_data = array(
				'walkin_id' => $walkin_id,
				'class_type_id' => $post['class_type_id'],
				'branch_id' => $this->branch_id,
			);
			if( $post['walkin_type'] == "member" ){
				$attendance_data['member_id'] = $post['member_id'];
			}
			$this->attendance_model->add($attendance_data);
			
			//insert sales
			$sales_data = array(
				'sales_type_id' => $sales_type_id,
				'amount'        => $post['amount'],
				'walkin_log_id' => $walkin_id,
				'class_type_id' => $post['class_type_id'],
				'date'   		=> date('Y-m-d'),
			);

			if( $post['walkin_type'] == "member" ){
				$sales_data['member_id'] = $post['member_id'];
			}

			$sales_id = $this->sales_model->add($sales_data);

			//update Cashflow
			$cashflow = array('date' =>$post['date'],'fieldselected' => 'coh','amount' => $post['amount']);				 
			$this->cashflow_audtrail($cashflow);
			redirect(base_url('dashboard'));

		}

		$js_files = array('modules/walkin_form.js');
		$data = array(
			'js' 		=> $js_files,
			'classes' 	=> $this->classes_model->get_activated_classes()
		);
		$this->template('walkins/form', $data);
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

}