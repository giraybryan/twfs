<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banks extends MY_Controller {
	public $table = "bank";
	public $table_transac = "bank_transactions";
	public function __construct(){
		parent::__construct();
		$this->load->model(array('banks_model'));
	}
	public function index(){

		$data['banks'] = $this->banks_model->gets();
		$data['js'] = array('modules/banks.js');
		$this->template('banks/index', $data );
	}

	public function add(){

		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			$rules = array(
           	'bankname' => array(
                     'field' => 'bankname',
                     'label' => 'Bank Name',
                     'rules' => 'trim|required'
                     ),
           	'amount' => array(
                     'field' => 'amount',
                     'label' => 'Amount',
                     'rules' => 'trim|required'
                     ),

           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				
				if( empty($post['date_update'])){
					$post['date_update'] = date('Y-m-d');
				} else {
					$post['date_update'] = date('Y-m-d', strtotime($post['date_update']));
				}
				
				 $result =  $this->banks_model->add($post);
				 if( $result) {
				 	redirect(base_url('banks'));
				 } else {
				 	die('error');
				 }
				
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}

		if( !empty($response)){
			$data = $response;
		} else {
			$response = array();
		}
		
		$js_files = array('modules/attendance_form.js');
		$this->template('banks/form', array('response' => $response,'js' => $js_files));
		
	}

	public function getBalance($bankid = ""){
		if( !empty($bankid) ){
			$bank = $this->banks_model->get($bankid);
			echo number_format($bank->amount,2);
		}
	}

	public function edit($id){
		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		$response = "";
		if( !empty($post) ){
			$action = 'edit';
		
		$rules = array(
           	'sales_type_id' => array(
                     'field' => 'sales_type_id',
                     'label' => 'Sales Type',
                     'rules' => 'trim|required'
                     ),
           	'amount' => array(
                     'field' => 'amount',
                     'label' => 'Amount',
                     'rules' => 'trim|required'
                     ),

           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				
				if( empty($post['date'])){
					$post['date'] = date('Y-m-d');
				} else {
					$post['date'] = date('Y-m-d', strtotime($post['date']));
				}
				
				 $result =  $this->sales_model->edit($post);
				 if( $result) {
				 	redirect(base_url('sales'));
				 } else {
				 	die('error');
				 }
			}
		}
		$sales_types = $this->sales_model->get_types();
		$sales = $this->sales_model->get($id);
		$this->template('sales/form', array('response' => $response,'sales' => $sales,'types' => $sales_types));
	}

	public function check_first_attend($log_date, $member_id){
			$log_date = date('Y-m-d',strtotime($log_date));
			$result = $this->memberships_model->get_membershiplog($member_id);
			
			$date_start = $result->date_start;
			
			if( $result->date_start > $log_date || $date_start == "0000-00-00"){
				
				$updatedate = array('date_start' => $log_date, 'id' => $result->id); 
				$date_start = $log_date;

			}  else {
				return false;
			}
			
			if( $result->date_end == "0000-00-00"){
				$started_date =strtotime($date_start);
				switch($result->membership_type_id){
					case 1:
						$updatedate['date_end'] = date("Y-m-d", strtotime("+1 month", $started_date));
						break;
					case 2:
						$updatedate['date_end'] = date("Y-m-d", strtotime("+3 month", $started_date));
						break;
					case 3:
						$updatedate['date_end'] = date("Y-m-d", strtotime("+6 month", $started_date));
						break;
				}
			}
			
			$this->memberships_model->edit($updatedate);
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	public function get_option_meters(){

		$data['meters'] = $this->fetch();
		$data['selected'] = $this->input->get('id');
		print_r($data['selected']);
		echo $this->load->view("meter_reading/options", $data, true);
	}

}