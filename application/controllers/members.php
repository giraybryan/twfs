<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends MY_Controller {
	public $table = "members";
	public function __construct(){
		parent::__construct();
		$this->load->model(array('members_model','classtype_model','membershiptype_model','classes_model','config_model','sales_model','config_model','memberships_model','class_durations_model','memberships_freeze_model','card_model'));
		$this->load->helper(array('form', 'url'));
		$this->load->library('pagination');
	}
	public function index(){

		$data 			 = $this->fetch();
		$data['js'] 	 = array('modules/member_list.js');
		$data['classes'] = $this->classes_model->get_activated_classes();
		$data['durations'] = $this->class_durations_model->get();
		$data['config_membershiptype'] = $this->config_model->get_valueby_code('membershiptype'); 
		$this->template('members/index', $data);
	}

	public function paginate($keywords = 0, $sort_by = 'members.id', $sort_order = 'DESC', $limit = 20, $offset = 0 ){

		$data = $this->fetch($keywords, $sort_by, $sort_order, $limit, $offset);
		$data['js'] = array('modules/member_list.js');
		$data['classes'] = $this->classes_model->get_activated_classes();
		// $this->template('members/index', $data);
		echo $this->load->view('members/list',$data, true);
	}

	public function fetch($keywords = 0, $sort_by = 'members.id', $sort_order = 'DESC', $limit = 20, $offset = 0){
		
		$params = array(
				'limit'  => $limit,
				'sort_by' => $sort_by, 
				'sort_order' => $sort_order, 
				'offset' => $offset, 
			);

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}

		$result['members'] = $this->members_model->get_members($params);
		unset($params['limit']);
		$result['total'] = $this->members_model->get_totalmembers($params);
		$config['uri_segment']		= 7;
		$config['base_url'] 		= base_url().'members/paginate/'.$keywords.'/'.$sort_by.'/'.$sort_order.'/'.$limit;
		$config['total_rows'] 		= $result['total'];
		$config['per_page'] 		= $limit;
		$config['full_tag_open'] 	= '<div><ul class="pagination pagination-small pagination-centered">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['num_tag_open'] 	= '<li class="page-item">';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] 	= '</a></li>';
        $config['next_tag_open'] 	= '<li class="page-item">';
        $config['next_tagl_close'] 	= '</a></li>';
        $config['prev_tag_open'] 	= '<li class="page-item">';
        $config['prev_tagl_close'] 	= '</li>';
        $config['first_tag_open'] 	= '<li class="page-item">';
        $config['first_tagl_close'] = '</li>';
        $config['last_tag_open'] 	= '<li class="page-item">';
        $config['last_tagl_close'] 	= '</a></li>';
		$this->pagination->initialize($config);
		$result['paginate'] = $this->pagination->create_links(); 
		return $result;
	}

	public function info($id){
		
		$post = $this->input->post();
		if( !empty($_FILES) ){
			
			$config['upload_path']          = './uploads/members/';
			$config['allowed_types']        = 'gif|jpg|png';
			$config['file_name'] 			= 'member_'.$id;

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('userfile'))
			{
					$error = array('error' => $this->upload->display_errors());
					$this->debug($error);
					$data['error'] = $error;
			}
			else
			{
					$file = array('upload_data' => $this->upload->data());
					//update image data
					$imagedata = array(
						'image'	=> $file['upload_data']['file_name'],
						'id'	=> $id,
					);
					$this->members_model->edit($imagedata);
					//$image_data = $this->upload->data();
					//$this->resize_image($file['upload_data']);
					//$this->debug($file);
			}
		}


		$data['member'] = $this->members_model->get_member($id);
		$data['histories'] = $this->memberships_model->get_all_by_member($id);
		$freeze_condition = array('member_id' => $id, 'date_created >=' => "".date("Y")."-01-01", 'date_created <=' => "".date("Y")."-12-31" );
		$date = ('2020-03-30');
		$date = date('Y-m-d');
		$where = array(
			'member_id'  => $id,
			'start_date <=' => $date,
			'end_date  >=' => $date
		);
		
		$data['latest_freeze'] = $this->memberships_freeze_model->get($where);
		//$latest_freeze = $this->memberships_freeze_model->get_by_current_date(date('Y-m-d'), $member_id);
		
		$data['latest_card'] = $this->card_model->latest_registered();
		$data['annual_freezectr'] = $this->memberships_freeze_model->get_annual_count($freeze_condition);
		$data['js'] = array('modules/member_info.js');
		$this->template('members/info', $data);
	}

	public function resize_image($image_data){
		$this->load->library('image_lib');
		$w = $image_data['image_width']; // original image's width
		$h = $image_data['image_height']; // original images's height
	
		$n_w = 273; // destination image's width
		$n_h = 246; // destination image's height
	
		$source_ratio = $w / $h;
		$new_ratio = $n_w / $n_h;
		if($source_ratio != $new_ratio){
	
			$config['image_library'] = 'gd';
			$config['source_image'] = './uploads/uploaded_image.jpg';
			$config['maintain_ratio'] = FALSE;
			if($new_ratio > $source_ratio || (($new_ratio == 1) && ($source_ratio < 1))){
				$config['width'] = $w;
				$config['height'] = round($w/$new_ratio);
				$config['y_axis'] = round(($h - $config['height'])/2);
				$config['x_axis'] = 0;
	
			} else {
	
				$config['width'] = round($h * $new_ratio);
				$config['height'] = $h;
				$size_config['x_axis'] = round(($w - $config['width'])/2);
				$size_config['y_axis'] = 0;
	
			}
	
			$this->image_lib->initialize($config);
			$this->image_lib->crop();
			$this->image_lib->clear();
		}
		$config['image_library'] = 'gd2';
		$config['source_image'] = './uploads/uploaded_image.jpg';
		$config['new_image'] = './uploads/new/resized_image.jpg';
		$config['maintain_ratio'] = TRUE;
		$config['width'] = $n_w;
		$config['height'] = $n_h;
		$this->image_lib->initialize($config);
	
		if (!$this->image_lib->resize()){
	
			echo $this->image_lib->display_errors();
	
		} else {
	
			echo "done";
	
		}
	}

	public function ajax_update(){
		$post = $this->input->post();
		if( !empty($post['id']) )
		{
			if( $post['field'] == 'bd')
			{
				$post['value'] = date('Y-m-d',strtotime($post['value']));
			}
			if( $post['field'] == 'memberid_no' ){
				//validate memberid_no
				$member = $this->members_model->get_by_memberidno($post['value']);
				if( !empty($member) ){
					//print error
					echo json_encode( array('result' => 0, "msg" => "MemberID is already taken") );
					die;
				}
			}

			//update
			$arr = array(
				'id' => $post['id'],
				$post['field'] => $post['value']
			);
			$result = $this->members_model->edit($arr);
			if( $result )
			{
				echo json_encode( array('result' => 1, "msg" => "Info Upadated") );
			} 
			else 
			{
				echo json_encode( array('result' => 0, "msg" => "not updated") );
			}
		}
		
	}

	public function add(){

		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
			$card_fee = '';
			$joining = '';

			$rules = array(
		           	'firstname' => array(
		                     'field' => 'firstname',
		                     'label' => 'First Name',
		                     'rules' => 'trim|required'
		                     ),
		           	'lastname' => array(
		                     'field' => 'lastname',
		                     'label' => 'lastname Name',
		                     'rules' => 'trim|required'
		                     ),
		           	'Number' => array(
		                     'field' => 'mobile',
		                     'label' => 'Number',
		                     'rules' => 'trim|required'
		                     )

		           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				
				$member_data = array(
					'firstname' => $this->input->post('firstname'),
					'lastname' => $this->input->post('lastname'),
					'middlename' => ' ',
					'mobile' => $this->input->post('mobile'),
					'gender' => $this->input->post('gender'),
					//'class_type_id' => $_POST['class_type_id'),
					//'membership_type_id' => $_POST['membership_type_id'),
					'address' =>$this->input->post('address'),
					'email' => $this->input->post('email'),
					'bd' 	=> date('Y-m-d', strtotime($this->input->post('bd'))),
					'branch_id' => $this->branch_id
			
					);

				if( !empty($this->input->post('joining')) ){
					$joining = $this->config_model->get_valueby_code('joiningfee');
					$joining_arr = array(
						'sales_type_id' => $this->sales_model->get_idby_code('joiningfee'),
						'amount'  => $joining,
						'description' => 'Joining Fee',
						'date' => date('Y-m-d'),
						'date_created' => date('Y-m-d'),
						'branch_id'	 => $this->branch_id
						);
				}
				if( !empty($this->input->post('access')) ){
					if( date('Y-m-d') <= '2020-02-18'):
		                echo '500 : Promo Rate';
		              else:
		                $card_fee = $this->config_model->get_valueby_code('access');
		              endif;
					

					$card_arr = array(
						'sales_type_id' => $this->sales_model->get_idby_code('access'),
						'amount'  => $card_fee,
						'description' => 'Access Card Fee',
						'date_created' => date('Y-m-d'),
						'date' => date('Y-m-d'),
						'branch_id'	 => $this->branch_id
						);
				}
				
				unset($post['joining']);
				unset($post['access']);

				if(!empty($_POST['referral_id'])){
					$member_data['referral_id'] = $_POST['referral_id'];
				}

				$post['bd'] = date('Y-m-d', strtotime($_POST['bd']));

				if( empty($_POST['id']) ){
					$result = $this->members_model->add($member_data);
					$member_id = $this->db->insert_id();
				} else {
					$result = $this->members_model->edit($post);
					$member_id = $_POST['id'];
				}

				//insert sales if joining and card is included
				if( !empty($joining_arr) ){
					$joining_arr['member_id'] = $member_id;
					$this->sales_model->add($joining_arr);
					//update COH
					$cashflow = array('date' =>date('Y-m-d'),'fieldselected' => 'coh','amount' => $joining );		
				 	$this->cashflow_audtrail($cashflow);
				}
				if( !empty($card_arr)){
					$card_arr['member_id'] = $member_id;
					$this->sales_model->add($card_arr);
					//update COH
					$cashflow = array('date' =>date('Y-m-d'),'fieldselected' => 'coh','amount' => $card_fee );				 
				 	$this->cashflow_audtrail($cashflow);
				}


				if(empty($_POST['id'])){
					redirect('/memberships/add/'.$member_id.'?new=1');
				}

				if ( $result ){
					$response = array('result' => 1, 'msg' => 'Successfully '.$action.'d');
				} else {
					$response = array('result' => 0, 'msg' => 'Failed to '.$action);
				}
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}

		if( !empty($response)){
			$data = $response;
		} else {
			$response = array();
		}
		$js_files = array('modules/member_form.js');
		$class_types = $this->classtype_model->get_types();
		$membershiptype_model = $this->membershiptype_model->get_membertypes();
		$this->template('members/form', array('response' => $response,'class_types' => $class_types,'membership_types' => $membershiptype_model,'js' => $js_files));
		
	}

	public function edit($member_id){


		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			$rules = array(
		           	'firstname' => array(
		                     'field' => 'firstname',
		                     'label' => 'First Name',
		                     'rules' => 'trim|required'
		                     ),
		           	'lastname' => array(
		                     'field' => 'lastname',
		                     'label' => 'lastname Name',
		                     'rules' => 'trim|required'
		                     ),
		           	'Number' => array(
		                     'field' => 'mobile',
		                     'label' => 'Number',
		                     'rules' => 'trim|required'
		                     ),
		           	'bd' => array(
		           			'field' => 'bd',
		           			'label' => 'Birth Date',
		           			'rules' => 'trim|required'
		           		)

		           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				
				$member_data = array(
					'firstname' => $_POST['firstname'],
					'lastname' => $_POST['lastname'],
					'middlename' => ' ',
					'mobile' => $_POST['mobile'],
					'gender' => $_POST['gender'],
					//'class_type_id' => $_POST['class_type_id'],
					//'membership_type_id' => $_POST['membership_type_id'],
					'address' => $_POST['address'],
					'email' => $_POST['email'],
					'bd' 	=> date('Y-m-d', strtotime($_POST['bd']))
			
					);

				$post['bd'] = date('Y-m-d', strtotime($_POST['bd']));

				if( empty($_POST['id']) ){
					$result = $this->members_model->add($member_data);
					$member_id = $this->db->insert_id();
				} else {
					$result = $this->members_model->edit($post);
					$member_id = $_POST['id'];
				}

				if(empty($_POST['id'])){
					redirect('/memberships/add/'.$member_id);
				}

				if ( $result ){
					$response = array('result' => 1, 'msg' => 'Successfully '.$action.'ed');
				} else {
					$response = array('result' => 0, 'msg' => 'Failed to '.$action);
				}
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}

		if( !empty($response)){
			$data = $response;
		} else {
			$response = array();
		}

		$member = $this->members_model->get_member($member_id);
		$js_files = array('modules/member_form.js');
		$class_types = $this->classtype_model->get_types();
		$membershiptype_model = $this->membershiptype_model->get_membertypes();
		$this->template('members/form', array('member' => $member,'response' => $response,'class_types' => $class_types,'membership_types' => $membershiptype_model,'js' => $js_files));
		
	}

	public function search(){
			$post = $this->input->post();
			$searchresult = $this->members_model->search($post);
			if( empty($searchresult)){

			} else {
				foreach ($searchresult as $result) {
					echo "<div style='padding-top: 5px; padding-bottom: 5px;'><span style='vertical-align: middle' class='btn btn-default btn-sm memberselect' data-id='".$result->id."' data-firstname='".$result->firstname."' data-lastname='".$result->lastname."' >Select</span><span style='display:inline-block; vertical-align: middle;'>".$result->firstname." ".$result->lastname."</span> </div>";
				}


			}
	}

	public function search_list($keywords = 0, $sort_by = 'members.id', $sort_order = 'DESC', $limit = 20, $offset = 0){
		$post = $this->input->post();
		if( $post['class_type_id']  != ""){
			$post['where'][] = array('membership_type.class_type_id' => $post['class_type_id']);
		}
		if( $post['class_duration_id']  != ""){
			$post['where'][] = array('membership_type.duration_id' => $post['class_duration_id']);
		}
		$post['limit'] = $limit;
		$post['offset'] = $offset;
		
		$searchresult = $this->members_model->search($post);
		$data['members'] = $searchresult;
		$params = array('limit'=> $limit, 'offset'=> $offset);
		if( !empty($post['namesearch']) )
		{
			$params['namesearch'] = $post['namesearch'];
		}
		if( !empty($post['where']) )
		{
			$params['where'] = $post['where'];
		}
		if( !empty($post['or_where']) )
		{
			$params['or_where'] = $post['or_where'];
		}
		$total = $this->members_model->get_totalmembers($params);
		$data['total'] = $total;
		$config['uri_segment']		= 7;
		$config['base_url'] 		= base_url().'members/paginate/'.$keywords.'/'.$sort_by.'/'.$sort_order.'/'.$limit;
		$config['total_rows'] 		= $total;
		$config['per_page'] 		= $limit;
		$config['full_tag_open'] 	= '<div><ul class="pagination pagination-small pagination-centered">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['num_tag_open'] 	= '<li class="page-item">';
        $config['num_tag_close'] 	= '</li>';
        $config['cur_tag_open'] 	= '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] 	= '</a></li>';
        $config['next_tag_open'] 	= '<li class="page-item">';
        $config['next_tagl_close'] 	= '</a></li>';
        $config['prev_tag_open'] 	= '<li class="page-item">';
        $config['prev_tagl_close'] 	= '</li>';
        $config['first_tag_open'] 	= '<li class="page-item">';
        $config['first_tagl_close'] = '</li>';
        $config['last_tag_open'] 	= '<li class="page-item">';
        $config['last_tagl_close'] 	= '</a></li>';
		$this->pagination->initialize($config);
		$data['paginate'] = $this->pagination->create_links(); 

		echo $this->load->view("members/list", $data, true);
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	public function summary_report(){
		
		$post = $this->input->post();
		$data['members'] = $this->members_model->report_by_status($post);	
		$data['total'] = count( (array) $data['members'] ) ;	
		$data['status'] = $post['status'];
		echo $this->load->view("members/summary_list", $data, true);

	}

	public function update_password(){
		$post = $this->input->post();
		$error = "";
		$success = "";
		if( $post ){
			$id = $post['id'];
			$member = $this->members_model->get_member($id);
			if( !empty($member->password) ){
				if( $member->password != $post['old_pass'] ){
					$error .= "<div>Old Password Did not match</div>";
					
				}
			}
			if( $post['confirm_pass'] != $post['new_pass'] ){
				$error .= "<div>COnfirm Password Did not match</div>";
			}

			if( $error == "" ){
				$arr = array(
					'id' => $id,
					'password' => $post['new_pass']
				);
				$result = $this->members_model->edit($arr);
				if( $result ){
					$success = "Password Updated";
				}
			} 

			if( $error == "" ){
				$response = array('result' => 1, 'msg' => $success);
			} else {
				$response = array('result' => 0, 'msg' => $error);
			}

			echo json_encode($response);
		}
	}

	public function freeze_account()
	{
		$post = $this->input->post();

		if( $post ){
			
			$post['start_date'] = date('Y-m-d',strtotime($post['start_date']));
			if( $post['months'] > 1 ){
				$months = "+".$post['months']." months";
			}  else {
				$months = "+".$post['months']." month";
			}
			$post['end_date'] = date('Y-m-d',strtotime($post['start_date'].' '.$months));	
			
			$freeze_totalcondition = array(
				'member_id' => $post['member_id'], 
				'date_created >=' => "".date("Y")."-01-01", 
				'date_created <=' => "".date("Y")."-12-31" 
			);

			$where = array('member_id' => $post['member_id']);
			$total = $this->memberships_freeze_model->get_annual_count($freeze_totalcondition);
			
			if( $total >= 3) {
				$response = array('result' => 0, 'msg' => 'Freezing Total exceeded!');
			} else {
				
				//add freeze data
				$data = array(
					'months' 		=> $post['months'],
					'member_id' 	=> $post['member_id'],
					'start_date' 	=> $post['start_date'],
					'end_date' 		=> $post['end_date'],
					'date_created' 	=> date('Y-m-d'),
				);
				$result = $this->memberships_freeze_model->add($data);

				if( $result ){
					//adjust existing form membership 
					$this->adjust_current_membership( $post['member_id'], $post['start_date'], $months );

					$response = array('result' => 1, 'msg' => 'Account Freezed!');
				} else {
					$response = array('result' => 0, 'msg' => 'Failed to Freeze the Account!');
				}
			}

			echo json_encode($response);

		}
	}
	
	public function adjust_current_membership($member_id, $start_date, $months)
	{
		//get member details
		$member = $this->members_model->get_member($member_id);
		
		$pt_membership = $this->memberships_model->get_memsession_byid($member_id);
		if( $pt_membership ){
			if( $pt_membership->date_end >= $start_date ){
				$additonal = $months;
				
				$new_end_date = array(
					'date_end' => date('Y-m-d', strtotime($pt_membership->date_end.' '.$additonal) ),
					'id' => $pt_membership->id,
				);
				
				$this->memberships_model->edit($new_end_date);
				
			} else {
				$response = array('result' => 1, 'msg' => 'PT date not valid!' );
			}
		
		}

		$access_membership = $this->memberships_model->get_access_byid($member->id);
		
		if( $access_membership ){
			if( $access_membership->date_end >= $start_date ){
				$additonal = $months;

				$new_end_date = array(
					'date_end' => date('Y-m-d', strtotime($access_membership->date_end.' '.$additonal) ),
					'id' => $access_membership->id,
				);
				$this->memberships_model->edit($new_end_date);
			}
		}

		return '';

	}

	public function get_latest_registeredcard(){
		$post = $this->input->post();

		if( $post ){

			if( !empty($post['member_id']) ){
			
				//che 
				$latest_card = $this->card_model->latest_registered();
				if( !empty($latest_card ) && $latest_card->unix > $post['unix']  ){

					//register for a member
					$post = array('card_id' => $latest_card->description , 'id' => $post['member_id'] );
					$this->members_model->edit($post);
					echo json_encode(array('card' =>$latest_card->description,'result' => 1));
				} else {
					echo json_encode(array('result' => 0,'old'=> $post['card'],'fetch' => $latest_card->description));
				}
				
			} else {
				echo json_encode(array('result' => 0,'msg' => 'empty member_id'));
			}
			
		}
	}

}