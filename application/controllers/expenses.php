<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expenses extends MY_Controller {
	public $table = "expenses";
	public function __construct(){
		parent::__construct();
		$this->load->model(array('expense_model','expense_type_model', 'classtype_model','sales_model'));
	}
	public function index(){

		$result = $this->fetch();
		$data['js'] = array('modules/sales_summary.js','modules/expense_summary.js');
		$data['sales'] = $this->sales_model->get_dailysalesdata(date('Y-m-d'));
		$data['expenses'] = $this->expense_model->get_dailyexpensedata(date('Y-m-d'));
		$data['coh'] = number_format($this->get_current_coh(),2);
		$this->template('expenses/index', $data);
	}

	public function expensedata_bydate(){
		$post = $this->input->post();
		if (!empty($post['type']) && $post['type'] == 'range'){
			$data['expenses'] = $this->expense_model->get_byrange($post['start_date'], $post['end_date']);
		} else {
			if(empty($post['date'])){
				$post['date'] = date('Y-m-d');
			} else {
				$post['date'] = date('Y-m-d', strtotime($post['date']));
			}

			$data['expenses'] = $this->expense_model->get_dailyexpensedata($post['date']);
		}
		echo $this->load->view('expenses/list', $data, true, true);
	}

	public function paginate($keywords = 0, $sort_by = 'class_type.id', $sort_order = 'DESC', $limit = 20, $offset = 0){

		$result['members'] = $this->fetch($keywords, $sort_by, $sort_order, $limit, $offset);
		echo $this->load->view('class_types/list',$result, true);
	}

	public function form(){

	}

	public function fetch($keywords = 0, $sort_by = 'class_type.id', $sort_order = 'DESC', $limit = 20, $offset = 0){
		
		$params = array(
				'limit'  => $limit,
				'sort_by' => $sort_by, 
				'sort_order' => $sort_order, 
				'offset' => $offset, 
			);

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}

		$result = $this->classtype_model->get_types($params);
		return $result;
	}

	public function add(){

		$this->load->library('form_validation');
		
		$post = $this->input->post();
		if( empty($post['id']) ){
			
			$action = 'save';
		} else {
			$action = 'edit';
		}

		$rules = array(
	           	'expense_type_id' => array(
	                     'field' => 'expense_type_id',
	                     'label' => 'Expense Type',
	                     'rules' => 'trim|required'
	                     ),
	           	'cost' => array(
	                     'field' => 'cost',
	                     'label' => 'Expense Cost',
	                     'rules' => 'trim|required',
	                     ),
	           	'date_paid' => array(
	                     'field' => 'date_paid',
	                     'label' => 'Date Paid',
	                     'rules' => 'trim|required',
	                     ),
	           ); 
		$this->form_validation->set_rules($rules);
		if ( $this->form_validation->run() ) {
			$post = $this->input->post();
			$post['date_paid'] = date('Y-m-d', strtotime($_POST['date_paid']));
			$post['datecreated'] = date('Y-m-d');
			if( empty($post['id']) ){
				$result = $this->expense_model->add($post);
			} else {
				$result = $this->expense_model->edit($post);
			}

			if ( $result ){
				if( $action == 'save'){
					$action = "sav";
				}
				//reduce coh
				$cashflow = array('date' =>$post['date_paid'],'fieldselected' => 'expenses','amount' => $post['cost']);				 
		 		$this->cashflow_audtrail($cashflow);
		 		
				$response = array('result' => 1, 'msg' => 'Successfully '.$action.'ed');
			} else {
				$response = array('result' => 0, 'msg' => 'Failed to '.$action);
			}
			
		} else {
			$response = array('result' => 0, 'msg' => validation_errors());
		}
		if( !empty($response) )
			$data['response'] = $response;
		if( !empty($_POST) )
			$data['post'] = $_POST;

		$dataresult = array();
		if ( !empty($result) )
			$dataresult = $result;

		$data['types'] = $this->expense_type_model->get_all();
		$js_files = array('modules/expense_form.js');
		$this->template('expenses/form', $data);
		
		
	}

	public function edit($id){

		$this->load->library('form_validation');
		$post = $this->input->post();
		$rules = array(
	           	'expense_type_id' => array(
	                     'field' => 'expense_type_id',
	                     'label' => 'Expense Type',
	                     'rules' => 'trim|required'
	                     ),
	           	'cost' => array(
	                     'field' => 'cost',
	                     'label' => 'Expense Cost',
	                     'rules' => 'trim|required',
	                     ),
	           	'date_paid' => array(
	                     'field' => 'date_paid',
	                     'label' => 'Date Paid',
	                     'rules' => 'trim|required',
	                     ),
	           ); 

		$action = 'edit';
		$this->form_validation->set_rules($rules);
		if ( $this->form_validation->run() ) {
			//get old value 
			$prev_expense = $this->expense_model->get_expense($id);
			$prev_cost = $prev_expense->cost;

			$post = $this->input->post();
			$post['date_paid'] = date('Y-m-d', strtotime($_POST['date_paid']));
			
			$result = $this->expense_model->update($post);

			if ( $result ){
				if( $action == 'save'){
					$action = "sav";
				}
				$response = array('result' => 1, 'msg' => 'Successfully '.$action.'ed');

				//undo coh value
				$prev_param = array('date' => $post['date_paid'], 'field' => 'expenses', 'expenses' => $prev_cost, 'amount' => $prev_cost);
				$this->recussiveUndoCoh($prev_param);
				
				//update coh
				$cashflow = array('date' =>$post['date_paid'],'fieldselected' => 'expenses','amount' => $post['cost']);				 
		 		$this->cashflow_audtrail($cashflow);
			} else {
				$response = array('result' => 0, 'msg' => 'Failed to '.$action);
			}
			
		} else {
			$response = array('result' => 0, 'msg' => validation_errors());
		}
		if( !empty($response) )
			$data['response'] = $response;
		if( !empty($_POST) )
			$data['post'] = $_POST;

		$dataresult = array();
		if ( !empty($result) )
			$dataresult = $result;

		

		$data['expense'] = $this->expense_model->get_expense($id);
		$data['types'] = $this->expense_type_model->get_all();
		$js_files = array('modules/expense_form.js');
		$this->template('expenses/form', $data);
	}

	public function add_type(){
		if( $this->input->post()){
			$post = $this->input->post();
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
			$result = $this->expense_type_model->add($post);
			if ( $result ){
				if( $action == 'save'){
					$action = "sav";
				}
				$response = array('result' => 1, 'msg' => 'Successfully '.$action.'ed');
				redirect(base_url('expenses'));
			} else {
				$response = array('result' => 0, 'msg' => 'Failed to '.$action);
			}

		} 
		$data = array();
		if( !empty($response) )
			$data['response'] = $response;
		$this->template('expenses/expense_type_form', $data);
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	public function get_option_meters(){

		$data['meters'] = $this->fetch();
		$data['selected'] = $this->input->get('id');
		print_r($data['selected']);
		echo $this->load->view("meter_reading/options", $data, true);
	}

}