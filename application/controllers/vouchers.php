<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vouchers extends MY_Controller {
	public $table = "vouchers";
	public function __construct(){
		parent::__construct();
		$this->load->model(array('vouchers_model','membershiptype_model','classtype_model'));
	}
	public function index(){

		$result = $this->fetch();
		$js_files = array('modules/voucher_form.js');
		$this->template('vouchers/index', array('classes' => $result,'js' => $js_files));
	}

	public function paginate($keywords = 0, $sort_by = 'class_type.id', $sort_order = 'DESC', $limit = 20, $offset = 0){

		$result['members'] = $this->fetch($keywords, $sort_by, $sort_order, $limit, $offset);
		echo $this->load->view('class_types/list',$result, true);
	}

	public function fetch($keywords = 0, $sort_by = 'class_type.id', $sort_order = 'DESC', $limit = 20, $offset = 0){
		
		$params = array(
				'limit'  => $limit,
				'sort_by' => $sort_by, 
				'sort_order' => $sort_order, 
				'offset' => $offset, 
			);

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}

		$result  = array();
		return $result;
	}

	public function add(){

		$this->load->library('form_validation');
		
		$post = $this->input->post();
		if( empty($post['id']) ){
			
			$action = 'save';
		} else {
			$action = 'edit';
		}

		$rules = array(
	           	'vouchercode' => array(
	                     'field' => 'vouchercode',
	                     'label' => 'Voucher Code',
	                     'rules' => 'trim|required'
	                     ),
	           ); 
		$this->form_validation->set_rules($rules);
		if ( $this->form_validation->run() ) {
			foreach ($post['voucherno'] as $key => $value) {
				# code...
			}
			if( empty($post['id']) ){
				$result = $this->classtype_model->add($this->input->post());
			} else {
				$result = $this->classtype_model->edit($this->input->post());
			}

			if ( $result ){
				$response = array('result' => 1, 'msg' => 'Successfully '.$action.'ed');
			} else {
				$response = array('result' => 0, 'msg' => 'Failed to '.$action);
			}
			
		} else {
			$response = array('result' => 0, 'msg' => validation_errors());
		}
		if( !empty($response) )
			$data['response'] = $response;
		if( !empty($_POST) )
			$data['post'] = $_POST;

		$dataresult = array();
		if ( !empty($result) )
			$dataresult = $result;

		$data['js'] = array('modules/voucher_form.js');
		$data['class_types'] = $this->classtype_model->get_types();
		$this->template('vouchers/forms', $data);
		
		
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	public function get_option_meters(){

		$data['meters'] = $this->fetch();
		$data['selected'] = $this->input->get('id');
		print_r($data['selected']);
		echo $this->load->view("meter_reading/options", $data, true);
	}

	public function walkin_rate(){

		$post = $this->input->post();

		if(!empty($post['class_type_id'])){
			$rate = $this->walkinrates_model->get_rate($post['class_type_id']);
		} else {
			$rate = 0;
		} 

		echo $rate;
	}

}