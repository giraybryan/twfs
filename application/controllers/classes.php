<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classes extends MY_Controller {
	public $table = "class_type";
	public $controller = "classes"; 

	public function __construct(){
		parent::__construct();
		$this->load->model(array('classtype_model','class_category_model','walkinrates_model'));
		$this->superadmin_access();
	}
	public function index(){

		$result = $this->fetch();
		$js_files = array('modules/delete.js','modules/meterreading.js');
		$this->template('class_types/index', array('classes' => $result,'controller' => $this->controller,'js' => $js_files));
	}

	public function paginate($keywords = 0, $sort_by = 'class_type.id', $sort_order = 'DESC', $limit = 20, $offset = 0){

		$result['members'] = $this->fetch($keywords, $sort_by, $sort_order, $limit, $offset);
		echo $this->load->view('class_types/list',$result, true);
	}

	public function fetch($keywords = 0, $sort_by = 'class_type.id', $sort_order = 'DESC', $limit = 20, $offset = 0){
		
		$params = array(
				'limit'  => $limit,
				'sort_by' => $sort_by, 
				'sort_order' => $sort_order, 
				'offset' => $offset, 
			);

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}

		$result = $this->classtype_model->get_types($params);
		return $result;
	}

	public function add(){

		$this->load->library('form_validation');
		
		$post = $this->input->post();
		if( empty($post['id']) ){
			
			$action = 'save';
		} else {
			$action = 'edit';
		}

		$rules = array(
	           	'class_title' => array(
	                     'field' => 'class_title',
	                     'label' => 'Class Title',
	                     'rules' => 'trim|required'
	                     ),
	           	'class_category_id' => array(
	                     'field' => 'class_category_id',
	                     'label' => 'Class Category',
	                     'rules' => 'trim|required'
	                     ),
	           ); 
		$this->form_validation->set_rules($rules);
		if ( $this->form_validation->run() ) {

			if( empty($post['id']) ){
				$result = $this->classtype_model->add($this->input->post());
			} else {
				$result = $this->classtype_model->edit($this->input->post());
			}

			if ( $result ){
				$response = array('result' => 1, 'msg' => 'Successfully '.$action.'ed');
				redirect(base_url('classes'));
			} else {
				$response = array('result' => 0, 'msg' => 'Failed to '.$action);
			}
			
		} else {
			$response = array('result' => 0, 'msg' => validation_errors());
		}
		if( !empty($response) )
			$data['response'] = $response;
		if( !empty($_POST) )
			$data['post'] = $_POST;

		$dataresult = array();
		if ( !empty($result) )
			$dataresult = $result;

		$data['categories'] = $this->class_category_model->get_categories();
		$this->template('class_types/class_type_form', $data);
		
		
	}

	public function edit($id){

		$this->load->library('form_validation');
		
		$post = $this->input->post();
		if( $post ){
			$rules = array(
				'class_title' => array(
					  'field' => 'class_title',
					  'label' => 'Class Title',
					  'rules' => 'trim|required'
					  ),
				'class_category_id' => array(
					  'field' => 'class_category_id',
					  'label' => 'Class Category',
					  'rules' => 'trim|required'
					  ),
			); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) 
			{
				$result = $this->classtype_model->edit($post);
				$response = array('result' => 1, 'msg' => 'Successfully '.$action.'ed');
				redirect(base_url('classes'));
			}	
			else 
			{
				$response = array('result' => 0, 'msg' => validation_errors());
			}
		}
		if( !empty($response) )
		{
			$data['response'] = $response;
		}

		$data['class'] = $this->classtype_model->get_by_id($id);
		$data['categories'] = $this->class_category_model->get_categories();
		$this->template('class_types/class_type_form', $data);
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->classtype_model->inactive($this->input->post('id'));
			print_r($result); die;

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'ClassType '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	public function get_option_meters(){

		$data['meters'] = $this->fetch();
		$data['selected'] = $this->input->get('id');
		print_r($data['selected']);
		echo $this->load->view("meter_reading/options", $data, true);
	}

	public function walkin_rate(){

		$post = $this->input->post();

		if(!empty($post['class_type_id'])){
			$rate = $this->walkinrates_model->get_rate($post['class_type_id']);
		} else {
			$rate = 0;
		} 

		echo $rate;
	}

}