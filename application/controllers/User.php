<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {
	private $user_session = "";
	
	public function __construct(){
		parent::__construct();

		$this->load->model( array('Users_model') );
	}

	public function index()
	{
		$this->user_session = $this->session->userdata('userdata');
		$data['info'] = $this->Users_model->get($this->user_session['username']);

		if( $this->input->post() ){
			if( $this->input->post('password') == $this->input->post('confirm') && !empty($this->input->post('password')) ){
				$result = $this->Users_model->update_pw($data['info']->username, $this->input->post('password'));
				
				if( $result ){
					$data['success_msg'] = "Password Changed";
				}
			} else {
				$data['error_msg'] = "password didnt match";
			}
		}

		$this->template('user/index', $data);
	}

	public function login(){
		
	}

	public function logout(){

	}
}
