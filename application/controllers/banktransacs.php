<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banktransacs extends MY_Controller {
	public $table = "bank";
	public $table_transac = "bank_transactions";
	public function __construct(){
		parent::__construct();
		$this->load->model(array('banks_model','bank_transactions_model','cashflows_model'));
	}
	public function index(){
		$date = date('Y-m-d');
		$data['banktransacs'] = $this->bank_transactions_model->gets(
				array(
					'where'=> array(
							array('date' => $date)
					)
				) 
			);
		$data['js'] = array('modules/banktransacs.js');

		$this->template('banktransacs/index', $data );
	}

	public function add(){

		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		$response = array();
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			$rules = array(
           	'bankid' => array(
                     'field' => 'bankid',
                     'label' => 'Bank Type',
                     'rules' => 'trim|required'
                     ),
           	'transaction_type' => array(
                     'field' => 'transaction_type',
                     'label' => 'Transaction Type',
                     'rules' => 'trim|required'
                     ),
           	'amount' => array(
                     'field' => 'amount',
                     'label' => 'Amount',
                     'rules' => 'trim|required'
                     ),

           ); 
			if( empty($post['date'])){
				$post['date'] = date('Y-m-d');
			} else {
				$post['date'] = date('Y-m-d', strtotime($post['date']));
			}
			$this->form_validation->set_rules($rules);
			$insuffecient_error = "";
			switch ($post['transaction_type']) {
				case 'deposit':
					$current_coh = $this->get_current_coh($post['date']);
					$insuffecient_error = "";
					if( empty( $current_coh ) || $current_coh < $post['amount'] ){
						$insuffecient_error = "Insufficient Fund for Cash On Hand.";
					}
					break;
				case 'withdraw':
					$current_balance = $this->get_bank_balance($post['bankid']);
					$insuffecient_error = "";
					if( empty( $current_balance ) || $current_balance < $post['amount'] ){
						$insuffecient_error = "Withdraw Cannot Proceed. Insufficient Fund for ".$this->banks_model->getBankName($post['bankid']);
					}
					break;
				default:

					break;
			}
			

			if ( $this->form_validation->run() && empty($insuffecient_error) ) {
				
				 $result =  $this->bank_transactions_model->add($post);
				 if( empty($post['id']) ){
				 	// adjust the current bank amount
				 	$bank = $this->banks_model->get($post['bankid']);
				 	
				 	switch( $post['transaction_type'] ) {
				 		case "deposit":
				 			$bank->amount = $bank->amount + $post['amount'];
				 		
				 			break;
				 		case "withdraw":
				 			$bank->amount = $bank->amount - $post['amount'];
				 			
				 			break;
				 		case "transfer":
				 			break;
				 	}
				 	$_bank = array('id' => $post['bankid'], 'amount' => $bank->amount);
				 	$this->banks_model->edit($_bank);
				 	//update cashflow
				 	$cashflow = array('date' =>$post['date'],'fieldselected' => $post['transaction_type'],'amount' => $post['amount']);				 
				 	$this->cashflow_audtrail($cashflow);
				 }
				 
 
				 if( $result) {
				 	redirect(base_url('banktransacs'));
				 } else {
				 	die('error');
				 }
				
				
			} else {
				$error = "";
				if( !empty($insuffecient_error ) ){
					$error = $insuffecient_error.' <br/>';
				}
				
				$error .= validation_errors();

				$response = array('result' => 0, 'msg' => $error );
			}	
		}

		if( !empty($response)){
			$data = $response;
		}
		$banktypes = $this->banks_model->gets();
		
		$js_files = array('modules/banktransac_form.js');
		$this->template('banktransacs/form', array('response' => $response,'banktypes' => $banktypes ,'js' => $js_files));
		
	}

	public function edit($id){
		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		$response = "";
		if( !empty($post) ){
			$action = 'edit';
		
		$rules = array(
           	'sales_type_id' => array(
                     'field' => 'sales_type_id',
                     'label' => 'Sales Type',
                     'rules' => 'trim|required'
                     ),
           	'amount' => array(
                     'field' => 'amount',
                     'label' => 'Amount',
                     'rules' => 'trim|required'
                     ),

           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				
				if( empty($post['date'])){
					$post['date'] = date('Y-m-d');
				} else {
					$post['date'] = date('Y-m-d', strtotime($post['date']));
				}
				
				 $result =  $this->sales_model->edit($post);
				 if( $result) {
				 	redirect(base_url('sales'));
				 } else {
				 	die('error');
				 }
			}
		}
		$sales_types = $this->sales_model->get_types();
		$sales = $this->sales_model->get($id);
		$this->template('sales/form', array('response' => $response,'sales' => $sales,'types' => $sales_types));
	}

	

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	public function get_bank_balance($bankid){
		if( empty($bankid)){
			die('Empty Bank ID');
		}
		$bank = $this->banks_model->get($bankid);
		if( !empty($bank))
			return $bank->amount;

		return false;
	}


	public function transaction_bydate(){
		$date = date('Y-m-d', strtotime($this->input->post('date')));
		$data['banktransacs'] = $this->bank_transactions_model->gets(
				array(
					'where'=> array(
							array('date' => $date)
					)
				) 
			);
		echo $this->load->view("banktransacs/list", $data, true);

	}
	


}