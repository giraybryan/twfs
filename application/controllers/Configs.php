<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configs extends MY_Controller {
	public $table = "config";
	public $controller = "configs";

	public function __construct(){
		parent::__construct();
		$this->load->model(array('config_model'));
		$this->superadmin_access();
	}
	public function index(){
		$date = date('Y-m-d');
		$data['configs'] = $this->config_model->gets();
		$data['js'] = array('modules/configs.js');
		$this->template('configs/index', $data );
	}

	public function add()
	{

		$this->load->library('form_validation');
		
		$post = $this->input->post();
		$response = array();
		$result = 1;
		$msg = "";
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
	
			$rules = array(
           	'config_name' => array(
                     'field' => 'config_name',
                     'label' => 'Config Name',
                     'rules' => 'trim|required'
                     ),
           	'value' => array(
                     'field' => 'value',
                     'label' => 'Config Value',
                     'rules' => 'trim|required'
                     ),

           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				
				 $result =  $this->config_model->add($post);
				 if( $result) {
					 redirect(base_url($this->controller));
					 $response = array('result' => 1, 'msg' => '2');
				 } else {
					$response = array('result' => 0, 'msg' => validation_errors());
				 }
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
				$result = 0;
			}	
		}

		if( !empty($response)){
			$data = $response;
		}
	
		$js_files = array('modules/config.js');
		$this->template('configs/form', array('result' => $result,'response' => $response ,'js' => $js_files));
		
	}

	public function getCOH($date){
		echo number_format($this->get_current_coh($date),2);
	}
	public function edit($id){
		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		$response = "";
		if( !empty($post) ){
			$action = 'edit';
		
			$rules = array(
				'config_name' => array(
					  'field' => 'config_name',
					  'label' => 'Config Name',
					  'rules' => 'trim|required'
					  ),
				'value' => array(
					  'field' => 'value',
					  'label' => 'Config Value',
					  'rules' => 'trim|required'
					  ),
 
			); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
			
				$result =  $this->config_model->edit($post);
				 if( $result) {
				 	redirect(base_url($this->controller));
				 } else {
					$response = array('result' => 0, 'msg' => validation_errors());
				 }
			}
		}
		$config = $this->config_model->get($id);
	
		$this->template('configs/form', array('response' => $response,'config' => $config));
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	public function get_option_meters(){

		$data['meters'] = $this->fetch();
		$data['selected'] = $this->input->get('id');
		print_r($data['selected']);
		echo $this->load->view("meter_reading/options", $data, true);
	}

}