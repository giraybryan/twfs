<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller {

	public $table = "users";
	public $controller = "users";

	public function __construct(){
		parent::__construct();
		$this->load->model(array('config_model','users_model'));

		$this->superadmin_access();
	}
	public function index()
	{
	
		$result = $this->fetch();
		$js_files = array('modules/delete.js','modules/meterreading.js');
		$this->template('users/index', array('users' => $result,'controller' => $this->controller,'js' => $js_files));
	}

	public function fetch($keywords = 0, $sort_by = 'users.id', $sort_order = 'DESC', $limit = 20, $offset = 0){
		
		$params = array(
				'limit'  => $limit,
				'sort_by' => $sort_by, 
				'sort_order' => $sort_order, 
				'offset' => $offset, 
			);

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}

		$result = $this->users_model->gets($params);
		return $result;
	}

	public function add(){
		$post = $this->input->post();
		$response = array();
		if( !empty($post) ){
			unset($post['id']);
			$post['branch_id'] = $this->branch_id;
			$result = $this->users_model->add($post);
			if( $result ){
				redirect('users');
			} else {
				$response = array('msg' => 'Error', 'result' => 0);
			}
		} 
		$js_files = array();
		$user = array();
		$this->template('users/form', array('response' => $response,'user' => $user,'controller' => $this->controller,'js' => $js_files));
	}

	public function edit($id = 0){
		//$user->
		if( empty($id ) || $id == 0 ){
			redirect('users');
		}
		$post = $this->input->post();
		if( !empty($post) ){

		} 
		$user = $this->users_model->get_by_id($id);
		$js_files = array();
		$this->template('users/form', array('user' => $user,'controller' => $this->controller,'js' => $js_files));
		
	}

	public function delete($id = 0){
		//$user->
		if( empty($id ) || $id == 0 ){
			redirect('users');
		}
		$post = $this->input->post();
		if( !empty($post) ){

		} 
		$this->db->where('id',$id)->delete($this->table);
		
	}
}
