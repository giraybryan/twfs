<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public $error_msg = "";
	public $success_msg = "";

	public function __construct(){
		parent::__construct();

		$this->load->model( array('members_model','classes_model','walkins_model','membershiptype_model','memberships_model','sales_model','expense_model','cashflows_model','banks_model','attendance_model') );
	}
	public function index()
	{	

		$todate = date('Y-m-d'); //today date
		$data['total_members'] = $this->members_model->total_members();
		$data['current_month_result'] = $this->members_model->montly_result(array('month_value' => date('Y-m-d')));
		$data['current_prevmonth_result'] = $this->members_model->montly_result(array('month_value' => '-1 month'));
		$data['sales_today'] = $this->sales_model->get_dailysales($todate);
		$data['sales_month'] = $this->sales_model->get_monthlysales($todate);
		$data['expenses_today'] = $this->expense_model->get_dailyexpenses($todate);
		$data['expenses_month'] = $this->expense_model->get_monthlyexpneses($todate);
		$data['sales_d'] =  $this->sales_model->get_dailysales('2017-11-22');
		$data['banks'] = $this->banks_model->gets();
		$data['attendance'] = $this->attendance_model->get_by_date(array('date' => $todate,'branch_id' => $this->branch_id)); 
		$data['date'] = $todate;
		$data['coh'] = $this->get_current_coh();
		if( $data['current_prevmonth_result'] > 0 ){				
			$data['percent_result'] = $data['current_month_result'] / $data['current_prevmonth_result'] * 100;
			if( $data['current_month_result'] < $data['current_prevmonth_result']){
				$data['percent_result'] = $data['percent_result'] * -1;
			}
		} else {
			$data['percent_result'] = "";
		}


		$first_day_this_month = date('Y-m-01',strtotime(date('Y-m-d'))); 
        $last_day_this_month  = date('Y-m-t',strtotime(date('Y-m-d')));
        $first_day_lastmonth = date('Y-m-01',strtotime('-1 months')); 
        $last_day_lastmonth  = date('Y-m-t',strtotime('-1 months'));
      
		$data['month_newenroll'] = $this->db->select('COUNT(id) as total')->where('date_registered >=',$first_day_this_month)->where('date_registered <=', $last_day_this_month)->get('members')->row();

		$data['month_renewal'] = $this->db->select('COUNT(id) as total')->where('renew',1)->where('date_register >=', $first_day_this_month.' 00:00:00')->where('date_register <=', $last_day_this_month.' 23:59:00')->get('membership_log')->row();

		$data['lastmonth_newenroll'] = $this->db->select('COUNT(id) as total')->where('date_registered >=',$first_day_lastmonth)->where('date_registered <=', $last_day_lastmonth)->get('members')->row();
		
		$data['lastmonth_renewal'] = $this->db->select('COUNT(id) as total')->where('renew',1)->where('date_register >=', $first_day_lastmonth.' 00:00:00')->where('date_register <=', $last_day_lastmonth.' 23:59:00')->get('membership_log')->row();


		$data['class1_total'] = $this->classes_model->get_class_enrol(1);
		$data['class2_total'] = $this->classes_model->get_class_enrol(4);

		$data['total_male'] = $this->members_model->total_gender('m');
		$data['total_female'] = $this->members_model->total_gender('f');
		$data['monthly_walkin'] = $this->walkins_model->total_monthlywalkin(array('month_value' => date('Y-m-d')));

		$data['members_attendance'] = $this->attendance_model->get_members_list(date('Y-m-d'));
		
		
		$data['prev_monthlywalkin'] = $this->walkins_model->total_monthlywalkin(array('month_value' => '-1 month'));
		if( $data['prev_monthlywalkin'] > 0){
			$data['walkin_result'] = ( $data['monthly_walkin'] - $data['prev_monthlywalkin'] ) / $data['monthly_walkin'] * 100;
		} else {
			$data['walkin_result'] = 0;
		}

		$data['willexpire'] = $this->memberships_model->get_expiry($todate);
		//echo "<pre>"; print_r($data['willexpire']); die;
		$this->template('dashboard', $data);
	}

	private function insert_reading(){
		/*echo "<pre>";
		print_r($this->input->post());*/

		$post = $this->input->post();
		//echo "<pre>";
		if( empty($post['date_read']) ){
			$this->error_msg = "Date Reading is Required";
			return false;
		}

		foreach( $post['meter'] as $key => $meter){
			//print_r($meter);
			if( !empty($meter['reading'])){
				$insert_data = array(
						'meter_id'  => $key,
						'reading'   => $meter['reading'],
						'date_read' => date('Y-m-d',strtotime($post['date_read'])), 
					);

				$insert_id = $this->reading_model->add($insert_data);

				if ( !empty($insert_id) ){
					$this->success_msg = "Data Inserted<br/>";
				}
			}
		}
		//die;
	}


}
