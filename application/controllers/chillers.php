<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chillers extends MY_Controller {
	public $table = "chillers";
	public function __construct(){
		parent::__construct();
		$this->load->model(array('chillers_model','chillersinventory_model','chillerslog_model'));
	}
	public function index(){

		$result = $this->fetch();
		$js_files = array('modules/meterform.js','modules/meterreading.js');

		$this->template('chillers/index', array('chillers' => $result,'js' => $js_files));
	}

	public function paginate($keywords = 0, $sort_by = 'members.id', $sort_order = 'DESC', $limit = 20, $offset = 0){

		$result['members'] = $this->fetch($keywords, $sort_by, $sort_order, $limit, $offset);
		echo $this->load->view('members/list',$result, true);
	}

	public function fetch($keywords = 0, $sort_by = 'chillers.id', $sort_order = 'DESC', $limit = 20, $offset = 0){
		
		$params = array(
				'limit'  => $limit,
				'sort_by' => $sort_by, 
				'sort_order' => $sort_order, 
				'offset' => $offset, 
			);

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}

		$result = $this->chillers_model->get_all($params);
		return $result;
	}

	public function add(){

		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			
			$rules = array(
	           	'name' => array(
	                     'field' => 'name',
	                     'label' => 'Member',
	                     'rules' => 'trim|required'
	                     ),
	           	'selling_price' => array(
	                     'field' => 'selling_price',
	                     'label' => 'Selling Pirce',
	                     'rules' => 'trim|required'
	                     ),
	           	'capital_price' => array(
	                     'field' => 'capital_price',
	                     'label' => 'Capital Pirce',
	                     'rules' => 'trim|required'
	                     ),

	           ); 
		
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				
			
				$result = $this->chillers_model->add($post);
				$this->session->msg = 'Chiller Item Successfully added';
				 redirect(base_url().'chillers');
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}

		if( !empty($response)){
			$data = $response;
		} else {
			$response = array();
		}
		$this->session->msg = 'Chiller Item Successfully added';
		$js_files = array('modules/attendance_form.js');
		$this->template('chillers/form', array('formaction' => 'add','js' => $js_files));
		
	}
	public function edit($id){

		$this->load->library('form_validation');
		
		$post = $this->input->post();
		
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			
			$rules = array(
	           	'name' => array(
	                     'field' => 'name',
	                     'label' => 'Member',
	                     'rules' => 'trim|required'
	                     ),
	           	'selling_price' => array(
	                     'field' => 'selling_price',
	                     'label' => 'Selling Pirce',
	                     'rules' => 'trim|required'
	                     ),
	           	'capital_price' => array(
	                     'field' => 'capital_price',
	                     'label' => 'Capital Pirce',
	                     'rules' => 'trim|required'
	                     ),

	           ); 
		
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				
			
				$result = $this->chillers_model->update($post);
				$this->session->msg = 'Chiller Item Updated';
				 redirect(base_url().'chillers');
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}

		if( !empty($response)){
			$data = $response;
		} else {
			$response = array();
		}
		
		$js_files = array('modules/attendance_form.js');
		$item = $this->chillers_model->get(array('where' =>array('id' => $id)));
		$this->template('chillers/form', array('formaction' => 'edit/'.$id,'item' => $item,'js' => $js_files));
		
	}

	public function add_inventory($id, $inventory_id = false){
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
			
			$rules = array(
	           	'chiller_id' => array(
	                     'field' => 'chiller_id',
	                     'label' => 'Chiller ID',
	                     'rules' => 'trim|required'
	                     ),
	           	'qty' => array(
	                     'field' => 'qty',
	                     'label' => 'Stock Quantity',
	                     'rules' => 'trim|required'
	                     ),

	           );
		
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				
				$post['date_added'] = date('Y-m-d', strtotime($post['date_added']));
				$result = $this->chillersinventory_model->add($post);
				//update chiller stock
				$this->update_chillstock($id, $post['qty'],"add", $post['date_added']);

				$chiller_info = $this->chillers_model->get(array('where' => array('id' => $id)));
				//minus expense inventory to capital
				if( $chiller_info ){
					$totalcapital = $chiller_info->capital_price * $post['qty'];
					$current_capital = $this->db->where('box_type', 'capital')->order_by('date','DESC')->get('chiller_daily_cash')->row();
					$capital = $current_capital->amount - $totalcapital;
					$lastday = $current_capital->lastday - $totalcapital;

					$this->db->where('date', $current_capital->date)->where('box_type', $current_capital->box_type)->update('chiller_daily_cash',array('amount' => $capital,'lastday' => $lastday));
					//echo $current_capital->amount.' '.$capital;

				} 

				$this->session->msg = 'Chiller Item Updated';
				 redirect(base_url().'chillers');
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}

		$item = $this->chillers_model->get(array('where' =>array('id' => $id)));
		$inventory = array();
		$formaction = 'add_inventory/'.$id;
		if($inventory_id){
			$inventory = $this->chillersinventory_model->get(array('where' => array('id' => $inventory_id)));
			$formaction .= '/'.$inventory_id;
		}
		

		$this->template('chillers/form_inventory', array('formaction' => $formaction,'inventory' => $inventory,'item' => $item));
	}

	public function update_chillstock($id, $newqty, $operator, $date){
		$item = $this->chillers_model->get(array('where' =>array('id' => $id)));
		$stock = 0;
		if($operator == "add"){
			$stock = $item->stock + $newqty;
		} else {
			$stock = $item->stock - $newqty;
		}
		$post['stock'] = $stock;
		$post['id'] = $id;
		//echo $stock = $item->stock - $newqty;
        print_r($post); print_r($item); 
		$result = $this->chillers_model->update($post);

		if($date){
			$result = $this->chillerslog_model->getsales($id, $date);
			
			if(empty($result)){
				$saleslog = array('chiller_id' => $id, 'remaining' => $stock, 'date' => $date);
			} else {
				$saleslog = array('id' => $result->id,'chiller_id' => $id, 'remaining' => $stock, 'date' => $date);
			}

			$this->chillerslog_model->savelogsales($saleslog);
		}

		return false;

	}


	public function sales(){
		$date = date('Y-m-d');
		$data['saleslog'] = $this->chillerslog_model->getlogs(array('where' => array('date' => $date)));
		$data['js'] = array('modules/chillers_sales.js');
		$this->template('chillers/sales/index',$data);
	}

	public function fetchsales(){
		$post = $this->input->post();
		$date = date('Y-m-d', strtotime($post['date']));
		$data['saleslog'] = $this->chillerslog_model->getlogs(array('where' => array('date' => $date,'deleted' => 0)));
		echo $this->load->view('chillers/sales/list', $data, true, true);
	}

	public function ajaxupdate_sales(){
		$post = $this->input->post();
		$result = $this->chillerslog_model->add($post);
		echo json_encode(array('result' => 1, 'msg' => 'updated'));
	}

	public function sales_summary(){
		$date = date('Y-m-d');
		$data['summary'] = $this->chillerslog_model->getsummary(array('where' => array('date' => $date)));
		$data['js'] = array('modules/chillers_summary.js');
		$this->template('chillers/sales/summary',$data);
	}

	public function fetchsummarysales(){
		$post = $this->input->post();
		$date = date('Y-m-d',strtotime($post['date']));
		$data['summary'] = $this->chillerslog_model->getsummary(array('where' => array('date' => $date)));
		$data['capital'] = $this->chillers_model->get_cashbox('capital');
		$data['profit'] = $this->chillers_model->get_cashbox('profit');
		echo $this->load->view('chillers/sales/summary_list', $data, true, true);
	}

	public function addsales($id = false){

		$this->load->library('form_validation');
		$post = $this->input->post();
		
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			
			$rules = array(
	           	'chiller_id' => array(
	                     'field' => 'chiller_id',
	                     'label' => 'Chiller ID',
	                     'rules' => 'trim|required'
	                     ),
	           	'qty' => array(
	                     'field' => 'qty',
	                     'label' => 'Stock Quantity',
	                     'rules' => 'trim|required'
	                     ),

	           ); 
			$post['date'] = date('Y-m-d', strtotime($post['date']));
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				if( $post['member_id'] == "" )
					unset($post['member_id']);

				if( $post['customer_id'] == "" )
					unset($post['customer_id']);

				$result = $this->chillerslog_model->add($post);
				//update chiller stock
				$this->update_chillstock($post['chiller_id'], $post['qty'],"minus", $post['date']);
				$this->session->msg = 'Chiller Item Updated';
				redirect(base_url().'chillers/sales');
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}

		if($id){
			$data['sales'] = $this->chillerslog_model->getsales_row($id);
		} else {
			$data['sales'] = (object) array();
		}
		$data['items'] = $this->chillers_model->get_all();
		$data['formaction'] = "addsales";
		$data['js'] = array('modules/chillers_logform.js');
		$this->template('chillers/sales/form',$data);
	}

	public function check_first_attend($log_date, $member_id){
			$log_date = date('Y-m-d',strtotime($log_date));
			$result = $this->memberships_model->get_membershiplog($member_id);
			
			$date_start = $result->date_start;
			
			if( $result->date_start > $log_date || $date_start == "0000-00-00"){
				
				$updatedate = array('date_start' => $log_date, 'id' => $result->id); 
				$date_start = $log_date;

			}  else {
				return false;
			}
			
			if( $result->date_end == "0000-00-00"){
				$started_date =strtotime($date_start);
				switch($result->membership_type_id){
					case 1:
						$updatedate['date_end'] = date("Y-m-d", strtotime("+1 month", $started_date));
						break;
					case 2:
						$updatedate['date_end'] = date("Y-m-d", strtotime("+3 month", $started_date));
						break;
					case 3:
						$updatedate['date_end'] = date("Y-m-d", strtotime("+6 month", $started_date));
						break;
				}
			}
			
			$this->memberships_model->edit($updatedate);
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	public function revertsales(){

		$post = $this->input->post();

		$sales = $this->chillerslog_model->getsales_row($post['id']);
		if( $sales->date == date('Y-m-d')){
			$sales->deleted = 1;
			$sales->deleted_by = $this->userdata['userid'];
			$sales = (array) $sales;
			unset($sales['name']);
			unset($sales['firstname']);
			unset($sales['lastname']);
			$this->chillerslog_model->updatesaleslog($sales);
			
			$chiller = $this->chillers_model->get(array('where'=> array('id' => $sales['chiller_id']))); 
			$chiller->stock = $chiller->stock + $sales['qty'];
			$chiller = (array) $chiller;
			$this->chillers_model->update($chiller);

			//update daily log sales
			$chiller_log = $chiller_log = $this->chillerslog_model->getsales($chiller['id'],date('Y-m-d'));
			$chiller_log->remaining = $chiller['stock'];
			$chiller_log = (array) $chiller_log;
			unset($chiller_log['name']);
			$this->chillerslog_model->savelogsales($chiller_log);
			
			echo json_encode(array('result' => 1,'msg' => 'deleted!'));	
		} else {
			echo json_encode(array('result' => 0,'msg' => 'cannot be deleted!'));	
		}
		
	}

}