<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classdurations extends MY_Controller {
	public $table = "class_duration";
	public $controller = "classdurations"; 
	public function __construct(){
		parent::__construct();
		$this->load->model(array('class_durations_model'));
		$this->superadmin_access();
	}
	public function index(){

		$result = $this->fetch();
		$js_files = array('modules/delete.js');
		$this->template('class_duration/index', array('durations' => $result,'controller' => $this->controller,'js' => $js_files));
	}


	public function paginate($keywords = 0, $sort_by = 'class_duration.id', $sort_order = 'DESC', $limit = 20, $offset = 0){

		$result['members'] = $this->fetch($keywords, $sort_by, $sort_order, $limit, $offset);
		echo $this->load->view('members/list',$result, true);
	}

	public function fetch($keywords = 0, $sort_by = 'class_duration.id', $sort_order = 'DESC', $limit = 20, $offset = 0){
		
		$params = array(
				'limit'  => $limit,
				'sort_by' => $sort_by, 
				'sort_order' => $sort_order, 
				'offset' => $offset, 
			);

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}

		$result = $this->class_durations_model->get($params);
		return $result;
	}
	public function edit($id){
		if( empty($id) ){
			redirect(base_url('classdurations'));
		}

		$this->load->library('form_validation');
		
		$post = $this->input->post();

		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			$rules = array(
		           	'duration' => array(
		                     'field' => 'durations',
		                     'label' => 'Duration',
		                     'rules' => 'trim|required'
		                     ),
		           	'duration_type' => array(
		                     'field' => 'duration_type',
		                     'label' => 'Duration Type',
		                     'rules' => 'trim|required'
		                     ),
		          
		           ); 

			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {

				unset($post['userid']);
				$result = $this->class_durations_model->edit($post);

				if ( $result ){
					redirect(base_url('classdurations'));
				} else {
					$response = array('result' => 0, 'msg' => 'Failed to '.$action);
				}
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}
	
		if( !empty($response)){
			$data = $response;
		} else {
			$response = array();
		}

		if( !empty($id) ){
			$duration = $this->class_durations_model->get_by_id($id);
		} else {
			$duration = array();
		}
		$js_files = array('modules/membershiptypes.js');
		$this->template('class_duration/form', array('duration' => $duration,'response' => $response,'response' => $response,'js' => $js_files ));
	}

	public function add(){

		$this->load->library('form_validation');
		
		$post = $this->input->post();
		
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			$rules = array(
		           	'duration' => array(
		                     'field' => 'durations',
		                     'label' => 'Duration',
		                     'rules' => 'trim|required'
		                     ),
		           	'duration_type' => array(
		                     'field' => 'duration_type',
		                     'label' => 'Duration Type',
		                     'rules' => 'trim|required'
		                     ),
		          
		           ); 

			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				unset($post['userid']);
				if( empty($_POST['id']) ){
					$result = $this->class_durations_model->add($post);
				} else {
					$result = $this->class_durations_model->edit($post);
					$membership_id = $_POST['id'];
				}
	
				if ( $result ){
					$response = array('result' => 1, 'msg' => 'Successfully '.$action.'d');
					redirect(base_url('classdurations'));

				} else {
					$response = array('result' => 0, 'msg' => 'Failed to '.$action);
				}
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}
	
		if( !empty($response) ){
			$data = $response;
		} else {
			$response = array();
		}

		if( !empty($id) ){
			$duration = $this->class_durations_model->get_by_id($id);
		} else {
			$duration = array();
		}
		$js_files = array('modules/membershiptypes.js');

		$this->template('class_duration/form', array('duration' => $duration,'response' => $response,'js' => $js_files ));
		
	}

	public function insert_or($sales_arr  = array()){
		$or  = $this->db->select('*')->where(array('sales_id' => $sales_arr['sales_id']))->get('or_issue')->row_array();
		if (empty($or) ){
			$this->db->insert('or_issue',$sales_arr);
		} else {
			$this->db->where('id', $or['id']); 
        	$this->db->update('or_issue', $sales_arr);
		}
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->class_durations_model->inactive($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

}