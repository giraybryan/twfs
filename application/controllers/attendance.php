<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance extends MY_Controller {
	public $table = "members";
	public function __construct(){
		parent::__construct();
		$this->load->model(array('members_model','classtype_model','membershiptype_model','attendance_model','memberships_model','sales_model'));
	}
	public function index(){

		$result = $this->fetch();
		$js_files = array('modules/meterform.js','modules/meterreading.js');

		$this->template('attendance/index', array('members' => $result,'js' => $js_files));
	}

	public function paginate($keywords = 0, $sort_by = 'members.id', $sort_order = 'DESC', $limit = 20, $offset = 0){

		$result['members'] = $this->fetch($keywords, $sort_by, $sort_order, $limit, $offset);
		echo $this->load->view('members/list',$result, true);
	}

	public function fetch($keywords = 0, $sort_by = 'members.id', $sort_order = 'DESC', $limit = 20, $offset = 0){
		
		$params = array(
				'limit'  => $limit,
				'sort_by' => $sort_by, 
				'sort_order' => $sort_order, 
				'offset' => $offset, 
			);

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}

		$result = $this->members_model->get_members($params);
		return $result;
	}

	public function add(){

		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			if ( $_POST['attendance_type'] == "member"){
				$rules = array(
		           	'member_id' => array(
		                     'field' => 'member_id',
		                     'label' => 'Member',
		                     'rules' => 'trim|required'
		                     ),
		           	'class_type_id' => array(
		                     'field' => 'class_type_id',
		                     'label' => 'Class Type',
		                     'rules' => 'trim|required'
		                     ),

		           ); 
			} else {
				$rules = array(
		           	'amount' => array(
		                     'field' => 'amount',
		                     'label' => 'Amount',
		                     'rules' => 'trim|required'
		                     ),
		           	'class_type_id' => array(
		                     'field' => 'class_type_id',
		                     'label' => 'Class Type',
		                     'rules' => 'trim|required'
		                     ),

		           ); 
			}
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {

				if( empty($post['date'])){
					$post['date'] = date('Y-m-d H:s:i');
				} else {
					$post['date'] = date('Y-m-d H:s:i', strtotime($post['date']));
				}
				
				if( $post['attendance_type'] == 'member'){
					
					$insert_data = array('member_id' => $post['member_id'],'class_type_id' => $post['class_type_id'], 'date' => $post['date']);
					$result = $this->attendance_model->add($insert_data);
					//get membershiplog 

					if( $result ){
						$membership_log_details = $this->memberships_model->get_log( array('membership_log.member_id' => $post['member_id'],'membership_log.class_type_id' => $post['class_type_id']));
						
						if( !empty( $membership_log_details) ){
							if( is_null($membership_log_details->date_end ) ){
								// the expiration is not yet set
								die('set expiration');
							}

							// get the memebership type details
							$membershiptype_details = $this->membershiptype_model->get_by_id($membership_log_details->membership_type_id);
						
							//check if it has per session
							if( !empty($membershiptype_details->session_duration_id) ){
								//insert new session

								$session_arr = array(
									'member_id' => $membership_log_details->member_id,
									'membership_log_id' => $membership_log_details->id,
									'total_allowed' => $membershiptype_details->session_duration *1,
									'membership_type_id' => $membership_log_details->membership_type_id
								);
								$this->create_sessioncounter($session_arr);
							}
						}
					}
					//$this->check_first_attend($post['date'], $post['member_id'], $post['class_type_id']);
					
					$response = array('result' => 1, 'msg' => 'Log Successfully added');
				} else {
					$postdata = array(
							'description' => $post['description'],
							'class_type_id' => $post['class_type_id'],
							'date'			=> $post['date'],
						);
					
					$walkinlog_id =  $this->attendance_model->addwalkin($postdata);
					//add sales
					$sales = array(
							
							'walkin_log_id' 	=> $walkinlog_id,
							'amount' 	 		=> $post['amount'],
							'date'				=> $post['date'],
							'class_type_id'		=> $post['class_type_id'],
							'sales_type_id'		=> 2, //walk-in sales type
						);
					if( $post['description'] ){
						$sales['description'] = $post['description'];
					}

					$this->sales_model->add($sales);
					redirect(base_url('attendance/add'));
				}
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}

		if( !empty($response)){
			$data = $response;
		} else {
			$response = array();
		}

		$class_types = $this->classtype_model->get_types();
		$js_files = array('modules/attendance_form.js');
		$this->template('attendance/form', array('response' => $response,'class_types' => $class_types,'js' => $js_files));
		
	}

	public function check_first_attend($log_date, $member_id, $class_type_id){


			if( $class_type_id == 1 || $class_type_id == 6 || $class_type_id || 7){
				$log_date = date('Y-m-d',strtotime($log_date));
				$result = $this->memberships_model->get_membershiplog($member_id);
				
				$date_start = $result->date_start;
				
				if( $result->date_start > $log_date || $date_start == "0000-00-00"){
					
					$updatedate = array('date_start' => $log_date, 'id' => $result->id); 
					$date_start = $log_date;

				}  else {
					return false;
				}


				
				if( $result->date_end == "0000-00-00"){
					$started_date =strtotime($date_start);
					if($result->membership_type_id == 7 || $result->membership_type_id == 8 || $result->membership_type_id == 4 ){
						return false;
					}
					
					switch($result->membership_type_id){
						case 1:
							$updatedate['date_end'] = date("Y-m-d", strtotime("+1 month", $started_date));
							break;
						case 5:
							$updatedate['date_end'] = date("Y-m-d", strtotime("+1 month", $started_date));
							break;
						case 9:
							$updatedate['date_end'] = date("Y-m-d", strtotime("+1 month", $started_date));
							break;
						case 6:
							$updatedate['date_end'] = date("Y-m-d", strtotime("+1 month", $started_date));
							break;
						case 2:
							$updatedate['date_end'] = date("Y-m-d", strtotime("+3 month", $started_date));
							break;
						case 3:
							$updatedate['date_end'] = date("Y-m-d", strtotime("+6 month", $started_date));
							break;
					}
				}
				
				$this->memberships_model->edit($updatedate);
			}
			
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	public function get_total_by_date(){
		$post = $this->input->post();
		$post['date'] = date('Y-m-d', strtotime($post['date']));
		$post['branch_id'] = $this->branch_id;

		$result = $this->attendance_model->get_by_date($post);

		if( $result ){
			echo $result->total_attendees;
		} else {
			echo 0;
		}
		
	}

	public function create_sessioncounter($session_arr){
		
		$membership_type = $this->membershiptype_model->get_by_id($session_arr['membership_type_id']);
		
		//if session_duration_id is empty
		if( empty($membership_type->session_duration_id) ){
			return false;
		}

		$total_session = 1;
		$createss = array(
				'member_id' => $session_arr['member_id'],
				'membership_log_id' => $session_arr['membership_log_id'],
				'class_type_id' => $membership_type->class_type_id,
				'total_allowed' => $session_arr['total_allowed'],
				'total_session' => $total_session
		  );

		$this->db->insert('session_counter', $createss);
		
		//get total_session
		$sessionobj = $this->db->select('COUNT(session_counter.total_session) as total')->where(array('member_id' => $session_arr['member_id'], 'membership_log_id' => $session_arr['membership_log_id']))->get('session_counter')->row(); 
		if( $sessionobj->total >= $session_arr['total_allowed']){
			//update the membership_log
			$membershiplog = array(
				'note ' => 'Session Consumed : '. date('Y-m-d'),
				'date_end' => date('Y-m-d'),
			);
			$this->db->where('id',$session_arr['membership_log_id'])->update('membership_log', $membershiplog);
		}
		return false;
	}

}