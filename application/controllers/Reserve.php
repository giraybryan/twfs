<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reserve extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('classtype_model','members_model','config_model','reserve_model'));
		//redirect(base_url('/dashboard'));
	}

	public function index()
	{
        $data = array();
        $data['js'] = array('modules/reserve_list.js');

        $logo = $this->config_model->get_valueby_code('logo');
        if( empty($logo) ){
            $data['logo_img'] = base_url('assets/img/psp.png');
        } else {
            $data['logo_img'] = base_url('uploads/logo/'.$logo);
        }
        $data['class_types'] = $this->classtype_model->get_types();

        $reserve_result = $this->reserve_model->get_available_timeslot(date('Y-m-d'));
        $timeslot = array();
        foreach ($reserve_result as $date) {
        	$datetime = explode(' ', $date->date);
        	$timeslot[$datetime[1]] = $date->time_consumed;
        }

        $data['timeslot'] = $timeslot;


        $query['start'] = date('Y-m-d').' 00:00:00';
        $query['end'] = date('Y-m-d').' 23:00:00';

        $data['reservations'] = $this->reserve_model->get_reservation($query);

        $this->load->view('reserve/index', $data);
    } 

    public function get_available_slot(){
        $post = $this->input->post();
        $post['date'] = date('Y-m-d', strtotime($post['date']));
        $reserve_result = $this->reserve_model->get_available_timeslot($post['date']);

        $timeslot = array();
        foreach ($reserve_result as $date) {
            $datetime = explode(' ', $date->date);
            $timeslot[$datetime[1]] = $date->time_consumed;
        }

        $data['timeslot'] = $timeslot;
        echo $this->load->view('reserve/timeslot', $data, true, true);
    }

    public function add()
    {
        $post = $this->input->post();

        $_data = array(
                'date'          => date('Y-m-d',strtotime($post['date'])).' '.$post['timeslot'],
                'firstname'     => $post['firstname'],
                'lastname'      => $post['lastname'],
                'member_id'     => $post['member_id'],
                'class_type_id' => $post['class_type_id'],
                'date_added'    => date('Y-m-d')
            );
        $result = $this->reserve_model->add($_data);
        if( $result ){
             $json = array('result' => 1,'msg' => 'successfully reserve a slot');
        } else {
            $json = array('result' => 0,'msg' => 'failed to reserve a slot');
        }
        echo json_encode($json);

    }

    public function edit($id)
    {

    }

    public function delete()
    {

    }

    public function searchmember(){
    	$post = $this->input->post();

    	$key = $post['searchkey'];
    	$memberid = (int) $key;

    	if( filter_var($memberid) ){
    		//search member id
    		$result = $this->members_model->get_member($memberid);
    		if( !empty($result) ){
    			echo "<div class='result'><span class='btn btn-default' data-key='".$result->id."' data-firstname='".$result->firstname."' data-lastname='".$result->lastname."'>select</span> ".$result->firstname.' '.$result->lastname."</div>";
    		} else {
    			echo "<div class='result alert alert-danger'>Empty</div>";
    		}
    	} else {
    		//search by lastname
    		$result = $this->members_model->search(array('namesearch' => $key));
    		if( !empty($result) ){
				foreach ($result as $member) {
					echo "<div class='result'><span class='btn btn-default' data-key='".$member->id."' data-firstname='".$member->firstname."' data-lastname='".$member->lastname."'>select</span> ".$member->firstname.' '.$member->lastname."</div>";
				}
    		} else {
    			echo "<div class='result alert alert-danger'>Empty</div>";
    		}
    		
    	}
    }

    public function reservation_list(){
        $post = $this->input->post();

        $query['start'] = date('Y-m-d', strtotime($post['date'])).' 00:00:00';
        $query['end'] = date('Y-m-d', strtotime($post['date'])).' 23:00:00';

        $data['reservations'] = $this->reserve_model->get_reservation($query);

        echo $this->load->view('reserve/reserved_list',$data, true,true); 
    }
}