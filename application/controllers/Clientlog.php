<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientlog extends CI_Controller {
    public $table = "members";
    public $branch_id = 1;
    
    public function __construct(){
		parent::__construct();
		$this->load->model(array('config_model','members_model','classtype_model','membershiptype_model','classes_model','config_model','sales_model','config_model','memberships_model','attendance_model','memberships_freeze_model','branch_model'));
		$this->load->helper(array('form', 'url'));
	}
	public function index(){
        $data = array();
        $data['js'] = array('modules/client_log.js');

        $logo = $this->config_model->get_valueby_code('logo');
        if( empty($logo) ){
            $data['logo_img'] = base_url('assets/img/psp.png');
        } else {
            $data['logo_img'] = base_url('uploads/logo/'.$logo);
        }
        $data['branch_name'] = $this->branch_model->get_branchname($this->branch_id);
        $data['latest_card'] = $this->db->where('branch_id',$this->branch_id)->order_by('id','DESC')->get('cardread_history')->row();
        $this->load->view('clients/index', $data);
    } 

    public function get_attendance()
    {
        $post = $this->input->post();
        if( empty($post['date']) )
            $post['date'] = date('Y-m-d');

        
        $data['attendance'] = $this->attendance_model->get_members_list($post['date']);
        $this->load->view('attendance/daily_list.php', $data );
    }

    public function search_member(){
        $post = $this->input->post();
        if( $post ){
            $number_id = $post['id'];

            $member = $this->members_model->get_member($number_id);
            //$member = $this->members_model->get_by_memberidno($number_id);
            if( $member )
            {

                $data['member'] = $member;
                $data['setcounter'] = 1;
                $data['setattendance'] = 1;
                $this->load->view('clients/member_log', $data );
            } else 
            {
                echo "<div class='alert alert-danger'>Member not found</div>";
            }
        }
    }

    public function confirm_session(){
        $post = $this->input->post();
        if( !empty( $post ) )
        {
            //confirm password 
            $memberid_no = $post['memberid_no'];
            $member = $this->members_model->get_by_memberidno($memberid_no);
            if( empty($member) )
            {
                $response = array('msg' => 'Member ID invalid', 'result' => 0);
            } 
            else 
            {
                if( $member->password != $post['password'] )
                {
                    $response = array('msg' => 'password invalid', 'result' => 0);
                } 
                else 
                {
                    //get membership details
                    $membership_log_details = $this->memberships_model->get_membership($post['membership_log_id']);
                    $membershiptype_details = $this->membershiptype_model->get_by_id($membership_log_details->membership_type_id);
                    
                    //update get updated session
                    $membership_arr = array(
                        'member_id' => $member->id,
                        'membership_log_id' => $membership_log_details->id,
                    );
                    $session_total = $this->get_total_session($membership_arr);
                    if( $session_total >= $membershiptype_details->session_duration)
                    {
                        $response = array('msg' => 'session already consumed', 'result' => 0);
                    } 
                    else 
                    {
                        //add new session
                        $session_total = $session_total + 1;
                        $session_arr = array(
                            'member_id' => $member->id,
                            'membership_log_id' => $membership_log_details->id,
                            'total_allowed' => $membershiptype_details->session_duration,
                            'total_session' => $session_total
                        );
                        $this->db->insert('session_counter', $session_arr);
                        
                        //add attendance
                        $this->count_attendance($post);

                        if( $session_total >= $membershiptype_details->session_duration)
                        {
                            //update the membership_log
                            $membershiplog = array(
                                'note ' => 'Session Consumed : '. date('Y-m-d'),
                                'date_end' => date('Y-m-d'),
                            );
                            $this->db->where('id',$post['membership_log_id'])->update('membership_log', $membershiplog);
                            $response = array('msg'=> 'Last session Consumed', 'result' => 1);
                        } else {
                            $response = array('msg'=> 'Session Counted', 'result' => 1);
                        }

                        
                    }
                    

                    
                }
            }
        } 
        else {
            $response = array('msg'=> 'Empty request', 'result' => 0);
        }

        echo json_encode( $response );
    }

    public function post_attendance(){
        $post = $this->input->post();
        $post['attendance'] = 1;
        $result = $this->count_attendance($post);
        echo $result;
    }

    public function count_attendance($post = array()){
        
        if( !empty($post) )
        {
            $memberid_no = $post['memberid_no'];
            $member = $this->members_model->get_by_memberidno($memberid_no);
            //$member->id = 2;
            if( empty($member) )
            {
                $response = array('msg' => 'Member ID invalid', 'result' => 0);
            } 
            else 
            {
                //check if the member is already logged in for attendance
                $params = array(
                    'date >='   => date('Y-m-d').' 00:00:00',
                    'date <='   => date('Y-m-d').' 23:59:59',
                    'member_id' => $member->id,
                    'branch_id' => $this->branch_id,
                );

                $result = $this->attendance_model->get($params);
                if( empty($result) )
                {   //insert new attendance
                    $add_attendance = array(
                      'branch_id'       => $this->branch_id,
                      'date'            => date('Y-m-d H:i:s'),
                      'member_id'       => $member->id,
                      'class_type_id'   => 0,
                      'paid'            => 1
                    );
                    $this->attendance_model->add($add_attendance);
                }
                $response = array( 'msg' => 'Attendance Counted', 'result' => 1);
            }
            if( !empty($post['attendance']) )
            {
               return  json_encode($response);
            } 
        }
        return false;
    }

    public function get_total_session( $membership_arr ){
        $sessionobj = $this->db->select('COUNT(session_counter.total_session) as total')->where(array('member_id' => $membership_arr['member_id'], 'membership_log_id' => $membership_arr['membership_log_id']))->get('session_counter')->row(); 

        if( $sessionobj ){
            return $sessionobj->total;
        } 
        return false;
    }

    public function fetch_attendance(){

        $post = $this->input->post();
        $logdata = $this->db->where('branch_id',$this->branch_id)->order_by('id','DESC')->get('cardread_history')->row();
        if( $post['unix'] == $logdata->unix){
            echo json_encode( array('msg' => 'same card','card_id'=> $logdata->description,'result' => 0 ) );
            return false;
        }

        if( $logdata->unix > $post['unix'] ){
            if( $logdata->process == 'failed' ){
                echo json_encode(array('msg'=> $logdata->description,'result' => 0, 'id' => $logdata->id ) );
            } else {
                $cardno = $logdata->description;
                $member = $this->members_model->get_by_cardno($cardno);
                if( $member ){
                     echo json_encode(array('history' => $logdata,'member'=> $member,'result' => 1, 'id' => $logdata->id ) );
                } else {
                    echo json_encode(array('msg'=> 'Card Not yet registered','result' => 0, 'id' => $logdata->id ) );
                }
            }
            return false;
        }
        if( $logdata ){
            if( $logdata->process == 'failed' ){
                echo json_encode(array('msg'=> $logdata->description,'result' => 0, 'id' => $logdata->id ) );
            } else {
                $cardno = $logdata->description;
                $member = $this->members_model->get_by_cardno($cardno);
                if( $member ){
                     echo json_encode(array('history' => $logdata,'member'=> $member,'result' => 1, 'id' => $logdata->id ) );
                } else {
                    echo json_encode(array('msg'=> 'Card Not yet registered','result' => 0, 'id' => $logdata->id ) );
                }
            }
        }
    }

    public function send_sms(){
        $post = $this->input->post();

        if( !empty($post) )
        {
            $memberid_no = $post['id'];
            $member = $this->members_model->get_by_memberidno($memberid_no);
            
            
            //$member->id = 2;
            if( empty($member) || empty($member->mobile) )
            {
                $response = array('msg' => 'No Number', 'result' => 0);
            }  else {
                $message = $member->firstname. " " . $member->lastname . " : Logged in : ".date('Y-m-d h:i:s');
                
                //echo json_encode($member); 
                $ch = curl_init();
                $parameters = array(
                'apikey' => 'ad64d477250d1d4f6754b4fc9c0ee568', //Your API KEY
                'number' => $member->mobile,
                'message' => $message,
                'sendername' => 'SEMAPHORE'
                );
                curl_setopt( $ch, CURLOPT_URL,'https://semaphore.co/api/v4/messages' );
                curl_setopt( $ch, CURLOPT_POST, 1 );

                //Send the parameters set above with the request
                curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $parameters ) );

                // Receive response from server
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                $output = curl_exec( $ch );
                curl_close ($ch);

                //Show the server response
                //echo $output;
                echo json_encode(array('msg' => 'SMS SENT', 'result' => 1) );
                return false;
            }

        }
    }
}