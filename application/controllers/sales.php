<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends MY_Controller {
	public $table = "sales";
	public function __construct(){
		parent::__construct();
		$this->load->model(array('members_model','classtype_model','membershiptype_model','attendance_model','memberships_model','sales_model','expense_model','config_model'));
	}
	public function index(){

		$data['sales'] = $this->sales_model->get_dailysalesdata(date('Y-m-d'));
		$data['expenses'] = $this->expense_model->get_dailyexpensedata(date('Y-m-d'));
		$data['js'] = array('modules/sales_summary.js','modules/expense_summary.js');
		$data['coh'] = number_format($this->get_current_coh(),2);
		$this->template('sales/index', $data );
	}

	public function salesdata_bydate(){
		$post = $this->input->post();

		if ($post['type'] == 'range'){
			$data['sales'] = $this->sales_model->get_salesrange($post['start_date'], $post['end_date']);
			$post['date'] = $post['end_date'];
		} else {
			if(empty($post['date'])){
				$post['date'] = date('Y-m-d');
			} else {
				$post['date'] = date('Y-m-d', strtotime($post['date']));
			}
			$data['sales'] = $this->sales_model->get_dailysalesdata($post['date']);
		}

		$data['coh'] = number_format($this->get_current_coh($post['date']),2);

		
		echo $this->load->view('sales/list', $data, true, true);
	}
	public function ajax_sales_or(){
		$post = $_POST;
		$id = $this->sales_or($post);
		if( !empty($id) ){
			echo "success";
		} else {
			echo "failed";
		}
	}

	public function sales_or($sales_arr  = array()){
		$or  = $this->db->select('*')->where(array('sales_id' => $sales_arr['sales_id']))->get('or_issue')->row_array();
		if (empty($or) ){
			$this->db->insert('or_issue',$sales_arr);
			return $this->db->insert_id();
		} else {
			$this->db->where('id', $or['id']); 
        	$this->db->update('or_issue', $sales_arr);
        	return $or['id'];
		}
	}

	public function paginate($keywords = 0, $sort_by = 'members.id', $sort_order = 'DESC', $limit = 20, $offset = 0){

		$result['members'] = $this->fetch($keywords, $sort_by, $sort_order, $limit, $offset);
		echo $this->load->view('members/list',$result, true);
	}

	public function fetch($keywords = 0, $sort_by = 'members.id', $sort_order = 'DESC', $limit = 20, $offset = 0){
		
		$params = array(
				'limit'  => $limit,
				'sort_by' => $sort_by, 
				'sort_order' => $sort_order, 
				'offset' => $offset, 
			);

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}

		$result = $this->members_model->get_members($params);
		return $result;
	}

	public function add($sales_type_id = '', $member_id = ''){

		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			$rules = array(
           	'sales_type_id' => array(
                     'field' => 'sales_type_id',
                     'label' => 'Sales Type',
                     'rules' => 'trim|required'
                     ),
           	'amount' => array(
                     'field' => 'amount',
                     'label' => 'Amount',
                     'rules' => 'trim|required'
                     ),

           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				
				if( empty($post['date'])){
					$post['date'] = date('Y-m-d');
				} else {
					$post['date'] = date('Y-m-d', strtotime($post['date']));
				}
				$post['date_created'] = date('Y-m-d');
				
				if( empty($post['userid']) )
					unset($post['userid']);

				$post['branch_id'] = $this->branch_id;
				$result =  $this->sales_model->add($post);
				 if( $result) {
				 	//insert coh
					$cashflow = array('date' =>$post['date'],'fieldselected' => 'coh','amount' => $post['amount']);				 
			 		$this->cashflow_audtrail($cashflow);
				 	redirect(base_url('sales'));
				 } else {
				 	die('error');
				 }
				
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}

		if( !empty($response)){
			$data = $response;
		} else {
			$response = array();
		}

		$sales_types 			= $this->sales_model->get_types();
		$data['response'] 		= $response; 
		$data['sales_type_id'] = $sales_type_id;
		if( !empty( $sales_type_id) ){
			//get amount 
			$sales_type = $this->config_model->get($sales_type_id);
			switch($sales_type->code){
				case "joiningfee":
				case "access":
					$data['sales_amount'] = $sales_type->value;
					break;
			}
		}
		$data['member_id'] 		= $member_id;
		$data['types'] 			= $sales_types;
		
		$js_files = array('modules/attendance_form.js');
		$data['js'] 			= $js_files;

		if( !empty($member_id) )
			$data['member'] = $this->members_model->get_member($member_id);
	
		$this->template('sales/form', $data);
		
	}

	public function edit($id){
		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		$response = "";
		if( !empty($post) ){
			$action = 'edit';
		
		$rules = array(
           	'sales_type_id' => array(
                     'field' => 'sales_type_id',
                     'label' => 'Sales Type',
                     'rules' => 'trim|required'
                     ),
           	'amount' => array(
                     'field' => 'amount',
                     'label' => 'Amount',
                     'rules' => 'trim|required'
                     ),

           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				//get old value 
				$prev_sales = $this->sales_model->get($id);
				$prev_amount = $prev_sales->amount;

				$update_coh = false;
				if( $prev_amount != $post['amount']){
					$update_coh = true;
				}

				if( empty($post['date'])){
					$post['date'] = date('Y-m-d');
				} else {
					$post['date'] = date('Y-m-d', strtotime($post['date']));
				}
				
				 $result =  $this->sales_model->edit($post);
				 if( $result) {

				 	if( $update_coh  == true ){
					 	//undo coh value
						$prev_param = array('date' => $post['date'], 'field' => 'coh','amount' => $prev_amount);
						$this->recussiveUndoCoh($prev_param);
						
						//update coh
						$cashflow = array('date' =>$post['date'],'fieldselected' => 'coh','amount' => $post['amount']);				 
				 		$this->cashflow_audtrail($cashflow);
					}
				 	redirect(base_url('sales'));
				 } else {
				 	die('error');
				 }
			}
		}
		$sales_types = $this->sales_model->get_types();
		$sales = $this->sales_model->get($id);
		$this->template('sales/form', array('response' => $response,'sales' => $sales,'types' => $sales_types));
	}

	public function check_first_attend($log_date, $member_id){
			$log_date = date('Y-m-d',strtotime($log_date));
			$result = $this->memberships_model->get_membershiplog($member_id);
			
			$date_start = $result->date_start;
			
			if( $result->date_start > $log_date || $date_start == "0000-00-00"){
				
				$updatedate = array('date_start' => $log_date, 'id' => $result->id); 
				$date_start = $log_date;

			}  else {
				return false;
			}
			
			if( $result->date_end == "0000-00-00"){
				$started_date =strtotime($date_start);
				switch($result->membership_type_id){
					case 1:
						$updatedate['date_end'] = date("Y-m-d", strtotime("+1 month", $started_date));
						break;
					case 2:
						$updatedate['date_end'] = date("Y-m-d", strtotime("+3 month", $started_date));
						break;
					case 3:
						$updatedate['date_end'] = date("Y-m-d", strtotime("+6 month", $started_date));
						break;
				}
			}
			
			$this->memberships_model->edit($updatedate);
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	public function get_option_meters(){

		$data['meters'] = $this->fetch();
		$data['selected'] = $this->input->get('id');
		print_r($data['selected']);
		echo $this->load->view("meter_reading/options", $data, true);
	}

}