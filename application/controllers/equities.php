<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equities extends MY_Controller {
	public $table = "cashflow";
	public $table_transac = "bank_transactions";
	public function __construct(){
		parent::__construct();
		$this->load->model(array('equities_model','banks_model','bank_transactions_model','cashflows_model','owners_model'));
	}
	public function index(){
		$date = date('Y-m-d');
		$data['equities'] = $this->equities_model->gets();
		$data['owners'] = $this->owners_model->gets();
		$data['js'] = array('modules/banks.js');
		$this->template('equities/index', $data );
	}

	public function add(){

		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		$response = array();
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			$rules = array(
           	'date' => array(
                     'field' => 'date',
                     'label' => 'Date',
                     'rules' => 'trim|required'
                     ),
           	'amount' => array(
                     'field' => 'amount',
                     'label' => 'Amount',
                     'rules' => 'trim|required'
                     ),

           ); 
			if( empty($post['date'])){
				$post['date'] = date('Y-m-d');
			} else {
				$post['date'] = date('Y-m-d', strtotime($post['date']));
			}

			$insuffecient_error = "";
			$current_coh = $this->get_current_coh($post['date']);
			
			if( $current_coh < $post['amount'] ){
				$insuffecient_error = "Insuffecient Fund for Cash on Hand : ".number_format($current_coh,2);
			} 
			

			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() && empty($insuffecient_error) ) {
				
				 $result =  $this->equities_model->add($post);
				 if( empty($post['id']) ){
				 	//update cashflow
				 	$cashflow = array('date' =>$post['date'],'fieldselected' => 'equity','amount' => $post['amount']);				 
				 	$this->cashflow_audtrail($cashflow);
				 }
				 
 
				 if( $result) {
				 	redirect(base_url('equities'));
				 } else {
				 	die('error');
				 }
				
				
			} else {
				$error = "";
				if( !empty($insuffecient_error ) ){
					$error = $insuffecient_error.' <br/>';
				}
				
				$error .= validation_errors();
				$response = array('result' => 0, 'msg' => $error );
			}	
		}

		if( !empty($response)){
			$data = $response;
		}
		
		$js_files = array('modules/equity_form.js');
		$this->template('equities/form', array('response' => $response,'js' => $js_files));
		
	}

	public function edit($id){
		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		$response = "";
		if( !empty($post) ){
			$action = 'edit';
		
		$rules = array(
           	'sales_type_id' => array(
                     'field' => 'sales_type_id',
                     'label' => 'Sales Type',
                     'rules' => 'trim|required'
                     ),
           	'amount' => array(
                     'field' => 'amount',
                     'label' => 'Amount',
                     'rules' => 'trim|required'
                     ),

           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				
				if( empty($post['date'])){
					$post['date'] = date('Y-m-d');
				} else {
					$post['date'] = date('Y-m-d', strtotime($post['date']));
				}
				
				 $result =  $this->sales_model->edit($post);
				 if( $result) {
				 	redirect(base_url('sales'));
				 } else {
				 	die('error');
				 }
			}
		}
		$sales_types = $this->sales_model->get_types();
		$sales = $this->sales_model->get($id);
		$this->template('sales/form', array('response' => $response,'sales' => $sales,'types' => $sales_types));
	}

	

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}


}