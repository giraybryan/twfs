<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchandises extends MY_Controller {

	public $table 			= "merchandise";
	public $tablevariant  	= "merchandise_variants";
	public $controller 		= "merchandises";
	public function __construct(){
		parent::__construct();
		$this->load->model(array('merchandises_model','members_model','classtype_model','membershiptype_model','classes_model'));
	}

	public function index(){
		$result = $this->fetch();
		$js_files = array('modules/merchandises_list.js');
		$classes = $this->classes_model->get_activated_classes();
		$this->template('merchandises/index', array('merchandises' => $result,'controller' => $this->controller, 'classes' => $classes,'js' => $js_files));
	}

	public function paginate($keywords = 0, $sort_by = 'members.id', $sort_order = 'DESC', $limit = 20, $offset = 0){

		$result['merchandises'] = $this->fetch($keywords, $sort_by, $sort_order, $limit, $offset);
		echo $this->load->view('merchandises/list',$result, true);
	}

	public function fetch($keywords = 0, $sort_by = 'merchandise.id', $sort_order = 'DESC', $limit = 20, $offset = 0){
		
		$params = array(
				'limit'  => $limit,
				'sort_by' => $sort_by, 
				'sort_order' => $sort_order, 
				'offset' => $offset, 
			);
		
		$params['where'] = array('merchandise.branch_id' => $this->branch_id);
		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}

		$result = $this->merchandises_model->get_merchandises($params);
		return $result;
	}

	public function add(){
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			$rules = array(
		           	'item' => array(
		                     'field' => 'item',
		                     'label' => 'Item Title',
		                     'rules' => 'trim|required'
		                     ),
		           /*	'cost' => array(
		                     'field' => 'cost',
		                     'label' => 'Cost',
		                     'rules' => 'trim|required'
		                     ),
		           	'price' => array(
		                     'field' => 'price',
		                     'label' => 'Price',
		                     'rules' => 'trim|required'
		                     ),*/

		           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				$variants = $this->input->post('variant');
				unset($post['variant']);
				unset($post['id']);
				$post['branch_id'] = $this->branch_id;
				$result_id = $this->merchandises_model->add($post);
				
				foreach($variants as $variant){
					$variant['merchandise_id'] = $result_id;
					$variant['branch_id'] = $this->branch_id;
					$this->merchandises_model->add_variant($variant);
				}

				if(!empty($result_id)){
					redirect('/merchandises/');
				}

				if ( $result ){
					$response = array('result' => 1, 'msg' => 'Successfully '.$action.'d');
				} else {
					$response = array('result' => 0, 'msg' => 'Failed to '.$action);
				}
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}

		if( !empty($response)){
			$data = $response;
		} else {
			$response = array();
		}
		$js_files = array('modules/merchandise_form.js');
		
		$this->template('merchandises/form', array('response' => $response,'variants' => array(),'js' => $js_files));
	}

	public function edit($id){
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			$rules = array(
		           	'item' => array(
		                     'field' => 'item',
		                     'label' => 'Item Title',
		                     'rules' => 'trim|required'
		                     ),
		           /*	'cost' => array(
		                     'field' => 'cost',
		                     'label' => 'Cost',
		                     'rules' => 'trim|required'
		                     ),
		           	'price' => array(
		                     'field' => 'price',
		                     'label' => 'Price',
		                     'rules' => 'trim|required'
		                     ),*/

		           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				$variants = $this->input->post('variant');
				unset($post['variant']);
				$result_id = $this->merchandises_model->edit($post);
		

				foreach( $variants as $variant){
					$variant['merchandise_id'] = $result_id;
					$this->merchandises_model->add_variant($variant);
				}

				if(!empty($result_id)){
					redirect('/merchandises/');
				}

				if ( $result_id ){
					$response = array('result' => 1, 'msg' => 'Successfully '.$action.'d');
				} else {
					$response = array('result' => 0, 'msg' => 'Failed to '.$action);
				}
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}

		if( !empty($response)){
			$data = $response;
		} else {
			$response = array();
		}
		$js_files = array('modules/merchandise_form.js');

		$merchandise = $this->merchandises_model->get_merchandise($id, $this->branch_id);
		$variants = $this->merchandises_model->get_variants($id, $this->branch_id);
		
		$this->template('merchandises/form', array('response' => $response, 'merchandise' => $merchandise , 'variants' => $variants ,'js' => $js_files));
	}

	public function add_inventory($id){
		$merchandise = $this->merchandises_model->get_merchandise($id, $this->branch_id);
		$variants = $this->merchandises_model->get_variants($id, $this->branch_id);
		$post = $this->input->post();
		
		if( !empty($post) ){
			$variants = $this->input->post('variant');
			$session = $this->session->userdata('userdata');
			
			foreach( $variants as $variant){
				$variant['merchandise_id'] = $variant['merchandise_id'];
				//get variant details 
				$_variant = $this->merchandises_model->get_variant($variant['id'], $this->branch_id);

				//get current stock
				$stock = $this->merchandises_model->get_current_stock($variant['id']);
				$variant['stock'] = $variant['stock'] + $stock;
				$variant['branch_id'] = $this->branch_id;
				$this->merchandises_model->add_variant($variant);
				//add inventory history
				$inventory_arr = array(
					'merchandise_id'=> $variant['merchandise_id'],
					'variant_id'	=> $variant['id'],
					'stock' 		=> $variant['stock'] - $stock,
					'date_added' 	=> date('Y-m-d',strtotime($post['date_added'])),
					'userid'		=> $session['userid'],
					'branch_id' 	=> $this->branch_id
				);
				$this->add_inventory_log($inventory_arr);
				
				//update summary log : stock
				$item = array(
					'qty' 			=> $variant['stock'],
					'variant_id' 	=> $variant['id'],
					'cost' 			=> $_variant->price
				);
				$this->update_stocksummarylog($variant['stock'],$item, date('Y-m-d',strtotime($post['date_added'])));

			}
			redirect('merchandises');
		}

		$response = array();
		$js_files = array('modules/merchandise_inventoryform.js');
		$this->template('merchandises/form_inventory', array('id' => $id,'response' => $response, 'merchandise' => $merchandise , 'variants' => $variants ,'js' => $js_files));
	}

	public function add_inventory_log($arr){
		$this->db->insert('merchandise_inventory', $arr);
	}

	public function form_variant(){
		$post = $this->input->get();
		$data['variants'] = "";
		if( !empty($post['id']) ){

		} 
		$data['has_variant'] = $post['has_variant'];
		echo $this->load->view('merchandises/form_variants',$data, true);
	}
	
	public function delete($id){

	}
	

	public function sales(){
		$result = array();
		$merchandises = $this->merchandises_model->get_sales(date('Y-m-d'),$this->branch_id);
		$js_files = array('modules/merchandise_sales.js','modules/delete.js');
		$this->template('merchandises/sales', array('sales' => $merchandises, 'date' => date('Y-m-d') ,'controller' => $this->controller,'js' => $js_files));
	}
	
	public function sales_add(){
		$merchandises = $this->merchandises_model->get_active_items( array('where' => array('merchandise_variants.branch_id' => $this->branch_id)));
		$sales = $this->merchandises_model->get_sales(date('Y-m-d'),$this->branch_id);
		$post = $this->input->post();
		
		if( $post )
		{
			$date = date('Y-m-d');
			$group = strtotime(date('Y-m-d h:s:i'));
			
			foreach($post['item'] as $item )
			{

				$variant = $this->merchandises_model->get_variant($item['variant_id'], $this->branch_id);
				$remaining_stock = $variant->stock - $item['qty'];
				if( $remaining_stock >= 0 )
				{
					//insert sales log
					$_item = array(
						'unix' 		 => $group,
						'variant_id' => $item['variant_id'],
						'qty_sell'	 => $item['qty'], 
						'cost'		 => $item['cost'],
						'date'       => $date,
						'branch_id'	 => $this->branch_id
					);
					$return = $this->merchandises_model->add_saleslog($_item);
					if( $return )
					{
						//update the quantity of the item
						$this->merchandises_model->update_variant(array('id' => $variant->id, 'stock' => $remaining_stock));
						//insert summary log : stock
						$item['cost'] = $variant->price;
						$this->update_stocksummarylog( $remaining_stock, $item, $date );
						
					}
					
				}
			}
			redirect(base_url($this->controller.'/sales'));
		}
		$js_files = array('modules/merchandise_sales_form.js');
		$this->template('merchandises/sales_form', array('items' => $merchandises, 'js'=> $js_files,'controller'=> $this->controller));
	}

	public function edit_sales($unix){
		
		$post = $this->input->post();
		if( $post ){
			$items = $post['item'];
			
			foreach($items as $item){
				if( $item['qty'] != $item['old_qty'] ){
					//$this->debug($item);
					if( $item['qty'] < $item['old_qty'] )
					{
						//current qty is lower than the old qty	
					} 
					else 
					{
						//current qty is higher than the old qty

					}
					$updated_qty = $item['stockqty'] - $item['qty'];

					//update merchandise variant stock
					$variant = array(
						'id' => $item['variant_id'],
						'stock' => $updated_qty
					);
					$this->merchandises_model->edit_variant($variant);

					//update the dailysales record
					$sales = array(
						'id' => $item['daily_id'],
						'qty_sell' => $item['qty'],
					);
					$this->merchandises_model->update_sales($sales);
					
					//update summary log : stock
					$_item = array(
						'variant_id' => $item['variant_id']
					);
					$this->update_stocksummarylog( $updated_qty, $_item, $item['date'] );
				}
			}
			redirect(base_url($this->controller.'/sales'));
		}

		//get item by unix
		$data['sales'] = $this->merchandises_model->get_salesby_unix($unix);
		$data['js'] = array('modules/merchandise_sales_form.js');

		$data['items'] = $this->merchandises_model->get_active_items( array('where' => array('merchandise_variants.branch_id' => $this->branch_id)));
		$data['unix'] = $unix;
		$this->template('merchandises/sales_form', $data);

	}

	public function update_stocksummarylog( $remaining_stock, $item, $date){

		$summary_item = $this->merchandises_model->get_summarylog_item($item['variant_id'],$date);
		if( $summary_item )
		{
			$summary_item = (array) $summary_item;
			$summary_item['remaining'] = $remaining_stock;
			$this->merchandises_model->update_summary($summary_item);
		} else 
		{
			$summary_item = array(
				'variant_id' => $item['variant_id'],
				'cost'		 => $item['cost'],
				'date'       => $date, 
				'remaining'	 => $remaining_stock,
				'branch_id'	 => $this->branch_id
			);
			$this->merchandises_model->update_summary($summary_item);
		}
	}

	public function daily_sales(){
		$post = $this->input->post();
		$date = date('Y-m-d',strtotime($post['date']));
		$data['sales'] = $this->merchandises_model->get_sales($date,$this->branch_id);
		$js_files = array('modules/merchandise_sales.js','modules/delete.js');
		$data['date'] = $date;
		echo $this->load->view('merchandises/sales_list', $data, true, true);

	}

	public function sales_summary(){
		$result = array();
		//$date = date('2019-12-03');
		$date = date('Y-m-d');
		$summary = $this->merchandises_model->summary_bydate($date, $this->branch_id);
		//echo "<pre>";print_r($summary); die;
		$js_files = array('modules/merchandise_summary.js');
		$this->template('merchandises/sales_summary', array('summary' => $summary,'controller' => $this->controller,'js' => $js_files));
	}

	public function summary(){
		$post = $this->input->post();
		$date = date('Y-m-d',strtotime($post['date']));
		$summary = $this->merchandises_model->summary_bydate($date, $this->branch_id);
		echo $this->load->view('merchandises/summary_list', array('summary' => $summary,'controller'),true,true);
	}
}