<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Memberships extends MY_Controller {
	public $table = "members";
	public function __construct(){
		parent::__construct();
		$this->load->model(array('members_model','classtype_model','membershiptype_model','memberships_model','promos_model','sales_model','durations_model'));
	}
	public function index(){

		$result = $this->fetch();
		$js_files = array('modules/meterform.js','modules/meterreading.js');

		$this->template('members/index', array('members' => $result,'js' => $js_files));
	}

	public function search_merbershiplog(){
		$result = $this->memberships_model->get_validmembership($_GET['id']);

		if( empty($result) ){
			$return = $this->memberships_model->get_expiredmembership($_GET['id']);
			
			if( empty($return) ){
				echo "<div class='alert alert-danger'>No Existing Membership</div>";
				echo "<a href='".base_url('memberships/add'.$_GET['id'])."' class='btn btn-primary'>Add Membership</a><br/>";
			} else{
				echo "<div class='alert alert-danger'>";
				echo "<table class='table table-border'>";
					echo "<thead><tr><th>Class Type</th><th>Started</th><th>Ended</th></tr></thead>";
					echo "<tr><td><span class='reset_start hide btn btn-default' data-id='".$return->id."'>reset</span>".$return->class_title." (".$return->title.")"."</td><td>".$return->date_start."</td><td>".$return->date_end."</td></tr>";
				echo "</table>";
				echo "</div>";
				echo "<a href='".base_url('memberships/add/'.$_GET['id'])."' class='btn btn-primary'>Renew Membership</a><br/>";
			}
		} else {

			echo "<div class=''>";
			echo "<table class='table table-border'>";
				echo "<thead><tr><th>Class Type</th><th>Started</th><th>Ended</th></tr></thead>";
				foreach($result as $res){
					echo "<tr data-id='".$res->id."'><td><span class='reset_start hide btn btn-default' data-id='".$res->id."'>reset</span>".$res->class_title." (".$res->title.")"."</td><td>".$res->date_start."</td><td>".$res->date_end."</td></tr>";
				}
			echo "</table>";
			echo "</div>";

		}
	}

	public function reset_start(){
		$this->memberships_model->edit(array('id' => $_POST['id'],'date_start' => null, 'date_end' => null));
	}



	public function paginate($keywords = 0, $sort_by = 'members.id', $sort_order = 'DESC', $limit = 20, $offset = 0){

		$result['members'] = $this->fetch($keywords, $sort_by, $sort_order, $limit, $offset);
		echo $this->load->view('members/list',$result, true);
	}

	public function fetch($keywords = 0, $sort_by = 'members.id', $sort_order = 'DESC', $limit = 20, $offset = 0){
		
		$params = array(
				'limit'  => $limit,
				'sort_by' => $sort_by, 
				'sort_order' => $sort_order, 
				'offset' => $offset, 
			);

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}

		$result = $this->members_model->get_members($params);
		return $result;
	}
	public function edit($membership_id, $id){
		$member = $this->members_model->get_member($id);
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		$sales = $this->db->select('*')->where('membership_log_id', $membership_id)->get('sales')->row_array();

		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}

			//get sales value 
			$prev_sales = $this->sales_model->get_bymembership_log($id);
			$prev_amount = $prev_sales->amount;
			die($prev_amount); die;
		
			$valid_monthly = 0;
			$valid_session = 0;

			$rules = array(
		           	'class_type_id' => array(
		                     'field' => 'class_type_id',
		                     'label' => 'Class Type',
		                     'rules' => 'trim|required'
		                     ),
		           	'membership_type_id' => array(
		                     'field' => 'membership_type_id',
		                     'label' => 'Membership type',
		                     'rules' => 'trim|required'
		                     ),
		           	'class_type_id' => array(
		                     'field' => 'class_type_id',
		                     'label' => 'Class Type',
		                     'rules' => 'trim|required'
		                     ),

		           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {

				$firstsession = false;
				$post['date_register'] = date('Y-m-d', strtotime($_POST['date_register']));
				$post['date_start'] = date('Y-m-d', strtotime($_POST['date_start']));
				if( $post['newmember'] == 1){
					$post['renew'] = 0;	
				}
				unset($post['newmember']);
				if(empty($_POST['paid']))
					$post['paid'] = 0;


				if( empty($_POST['id']) ){
					$result = $this->memberships_model->add($post);
					$membership_id = $this->db->insert_id();
				} else {
					$result = $this->memberships_model->edit($post);
					$membership_id = $_POST['id'];
				}

				//set the end month expiry
				if( $firstsession == true){
					$this->setfirst_session($post['date_start'], $post['member_id'], $valid_monthly);
				}

				//check if there is max session
				if( $valid_session > 0 && !empty( $post['date_start'] ) ){
					$this->create_sessioncounter(array('membership_log_id' => $membership_id, 'member_id' => $post['member_id'], 'membership_type_id' => $post['membership_type_id'], 'total_allowed' => $valid_session));
				}
			
				
				$memberdata = array('membership_type_id' => $post['membership_type_id'], 'class_type_id' => $post['class_type_id'],'id' => $post['member_id']);

				$this->members_model->edit($memberdata);

	
				if ( $result ){
					$response = array('result' => 1, 'msg' => 'Successfully '.$action.'d');
					// input sales 
					if( empty($_POST['id'])){
						$sales = array(
								'member_id' => $post['member_id'],
								'membership_type_id' => $post['membership_type_id'],
								'class_type_id' 	 => $post['class_type_id'],
								'date'				=> $post['date_register'],
								'amount'			=> $post['amount'],
								'membership_log_id' => $membership_id,
							);
						if( !empty($post['promo_id']) ){
							$sales['promo_id'] = $post['promo_id'];
						}
						$this->sales_model->add($sales);
						redirect(base_url('members'));
					} else {
						//edit sales
						$sales = $this->db->select('*')->where('membership_log_id', $membership_id)->get('sales')->row_array();
						$sales['amount'] = $post['amount'];
						$sales['membership_type_id'] = $post['membership_type_id'];
						$sales['class_type_id'] = $post['class_type_id'];
						$this->sales_model->edit($sales);

						redirect(base_url('members'));
					}

				} else {
					$response = array('result' => 0, 'msg' => 'Failed to '.$action);
				}
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}
	
		if( !empty($response)){
			$data = $response;
		} else {
			$response = array();
		}

		if( !empty($membership_id) ){
			$membership = $this->memberships_model->get_membership($membership_id);
		} else {
			$membership = array();
		}

		$class_types = $this->classtype_model->get_types();
		$membershiptypes = $this->membershiptype_model->get_membertypes();
		$promos = $this->promos_model->get_promos();
		$js_files = array('modules/membership_form.js');
		$this->template('memberships/form', array('member' => $member,'membership' => $membership,'response' => $response,'class_types' => $class_types,'membership_types' => $membershiptypes,'promos' => $promos,'js' => $js_files ));
	}
	public function add($id, $membership_id = 0){

		if( empty($id) ){
			redirect('/members');
		}

		$member = $this->members_model->get_member($id);
		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		
		if( !empty($post) ){
			
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';

				$membership = $this->memberships_model->get_membership($post['id']);
				//get sales value 
				$prev_sales = $this->sales_model->get_bymembership_log($membership->id);
				$prev_amount = $prev_sales->amount;
				
				$update_coh = false;
				if( $prev_amount != $post['amount']){
					$update_coh = true;
				}
			}
			
			$valid_session = 0;
			$valid_monthly = 0;

			$rules = array(
		           	'class_type_id' => array(
		                     'field' => 'class_type_id',
		                     'label' => 'Class Type',
		                     'rules' => 'trim|required'
		                     ),
		           	'membership_type_id' => array(
		                     'field' => 'membership_type_id',
		                     'label' => 'Membership type',
		                     'rules' => 'trim|required'
		                     ),
		           	'class_type_id' => array(
		                     'field' => 'class_type_id',
		                     'label' => 'Class Type',
		                     'rules' => 'trim|required'
		                     ),

		           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {

				$firstsession = false;
				$post['date_register'] = date('Y-m-d', strtotime($_POST['date_register']));
				$post['date_start'] = date('Y-m-d', strtotime($_POST['date_start']));
				if( empty($post['promo_id']) ){
					$post['promo_id'] = 0;
				}
				// get membership type details 
				$membership_type_selected = $this->membershiptype_model->get_by_id($this->input->post('membership_type_id'));

				//get per session details
				if( !empty($membership_type_selected->session_duration_id) ){
					$session_details = $this->durations_model->get_by_id($membership_type_selected->session_duration_id);
					if( !empty( $session_details ) && $session_details->duration_type == 'session' ){
						//create a session
						$valid_session = $session_details->durations * 1;
						
					}

				}
				//get max months validity 
				if( !empty( $membership_type_selected->duration_id ) ){
					$monthly_details = $this->durations_model->get_by_id($membership_type_selected->duration_id);
					if( !empty( $monthly_details ) && $monthly_details->duration_type == 'monthly' ){
						$valid_monthly = $monthly_details->durations;
						
					}
				}
				if( !empty($post['or']) ){
					$or = $post['or'];
				}
				unset($post['or']);
				
				if( $post['newmember'] == 1){
					$post['renew'] = 0;	
				}
				unset($post['newmember']);
				if(empty($_POST['paid']))
					$post['paid'] = 0;

				if( !empty($post['password']) ){
					$this->members_model->edit(array( 'password' => $post['password'], 'id' => $post['member_id']));
				}
				unset($post['password']); 


				if( empty($_POST['id']) ){
					$result = $this->memberships_model->add($post);
					$membership_id = $this->db->insert_id();
				} else {
					$result = $this->memberships_model->edit($post);
					$membership_id = $_POST['id'];
				}

				//set the end month expiry
				if( $valid_monthly > 0 ){
					$this->setfirst_session($post['date_start'], $post['member_id'], $valid_monthly);
				}

				//check if there is max session
				if( $valid_session > 0 && !empty( $post['date_start'] ) ){
					//2019.12.07 commented out as registered date should not auto charge 1 session
					//$this->create_sessioncounter(array('membership_log_id' => $membership_id, 'member_id' => $post['member_id'], 'membership_type_id' => $post['membership_type_id'], 'total_allowed' => $valid_session, 'session' => 0));
				}
				
				$memberdata = array('membership_type_id' => $post['membership_type_id'], 'class_type_id' => $post['class_type_id'],'id' => $post['member_id']);

				$this->members_model->edit($memberdata);

	
				if ( $result ){
					$response = array('result' => 1, 'msg' => 'Successfully '.$action.'d');
					// input sales 
					if( empty($_POST['id'])){
						$sales = array(
								'member_id' => $post['member_id'],
								'membership_type_id' => $post['membership_type_id'],
								'class_type_id' 	 => $post['class_type_id'],
								'date'				=> $post['date_register'],
								'amount'			=> $post['amount'],
								'membership_log_id' => $membership_id,
								'sales_type_id'		=> $this->sales_model->get_idby_code("classfee")
							);
						if( !empty($post['promo_id']) ){
							$sales['promo_id'] = $post['promo_id'];
						}
						$sales_id = $this->sales_model->add($sales);

						//insert coh
						$cashflow = array('date' =>$post['date_register'],'fieldselected' => 'coh','amount' => $post['amount']);				 
				 		$this->cashflow_audtrail($cashflow);
						
					} else {
						//edit sales
						$sales = $this->db->select('*')->where('membership_log_id', $membership_id)->get('sales')->row_array();
						$sales_id = $sales['id'];
						$sales['amount'] = $post['amount'];
						$sales['membership_type_id'] = $post['membership_type_id'];
						$sales['class_type_id'] = $post['class_type_id'];
						$this->sales_model->edit($sales);

						//update coh
						if( $update_coh  == true ){
						 	//undo coh value
							$prev_param = array('date' => date('Y-m-d',strtotime($post['date_register'])), 'field' => 'coh','amount' => $prev_amount);
							$this->recussiveUndoCoh($prev_param);
							
							//update coh
							$cashflow = array('date' =>date('Y-m-d',strtotime($post['date_register'])),'fieldselected' => 'coh','amount' => $post['amount']);				 
					 		$this->cashflow_audtrail($cashflow);
						}

					}
					if( $or == "1"){
						$or_arr  = array('date' => $post['date_register'], 'sales_id' => $sales_id, 'member_id' => $post['member_id'], 'total' => $post['amount']);
						$this->insert_or($or_arr);
					}
					redirect(base_url('members'));

				} else {
					$response = array('result' => 0, 'msg' => 'Failed to '.$action);
				}
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}
	
		if( !empty($response)){
			$data = $response;
		} else {
			$response = array();
		}

		if( !empty($membership_id) ){
			$membership = $this->memberships_model->get_membership($membership_id);
		} else {
			$membership = array();
		}

		$class_types = $this->classtype_model->get_types();
		$membershiptypes = $this->membershiptype_model->get_membertypes();
		$promos = $this->promos_model->get_promos();
		$js_files = array('modules/membership_form.js');
		$this->template('memberships/form', array('branch_id' => $this->branch_id,'member' => $member,'membership' => $membership,'response' => $response,'class_types' => $class_types,'membership_types' => $membershiptypes,'promos' => $promos,'js' => $js_files ));
		
	}

	public function insert_or($sales_arr  = array()){
		$or  = $this->db->select('*')->where(array('sales_id' => $sales_arr['sales_id']))->get('or_issue')->row_array();
		if (empty($or) ){
			$this->db->insert('or_issue',$sales_arr);
		} else {
			$this->db->where('id', $or['id']); 
        	$this->db->update('or_issue', $sales_arr);
		}
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	public function setfirst_session($log_date, $member_id, $max_month){

			$log_date = date('Y-m-d',strtotime($log_date));
			$result = $this->memberships_model->get_membershiplog($member_id);
			
			$date_start = $result->date_start;
			
			if( $result->date_start > $log_date || $date_start == "0000-00-00"){
				
				$updatedate = array('date_start' => $log_date, 'id' => $result->id); 
				$date_start = $log_date;

			} 

			if( $max_month > 0 ){

				if( $max_month == 1.5 ){
					$updatedate['date_end'] =  date('Y-m-d', strtotime($date_start. ' + 45 days'));
				} else {
					$max_month = (string) $max_month * 1;

					$add_month = "+".$max_month." month";
					$started_date =strtotime($date_start);	
					$updatedate['date_end'] = date('Y-m-d', strtotime($add_month, $started_date));
				}
				
				$this->db->where('id', $result->id); 
	        	$this->db->update('membership_log', $updatedate);

		        if( $this->db->affected_rows() ){
		            return $result->id;
		        } else {
		            return false;
		        }
			}

			return false;     
	}

	public function getpromo_price(){
		$post = $this->input->post();
		$amount = $this->membershiptype_model->get_class_price($post['membership_type_id']);
		if( !empty($post['promo_id']) ){
			$promo = $this->promos_model->get_promo($post['promo_id'],$post['class_type_id'],$post['membership_type_id']);
			if( !empty($promo ) ){
				switch($promo->promo_type_id ){
					case 1:
						echo $amount - ($amount * ($promo->discount_rate / 100));
						break;
					case 2:
						echo $promo->price_rate;
						break;
				}
			} else {
				echo $amount;
			}
			return false;
			
		}
		
		echo $amount;
	}

	public function create_sessioncounter($session_arr){
		
		$membership_type = $this->membershiptype_model->get_by_id($session_arr['membership_type_id']);
		
		//if session_duration_id is empty
		if( empty($membership_type->session_duration_id) ){
			return false;
		}

		$createss = array(
				'member_id' => $session_arr['member_id'],
				'membership_log_id' => $session_arr['membership_log_id'],
				'class_type_id' => $membership_type->class_type_id,
				'total_allowed' => $session_arr['total_allowed'],
		  );
		if( empty($session_arr['session']) ){
			$createss['total_session'] = 0;
		}

		$this->db->insert('session_counter', $createss);	
		return false;
	}
	public function update_membership(){
		$data = $this->db->select('membership_log.*,sales.date')->join('membership_log','membership_log.id = sales.membership_log_id','left')->where('membership_log.date_start','2018-12-01')->get('sales')->result();
		;
		foreach ($data as $membership) {
			$_data = array('id' => $membership->id,'date_register' => $membership->date);
			$this->memberships_model->edit($_data);
			print_r($_data);
		}
	}

	//ajax class_type field 
	public function filter_class_type(){
		$post = $this->input->post();

		if( !empty($post['member_id'])){
			//get memebership_log_id 
			$result = $this->memberships_model->get_log( array( 'membership_log.member_id' => $post['member_id'], 'membership_log.date_end >=' => date('Y-m-d') ));
			if( !empty($result) ){
				$class_details = $class_types = $this->classtype_model->get_by_id($result->class_type_id);
				
				if( !empty($class_details)){
					echo "<option value='".$class_details->id."'>".$class_details->class_title."</option>";
				} else {
					echo "<option value=''>No membership valid for this member</option>";
				}
			} else {
				//prompt register first a membership
				echo "<option value=''>No membership valid for this member</option>";
			}
		}
	}
	
	public function renew_membership($member_id){

	}

}