<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cashflows extends MY_Controller {
	public $table = "bank";
	public $table_transac = "bank_transactions";
	public function __construct(){
		parent::__construct();
		$this->load->model(array('banks_model','bank_transactions_model'));
	}
	public function index(){
		$date = date('Y-m-d');
		$data['banktransacs'] = $this->bank_transactions_model->gets(
				array(
					'where'=> array(
							array('date' => $date)
					)
				) 
			);
		$data['js'] = array('modules/banks.js');
		$this->template('banktransacs/index', $data );
	}

	public function add(){

		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		$response = array();
		if( !empty($post) ){
			if( empty($post['id']) ){
				
				$action = 'save';
			} else {
				$action = 'edit';
			}
		

			$rules = array(
           	'bankid' => array(
                     'field' => 'bankid',
                     'label' => 'Bank Type',
                     'rules' => 'trim|required'
                     ),
           	'transaction_type' => array(
                     'field' => 'transaction_type',
                     'label' => 'Transaction Type',
                     'rules' => 'trim|required'
                     ),
           	'amount' => array(
                     'field' => 'amount',
                     'label' => 'Amount',
                     'rules' => 'trim|required'
                     ),

           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				
				if( empty($post['date'])){
					$post['date'] = date('Y-m-d');
				} else {
					$post['date'] = date('Y-m-d', strtotime($post['date']));
				}
				
				 $result =  $this->bank_transactions_model->add($post);
				 if( empty($post['id']) ){
				 	// adjust the current bank amount
				 	$bank = $this->banks_model->get($post['bankid']);
				 	
				 	switch( $post['transaction_type'] ) {
				 		case "deposit":
				 			$bank->amount = $bank->amount + $post['amount'];

				 			break;
				 		case "withdraw":
				 			$bank->amount = $bank->amount - $post['amount'];
				 			break;
				 		case "transfer":
				 			break;
				 	}
				 	$_bank = array('id' => $post['bankid'], 'amount' => $bank->amount);
				 	
				 	
				 	$this->banks_model->edit($_bank);
				 } else {

				 }

				 if( $result) {
				 	redirect(base_url('banktransacs'));
				 } else {
				 	die('error');
				 }
				
				
			} else {
				$response = array('result' => 0, 'msg' => validation_errors());
			}	
		}

		if( !empty($response)){
			$data = $response;
		}
		$banktypes = $this->banks_model->gets();
		
		$js_files = array('modules/attendance_form.js');
		$this->template('banktransacs/form', array('response' => $response,'banktypes' => $banktypes ,'js' => $js_files));
		
	}

	public function getCOH($date){
		echo number_format($this->get_current_coh($date),2);
	}
	public function edit($id){
		
		$this->load->library('form_validation');
		
		$post = $this->input->post();
		$response = "";
		if( !empty($post) ){
			$action = 'edit';
		
		$rules = array(
           	'sales_type_id' => array(
                     'field' => 'sales_type_id',
                     'label' => 'Sales Type',
                     'rules' => 'trim|required'
                     ),
           	'amount' => array(
                     'field' => 'amount',
                     'label' => 'Amount',
                     'rules' => 'trim|required'
                     ),

           ); 
			$this->form_validation->set_rules($rules);
			if ( $this->form_validation->run() ) {
				
				if( empty($post['date'])){
					$post['date'] = date('Y-m-d');
				} else {
					$post['date'] = date('Y-m-d', strtotime($post['date']));
				}
				
				 $result =  $this->sales_model->edit($post);
				 if( $result) {
				 	redirect(base_url('sales'));
				 } else {
				 	die('error');
				 }
			}
		}
		$sales_types = $this->sales_model->get_types();
		$sales = $this->sales_model->get($id);
		$this->template('sales/form', array('response' => $response,'sales' => $sales,'types' => $sales_types));
	}

	public function check_first_attend($log_date, $member_id){
			$log_date = date('Y-m-d',strtotime($log_date));
			$result = $this->memberships_model->get_membershiplog($member_id);
			
			$date_start = $result->date_start;
			
			if( $result->date_start > $log_date || $date_start == "0000-00-00"){
				
				$updatedate = array('date_start' => $log_date, 'id' => $result->id); 
				$date_start = $log_date;

			}  else {
				return false;
			}
			
			if( $result->date_end == "0000-00-00"){
				$started_date =strtotime($date_start);
				switch($result->membership_type_id){
					case 1:
						$updatedate['date_end'] = date("Y-m-d", strtotime("+1 month", $started_date));
						break;
					case 2:
						$updatedate['date_end'] = date("Y-m-d", strtotime("+3 month", $started_date));
						break;
					case 3:
						$updatedate['date_end'] = date("Y-m-d", strtotime("+6 month", $started_date));
						break;
				}
			}
			
			$this->memberships_model->edit($updatedate);
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	public function get_option_meters(){

		$data['meters'] = $this->fetch();
		$data['selected'] = $this->input->get('id');
		print_r($data['selected']);
		echo $this->load->view("meter_reading/options", $data, true);
	}

}