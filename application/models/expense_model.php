<?php

class Expense_model extends MY_Model {
	
	var $table = "expenses";
    var $table_type = "expense_type";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    function get_dailyexpensedata($date){
        $date = date('Y-m-d', strtotime($date));
        $result =  $this->db->select($this->table.'.*,'.$this->table_type.'.expense_type')
                 ->join($this->table_type,$this->table_type.'.id = expenses.expense_type_id','left')
                 ->where('expenses.date_paid',$date.' 00:00:00')
                 ->get($this->table)->result();
                // echo $this->db->last_query();
        return $result;
    }
    
    function get_expense($id){
        $params['where']['id'] = $id;
        return $this->get($params);
    }
    function get_all($params = array()){
    	$this->db->select("*");
    	if ( !empty($params['where']) ){
            $this->db->where($params);
        }
         if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);

    	$result = $this->db->get($this->table)->result();

    	return $result;
    }
     function get($params){
    	$this->db->select("*");
 
        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        
       	if(!empty($params['where'])){
       		foreach ($params['where'] as $key => $value) {
       			$this->db->where($key, $value);
       		}
       	}
    	$result = $this->db->get($this->table)->row();

    	return $result;
    }

    
    function search($post){
        
    }

  
    function add($post){
        unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }
   
    function update($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }

    function get_dailyexpenses($date){
        $date = date('Y-m-d', strtotime($date));
        $result =  $this->db->select('SUM(cost) as total_expense')
                 ->where('date_paid',$date)
                 ->get($this->table)->row();

        if(!empty($result->total_expense)){
        return $result->total_expense;
        } else{
            return 0;
        }
    }

    function get_monthlyexpneses($date){
        $first_day_this_month = date('Y-m-01', strtotime($date)); // hard-coded '01' for first day
        $last_day_this_month  = date('Y-m-t', strtotime($date));

        $result = $this->db->select('SUM(cost) as total_expense')
                            ->where('date_paid >=',$first_day_this_month)
                            ->where('date_paid <=', $last_day_this_month)
                            ->get($this->table)->row();
        if(!empty($result->total_expense)){
                return $result->total_expense;
        } else{
            return 0;
        }
    }
    public function get_byrange($start, $end){
         $start = date('Y-m-d', strtotime($start));
         $end = date('Y-m-d', strtotime($end));
        $result =  $this->db->select($this->table.'.*,'.$this->table_type.'.expense_type')
                 ->join($this->table_type,$this->table_type.'.id = expenses.expense_type_id','left')
                 ->where($this->table.'.date_paid >=',$start)->where($this->table.'.date_paid <=',$end)
                 ->order_by($this->table.'.date_paid','ASC')
                 ->get($this->table)->result();
        return $result;
    }

    public function gettotal_byrange($start, $end){
         $start = date('Y-m-d', strtotime($start));
         $end = date('Y-m-d', strtotime($end));
        $result = $this->db->select('SUM(cost) as expense, date_paid')
                            ->where('date_paid >=',$start)
                            ->where('date_paid <=', $end)
                            ->group_by("date_paid")
                            ->order_by('date_paid','ASC')
                 ->get($this->table)->result();
        return $result;
    }
}