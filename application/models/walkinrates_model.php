<?php

class Walkinrates_model extends MY_Model {
	
	var $table = "walkin_rates";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_rate($class_type_id){
        $rate = $this->db->where('class_type_id', $class_type_id)->get($this->table)->row();
        if( $rate ){
            return $rate->rate;
        } else {
            return 0;
        }
    }

    function get_walkinrate_by_class($class_type_id){
        return $rate = $this->db->where('class_type_id', $class_type_id)->get($this->table)->row();
    }


    function get($params){
        $this->db->select($this->table.'.*, class_type.class_title, class_type.id as class_type_id');
        $this->db->join($this->table,'class_type.id = '.$this->table.'.class_type_id','left');
        $this->db->order_by('class_type.class_title','ASC');
        return $this->db->get('class_type')->result();
    }

    function add($post){
        unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }

    function edit($post){

        $id = $post['class_type_id'];
        unset($post['class_type_id']);
        $this->db->where('class_type_id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }


}