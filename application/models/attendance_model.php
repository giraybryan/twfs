<?php

class Attendance_model extends MY_Model {
	
    var $table = "daily_attendance";
	var $table_walkin = "walkin_log";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function add($post){
        unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }

    function addwalkin($post){
        $this->db->insert($this->table_walkin, $post);
        return $this->db->insert_id();
    }

    function edit($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }

    function get($params){
        return $this->db->where($params)->get($this->table)->row();
    }

    function get_by_date($post){
        return $this->db->select('COUNT(id) as total_attendees')
                        ->where('date >=', $post['date'].' 00:00:00')
                        ->where('date <=', $post['date'].' 23:59:59')
                        ->where('branch_id', $this->branch_id)
                        ->get($this->table)
                        ->row();
    }

    function get_members_list($date = "")
    {
        if( empty($date) ){
            $date = date('Y-m-d');
        } else {
            $date = date('Y-m-d', strtotime($date));
        }
        $this->db->select('daily_attendance.*, members.firstname, members.lastname');
        $this->db->join('members','members.id = daily_attendance.member_id','left');
        $this->db->where('date >=', $date.' 00:00:00');
        $this->db->where('date <=', $date.' 23:59:59');
        $this->db->order_by('date','DESC');
        return $this->db->get('daily_attendance')->result();
    }

}