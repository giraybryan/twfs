<?php

class Durations_model extends MY_Model {
	
	var $table = "class_duration";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_durations(){
        $return = $this->db->get($this->table)->result();
        return $return;
    }    

    function get_by_id($id){
        if( empty($id) ){
            return false;
        }
        $return = $this->db->where('id',$id)->get($this->table)->row();
        return $return;
    }

    function add($post){
        unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }
    function add_log($post){
        unset($post['id']);
        $this->db->insert('membership_log', $post);
        return $this->db->insert_id();
    }

    function edit($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }


}