<?php

class Expense_type_model extends MY_Model {
	
	var $table = "expense_type";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all($params = array()){
    	$this->db->select("*");
    	if ( !empty($params['where']) ){
            $this->db->where($params);
        }
         if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);

    	$result = $this->db->get($this->table)->result();

    	return $result;
    }
     function get($params){
    	$this->db->select("*");
 
        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        
       	if(!empty($params['where'])){
       		foreach ($params['where'] as $key => $value) {
       			$this->db->where($key, $value);
       		}
       	}
    	$result = $this->db->get($this->table)->row();

    	return $result;
    }

    function add($post){
        unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }
   
    function update($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }


}