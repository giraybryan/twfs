<?php

class Class_durations_model extends MY_Model {
	
	var $table = "class_duration";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_activated_classes($params = array()){
        $this->db->select($this->table.'.*, class_type_categories.category_title as class_type');
        if ( !empty($params['where']) ){
            $this->db->where($params);
        }
        $this->db->join('class_type_categories', 'class_type_categories.id = class_type.class_category_id', 'left');
        $this->db->where($this->table.'.on_off', 1);
        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
        $result = $this->db->get($this->table)->result();
        return $result;
    }
    function get(){
       
        $this->db->select('*');
        $this->db->where('active', 1);
        $result = $this->db->get($this->table)->result();
        return $result;
    }

    function get_by_id($id){
        if( empty($id) ){
            return false;
        }

        $this->db->select('*');
        if ( !empty($id) ){
            $this->db->where('id',$id);
        }
        $this->db->where('active', 1);
    	$result = $this->db->get($this->table)->row();
    	return $result;
    }
    
    function inactive($id){
        $_arr = array('active' => 0);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $_arr);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function get_class_enrol($class_id){
        $this->db->select('COUNT(id) as total_member');
        $this->db->where('class_type_id',$class_id);
        $first_day_this_month = date('Y-m-01',strtotime(date('Y-m-d'))); // hard-coded '01' for first day
        $last_day_this_month  = date('Y-m-t',strtotime(date('Y-m-d')));
        $this->db->where('date_register >=',$first_day_this_month);
        $this->db->where('date_register <=',$last_day_this_month);
        
        $return = $this->db->get('membership_log')->row();

        return $return->total_member;
    }


    function add($post){
        unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }

    function edit($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }

    public function get_by_type($type = "monthly")
    {  
        $this->db->select('*');
        $this->db->where('active', 1);
        $this->db->where('duration_type', $type);
        $result = $this->db->get($this->table)->result();
        return $result;
    }

}