<?php

class Card_model extends CI_Model {
	
    var $table = "member_cardid";
    var $cardhistory = "cardread_history";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function get_bycardID($cardno){
    	return $this->db->where('cardno', $cardno)->get('member_cardid')->row();
    }

    public function add($cardhistory){
    	$this->db->insert($this->cardhistory,$cardhistory);
    	return $this->db->insert_id();
    }

    public function add_card($post){
        $this->db->insert($this->table,$post);
        //echo $this->db->last_query();
        return $this->db->insert_id();
    }

    public function latest_registered(){
        
        $result = $this->db->select('*')
                        ->where('process','register')
                        ->where('branch_id', $this->branch_id)
                        ->order_by('id',"DESC")
                        ->get($this->cardhistory)->row();   
        if( $result ){
            return  $result;
        } else {
            return false;
        }
    }

    public function latest_attendance(){
        
        $result = $this->db->select('*')
                        ->where('process','attendance')
                        ->where('branch_id', $this->branch_id)
                        ->order_by('id',"DESC")
                        ->get($this->cardhistory)->row();   
        if( $result ){
            return  $result;
        } else {
            return false;
        }
    }

}