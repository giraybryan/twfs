<?php

class Cashflows_model extends MY_Model {
	
    var $table = "cashflow";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
    function add($post){

        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }
    function gets(){
        $this->db->where('id', $id)->get($this->table)->result();
    }
    
    function get($id){
        return $this->db->where('id', $id)->get($this->table)->row();
    }
    function get_by_date($date){
        return $this->db->where('date', $date)->get($this->table)->row();
    }

     function get_cashflow_abovedate($date){
        return $this->db->where('date >', $date)->order_by('date','ASC')->get($this->table)->row();
    }
    
    function get_latestcash(){
        return $this->db->order_by('date', 'DESC')->get($this->table)->row();
    }

    function edit($post){

        $id = $post['cashid'];
        unset($post['id']);
        $this->db->where('cashid', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }

    
}