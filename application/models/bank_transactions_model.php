<?php

class Bank_transactions_model extends MY_Model {
	
    var $table = "bank_transactions";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
    function add($post){
        unset($post['id']);
        if( empty($post['userid'])){
            $post['userid'] = 0;
        }
        if( empty($post['description'])){
            $post['description'] = '';
        }
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }
    function gets($params){
        $this->db->select($this->table.'.*, bank.bankname')->join('bank','bank.id = bank_transactions.bankid', 'left');
        if ( !empty($params['where']) ){
            
            foreach($params['where'] as $where){
               
                $this->db->where($where);
            }
        }
        $return = $this->db->get($this->table)->result();
       
        return $return;

    }
    
    function get($id){
        return $this->db->where('id', $id)->get($this->table)->row();
    }
    

    function edit($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }

    public function get_dailysales($date){
        $date = date('Y-m-d', strtotime($date));
        $result =  $this->db->select('SUM(amount) as total_sales')
                 ->where('date',$date)
                 ->get($this->table)->row();
        if($result->total_sales){
                return $result->total_sales;
        } else {
            return 0;
        }
    }

    public function get_salesrange($start, $end){
        
        $result =  $this->db->select($this->table.'.*,'.$this->table_type.'.sales_type,'.$this->table_member.'.firstname,'.$this->table_member.'.lastname,'.$this->table_promo.'.promo_title,'.$this->table_classtype.'.class_title,membership_type.duration_id, class_duration.durations, class_duration.duration_type')
                 ->join('sales_type','sales_type.id = sales.sales_type_id','left')
                 ->join('members','members.id = sales.member_id','left')
                 ->join('promos','promos.id = sales.promo_id','left')
                 ->join('class_type','class_type.id = sales.class_type_id','left')
                 ->join('membership_type','membership_type.id = sales.membership_type_id','left')
                 ->join('class_duration','class_duration.id = membership_type.duration_id','left')
                 ->where('date >=',$start)->where('date <=',$end)
                 ->get($this->table)->result();
        return $result;
    }

    public function get_dailysalesdata($date){
        $date = date('Y-m-d', strtotime($date));
        $result =  $this->db->select($this->table.'.*,'.$this->table_type.'.sales_type,'.$this->table_member.'.firstname,'.$this->table_member.'.lastname,'.$this->table_promo.'.promo_title,'.$this->table_classtype.'.class_title,membership_type.duration_id, class_duration.durations, class_duration.duration_type')
                 ->join('sales_type','sales_type.id = sales.sales_type_id','left')
                 ->join('members','members.id = sales.member_id','left')
                 ->join('promos','promos.id = sales.promo_id','left')
                 ->join('class_type','class_type.id = sales.class_type_id','left')
                 ->join('membership_type','membership_type.id = sales.membership_type_id','left')
                 ->join('class_duration','class_duration.id = membership_type.duration_id','left')
                 ->where('sales.date',$date)
                 ->get($this->table)->result();
        return $result;
    }

    public function get_monthlysales($date){
        $first_day_this_month = date('Y-m-01', strtotime($date)); // hard-coded '01' for first day
        $last_day_this_month  = date('Y-m-t', strtotime($date));

        $result = $this->db->select('SUM(amount) as total_sales')
                            ->where('date >=',$first_day_this_month)
                            ->where('date <=', $last_day_this_month)
                            ->get($this->table)->row();
        if($result->total_sales){
                return $result->total_sales;
        } else{
            return 0;
        }
    }


    public function get_types(){
        return $this->db->get('sales_type')->result();
    }
}