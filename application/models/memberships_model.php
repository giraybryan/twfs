<?php

class Memberships_model extends MY_Model {
	
	var $table = "membership_log";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_validmembership($member_id){
        $this->db->select('membership_log.*,  membership_type.title, class_type.class_title');
        $this->db->join('class_type','class_type.id = membership_log.class_type_id','left');
        $this->db->join('membership_type','membership_type.id = membership_log.membership_type_id','left');
        $this->db->where("( membership_log.date_end >= '".date('Y-m-d')."' OR membership_log.date_end = '0000-00-00' ) AND membership_log.member_id  = ".$member_id);
        $return = $this->db->get($this->table)->result();
        return $return;
    }    


    function get_expiredmembership($member_id){
        $this->db->select('membership_log.*,  membership_type.title, class_type.class_title');
        $this->db->join('class_type','class_type.id = membership_log.class_type_id','left');
        $this->db->join('membership_type','membership_type.id = membership_log.membership_type_id','left');
        $this->db->where(" membership_log.date_end < '".date('Y-m-d')."' AND membership_log.date_end != '0000-00-00'  AND membership_log.member_id  = ".$member_id);
        $this->db->order_by('id','DESC');
        $return = $this->db->get($this->table)->row();
        return $return;
    }

    function get_memberships($params = array()){

        $this->db->select('members.*, membership_type.title as membership_type,class_type.class_title, (SELECT status FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as mermbership_status, (SELECT paid FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as payment_status');
        $this->db->join('membership_type','membership_type.id = members.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = members.class_type_id', 'left');
        if ( !empty($params['where']) ){
            $this->db->where($params);
        }

        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
    	$result = $this->db->get($this->table)->result();
    	return $result;
    }

    function get_all_by_member($id){
        $this->db->select($this->table.'.*, promos.promo_title, membership_type.title as membership_type,class_type.class_title,class_duration.durations as session_duration, sales.date, sales.amount');
        $this->db->join('membership_type','membership_type.id = membership_log.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = membership_log.class_type_id', 'left');
        $this->db->join('promos','promos.id = membership_log.promo_id', 'left');
        $this->db->join('sales','sales.membership_log_id = '.$this->table.'.id','left');
        $this->db->join('class_duration','class_duration.id = membership_type.session_duration_id', 'left');
        $this->db->where($this->table.'.member_id', $id);
        $this->db->order_by($this->table.'.id','DESC');
        return $this->db->get($this->table)->result();
    }

    function get_memsession_byid($member_id){ 
       $this->db->select('membership_log.*,members.firstname, members.lastname, promos.promo_title, membership_type.title as membership_type,class_type.class_title,class_duration.durations as session_duration ');
        $this->db->join('membership_type','membership_type.id = membership_log.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = membership_log.class_type_id', 'left');
        $this->db->join('members','members.id = membership_log.member_id', 'left');
        $this->db->join('promos','promos.id = membership_log.promo_id', 'left');
        $this->db->join('class_duration','class_duration.id = membership_type.session_duration_id', 'left');
        if ( !empty($params['where']) ){
            $this->db->where($params);
        }
        $this->db->where('membership_log.member_id', $member_id);
        if (!empty($params['sort_by'])){
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        } else {
            $this->db->order_by('membership_log.id','DESC');
        }
        $this->db->where('membership_type.session_duration_id <>',0);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
        $result = $this->db->get($this->table)->row();
        return $result;   
    }

    function get_access_byid($member_id){ 
       $this->db->select('membership_log.*,members.firstname, members.lastname, promos.promo_title, membership_type.title as membership_type,class_type.class_title, ');
        $this->db->join('membership_type','membership_type.id = membership_log.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = membership_log.class_type_id', 'left');
        $this->db->join('members','members.id = membership_log.member_id', 'left');
        $this->db->join('promos','promos.id = membership_log.promo_id', 'left');
        if ( !empty($params['where']) ){
            $this->db->where($params);
        }
        $this->db->where('membership_log.member_id', $member_id);
        if (!empty($params['sort_by'])){
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        } else {
            $this->db->order_by('membership_log.id','DESC');
        }
        $this->db->where('membership_type.session_duration_id',0);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
        $result = $this->db->get($this->table)->row();
        return $result;   
    }

    function get_membership($id){
       
        $this->db->select('membership_log.*,members.firstname, members.lastname, promos.promo_title, membership_type.title as membership_type,class_type.class_title, ');
        $this->db->join('membership_type','membership_type.id = membership_log.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = membership_log.class_type_id', 'left');
        $this->db->join('members','members.id = membership_log.member_id', 'left');
        $this->db->join('promos','promos.id = membership_log.promo_id', 'left');
        if ( !empty($params['where']) ){
            $this->db->where($params);
        }
        $this->db->where('membership_log.id', $id);
        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
        $result = $this->db->get($this->table)->row();
        return $result;
    }

    function get_membershiplog($member_id){
        $this->db->select('membership_log.*,members.firstname, members.lastname, promos.promo_title, membership_type.title as membership_type,class_type.class_title, ');
        $this->db->join('membership_type','membership_type.id = membership_log.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = membership_log.class_type_id', 'left');
        $this->db->join('members','members.id = membership_log.member_id', 'left');
        $this->db->join('promos','promos.id = membership_log.promo_id', 'left');
        if ( !empty($params['where']) ){
            $this->db->where($params);
        }
        $this->db->where('membership_log.member_id', $member_id);
        if (!empty($params['sort_by'])){
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        } else {
            $this->db->order_by('membership_log.id','DESC');
        }
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
        $result = $this->db->get($this->table)->row();
        return $result;   
    }

    function get_log($where){
        $this->db->select('membership_log.*,members.firstname, members.lastname, promos.promo_title, membership_type.title as membership_type,class_type.class_title, ');
        $this->db->join('membership_type','membership_type.id = membership_log.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = membership_log.class_type_id', 'left');
        $this->db->join('members','members.id = membership_log.member_id', 'left');
        $this->db->join('promos','promos.id = membership_log.promo_id', 'left');
        if ( !empty($params['where']) ){
            $this->db->where($params);
        }

        foreach ($where as $key => $val) {
             $this->db->where($key, $val);
        }
        
        $this->db->order_by('membership_log.id','DESC');
        $result = $this->db->get($this->table)->row();
        //echo $this->db->last_query();
        return $result;   
    }

    function get_memberid_enrolled($start_date, $end_date){
        $result = $this->db->select('sales.member_id')->where("date >= '".$start_date."'")->where("date <= '".$end_date."'")->get('sales')->result();

        if($result){
            $member_ids = array();
            foreach($result as $res){
                    $member_ids[] = $res->member_id;
            }
            return $member_ids;
        }
        
        return false;
    }
    function get_expiry($date){

        $seventhday = date('Y-m-d', strtotime($date.' + 7 days'));
        $stardate = date('Y-m-d', strtotime($date.' - 2 days'));
        
        $member_ids = $this->get_memberid_enrolled(date('Y-m-d', strtotime($date.' - 15 days')), $date);

        $this->db->select('membership_log.*,members.mobile, members.firstname, members.lastname, membership_type.title as membership_type,class_type.class_title');
        $this->db->join('membership_type','membership_type.id = membership_log.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = membership_log.class_type_id', 'left');
        $this->db->join('members','members.id = membership_log.member_id', 'left');
        if ( !empty($params['where']) ){
            $this->db->where($params);
        }
        if(!empty($member_ids)){
            $this->db->where_not_in('membership_log.member_id', $member_ids);
        }
        $this->db->where("membership_log.date_end > '".$stardate."'  AND membership_log.date_end <= '".$seventhday."'");
        if (!empty($params['sort_by'])){
                    $this->db->order_by($params['sort_by'], $params['sort_order']); 
                } else {
                    $this->db->order_by('membership_log.date_end','ASC');
                }
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
        $result = $this->db->get($this->table)->result();
        //echo $this->db->last_query();
        return $result;
    }

    function total_meters(){
        $result = $this->db->get($this->table)->num_rows();
        return $result;
    }

    function add($post){
        unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }
    function add_log($post){

        $this->db->insert('membership_log', $post);
        return $this->db->insert_id();
    }

    function edit($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }


    function get_renewal_newmember_range($post){

        if( $post['type'] == "new"){
            $result = $this->db->select('COUNT(id) as total')->where('date_registered >=',$post['date_start'])->where('date_registered <=', $post['date_end'])->get('members')->row();
        } else {
            $result = $this->db->select('COUNT(id) as total')->where('renew',1)->where('date_register >=', $post['date_start'].' 00:00:00')->where('date_register <=', $post['date_end'].' 23:59:00')->get('membership_log')->row();
        }

        return $result;
    }


    function get_renewal_gender_newmember_range($post){

        if( $post['type'] == "new"){
            $result = $this->db->select('COUNT(id) as total')->where('date_registered >=',$post['date_start'])->where('date_registered <=', $post['date_end'])->where('gender', $post['gender'])->get('members')->row();
        } else {
            $result = $this->db->select('COUNT(membership_log.id) as total')->join('members','members.id = membership_log.member_id')->where('renew',1)->where('membership_log.date_register >=', $post['date_start'].' 00:00:00')->where('membership_log.date_register <=', $post['date_end'].' 23:59:00')->where('members.gender', $post['gender'])->get('membership_log')->row();
        }

        return $result->total;
    }

}