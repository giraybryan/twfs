<?php

class Promos_model extends MY_Model {
	
	var $table = "promos";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get($params = array())
    {
       $this->db->select($this->table.'.*,  membership_type.title');
       $this->db->join('membership_type','membership_type.id = '.$this->table.'.membership_type_id','left');
       return $this->db->get($this->table)->result();
    }
    

    function get_promos($params = array()){

        $this->db->select('*');
        if ( !empty($params['where']) ){
            $this->db->where($params);
        }

        $this->db->where('date_end >=', date('Y-m-d'));
        $this->db->or_where('date_end','0000-00-00');
        $this->db->where('suspended',0);
        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
    	$result = $this->db->get($this->table)->result();
    	return $result;
    }

    function get_types(){
        return $this->db->get('promo_types')->result();
    }

    function get_membership($id){
       
        $this->db->select('membership_log.*,members.firstname, members.lastname, promos.promo_title, membership_type.title as membership_type,class_type.class_title, ');
        $this->db->join('membership_type','membership_type.id = membership_log.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = membership_log.class_type_id', 'left');
        $this->db->join('members','members.id = membership_log.member_id', 'left');
        $this->db->join('promos','promos.id = membership_log.promo_id', 'left');
        if ( !empty($params['where']) ){
            $this->db->where($params);
        }
        $this->db->where('membership_log.id', $id);
        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
        $result = $this->db->get($this->table)->row();
        return $result;
    }

  
    function add($post){
        unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }
   

    function edit($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }

    function get_promo( $id, $class_type_id, $membership_duration ){
        $result = $this->db->where('id', $id)->where('class_type_id', $class_type_id)->where('membership_type_id', $membership_duration)->get($this->table)->row();
        if( empty( $result ) ) {
            return false;
        } else {
           return $result;
        }
    }   


}