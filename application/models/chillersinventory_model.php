<?php

class Chillersinventory_model extends MY_Model {
	
	var $table = "chiller_inventory";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_all($params){
    	$this->db->select("*");
    	if ( !empty($params['where']) ){
            $this->db->where($params);
        }
         if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);

    	$result = $this->db->get($this->table)->result();

    	return $result;
    }
     function get($params){
    	$this->db->select("*");
 
        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        
       	if(!empty($params['where'])){
       		foreach ($params['where'] as $key => $value) {
       			$this->db->where($key, $value);
       		}
       	}
    	$result = $this->db->get($this->table)->row();

    	return $result;
    }

    
    function search($post){
        $this->db->select('members.*, membership_type.title as membership_type,class_type.class_title, (SELECT status FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as mermbership_status, (SELECT paid FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as payment_status,(SELECT note FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as note');
        $this->db->join('membership_type','membership_type.id = members.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = members.class_type_id', 'left');
        if ( !empty($params['where']) ){
            $this->db->where($params);
        }

        $this->db->where('members.firstname LIKE "%'.$post['namesearch'].'%" OR members.lastname LIKE "%'.$post['namesearch'].'%"');

        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
        $result = $this->db->get($this->table)->result();
        return $result;
    }

    function get_member($id){

        $this->db->select('members.*, membership_type.title as membership_type,class_type.class_title, (SELECT status FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as mermbership_status, (SELECT paid FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as payment_status');
        $this->db->join('membership_type','membership_type.id = members.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = members.class_type_id', 'left');
        
        $this->db->where('members.id', $id);
        
        $result = $this->db->get($this->table)->row();
       
        return $result;
    }


    function add($post){

    	if( empty($post['id']) ){
            unset($post['id']);
			$this->db->insert($this->table, $post);
			return $this->db->insert_id();
    	} else {
	    	$id = $post['id'];
	        unset($post['id']);
	        $this->db->where('id', $id); 
	        $this->db->update($this->table, $post);

	        if( $this->db->affected_rows() ){
	            return $id;
	        } else {
	            return false;
	        }
    	}
       
    }
   
    function update($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }


}