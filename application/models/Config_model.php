<?php

class Config_model extends MY_Model {
	
    var $table = "config";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
    function add($post){
        unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }
    function gets(){
        return $this->db->get($this->table)->result();
    }

    function get($id){
        return $this->db->where('id', $id)->get($this->table)->row();
    }
    function get_valueby_code($code){
        $result = $this->db->select('value')->where('code', $code)->get($this->table)->row();
        if( $result ){
            return $result->value;
        } else {
            return false;
        }
    }
    
    function edit($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }

    public function get_types(){
        return $this->db->get('sales_type')->result();
    }
}