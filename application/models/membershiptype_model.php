<?php

class Membershiptype_model extends MY_Model {
	
    var $table = "membership_type";
    

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_class_price($id){
        $result = $this->db->where('id', $id)->get($this->table)->row();
        return $result->price;
    }


    function get_membertypes($params = array()){

        $this->db->select($this->table.'.*, class_duration.durations, class_duration.duration_type, class_type.class_title, (SELECT durations FROM class_duration b WHERE b.id = membership_type.session_duration_id ORDER BY id DESC LIMIT 1) as session_duration');
        if ( !empty($params['where']) ){
            $this->db->where($params['were']);
        }
        $this->db->join('class_duration','class_duration.id = '.$this->table.'.duration_id','left');
        $this->db->join('class_type','class_type.id = '.$this->table.'.class_type_id','left');

        if (!empty($params['sort_by'])){
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        } else {
            $this->db->order_by('class_type.class_title', 'ASC');
        }
           
        $this->db->where($this->table.'.active',1);
    	$result = $this->db->get($this->table)->result();
    	return $result;
    }

    function get_membertype($params = array()){
    
        $this->db->select($this->table.'.*, class_duration.durations, class_duration.duration_type ');
        if ( !empty($params['where']) ){
            $this->db->where($params['where']);
        }
        $this->db->join('class_duration','class_duration.id = '.$this->table.'.duration_id','left');
        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
        $result = $this->db->get($this->table)->row();
        return $result;
    }

    function get_by_id($id){
        if( empty($id) ){
            return false;
        }

        $this->db->select($this->table.'.*, (SELECT durations FROM class_duration b WHERE b.id = membership_type.duration_id ) as monthly_duration,(SELECT durations FROM class_duration b WHERE b.id = membership_type.session_duration_id ) as session_duration');
        if ( !empty($id) ){
            $this->db->where('id',$id);
        }
    
        $result = $this->db->get($this->table)->row();
        return $result;
    }
    
    function add($post){
        unset($post['id']);
        if( empty($post['session_duration_id']))
            unset($post['session_duration_id']);
        
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }

    function edit($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }

    function inactive($id){
        $_arr = array('active' => 0);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $_arr);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }
}