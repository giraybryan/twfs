<?php

class Memberships_freeze_model extends MY_Model {
	
	var $table = "membership_freeze";

	function __construct()
    {

    }

    function get_annual_count($where){
        $this->db->select('SUM(months) as total');
        $this->db->where($where);
        $result = $this->db->get($this->table)->row();
        return $result->total;
    }

    function add( $post )
    {
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }

    function get($where)
    {
        return $this->db->where($where)->get($this->table)->row();
    }

    
}