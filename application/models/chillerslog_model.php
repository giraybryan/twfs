<?php

class Chillerslog_model extends MY_Model {
	
	var $table = "chillers_log";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    function  getsales_row($id){
    	$this->db->select('chillers_log.*, members.firstname, members.lastname, chillers.name');
    	$this->db->join('chillers', 'chillers.id = chillers_log.chiller_id', 'left');
    	$this->db->join('members', 'members.id = chillers_log.member_id', 'left');

    	$this->db->where('chillers_log.id', $id);
    	return $this->db->get($this->table)->row();
    }
    function getsales($id, $date){
    	$this->db->select('chiller_dailysales_log.*,  chillers.name');
    	$this->db->join('chillers', 'chillers.id = chiller_dailysales_log.chiller_id', 'left');
    	$this->db->where('chiller_dailysales_log.chiller_id', $id);
    	$this->db->where('chiller_dailysales_log.date', $date);
    	return $this->db->get('chiller_dailysales_log')->row();
    }
    function savelogsales($post){
    	if( empty($post['id']) ){
            unset($post['id']);
			$this->db->insert('chiller_dailysales_log', $post);
			return $this->db->insert_id();
    	} else {
	    	$id = $post['id'];
	        unset($post['id']);
	        $this->db->where('id', $id); 
	        $this->db->update('chiller_dailysales_log', $post);

	        if( $this->db->affected_rows() ){
	            return $id;
	        } else {
	            return false;
	        }
    	}
    }
    function updatesaleslog($post){
        if( empty($post['id']) ){
            unset($post['id']);
            $this->db->insert('chillers_log', $post);
            return $this->db->insert_id();
        } else {
            $id = $post['id'];
            unset($post['id']);
            $this->db->where('id', $id); 
            $this->db->update('chillers_log', $post);

            if( $this->db->affected_rows() ){
                return $id;
            } else {
                return false;
            }
        }
    }

    function getsummary($params){
    	$this->db->select("chillers.name, chillers.capital_price, chillers.selling_price, chillers.stock as base_stock, chiller_dailysales_log.remaining,chiller_dailysales_log.id as summary_id, (SELECT SUM(qty) FROM chillers_log a WHERE a.chiller_id = chillers.id AND date='".$params['where']['date']."' AND a.deleted = 0) as sold, (SELECT SUM(qty) FROM chiller_inventory b WHERE date_added='".$params['where']['date']." 00:00:00' AND chillers.id = b.chiller_id) as additional ");
        
    	if(!empty($params['where'])){
       		foreach ($params['where'] as $key => $value) {
       			$this->db->where($key, $value);
       		}
       	}
       	$this->db->join('chiller_dailysales_log','chiller_dailysales_log.chiller_id = chillers.id', 'left');
       	$this->db->order_by('chillers.ordering', 'ASC');
       	$this->db->group_by('chiller_dailysales_log.chiller_id');
       	$return = $this->db->get('chillers')->result();
       
       	return $return;
    }

    function getlogs($params){
    	$this->db->select("chillers_log.*,chillers.name, members.firstname, members.lastname");
    	$this->db->join('members','members.id = chillers_log.member_id', 'left');
    	$this->db->join('chillers','chillers.id = chillers_log.chiller_id', 'left');
    	if ( !empty($params['where']) ){
            foreach ($params['where'] as $key => $value) {
       			$this->db->where($key, $value);
       		}
        }

         if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);

    	$result = $this->db->get($this->table)->result();
    	return $result;
    }
    
    function get($params){
    	$this->db->select("*");
 
        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        
       	if(!empty($params['where'])){
       		foreach ($params['where'] as $key => $value) {
       			$this->db->where($key, $value);
       		}
       	}
    	$result = $this->db->get($this->table)->row();

    	return $result;
    }

    
    function search($post){
        $this->db->select('members.*, membership_type.title as membership_type,class_type.class_title, (SELECT status FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as mermbership_status, (SELECT paid FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as payment_status,(SELECT note FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as note');
        $this->db->join('membership_type','membership_type.id = members.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = members.class_type_id', 'left');
        if ( !empty($params['where']) ){
            $this->db->where($params);
        }

        $this->db->where('members.firstname LIKE "%'.$post['namesearch'].'%" OR members.lastname LIKE "%'.$post['namesearch'].'%"');

        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
        $result = $this->db->get($this->table)->result();
        return $result;
    }

    function get_member($id){

        $this->db->select('members.*, membership_type.title as membership_type,class_type.class_title, (SELECT status FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as mermbership_status, (SELECT paid FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as payment_status');
        $this->db->join('membership_type','membership_type.id = members.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = members.class_type_id', 'left');
        
        $this->db->where('members.id', $id);
        
        $result = $this->db->get($this->table)->row();
       
        return $result;
    }


    function add($post){

    	if( empty($post['id']) ){
            unset($post['id']);
			$this->db->insert($this->table, $post);
			return $this->db->insert_id();
    	} else {
	    	$id = $post['id'];
	        unset($post['id']);
	        $this->db->where('id', $id); 
	        $this->db->update($this->table, $post);

	        if( $this->db->affected_rows() ){
	            return $id;
	        } else {
	            return false;
	        }
    	}
       
    }
   
    function update($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }


}