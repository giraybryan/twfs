<?php

class Sales_model extends MY_Model {
	
    var $table = "sales";
    var $table_type = "sales_type";
    var $table_member = "members";
	var $table_promo = "promos";
    var $table_classtype = "class_type";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    
    function add($post){
        unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }
    function get($id){
        return $this->db->where('id', $id)->get($this->table)->row();
    }

    function get_bymembership_log($membership_log_id){
        return $this->db->where('membership_log_id', $membership_log_id)->get($this->table)->row();
    }


    function add_log($post){

        $this->db->insert('membership_log', $post);
        return $this->db->insert_id();
    }

    function edit($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }

    public function get_dailysales($date){
        $date = date('Y-m-d', strtotime($date));
        $result =  $this->db->select('SUM(amount) as total_sales')
                 ->where('date',$date)
                 ->get($this->table)->row();
        if($result->total_sales){
                return $result->total_sales;
        } else {
            return 0;
        }
    }

    public function get_salesrange($start, $end){
        
        $result =  $this->db->select($this->table.'.*,'.$this->table_type.'.sales_type,'.$this->table_member.'.firstname,'.$this->table_member.'.lastname,'.$this->table_promo.'.promo_title,'.$this->table_classtype.'.class_title,membership_type.duration_id, class_duration.durations, class_duration.duration_type, or_issue.id as orid')
                 ->join('sales_type','sales_type.id = sales.sales_type_id','left')
                 ->join('members','members.id = sales.member_id','left')
                 ->join('promos','promos.id = sales.promo_id','left')
                 ->join('class_type','class_type.id = sales.class_type_id','left')
                 ->join('membership_type','membership_type.id = sales.membership_type_id','left')
                 ->join('class_duration','class_duration.id = membership_type.duration_id','left')
                 ->join('or_issue','or_issue.sales_id = sales.id', 'left')
                 ->where('sales.date >=',$start)->where('sales.date <=',$end)
                 ->order_by('sales.date','ASC')
                 ->get($this->table)->result();
        return $result;
    }

    public function get_total_bytype($start, $end){
        $result =  $this->db->select('SUM('.$this->table.'.amount) as total')
                 ->where('date >=',$start)->where('date <=',$end)
                 ->group_by('date')
                 ->get($this->table)->result();
        return $result;
    }

    public function gettotal_byrange($start, $end){
         $start = date('Y-m-d', strtotime($start));
         $end = date('Y-m-d', strtotime($end));
        $result = $this->db->select('SUM(amount) as sales, date')
                            ->where('date >=',$start)
                            ->where('date <=', $end)
                            ->group_by("date")
                            ->order_by('date','ASC')
                 ->get($this->table)->result();
        return $result;
    }

    public function get_dailysalesdata($date){
        $date = date('Y-m-d', strtotime($date));
        $result =  $this->db->select($this->table.'.*,'.$this->table_type.'.sales_type,'.$this->table_member.'.firstname,'.$this->table_member.'.lastname,'.$this->table_promo.'.promo_title,'.$this->table_classtype.'.class_title,membership_type.duration_id, class_duration.durations, class_duration.duration_type, or_issue.id as orid')
                 ->join('sales_type','sales_type.id = sales.sales_type_id','left')
                 ->join('members','members.id = sales.member_id','left')
                 ->join('promos','promos.id = sales.promo_id','left')
                 ->join('class_type','class_type.id = sales.class_type_id','left')
                 ->join('membership_type','membership_type.id = sales.membership_type_id','left')
                 ->join('class_duration','class_duration.id = membership_type.duration_id','left')
                 ->join('or_issue',' or_issue.sales_id = sales.id','left')
                 ->where('sales.date',$date)
                 ->order_by('sales.date','ASC')
                 ->get($this->table)->result();
        return $result;
    }

    public function get_monthlysales($date){
        $first_day_this_month = date('Y-m-01', strtotime($date)); // hard-coded '01' for first day
        $last_day_this_month  = date('Y-m-t', strtotime($date));

        $result = $this->db->select('SUM(amount) as total_sales')
                            ->where('date >=',$first_day_this_month)
                            ->where('date <=', $last_day_this_month)
                            ->get($this->table)->row();
        if($result->total_sales){
                return $result->total_sales;
        } else{
            return 0;
        }
    }


    public function get_types(){
        return $this->db->get('sales_type')->result();
    }

    public function get_typecode_byid($id){
       return  $this->db->select('code')->where('id', $id)->get($this->table_type)->row();
    }

    public function get_idby_code($code){
        $result =  $this->db->select('id')->where('code', $code)->get($this->table_type)->row();
        return $result->id;
    }


    public function get_last_membership_fee($params){
        $result = $this->db->select('*')->where($params)->order_by('id','DESC')->get($this->table)->row();
        if( $result ){
            return $result;
        } 
        return false;
    }
}