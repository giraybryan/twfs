<?php

class Reserve_model extends MY_Model {
	
	var $table = "reserve";

	function __construct()
    {

 	}

 	public function get_available_timeslot($date){
 		$starttime = $date.' 00:00:00';
 		$endtime   = $date.' 23:00:00'; 
 		return $this->db->select('COUNT(date) as time_consumed, date')
 				 ->where('date >=', $starttime)
 				 ->where('date <=', $endtime)
 				 ->group_by('date')
 				 ->get($this->table)
 				 ->result();
 	}

 	public function get_reservation( $query ){
 		return $this->db->select('reserve.*, class_type.class_title')
 					    ->where('date >=', $query['start'])
 					    ->where('date <=', $query['end'])
 					    ->join('class_type', 'class_type.id = reserve.class_type_id')
 					    ->order_by('date','ASC')
 					    ->get($this->table)
 					    ->result();
 	}

 	public function add( $post ){
 		unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
 	}
 }