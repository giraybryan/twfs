<?php

class Members_model extends MY_Model {
	
	var $table = "members";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function total_members($params = array()){
        $this->db->select('COUNT(id) as total_members');
        $this->db->where('status','active');
        $this->db->where('branch_id', $this->branch_id);
        $return = $this->db->get($this->table)->row();

        return $return->total_members;
    }

    function total_gender($gender_value){
        $this->db->select('COUNT(id) as total_members');
        $this->db->where('gender',$gender_value)->where('branch_id', $this->branch_id);

        $return = $this->db->get($this->table)->row();

        return $return->total_members;
    }

    function total_gender_byrange($date_start, $date_end, $gender_value){

        $first_day_this_month = date('Y-m-01',strtotime($date_start)); // hard-coded '01' for first day
        $last_day_this_month  = date('Y-m-t',strtotime($date_end));
        $this->db->select('COUNT(id) as total_members');
        $this->db->where('gender',$gender_value);
        $this->db->where('branch_id', $this->branch_id);
        $this->db->where('date_registered >=', $first_day_this_month);
        $this->db->where('date_registered <=', $last_day_this_month);
        $return = $this->db->get($this->table)->row();

        return $return->total_members;
    }

    function montly_result($params = array()){
        $this->db->select('COUNT(id) as total_members');
        if($params['month_value']){
            $first_day_this_month = date('Y-m-01',strtotime($params['month_value'])); // hard-coded '01' for first day
            $last_day_this_month  = date('Y-m-t',strtotime($params['month_value']));
            $this->db->where('branch_id', $this->branch_id);
            $this->db->where('date_start >=',$first_day_this_month);
            $this->db->where('date_start <=',$last_day_this_month);
            
        }
        $return = $this->db->get('membership_log')->row();

        return $return->total_members;
    }

    function get_members($params = array()){

        $this->db->select('members.*, membership_type.title as membership_type,class_type.class_title, (SELECT status FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as mermbership_status,(SELECT date_end FROM membership_log b WHERE b.member_id = members.id ORDER BY b.id DESC LIMIT 1) as expire_date,(SELECT id FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as membership_log_id,(SELECT note FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as note, (SELECT paid FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as payment_status');
        $this->db->join('membership_type','membership_type.id = members.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = members.class_type_id', 'left');
        if ( !empty($params['where']) ){
            $this->db->where($params);
        }
        $this->db->where('branch_id',$this->branch_id);
        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
    	$result = $this->db->get($this->table)->result();
    	return $result;
    }

    function get_totalmembers($params = array()){

        $this->db->select('COUNT(members.id) as total');
        $this->db->join('membership_type','membership_type.id = members.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = members.class_type_id', 'left');
        $this->db->where('branch_id',$this->branch_id);
        if ( !empty($params['where']) ){
            foreach($params['where'] as $where){
                $this->db->where($where);
            }
        }
        if ( !empty($params['or_where']) ){
            foreach($params['or_where'] as $_orwhere){
                $this->db->or_where($_orwhere);
            }
        }
        if( !empty($params['namesearch'])){
            $this->db->where('members.firstname LIKE "%'.$params['namesearch'].'%" OR members.lastname LIKE "%'.$params['namesearch'].'%"');
        }
        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
        $result = $this->db->get($this->table)->row();
        return $result->total;
    }

    function search($post){
        $this->db->select('members.*, membership_type.title as membership_type,class_type.class_title, (SELECT status FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as mermbership_status, (SELECT date_end FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as expire_date,(SELECT id FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as membership_log_id, (SELECT paid FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as payment_status,(SELECT note FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as note');
        $this->db->join('membership_type','membership_type.id = members.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = members.class_type_id', 'left');
        if ( !empty($post['where']) ){

            foreach($post['where'] as $where){
                $this->db->where($where);
            }
           
        }
        $this->db->where('branch_id',$this->branch_id);

        if ( !empty($post['or_where']) ){
            foreach($post['or_where'] as $_orwhere){
                $this->db->or_where($_orwhere);
            }
        }
        if( !empty($post['namesearch'])){
            $this->db->where('members.firstname LIKE "%'.$post['namesearch'].'%" OR members.lastname LIKE "%'.$post['namesearch'].'%"');
        }

        $this->db->where('members.branch_id', $this->branch_id);
        
        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        if ( !empty($post['limit']) )
            $this->db->limit($post['limit'],$post['offset']);
        $result = $this->db->get($this->table)->result();
        return $result;
    }

    function get_member($id){

        $this->db->select('members.*, membership_type.title as membership_type,class_type.class_title, (SELECT status FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as mermbership_status, (SELECT paid FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as payment_status, (SELECT id FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as membership_log_id');
        $this->db->join('membership_type','membership_type.id = members.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = members.class_type_id', 'left');
        
        $this->db->where('members.id', $id);
        
        $result = $this->db->get($this->table)->row();
       
        return $result;
    }

    function get_by_cardno($cardno){
        $this->db->select('members.*, membership_type.title as membership_type,class_type.class_title, (SELECT status FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as mermbership_status, (SELECT paid FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as payment_status, (SELECT id FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as membership_log_id');
        $this->db->join('membership_type','membership_type.id = members.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = members.class_type_id', 'left');
        
        $this->db->where('members.card_id', $cardno);
        
        $result = $this->db->get($this->table)->row();
       
        return $result;
    }

    function total_meters(){
        $result = $this->db->get($this->table)->num_rows();
        return $result;
    }

    function add($post){
        unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }
    function add_log($post){

        $this->db->insert('membership_log', $post);
        return $this->db->insert_id();
    }
    function get_by_memberidno( $memberid_no){
        $this->db->select('members.*, membership_type.title as membership_type,class_type.class_title, (SELECT status FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as mermbership_status, (SELECT paid FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as payment_status, (SELECT id FROM membership_log b WHERE b.member_id = members.id ORDER BY date_register DESC LIMIT 1) as membership_log_id,
        ( SELECT COUNT(id) FROM session_counter b WHERE b.member_id = members.id=1 AND  b.membership_log_id = members.membership_type_id ) as session_consumed, (SELECT date_end FROM membership_log b WHERE b.member_id = members.id ORDER BY b.id DESC LIMIT 1) as expire_date, branches.branch_name');
        $this->db->join('membership_type','membership_type.id = members.membership_type_id', 'left');
        $this->db->join('class_type','class_type.id = members.class_type_id', 'left');
        $this->db->join('branches','branches.id = members.branch_id', 'left');
        
        $this->db->where('members.id', $memberid_no);
        
        $result = $this->db->get($this->table)->row();
       
        return $result;
    }

    function edit($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }

    public function update($post){
        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }


    function report_by_status($params){

        switch( $params['status'] ){
            case "expired":
                $this->db->select('members.*, membership_log.id as logid, membership_log.date_end, membership_type.title, (SELECT id FROM membership_log b WHERE b.member_id = members.id ORDER BY id DESC LIMIT 1) as latest_log');
                $this->db->join('membership_log','membership_log.member_id = members.id','inner');
                $this->db->join('membership_type','membership_type.id = membership_log.membership_type_id','left');
                $this->db->where('membership_log.date_end < ',date('Y-m-d'));
                $this->db->where('members.branch_id',$this->branch_id);
                $this->db->group_by('members.id');
                $this->db->order_by('membership_log.id','DESC');
                return $this->db->get($this->table)->result();
                
                break;
            case "freezed":
                $this->db->select('members.*, membership_log.id as logid, membership_log.date_end, membership_type.title, (SELECT id FROM membership_log b WHERE b.member_id = members.id ORDER BY id DESC LIMIT 1) as latest_log, membership_freeze.start_date as freeze_start, membership_freeze.end_date as freeze_end');
                $this->db->join('membership_freeze','membership_freeze.member_id = members.id','inner');
                $this->db->join('membership_log','membership_log.member_id = members.id','inner');
                $this->db->join('membership_type','membership_type.id = membership_log.membership_type_id','left');
                $this->db->where("'".date('Y-m-d')."'".' BETWEEN membership_freeze.start_date AND membership_freeze.end_date');
                $this->db->where('members.branch_id',$this->branch_id);
                $this->db->group_by('members.id');
                $this->db->order_by('membership_log.id','DESC');
                return $this->db->get($this->table)->result();
                break;
            case "membership":
                //get sales_type_id for 
                $time = strtotime("-1 year", time());
                $last_year_date = date("Y-m-d", $time); 
                $sales_type =  $this->db->select('id')->where('code', 'joiningfee')->get('sales_type')->row();
                $this->db->select('members.*,sales.sales_type_id, sales.date as date_end, sales.id as logid, (SELECT id FROM sales b WHERE b.member_id = members.id AND b.sales_type_id ='.$sales_type->id.' ORDER BY id DESC LIMIT 1) as latest_log');
                $this->db->join('members','members.id = sales.member_id','left');
                $this->db->where('sales.date <=', $last_year_date);
                $this->db->where('sales.sales_type_id', $sales_type->id);
                $this->db->where('members.branch_id',$this->branch_id);
                $this->db->or_where('members.card_id','');
                $this->db->order_by('sales.id','DESC');
                $this->db->group_by('sales.member_id');
                return $this->db->get('sales')->result();
                break;
            default:
                $this->db->select('members.*, membership_log.id as logid, membership_log.date_end, membership_type.title, (SELECT id FROM membership_log b WHERE b.member_id = members.id ORDER BY id DESC LIMIT 1) as latest_log');
                $this->db->join('membership_log','membership_log.member_id = members.id','inner');
                $this->db->join('membership_type','membership_type.id = membership_log.membership_type_id','left');
                $this->db->where('membership_log.date_end >= ',date('Y-m-d'));
                $this->db->where('members.branch_id',$this->branch_id);
                $this->db->group_by('members.id');
                $this->db->order_by('membership_log.id','DESC');
                return $this->db->get($this->table)->result();
                break;
        }
        
    }
}