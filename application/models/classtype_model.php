<?php

class Classtype_model extends MY_Model {
	
	var $table = "class_type";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_types($params = array()){

        $this->db->select($this->table.'.*, class_type_categories.category_title as class_type');
        if ( !empty($params['where']) ){
            $this->db->where($params);
        }
        $this->db->join('class_type_categories', 'class_type_categories.id = class_type.class_category_id', 'left');
        $this->db->where('on_off',1);
        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
    	$result = $this->db->get($this->table)->result();
    	return $result;
    }

    function get_by_id($id){
        if( empty($id) ){
            return false;
        }

        $this->db->select('*');
        if ( !empty($id) ){
            $this->db->where('id',$id);
        }
    
        $result = $this->db->get($this->table)->row();
        return $result;
    }


    function add($post){
        unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }

    function edit($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }
    function inactive($id){
        $this->db->where('id',$id)->update($this->table, array('on_off' => 0));
        return $this->db->affected_rows();
    }
    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }


}