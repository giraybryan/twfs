<?php

class Users_model extends MY_Model {
	
	var $table = "users";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function login($username, $password){
    	$where = array('username' => $username, 'password' => $password);
    	$result = $this->db->where($where)->get($this->table)->row();
    	return $result;
    }

     function get($username){
        $where = array('username' => $username );
        $result = $this->db->where($where)->get($this->table)->row();
        return $result;
    }

    function get_by_id($id){
        $where = array('id' => $id );
        $result = $this->db->where($where)->get($this->table)->row();
        return $result;
    }

     function gets($params){
        
        if (!empty($params['sort_by'])){
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        } else {
            $this->db->order_by('users.id','DESC');
        }
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
        $result = $this->db->get($this->table)->result();

        return $result;
    }

     function update_pw($username,$password){

        $pw = array('password' => $password );
        $this->db->where('username', $username); 
        $this->db->update($this->table, $pw);

        $this->db->last_query();

       if( $this->db->affected_rows() ){
            return $username;
        } else {
            return false;
        }
    }

    function add($post){
        unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }

    function edit($post){
        $this->db->where('id',$post['id']);
        unset($post['id']);
        $this->db->update($this->table, $post);
        return $this->db->affected_rows();
    }


}