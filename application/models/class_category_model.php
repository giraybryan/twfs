<?php

class Class_category_model extends MY_Model {
	
	var $table = "class_type_categories";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_categories($params = array()){

        $this->db->select('*');
        if ( !empty($params['where']) ){
            $this->db->where($params);
        }

        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
    	$result = $this->db->get($this->table)->result();
    	return $result;
    }

    function total_meters(){
        $result = $this->db->get($this->table)->num_rows();
        return $result;
    }

    function add($post){
        unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }

    function edit($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }


}