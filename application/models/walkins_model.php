<?php

class Walkins_model extends MY_Model {
	
	var $table = "walkin_log";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function total_monthlywalkin($params = array()){
        $this->db->select('COUNT(id) as total_members');
        $first_day_this_month = date('Y-m-01',strtotime(date('Y-m-d'))); // hard-coded '01' for first day
        $last_day_this_month  = date('Y-m-t',strtotime(date('Y-m-d')));
        if($params['month_value']){
            $this->db->where('date >=',$params['month_value']);
            $this->db->where('date <=',$params['month_value']);
        }
        $return = $this->db->get($this->table)->row();

        return $return->total_members;
    }

    function montly_result($params = array()){
        $this->db->select('COUNT(id) as total_members');
        if($params['month_value']){
            $first_day_this_month = date('Y-m-01',strtotime($params['month_value'])); // hard-coded '01' for first day
            $last_day_this_month  = date('Y-m-t',strtotime($params['month_value']));

            $this->db->where('date_start >=',$first_day_this_month);
            $this->db->where('date_start <=',$last_day_this_month);
            
        }
        $return = $this->db->get('membership_log')->row();

        return $return->total_members;
    }

    function add($post){
        unset($post['id']);
        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }

    function edit($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);

        if( $this->db->affected_rows() ){
            return $id;
        } else {
            return false;
        }
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }


}