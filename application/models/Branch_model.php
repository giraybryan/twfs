<?php

class Branch_model extends MY_Model {
	
    var $table = "branches";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function get_branchname($id)
    {
        if( empty($id) ){
            $id = $this->branch_id;
        }
        $branch = $this->db->select('branch_name')->where('id', $id)->get($this->table)->row();
        if( $branch ){
            return $branch->branch_name;
        } else {
            if( empty( $branch )){
                $uri = explode('/',$_SERVER['REQUEST_URI']);
                if (!empty($uri[1]) ){
                    return $uri[1];
                } else {
                    return false;
                }

            } else {
                return false;
            }
        }
    }

}