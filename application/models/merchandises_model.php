<?php

class Merchandises_model extends MY_Model {
	
    var $table          = "merchandise";
    var $table_variant  = "merchandise_variants";
    var $table_salestable = "merchandise_dailysales_log";
    var $table_summary    = "merchandise_summary_log";

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }


    function montly_result($params = array()){
        $this->db->select('COUNT(id) as total_members');
        if($params['month_value']){
            $first_day_this_month = date('Y-m-01',strtotime($params['month_value'])); // hard-coded '01' for first day
            $last_day_this_month  = date('Y-m-t',strtotime($params['month_value']));

            $this->db->where('date_start >=',$first_day_this_month);
            $this->db->where('date_start <=',$last_day_this_month);
            
        }
        $return = $this->db->get('membership_log')->row();

        return $return->total_members;
    }

    function get_merchandises($params = array()){

        $this->db->select('merchandise.*, merchandise_variants.cost,merchandise_variants.cost as cost_var, merchandise_variants.price as price_var, merchandise_variants.stock as stock_var');
        $this->db->join($this->table_variant,$this->table_variant.'.merchandise_id = merchandise.id', 'left');
        if ( !empty($params['where']) ){
            if( is_array($params['where']))
            {
                foreach($params['where'] as $key => $value)
                {
                    $this->db->where($key, $value);
                }
            } else 
            {
                $this->db->where($params['where']);
            }
            
        }

        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
    	$result = $this->db->get($this->table)->result();
    	return $result;
    }

    function get_sales($date, $branch_id){
        $date = date('Y-m-d',strtotime($date));

        $this->db->select($this->table_variant.'.*, '. $this->table.'.item,'.$this->table.'.ordering,'.$this->table_salestable.'.*');
        $this->db->join($this->table_salestable,$this->table_salestable.'.variant_id = '.$this->table_variant.'.id','left');
        $this->db->join($this->table,$this->table.'.id = '.$this->table_variant.'.merchandise_id','left');
        $this->db->where($this->table_salestable.'.date',$date);
        $this->db->where($this->table_salestable.'.branch_id',$branch_id);
        $this->db->order_by($this->table.'.ordering','ASC');
        $result = $this->db->get($this->table_variant)->result();
        return $result;
    }

    function get_active_items($params = array()){

        $this->db->select('merchandise.*,merchandise_variants.id as variant_id, merchandise_variants.stock as item_stock, merchandise_variants.cost,merchandise_variants.cost as cost_var, merchandise_variants.price as price_var, merchandise_variants.stock as stock_var');
        $this->db->join($this->table_variant,$this->table_variant.'.merchandise_id = merchandise.id', 'left');
        if ( !empty($params['where']) ){
            if( is_array($params['where']))
            {
                foreach($params['where'] as $key => $value)
                {
                    $this->db->where($key, $value);
                }
            } else 
            {
                $this->db->where($params['where']);
            }
        }

        $this->db->where('merchandise_variants.stock >',0);
        if (!empty($params['sort_by']))
            $this->db->order_by($params['sort_by'], $params['sort_order']);
        if ( !empty($params['limit']) )
        $this->db->limit($params['limit'],$params['offset']);
    	$result = $this->db->get($this->table)->result();
    	return $result;
    }

    function get_merchandise($id, $branch_id){

        $this->db->select($this->table.'.*, (SELECT price FROM '.$this->table_variant.' b WHERE b.merchandise_id = merchandise.id ORDER BY id ASC LIMIT 1) as price_var, (SELECT cost FROM '.$this->table_variant.' b WHERE b.merchandise_id = merchandise.id ORDER BY id ASC LIMIT 1) as cost_var, (SELECT stock FROM '.$this->table_variant.' b WHERE b.merchandise_id = merchandise.id ORDER BY id ASC LIMIT 1) as stock_var, (SELECT id FROM '.$this->table_variant.' b WHERE b.merchandise_id = merchandise.id ORDER BY id ASC LIMIT 1) as variant_id');
        $this->db->where('id', $id);
        $this->db->where('branch_id',$branch_id);
        $result = $this->db->get($this->table)->row();
       
        return $result;
    }

     function get_variants($id, $branch_id){

        $this->db->select('*');
        $this->db->where('merchandise_id', $id);
        $this->db->where('branch_id', $branch_id);
        $result = $this->db->get($this->table_variant)->row();
        return $result;
    }

    function update_variant($post){
        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table_variant, $post);
        return $id;
    }

    function add($post){

        $this->db->insert($this->table, $post);
        return $this->db->insert_id();
    }
    function get_current_stock( $variant_id ){
        $item = $this->db->select('stock')->where('id',$variant_id)->get('merchandise_variants')->row();
        if( $item ){
            return $item->stock;
        } else {
            return 0;
        }
    }

    function add_variant($post){
        if( !empty($post['id']) ){
            $id = $post['id'];
            unset($post['id']);
            $this->db->where('id', $id); 
            $this->db->update($this->table_variant, $post);
            return $id;
        } else {
            $this->db->insert($this->table_variant, $post);
            return $this->db->insert_id();
        }
        
    }
    function edit_variant($post){
        if( !empty($post['id']) ){
            $id = $post['id'];
            unset($post['id']);
            $this->db->where('id', $id); 
            $this->db->update($this->table_variant, $post);
            return $id;
        } 
    }
    function get_variant($id, $branch_id){
        return $this->db->where('id', $id)->where('branch_id', $branch_id)->get($this->table_variant)->row();
    }

    function add_saleslog($post){

        $this->db->insert($this->table_salestable, $post);
        return $this->db->insert_id();
    }
    function update_sales($post){
        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table_salestable, $post);
    }

    function get_salesby_unix($unix){
        return $this->db->select($this->table_salestable.'.*,'.$this->table.'.item, (SELECT remaining FROM merchandise_summary_log b WHERE b.date = '.$this->table_salestable.'.date AND b.variant_id = '.$this->table_salestable.'.variant_id LIMIT 1 ) as current_stock')
                        ->join($this->table_variant,$this->table_variant.'.id = '.$this->table_salestable.'.variant_id', 'left')
                        ->join($this->table,$this->table.'.id = '.$this->table_variant.'.merchandise_id','left')
                        ->where($this->table_salestable.'.unix', $unix)
                        ->get($this->table_salestable)->result();
    }

    function get_summarylog_item($id, $date){
        $result = $this->db->where('variant_id',$id)->where('date',$date)->get($this->table_summary)->row();
        return $result;
    }

    function update_summary($data){
        if( !empty($data['id']) )
        {
            $id = $data['id'];
            unset($data['id']);
            $this->db->where('id',$id);
            $result = $this->db->update($this->table_summary, $data);
            return $result;

        } else {
            $this->db->insert($this->table_summary,$data);
            return $this->db->insert_id();
        }
    }

    function summary_bydate($date, $branch_id){
        $result = $this->db->select($this->table_variant.".*,".$this->table.".item, ".$this->table_summary.".remaining,".$this->table_summary.".cost as summary_price,  (SELECT SUM(qty_sell) FROM merchandise_dailysales_log b WHERE b.variant_id = 
        ".$this->table_variant.".id AND date='".$date."' LIMIT 1) as qty_sell, (SELECT SUM(stock) FROM merchandise_inventory b WHERE b.variant_id = ".$this->table_variant.".id AND date_added='".$date."' LIMIT 1) as additional")
                            ->join($this->table,$this->table.'.id = '.$this->table_variant.'.merchandise_id','left')
                            ->join($this->table_summary,$this->table_summary.'.variant_id = '.$this->table_variant.'.id','left')
                            ->order_by($this->table.'.item','ASC')
                            ->where($this->table_variant.'.branch_id', $branch_id)
                            ->where($this->table_summary.'.date', $date)
                            ->get($this->table_variant)->result();

        return $result;
    }                       


    function edit($post){

        $id = $post['id'];
        unset($post['id']);
        $this->db->where('id', $id); 
        $this->db->update($this->table, $post);
        return $id;
    
    }

    function delete($id){

        $this->db->where('id', $id)->delete($this->table);
        return $this->db->affected_rows();
    }


}