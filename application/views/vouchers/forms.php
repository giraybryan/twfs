
<?php if(isset($response['result'])): ?>
  <?php if($response['result'] == 1):?>
     <div class="alert alert-success"><?php echo $response['msg']; ?></div>
  <?php else:?>
      <div class="alert alert-danger"><?php echo $response['msg']; ?></div>
  <?php endif;?>
<?php else:?>

<?php endif; ?> 

  <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo !empty($sales->id) ? base_url('sales/edit/'.$sales->id) :  base_url('vouchers/add');?>">
    <div class="form_result"></div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Voucher Code:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" required style="text-transform:uppercase" id="vouchercode" name="vouchercode">
            <input type="hidden" class="form-control"  id="userid" name="userid" value="<?php echo $this->session->userdata['userdata']['userid']; ?>">
        </div>
    </div>
    <div class="form-group">
      <label class="col-lg-4 col-sm-3 control-label">Control Number</label>
      <div class='col-lg-8'>
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1">Starting Number</span>
          <input type="text" class="form-control" required name="startwith" id="startwith" aria-label="...">
        </div>
        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1">How Many Voucher</span>
           <input type="text" class="form-control" required name="timesvoucher" id="timesvoucher" aria-label="...">
        </div>
      </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Voucher Expire:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control datepick" required id="expiration" name="expiration">
            <input type="hidden" class="form-control" id="" name="" value="">
        </div>
    </div>
    M
    <!--
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Middle Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="middle" name="middlename" placeholder="Middle Name">
        </div>
    </div>
    -->
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Class:</label>
        <div class="col-lg-8">
           <select required class="form-control" name="membership_type_id" id="membership_type_id">
             <option value="">Select Promo Class</option>
             <?php foreach( $class_types as $classtype): ?>
              <option value="<?=$classtype->id?>"><?php echo $classtype->class_title?></option>
             <?php endforeach; ?>
           </select>

        </div>
    </div>
    <br/>
    <div id="voucher_list" style="width: 300px; margin: auto;"></div>
    
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
</form>