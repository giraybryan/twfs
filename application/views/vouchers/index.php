<div class="row">
	<div class="col-md-12">
		<div class="panel">
            <ul class="nav nav-tabs">
              <li role="presentation" class=""><a href="<?php echo base_url().'promotions/'?>">Promo</a></li>
              <li role="presentation" class="active"><a href="<?php echo base_url().'vouchers/'?>">Vouchers</a></li>
            </ul>
            <header class="panel-heading">
                Voucher List
            </header>

            <div class="panel-body">
                <div class="box-tools">
                	<a href="<?php echo base_url().'vouchers/add'?>"><button class='btn btn-success btn-sm' data-toggle="modal" data-target="#metermodal">Add Voucher</button></a>
                    <!--
                    <ul class="pagination pagination-sm m-b-10 m-t-10 pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                	-->
                </div>
                <div class="form-horizontal">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group ">
                            <div class="col-sm-8">
                            <label>Filter Voucher Code</label>
                            
                            <input type="text" class="form-control datepick" id="vouchercode" name="vouchercode" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                           
                            <div class="col-sm-4">
                                <label>Filter By Date Range </label>
                                <input type="text" name="daterange" class="form-control" value="" />
                            
                           </div>
                        </div>
                    </div>
                </div>
                </div>
                <div id="module_list">
                	<?php $this->load->view("vouchers/list"); ?>
                </div>
            </div><!-- /.panel-body -->
        </div>
	</div>
</div>

