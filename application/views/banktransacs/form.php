
<?php if(isset($response['result'])): ?>
  <?php if($response['result'] == 1):?>
     <div class="alert alert-success"><?php echo $response['msg']; ?></div>
  <?php else:?>
      <div class="alert alert-danger"><?php echo $response['msg']; ?></div>
  <?php endif;?>
<?php else:?>

<?php endif; ?> 

  <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo !empty($banktransac->id) ? base_url('banktransacs/edit/'.$banktransacs->id) :  base_url('banktransacs/add');?>">
    <div class="form_result"></div>
    <div class="form-group">
      <label class="col-sm-4 control-label">Bank Type</label>
      <div class="col-sm-8">
          <select name="bankid" id="bankid" required class="form-control">
              <option value="">Select Bank type</option>
              <?php
              foreach ($banktypes as $bank):?>
                <option value="<?php echo $bank->id ?>" 
                  <?php 
                  $prevalue = "";
                  if( !empty($_POST['bankid']) ){
                    $prevalue = $_POST['bankid']; 
                  } elseif (!empty($banktransac->bankid) ) {
                    $prevalue = $banktransac->bankid;
                  }
                  echo !empty($prevalue) && $prevalue == $bank->id ? "selected" : "" ?>
                  ><?php echo $bank->bankname ?></option>
                <?php endforeach;?>
          
            </select>
            <div id="current_balance"></div>
      </div>
    </div>
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Date Transaction:</label>
        <div class="col-lg-8">
          <?php 
              $prevalue = "";
              if( !empty($_POST['date']) ){
                $prevalue = $_POST['date'];
              } elseif ( !empty($banktransac->date) ) {
                $prevalue = date('m/d/Y',strtotime($banktransac->date));
              }
              
          ?>
            <input type="text" class="form-control datepick" id="date" required name="date" placeholder="Date" value="<?php echo !empty($prevalue) ? $prevalue : date('m/d/Y') ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Bank Transaction Type:</label>
        <div class="col-lg-8">
            <?php 
              $prevalue = "";
              if( !empty($_POST['transaction_type']) ){
                $prevalue = $_POST['transaction_type'];
              } elseif ( !empty($banktransac->transaction_type) ) {
                $prevalue = $banktransac->transaction_type;
              }
              
            ?>
            <select name="transaction_type" id="transaction_type" required class="form-control">
              <option value="">Select Sales type</option>
              <option value="deposit" <?php echo!empty($prevalue) && $prevalue == "deposit" ? "selected" : "" ?>>Deposit</option>
              <option value="withdraw" <?php echo!empty($prevalue) && $prevalue == "withdraw" ? "selected" : "" ?>>Withdraw</option>
              <option value="transfer" <?php echo!empty($prevalue) && $prevalue == "transfer" ? "selected" : "" ?>>Transfer</option>
            </select>
           
            <input type="hidden" class="form-control" id="id" name="id" value="<?php echo !empty($banktransac->id) ? $banktransac->id : '' ?>">
            <input type="hidden" class="form-control" id="userid" name="userid" value="<?php echo $this->session->userdata['userdata']['userid']; ?>">
            <div id="current_coh"></div>
        </div>
    </div>
    <!--
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Middle Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="middle" name="middlename" placeholder="Middle Name">
        </div>
    </div>
    -->
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Amount:</label>
        <div class="col-lg-8">
          <?php 
              $prevalue = "";
              if( !empty($_POST['amount']) ){
                $prevalue = $_POST['amount'];
              } elseif ( !empty($banktransac->amount) ) {
                $prevalue = $banktransac->amount;
              }
              
          ?>
            <input type="number" class="form-control" required step="any" step="0.01" name="amount" value="<?php echo !empty($prevalue) ? $prevalue : '' ?>"/>
        </div>
    </div>
    
  
   
  
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
</form>