<div class="table-responsive">
<table class="table">
    <thead>
    	<tr>
            <th style="width: 10px">ID</th>
            <th>Bank</th>
            <th>Deposit</th>
            <th>Withdraw</th>
            <th>Balance</th>
            <th>Action</th>
    	</tr>
    </thead>
    <?php if( !empty($banktransacs) ): ?>
        <tbody>
        <?php $total_balance = 0; ?>
        <?php foreach($banktransacs as $bank):?>
            <tr>
                <td><?php echo $bank->bankid ?></td>
                <td><?php echo $bank->bankname ?></td>
                <?php if($bank->transaction_type == "deposit"): ?>
                <td>
                    <?php echo $bank->amount?>
                    <?php 
                    $total_balance  = $total_balance + $bank->amount;
                    ?>
                </td>
                <td></td>
                <?php else: ?>
                <td></td>
                <td>
                    <?php echo $bank->amount?>
                    <?php 
                    $total_balance  = $total_balance + $bank->amount;
                    ?>
                </td>
                <?php endif; ?>
                <td></td>
                <td data-encode='<?php echo json_encode($bank)?>'>
                   
                    <button class="btn btn-primary btn-sm action-btn hide" data-action='add'><i class="fa fa-plus"></i></button>

                    
                   <a href="<?php echo base_url('banks/edit/'.$bank->bankid.'/'.$bank->bankid) ?>">
                	<button class="btn btn-warning btn-sm action-btn" data-action="edit"><i class="fa fa-pencil"></i>Edit</button>
                        </a>
                	<button class="btn btn-danger btn-sm action-btn hide" data-action="delete" ><i class="fa fa-times"></i></button>
                </td>
            </tr>
        <?php endforeach;?>
    </tbody>
    <tfoot>
        <tr>
            <td></td>
           
            <td align="right"><strong>Total</strong></td>
            <td><?php echo $total_balance; ?></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tfoot>
	<?php else: ?>
    <tbody>
        <tr>
			<td colspan='4'>No records</td>
		</tr>
    </tbody>
    <?php endif; ?>
	
</table>
</div>