<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <header class="panel-heading">Member's Info</header>
            <div class="panel-body">
                <a href="<?php echo base_url('members');?>" class="btn btn-warning"><i class='fa fa-angle-left'></i> Back to Member List</a>
                <br/><br/>
                <?php 
                //print_r($member);
                ?>
                <?php 
                if( !empty($error))
                    //print_r($error);
                ?>
                <form method="post" style="max-width: 700px; " action="<?php echo base_url('members/info/'.$member->id) ?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-3 col-xs-12">
                            <div class="img-wrapper">
                            <?php if( !empty($member->image)): ?>
                                <img src="<?php echo base_url('uploads/members/'.$member->image)?>" class="img-responsive">
                            <?php else:?>

                            <?php endif;?>
                            <button class='btn btn-warning' id="uploadimage"><i class='fa fa-pencil'></i> Upload</button>
                            <input type="file" class='hide' name="userfile" id="userfile" value="" />
                            <input type="submit" class="btn btn-default" />
                            </div>
                            <br/>
                        </div>
                        <div class="col-sm-9">
                            
                            <div class="form-horizontal">
                            <?php if( $annual_freezectr < 3 ): ?>
                                 <div class="alert alert-info">
                                    <label>Annual Freeze Request: <?php echo !empty($annual_freezectr) ? $annual_freezectr : 0 ?></label>
                                 </div>
                                 <?php if( !empty($member->membership_log_id)): ?>
                                    <span class="btn btn-info freeze" data-toggle="tooltip" title="Freeze" data-id="<?php echo $member->id ?>"><i class="fa fa-lock" aria-hidden="true"></i> Freeze Account</span>
                                <?php endif; ?>
                            <?php else: ?>
                                <div class="alert alert-danger">
                                    <label>Annual Freeze Request: <?php echo $annual_freezectr ?></label>
                                 </div>
                            <?php endif; ?>
                           
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Basic Info</a></li>
                                    <li role="presentation"><a href="#subscription" aria-controls="subscription" role="tab" data-toggle="tab">Subscription</a></li>
                                    <li role="presentation" class=''><a href="#history" aria-controls="attendance" role="attendance" data-toggle="tab">Subscription History</a></li>
                                    <li role="presentation" class='hide'><a href="#attendance" aria-controls="attendance" role="attendance" data-toggle="tab">Attendance</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="info">
                                        <?php $this->view('members/info_form')?>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="subscription">
                                        <?php $this->view('members/subscription')?>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="history">
                                        <?php $this->view('members/history')?>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="attendance">
                                        <?php $this->view('members/attendance')?>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                    
                </form>
            </div>
        </div>
    </div>
</div>
<style>
.img-wrapper {
    position: relative;border: 1px solid #ddd; background: #eee; padding: 5px; border-radius: 8px; max-width: 200px; margin: auto; min-height: 100px;
}
.img-wrapper img { display: block; margin: auto;}
.img-wrapper [type="submit"] { display: none; }
#uploadimage { position: absolute; bottom: 0px; z-index: 10; right: 0px;}
</style>

<div class="modal fade" id="freeze_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Freeze Account</h4>
      </div>
      <div class="modal-body form-horizontal">
        <div class="container"><p>Member's Account may be frozen for up to 3 months (Annual consumable)</p></div>
        <div class="form-group">
            <label class="col-sm-3">Months Freeze</label>
            <div class="col-sm-6">
                <select class="form-control" name="month_freeze" id="month_freeze">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3">Start Date</label>
            <div class="col-sm-6">
                <input type="text" name="start_date" value="<?php echo date('m/d/Y'); ?>" class="datepick form-control" id="start_date"/>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-save save_freeze">Save changes</button>
      </div>
    </div>
  </div>
</div>