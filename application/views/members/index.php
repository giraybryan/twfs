<div class="row">
	<div class="col-md-12">
		<div class="panel">
            <header class="panel-heading">
                Member List
            </header>
            <div class="panel-body">
                <div class="box-tools">
                    <div class="row">
                        <div class="col-xs-4">
                	       <a href="<?php echo base_url().'members/add'?>"><button class='btn btn-success btn-sm' data-toggle="modal" data-target="#metermodal">Add Member</button></a>
                        </div>
                        <div class="col-xs-4">

                            

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            <select class="form-control" name="filter_class" id="filter_class">
                                <option value="">Select By Class</option>
                                <?php foreach($classes as $class): ?>
                                    <option value="<?php echo $class->id?>"><?php echo $class->class_title ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <select class="form-control" name="durations" id="durations">
                                <option value="">Select By Duration</option>
                                <?php foreach($durations as $duration):?>
                                <option value="<?php echo $duration->id ?>">
                                    <?php 
                                    if($duration->durations > 1):
                                        $plural = "s";
                                    else:
                                        $plural = "";
                                    endif;
                                    if($duration->duration_type == "monthly"): 
                                        echo $duration->durations.' Month'.$plural; 
                                    else: 
                                        echo $duration->durations.' Session'.$plural;
                                    endif; ?>
                                </option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">

                            <input type="text" class="form-control" id="membersearch" name="member_namesearch" placeholder="Member Name Search">
                            <span class="input-group-addon btn btn-default" id="simple_go" id="basic-addon1">Go</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            Report Summary By : 
                            <div class="btn-group" role="group" aria-label="...">
                                <button type="button" data-status="active" class="summary btn btn-default">Active</button>
                                <button type="button" data-status="expired"class="summary btn btn-danger">Expired</button>
                                <button type="button" data-status="freezed" class="summary btn btn-primary">Freezed</button>
                                 <?php 
                                if( $config_membershiptype == "annual"):
                                ?>
                                <button type="button" data-status="membership" class="summary btn btn-warning">Expired Membership</button>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                    <!--
                    <ul class="pagination pagination-sm m-b-10 m-t-10 pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                	-->
                </div>
                <div id="module_list">
                	<?php $this->load->view("members/list"); ?>
                </div>
            </div><!-- /.panel-body -->
        </div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="report" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Summary Report</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>