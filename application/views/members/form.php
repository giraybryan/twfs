
<?php if(isset($response['result'])): ?>
  <?php if($response['result'] == 1):?>
     <div class="alert alert-success"><?php echo $response['msg']; ?></div>
  <?php else:?>
    sdfsadf
      <div class="alert alert-danger"><?php echo $response['msg']; ?></div>
  <?php endif;?>
<?php else:?>

<?php endif; ?> 
  <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo !empty($member->id) ? base_url('members/edit/'.$member->id) :  base_url('members/add');?>">
    <div class="form_result"></div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">First Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name" value="<?php echo !empty($member->firstname) ? $member->firstname : '' ?>">
            <input type="hidden" class="form-control" id="id" name="id" value="<?php echo !empty($member->id) ? $member->id : '' ?>">
        </div>
    </div>
    <!--
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Middle Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="middle" name="middlename" placeholder="Middle Name">
        </div>
    </div>
    -->
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Last Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name" value="<?php echo !empty($member->lastname) ? $member->lastname : '' ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Address:</label>
        <div class="col-lg-8">
            <textarea name="address" placeholder="Address"><?php echo !empty($member->address) ? $member->address : '' ?></textarea>
        </div>
    </div>
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Birth Date:</label>
        <div class="col-lg-8">
           <input type="text" class="form-control datepick" name="bd" id="bd" value="<?php echo !empty($member->bd) ? date('m/d/Y',strtotime($member->bd)) : '' ?>">
        </div>
    </div>
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Email:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo !empty($member->email) ? $member->email : '' ?>">
        </div>
    </div>
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Number:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" value="<?php echo !empty($member->mobile) ? $member->mobile : '' ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Gender:</label>
        <div class="col-lg-8">
            <select name="gender" class="form-control" >
              <option value="" >Select Gender</option>
              <option value="m" <?php echo !empty($member->gender) && $member->gender == 'm' ? 'selected' : '' ?>>Male</option>
              <option value="f" <?php echo !empty($member->gender) && $member->gender == 'f' ? 'selected' : '' ?>>Female</option>
            </select>
        </div>
    </div>
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Referral By:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="referralby" placeholder="Member Name" value="<?php echo !empty($member->referral_id) ? $member->referral_id : '' ?>">
            <div id="member_searchresult"></div>
            <div id="customername" style="padding-top: 10px;"></div>
            <input type="hidden" id="referral_id" name="referral_id">
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
</form>