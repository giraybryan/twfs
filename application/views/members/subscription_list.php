<?php  
    $date = ('2020-03-30');
    $date = date('Y-m-d');
    $where = array(
        'member_id'  => $member->id,
        'start_date <=' => $date,
        'end_date  >=' => $date
    );
    
    $latest_freeze = $this->memberships_freeze_model->get($where);
    $config_membership = $this->config_model->get_valueby_code('membershiptype'); 

    $this->db->select('membership_log.*,class_type.class_title,membership_type.price, class_duration.duration_type,  class_duration.durations as duration_count');
    $this->db->where('membership_log.member_id', $member->id);
    $this->db->where('membership_log.date_end >=', date('Y-m-d') );
    $this->db->join('class_type','class_type.id = membership_log.class_type_id', 'left');
    $this->db->join('membership_type','membership_type.id= membership_log.membership_type_id', 'left');
    $this->db->join('class_duration','class_duration.id =membership_type.duration_id', 'left');
    $this->db->order_by('membership_log.id','DESC');
    $memberships = $this->db->get('membership_log')->result();
        ?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>
                       Subscription
                    </th>
                    <th>
                        Status
                    </th>
                    <th>
                        Duration
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if ( !empty($member->annual_membership) ):
                ?>
                <tr>
                    <td> Annual Membership Expiration: </td>
                    <?php 
                    $expiration_membership = date('Y-m-d', strtotime($member->annual_membership." + 1 year"));
                    ?>
                    <td> 
                        <?php if ($expiration_membership <= date('Y-m-d')): ?>
                            <div class='alert alert-danger'>Expired</div>
                        <?php else: ?>
                            Valid
                        <?php endif; ?>
                    </td>
                    <td> 
                        <?php echo date('M d, Y', strtotime($expiration_membership)) ?>
                        <span data-membership="" data-member='"<?php echo $member->id?>"' class='hide btn btn-default setatten'> Count Attendance</span><br/>
                    </td>
                </tr>
                <?php
                endif;
                if( $memberships ):
                    foreach($memberships as $membership ):
                ?>
                <tr>
                    <td>
                        <?php echo $membership->class_title  ?>
                        <?php echo "<br/>Duration :".$membership->duration_count.' ('.$membership->duration_type.')' ?>
                    </td>
                    <td><?php echo "Valid" ?></td>
                    <td class='text-center'>
                        <?php echo date('M d, Y', strtotime($membership->date_start) ).' <br/> to <br/> '.date('M d, Y', strtotime($membership->date_end)) ?>
                        <?php if( $this->uri->segment(1) == 'clientlog' ):  ?>
                        <div></div>
                        <span data-membership="<?php echo $membership->id ?>" data-member='"<?php echo $member->id?>"' class='hide btn btn-default setatten'> Count Attendance</span><br/><br/>
                        <?php endif; ?>

                    </td>
                </tr>
                <?php 
                 endforeach;
                else: 
                    //get the latest expired subscription
                    $this->db->select('membership_log.*,class_type.class_title,  class_duration.duration_type,  class_duration.durations as duration_count');
                    $this->db->where('membership_log.member_id', $member->id);
                    $this->db->join('class_type','class_type.id = membership_log.class_type_id', 'left');
                    $this->db->join('membership_type','membership_type.id= membership_log.membership_type_id', 'left');
                    $this->db->join('class_duration','class_duration.id = membership_type.duration_id', 'left');
                    $this->db->order_by('membership_log.id','DESC');
                    $membership = $this->db->get('membership_log')->row();
                    if( $membership ):
                    ?>
                    <tr>
                        <td>
                            <?php echo $membership->class_title  ?>
                            <?php echo "<br/>Duration :".$membership->duration_count.' ('.$membership->duration_type.')' ?>
                        </td>
                        <td><?php echo "<div class='alert alert-danger'>Expired</div>"?></td>
                        <td class='text-center'><?php echo date('M d, Y', strtotime($membership->date_start) ).' <br/> to <br/> '.date('M d, Y', strtotime($membership->date_end)) ?></td>
                    </tr>
                    <?php
                    endif;

                endif;
                ?>
            </tbody>
        </table>