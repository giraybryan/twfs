<br/>
<div class="form-group">
    <label class="col-sm-3 control-label">Status: </label>
    <label class="col-sm-6 <?php echo !empty($latest_freeze) ? " Alert alert-info" : "" ?>" style="margin-top: 6px;"><?php echo !empty($latest_freeze) ? "Frozen until ".date('M d, Y', strtotime($latest_freeze->end_date)) : "Active" ?></label>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Membership Card</label>
    <div class="col-sm-9">
        <div class="input-group">
            <input type="text" id="card_id" class="form-control" readonly value="<?php echo $member->card_id?>">
            <span class="btn btn-success assign_card input-group-addon"  id="assign_card" onclick="get_newcard()" data-toggle="tooltip" title="assign" data-id="<?php echo $member->id ?>"> Assign Card</span>
           
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">First Name</label>
    <div class="col-sm-9"><input type="text" class="form-control info-edit" data-field="firstname" name="firstname" value="<?php echo $member->firstname?>" id="firstname"/>
<input type='hidden' name='member_id' id='member_id' value='<?php echo $member->id?>' />
</div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Last Name</label>
    <div class="col-sm-9"><input type="text" class="form-control info-edit" data-field="lastname" name="lastname" value="<?php echo $member->lastname?>" id="lastname"/></div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Email</label>
    <div class="col-sm-9"><input type="email" class="form-control info-edit" data-field="email" name="email" value="<?php echo $member->email?>" id="email"/></div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Mobile No:</label>
    <div class="col-sm-9"><input type="email" class="form-control info-edit" data-field="mobile" name="mobile" value="<?php echo $member->mobile?>" id="mobile"/></div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Address:</label>
    <div class="col-sm-9"><textarea type="email" class="form-control info-edit" data-field="address" name="address" id="address"><?php echo $member->address?></textarea></div>
</div>
<div>
<br/>
<h5 class="text-center">Password</h5>
<?php if( !empty($member->password)): ?>
<div class="form-group">
    <label class="col-sm-3 control-label">Old Password:</label>
    <div class="col-sm-9"><input type="password" name="old_pass" required id="old_pass" class="form-control" placeholder="Old Password" /></div>
</div>
<?php endif; ?>
<div class="form-group">
    <label class="col-sm-3 control-label">New Password:</label>
    <div class="col-sm-9"><input type="password" name="new_pass" id="new_pass" required class="form-control" placeholder="New Password" /></div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Confirm New Password:</label>
    <div class="col-sm-9"><input type="password" name="confirm_pass" required id="confirm_pass" class="form-control" placeholder="Confirm New Password" /></div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label"></label>
    <div class="col-sm-9"><span class="btn-primary btn" id="update_pass">UPDATE PASSWORD</span></div>
</div>
</div>

<script type="text/javascript">
    var latest_card = "<?php echo $latest_card->process == "register" ? $latest_card->description : "" ?>";
    var unix = "<?php echo $latest_card->unix ?>";
    function get_newcard()
    {
        $('#assign_card').html('assigning...')
        $.ajax({
            type: "POST",
            url : "<?php echo base_url('members/get_latest_registeredcard'); ?>",
            data : { card : latest_card,unix : unix,  member_id : <?php echo $member->id ?> },
            dataType: "json",
            success: function(response){
                console.log(response)
                if( response.result == 1){
                    latest_card =response.card;
                    $('#card_id').val(latest_card);
                    alertify.success('Card Registered');

                    $('#assign_card').html('Assign Card');
                } else {
                    $('#assign_card').html('Fetching...');
                    setTimeout(function(){
                        get_newcard();
                    },1500)
                }
            }

        })
    }
   

    
</script>