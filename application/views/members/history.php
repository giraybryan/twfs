<br/>
<div class="form-group">
   <div class="col-sm-12">
    <div style='margin-top: 8px;'>
       <div class="form-group">
        <label class="col-sm-12">Total Annual Freeze Counter: <span id="annual_freeze"><?php echo $annual_freezectr; ?></span> </label>
       </div>
       <table class="table table-striped">
       <thead>
        <tr>
            <th>Subscription</th>
            <th>Action</th>
        </tr>
       </thead>
        <tbody>
        <?php if( !empty($histories) ): ?>
            <?php foreach($histories as $history):?>
                <tr>
                    <td><div style="margin-bottom: 5px;"><?php echo $history->membership_type ?> : <?php echo $history->amount?>php
                        <?php if( $history->note != ""): ?>
                        <p>Note : 
                            <div class="editnote">
                                <div class="note_display">
                                    <span class="historynote"><?php echo $history->note; ?></span> <i class="edit-note fa fa-pencil" ></i></p>
                                </div>
                                <div class="note_edit hide">
                                    <textarea class="form-control notefield" style="margin-bottom: 5px;" data-id="<?php echo $history->note ?>"><?php echo $history->note; ?></textarea>
                                    <span class="btn btn-primary save-note">Save Changes</span>
                                </div>
                                <br/>
                            </div>
                        <?php endif; ?>
                            
                    </div>
                        <div style="margin-bottom: 5px;"><strong>Start </strong> <?php echo date('m/d/Y ', strtotime($history->date_start)) ?></div>
                        <div style="margin-bottom: 5px;"><strong>End </strong> <?php echo date('m/d/Y ', strtotime($history->date_end)) ?></div>
                    </td>
                    <td>
                        <?php if( $history->date_end < date('Y-md') ) : ?>
                        <span class="btn btn-warning" data-toggle="tooltip" title="edit" data-id="<?php echo $history->id ?>"><i class="fa fa-pencil"></i></span>
                        <?php endif; ?>
                        
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
       </table>
    </div>
   </div>
</div>


