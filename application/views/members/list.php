Total Result : <?php echo $total; ?>
 <?php echo $paginate; ?>
<div class="table-responsive">
<table class="table">
    <thead>
    	<tr>
            <th style="width: 10px">ID</th>
            <th>Member Name</th>
            <th>Contact</th>
            <th>Membership</th>
            <th>Note</th>
            <th>Action</th>
    	</tr>
    </thead>
    <tbody>
    <?php if( !empty($members) ): ?>
        <?php foreach($members as $member):?>
            <tr>
                <td><?php echo $member->id ?></td>
                <td><?php echo $member->firstname.' '.$member->lastname; ?></td>
                <td><?php echo $member->mobile; ?></td>
                <td class="">
                    <?php if( !empty($member->class_type_id) ): ?>
                     <?php $this->view('members/subscription_list', array('member' => $member)); ?> 
                    <?php endif; ?>
                </td>
                <td><?php echo !empty($member->note) ? $member->note : ''   ?></td>
                <td>
                    <a class="btn btn-info" href="<?php echo base_url('members/info/'.$member->id)?>"><i class="fa fa-eye"></i> View</a>
                    <?php if( !empty($member->class_type_id) ): ?>
                        <?php if( $member->expire_date < date('Y-m-d') && date('Y-m-d', strtotime($member->expire_date)) != '1970-01-01' ):  ?>
                        <a href="<?php echo base_url('memberships/add/'.$member->id)?>"><button class="btn btn-primary btn-sm action-btn" data-action='add'>Renew</button></a>
                        <?php else: ?>
                            <a href="<?php echo base_url('memberships/add/'.$member->id)?>" class="btn btn-primary">Add Subscription</a>
                        <?php endif;?>
                    <?php else: ?>
                        <a href="<?php echo base_url('/memberships/add/'.$member->id.'?new=1')?>" class="btn btn-primary">Add Subscription</a>
                    <?php endif; ?>

                    <?php if( empty($member->annual_membership) OR (date('Y-m-d',strtotime($member->annual_membership." +1 year") ) <= date('Y-m-d')) ): ?> 
                         <a href="<?php echo base_url('/memberships/membership_fee/'.$member->id)?>" class="btn btn-warning">Add Membership</a>
                    <?php endif; ?>
                    
                    
                
                </td>
            </tr>
        <?php endforeach;?>
	<?php else: ?>
		<tr>
			<td colspan='4'>No records</td>
		</tr>
    <?php endif; ?>
	</tbody>
</table>
</div>
