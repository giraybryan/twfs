<h5><strong>Total <?php echo $status; ?></strong>: <span id="total"></span></h5> 
<div style="max-height: 250px; overflow: auto;">
<table class="table table-striped">
    <thead>
        <tr>
            <th>Name</th>
            <th>Last Subscription</th>
            <th>Number</th>
        </tr>
    </thead>
<?php
 $ctr = 0; 
if( !empty($members) ): ?>
    <?php
       
        foreach($members as $member):
            if( $status == "membership"):
            $ctr++;
            ?>
              <tr>
                <td><?php echo $member->firstname.' '.$member->lastname ?></td>         
                <td style="max-width: 200px">
                <?php echo !empty( $member->title ) ?  $member->title.' <br/>': ''?>
                <?php echo $member->date_end ?></td>         
                <td><?php echo $member->mobile ?></td>         
            </tr>
            <?php     
            
            elseif( !empty($member->latest_log ) && $member->latest_log == $member->logid ): 
            $ctr++;
            ?>
              <tr>
                <td><?php echo $member->firstname.' '.$member->lastname ?></td>         
                <td style="max-width: 200px">
                <?php echo !empty( $member->title ) ?  $member->title.' <br/>': ''?>
                <?php echo $member->date_end ?></td>         
                <td><?php echo $member->mobile ?></td>         
            </tr>
            <?php
        else:
            echo '<br/>'.$member->latest_log.' sales_type_id :'.$member->sales_type_id.', logid'.$member->logid.' <br/>';
        endif;
    endforeach;
endif;
?>
 </table>
 </div>

 <script>
 document.getElementById("total").innerHTML = "<?php echo $ctr ?>"
 </script>