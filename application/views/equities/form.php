
<?php if(isset($response['result'])): ?>
  <?php if($response['result'] == 1):?>
     <div class="alert alert-success"><?php echo $response['msg']; ?></div>
  <?php else:?>
      <div class="alert alert-danger"><?php echo $response['msg']; ?></div>
  <?php endif;?>
<?php else:?>

<?php endif; ?> 
  
  <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo !empty($equity->id) ? base_url('equities/edit/'.$equity->id) :  base_url('equities/add');?>">
    <div class="form_result"></div>
    <div class="form-group">
      <div class="col-sm-4"></div>
      <div class="col-sm-8"><h3>Equity / Share</h3></div>
    </div>
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Date:</label>
        <div class="col-lg-8">
          <?php 
              $prevalue = "";
              if( !empty($_POST['date']) ){
                $prevalue = $_POST['date'];
              } elseif ( !empty($equity->date) ) {
                $prevalue = date('m/d/Y',strtotime($equity->date));
              }
              
          ?>
            <input type="text" class="form-control datepick" id="date" required name="date" placeholder="Date" value="<?php echo !empty($prevalue) ? $prevalue : date('m/d/Y') ?>">
            <input type="hidden" name="eqid" value="<?php echo !empty($equity->eqid) ? $eqid : '' ?>" >
        </div>
    </div>
   
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Amount:</label>
        <div class="col-lg-8">
          <?php 
              $prevalue = "";
              if( !empty($_POST['amount']) ){
                $prevalue = $_POST['amount'];
              } elseif ( !empty($equity->amount) ) {
                $prevalue = $equity->amount;
              }
              
          ?>
            <input type="number" class="form-control" required step="any" step="0.01" name="amount" value="<?php echo !empty($prevalue) ? $prevalue : '' ?>"/>
        </div>
    </div>
    
  
   
  
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
</form>