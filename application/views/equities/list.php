<div class="row">
    <div class="col-sm-7">
        <div class="table-responsive">
        <table class="table">
            <thead>
            	<tr>
                    <th style="width: 10px">ID</th>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Action</th>
            	</tr>
            </thead>
            <?php if( !empty($equities) ): ?>
                <tbody>
                <?php $total_equities = 0; ?>
                <?php foreach($equities as $equity):?>
                    <tr>
                        <td><?php echo $equity->eqid ?></td>
                        <td><?php echo date('d-M-Y',strtotime( $equity->date ))?></td>
                        <td>
                            <?php echo $equity->amount?>
                            <?php 
                            $total_equities  = $total_equities + $equity->amount;
                            ?>
                        </td>
                        <td data-encode='<?php echo json_encode($bank)?>'>
                           
                            <button class="btn btn-primary btn-sm action-btn hide" data-action='add'><i class="fa fa-plus"></i></button>

                            
                           <a href="<?php echo base_url('equities/edit/'.$equity->eqid) ?>">
                        	<button class="btn btn-warning btn-sm action-btn" data-action="edit"><i class="fa fa-pencil"></i>Edit</button>
                                </a>
                        	<button class="btn btn-danger btn-sm action-btn hide" data-action="delete" ><i class="fa fa-times"></i></button>
                        </td>
                    </tr>
                <?php endforeach;?>
            </tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td align="right"><strong>Total</strong></td>
                    <td><?php echo $total_equities; ?></td>
                    <td></td>
                    <td></td>
                </tr>
            </tfoot>
        	<?php else: ?>
            <tbody>
                <tr>
        			<td colspan='4'>No records</td>
        		</tr>
            </tbody>
            <?php endif; ?>
        	
        </table>
        </div>

     </div>               
    <div class="col-sm-5">
        <table class="table table-striped">
            <thead>
                <th>Name</th>
                <th>Percentage</th>
                <th>Returned</th>
            </thead>
            <tbody>
                <?php 
                $returned = 0;
                foreach($owners as $owner):
                    ?>
                    <tr>
                        <td><?php echo $owner->name ?></td>
                        <td><?php echo $owner->real_rate * 100 ?>%</td>
                        <td>
                            <?php 
                            $row_total = $total_equities * $owner->real_rate;
                            $returned += $row_total;
                            echo number_format($row_total,2)
                            ?></td>
                    </tr>
                    <?php
                endforeach;
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td>total</td>
                    <td><?php echo number_format($returned,2) ?></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>