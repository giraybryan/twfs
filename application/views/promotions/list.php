<div class="table-responsive">
<table class="table">
    <thead>
    	<tr>
            <th>Voucher Code</th>
            <th>Control Number</th>
            <th>Class</th>
            <th>Used</th>
            <th>Expiration</th>
            <th>Action</th>
    	</tr>
    </thead>
   
    <?php if( !empty($vouchers) ): ?>
        <tbody>
        <?php $total_vouchers = 0; ?>
        <?php foreach($vouchers as $_voucher):?>
            <tr>
                <td><?php echo $_voucher->vouchercode ?></td>
                <td><?php echo $_voucher->controlnum ?></td>
                <td>
                    <?php 
                    echo $_voucher->controlnum;
                    ?>
                </td>
                <td><?php 
                echo $_voucher->used == 0 ? "Available" : "Used";
                 ?></td>
                }
                <td><?php echo $_voucher->expiration ?></td>
                
                <td data-encode='<?php echo json_encode($_voucher)?>'>
                   
                    <button class="btn btn-primary btn-sm action-btn hide" data-action='add'><i class="fa fa-plus"></i></button>

                    <a href="<?php echo base_url('vouchers/edit/'.$_voucher->id) ?>">
                	<button class="btn btn-warning btn-sm action-btn" data-action="edit"><i class="fa fa-pencil"></i></button>
                        </a>
                	<button data-id="<?php echo $_voucher->id ?>" data-table="vouchers" class="btn btn-danger btn-sm action-btn" data-action="delete" ><i class="fa fa-times"></i></button>
                </td>
            </tr>
           
        <?php endforeach;?>
    </tbody>
	<?php else: ?>
    <tbody>
        <tr>
			<td colspan='4'>No records</td>
		</tr>
    </tbody>
    <?php endif; ?>
	
</table>
</div>