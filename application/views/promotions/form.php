
<?php if(isset($response['result'])): ?>
  <?php if($response['result'] == 1):?>
     <div class="alert alert-success"><?php echo $response['msg']; ?></div>
  <?php else:?>
      <div class="alert alert-danger"><?php echo $response['msg']; ?></div>
  <?php endif;?>
<?php else:?>

<?php endif; ?> 

  <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo !empty($sales->id) ? base_url('sales/edit/'.$sales->id) :  base_url('sales/add');?>">
    <div class="form_result"></div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Sales Type:</label>
        <div class="col-lg-8">
            <select name="sales_type_id" id="sales_type_id" required class="form-control">
              <option value="">Select Sales type</option>
              <?php if( !empty($types)):?>
                <?php foreach($types as $type):?>
                  <option value="<?php echo $type->id ?>" <?php echo !empty($sales->sales_type_id) && $sales->sales_type_id == $type->id ? "selected" : "" ?>><?php echo $type->sales_type ?></option>
                <?php endforeach;?>
              <?php endif;?>
            </select>
           
            <input type="hidden" class="form-control" id="id" name="id" value="<?php echo !empty($sales->id) ? $sales->id : '' ?>">
            <input type="hidden" class="form-control" id="userid" name="userid" value="<?php echo $this->session->userdata['userdata']['userid']; ?>">

        </div>
    </div>
    <!--
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Middle Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="middle" name="middlename" placeholder="Middle Name">
        </div>
    </div>
    -->
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Description:</label>
        <div class="col-lg-8">
           <textarea class="form-control" name="description"><?php echo !empty($sales->description) ? $sales->description : '' ?></textarea>
        </div>
    </div>
    
   
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Amount:</label>
        <div class="col-lg-8">
            <input type="number" class="form-control" id="amount"  required name="amount" placeholder="amount" value="<?php echo !empty($sales->amount) ? $sales->amount : '' ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Date Received:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control datepick" id="date" required name="date" placeholder="Date" value="<?php echo !empty($sales->date) ? date('m/d/Y',strtotime($sales->date)) : date('m/d/Y') ?>">
        </div>
    </div>
  
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
</form>