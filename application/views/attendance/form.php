
<?php if(isset($response['result'])): ?>
  <?php if($response['result'] == 1):?>
     <div class="alert alert-success"><?php echo $response['msg']; ?></div>
  <?php else:?>
    sdfsadf
      <div class="alert alert-danger"><?php echo $response['msg']; ?></div>
  <?php endif;?>
<?php else:?>

<?php endif; ?> 
  <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo base_url('attendance/add');?>">
    <div class="form_result"></div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Attendance Type:</label>
        <div class="col-lg-8">
            <select name="attendance_type" id="attendance_type" required class="form-control">
              <option value="">Select Type</option>
              <option value="member">Member</option>
              <option value="walkin">Walk-in</option>
            </select>
        </div>
    </div>
    <input type="hidden" value="" name="member_id" id="member_id" />
    <input type="hidden" value="" name="walkin_id" id="walkin_id" />
     

     <div class="form-group memberform formtype hide">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Name Search:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="membersearch" name="member_namesearch" placeholder="Member Name Search">
            <div id="member_searchresult" class="" style="background: #fff;">
              
            </div>
        </div>
    </div>

    
    <div class="form2 hide">  
      <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Customer Name:</label>
        <div class="col-lg-8" id="customername">
            
        </div>
      </div>
      <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Valid Existing Mebership:</label>
        <div class="col-lg-8" id="membership_log_result">
            
        </div>
      </div>
      <div class="form-group hide">
          <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">MemberShip Status:</label>
          <div class="col-lg-8" id="memberstatus">
             
          </div>
      </div>
      <div class="form-group walkinform hide">
          <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label"></label>
          <div class="col-lg-8">
              <input type="checkbox" name="paid" value="1"> Paid:
          </div>
      </div>
        
    </div>
    <div class="form-group">
          <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Class Type</label>
          <div class="col-lg-8">
        
              <select name="class_type_id" required class="form-control" >
                <option value="">Select Class Type</option>
                <?php if (!empty($class_types)):?>
                  <?php foreach($class_types as $class_type): ?>
                    <option value="<?php echo $class_type->id?>" <?php echo (!empty($_POST['class_type_id']) && $_POST['class_type_id']  == $class_type->id ? "selected" : "") ?>>
                      <?php echo $class_type->class_title ?>
                    </option>
                  <?php endforeach; ?>
                <?php endif;?>
              </select>
          </div>
      </div>
       <div class="walkinform formtype hide">
      
       <div class="form-group">
          <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Description:</label>
          <div class="col-lg-8">
              <input type="text" class="form-control" id="lastname" name="description" placeholder="">
          </div>
      </div>
      
    
          <div class="form-group">
            <label class="col-lg-4 control-label">Amount</label>
            <div class="col-lg-8">
              <input type="" readonly="" value="" name="amount" class="form-control">
            </div>
          </div>
      </div>
      <div class="form-group">
          <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Date</label>
          <div class="col-lg-8">
            <input type="text" class="datepick form-control" name="date"  value="<?php echo (!empty($_POST['date']) ? $_POST['date']: date('m/d/Y')) ?>" />
          </div>
      </div>
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
</form>