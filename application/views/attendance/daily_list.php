<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Time In</th>
		</tr>
	</thead>
	<tbody>
		<?php if( !empty($attendance)): ?>
			<?php foreach ($attendance as $member): ?>
			<tr>
				<td>
					<?php echo $member->firstname.' '.$member->lastname; ?>
				</td>
				<td> <?php echo date('F d,Y h:i', strtotime($member->date))?></td>
			</tr>
			<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="2">
					<div class="alert alert-warning">No Record</div>
				</td>
			</tr>
		<?php endif; ?>
	</tbody>
</table>