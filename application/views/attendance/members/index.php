<div class="row">
	<div class="col-md-12">
		<div class="panel">
            <header class="panel-heading">
                Member List
            </header>
            <div class="panel-body">
                <div class="box-tools">
                	<a href="<?php echo base_url().'members/add'?>"><button class='btn btn-success btn-sm' data-toggle="modal" data-target="#metermodal">Add Member</button></a>
                    <!--
                    <ul class="pagination pagination-sm m-b-10 m-t-10 pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                	-->
                </div>
                <div id="module_list">
                	<?php $this->load->view("members/list"); ?>
                </div>
            </div><!-- /.panel-body -->
        </div>
	</div>
</div>

