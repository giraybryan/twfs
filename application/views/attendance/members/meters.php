<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meters extends MY_Controller {
	public $table = "meters";
	public function __construct(){
		parent::__construct();
		$this->load->model('meters_model');
	}
	public function index(){

		$result = $this->fetch();
		$js_files = array('modules/meterform.js','modules/meterreading.js');
		$this->template('meters/index', array('meters' => $result,'js' => $js_files));
	}

	public function paginate($keywords = 0, $sort_by = 'meters.id', $sort_order = 'DESC', $limit = 20, $offset = 0){

		$result['meters'] = $this->fetch($keywords, $sort_by, $sort_order, $limit, $offset);
		echo $this->load->view('meters/list',$result, true);
	}

	public function fetch($keywords = 0, $sort_by = 'meters.id', $sort_order = 'DESC', $limit = 20, $offset = 0){
		
		$params = array(
				'limit'  => $limit,
				'sort_by' => $sort_by, 
				'sort_order' => $sort_order, 
				'offset' => $offset, 
			);

		if(!empty($keywords)){
				$columns = $this->db->list_fields($this->table);
				foreach ($columns as $key => $value) {
					if(empty($params['like'])) {

						$params['like'][$this->table.'.'.$value] = $keywords;
					} else {
						$params['or_like'][$this->table.'.'.$value] = $keywords;
					}		
				}
		}

		$result = $this->meters_model->get_meters($params);
		return $result;
	}

	public function add(){

		$this->load->library('form_validation');
		
		$post = $this->input->post();
		if( empty($post['id']) ){
			
			$action = 'save';
		} else {
			$action = 'edit';
		}

		$rules = array(
	           	'meter_room_name' => array(
	                     'field' => 'meter_room_name',
	                     'label' => 'Room Name',
	                     'rules' => 'trim|required|is_unique[meters.meter_room_name]'
	                     ),
	           ); 
		$this->form_validation->set_rules($rules);
		if ( $this->form_validation->run() ) {

			if( empty($post['id']) ){
				$result = $this->meters_model->add($this->input->post());
			} else {
				$result = $this->meters_model->edit($this->input->post());
			}

			if ( $result ){
				$response = array('result' => 1, 'msg' => 'Successfully '.$action.'ed');
			} else {
				$response = array('result' => 0, 'msg' => 'Failed to '.$action);
			}
			
		} else {
			$response = array('result' => 0, 'msg' => validation_errors());
		}

		echo json_encode($response);
		
	}

	public function delete(){
		$action = 'delete';
		$post = $this->input->post();
		if( !empty($post['id']) ){
			$result = $this->meters_model->delete($this->input->post('id'));

			if ( $result ) {	
				$response = array(
					'msg' 	 => 'Meter '.$action."d",
					'result' => true, 
					);
			} else {
				$response = array(
					'msg' 	 => 'Failed to '.$action,
					'result' => false, 
					);
			}

			echo json_encode($response);
		}
	}

	public function get_option_meters(){

		$data['meters'] = $this->fetch();
		$data['selected'] = $this->input->get('id');
		print_r($data['selected']);
		echo $this->load->view("meter_reading/options", $data, true);
	}

}