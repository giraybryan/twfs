
<a class="btn btn-warning" href="<?php echo base_url('configs')?>"><i class="fa fa-angle-left"></i> Back to Config</a>

<?php if(isset($response['result'])): ?>
  <?php if($response['result'] == 1):?>
     <div class="alert alert-success"><?php echo $response['msg']; ?></div>
  <?php else:?>
      <div class="alert alert-danger"><?php echo $response['msg']; ?></div>
  <?php endif;?>
<?php else:?>

<?php endif; ?> 

  <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo !empty($config->id) ? base_url('configs/edit/'.$config->id) :  base_url('configs/add');?>">
    <div class="form_result"></div>
    <!--
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Middle Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="middle" name="middlename" placeholder="Middle Name">
        </div>
    </div>
    -->
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Config Name:</label>
        <div class="col-lg-8">
           <input type="text" name="config_name" class="form-control" value="<?php echo !empty($config->config_name) ? $config->config_name : "" ?>" id="config_name"/>
            <?php if (!empty($config) ): ?>
            <input type="hidden" name="id" class="form-control" value="<?php echo !empty($config->id) ? $config->id : "" ?>" id="id"/>
            <?php endif; ?>
        </div>
    </div>
    
   
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Config Value:</label>
        <div class="col-lg-8">
            <?php if( !empty($config->value ) && $config->code == "membershiptype"): ?>
                <select class="form-control" id="value"  required name="value" >
                    <option value="lifetime" <?php echo $config->value == "lifetime" ? " selected " : ""; ?>>Lifetime</option>
                    <option value="annual" <?php echo $config->value == "annual" ? " selected " : ""; ?>>Annual</option>
                </select>
            <?php else: ?>
                <input type="number" class="form-control" id="value"  required name="value" placeholder="Config value" value="<?php echo !empty($config->value) ? $config->value : '' ?>">
            <?php endif; ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
</form>