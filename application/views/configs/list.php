<div class="table-responsive">
<table class="table">
    <thead>
    	<tr>
            <th style="width: 10px">ID</th>
            <th>Config</th>
            <th>Value</th>
            <th>Action</th>
    	</tr>
    </thead>
    <?php if( !empty($configs) ): ?>
        <tbody>
        <?php foreach($configs as $config):?>
            <tr>
                <td><?php echo $config->id ?></td>
                <td><?php echo $config->config_name ?></td>
                <td>
                    <?php echo $config->value ?>
                </td>
                <td data-encode='<?php echo json_encode($config)?>'>
                   
                    <button class="btn btn-primary btn-sm action-btn hide" data-action='add'><i class="fa fa-plus"></i></button>

                    
                   <a href="<?php echo base_url('configs/edit/'.$config->id.'/'.$config->id) ?>">
                	<button class="btn btn-warning btn-sm action-btn" data-action="edit"><i class="fa fa-pencil"></i>Edit</button>
                        </a>
                	<button class="btn btn-danger btn-sm action-btn hide" data-action="delete" ><i class="fa fa-times"></i></button>
                </td>
            </tr>
        <?php endforeach;?>
    </tbody>
    <tfoot>
       
    </tfoot>
	<?php else: ?>
    <tbody>
        <tr>
			<td colspan='4'>No records</td>
		</tr>
    </tbody>
    <?php endif; ?>
	
</table>
</div>