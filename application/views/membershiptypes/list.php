<div class="table-responsive">
<table class="table">
    <thead>
    	<tr>
            <th>Class</th>
            <th>Rates</th>
            <th>Action</th>
    	</tr>
    </thead>
   
    <?php if( !empty($rates) ): ?>
        <tbody>
        <?php $total_rates = 0; ?>
        <?php foreach($rates as $rate):?>
            <tr>
                <td><?php 
                    $class_desc = "";
                    if( !empty($rate->durations) ):
                        $class_desc = $rate->class_title.' - '.$rate->durations.' Month(s) ';
                    endif;
                
                    if( !empty($rate->session_duration) ):
                        if( !empty($class_desc) ):
                            $class_desc .= ' w/ ';
                        else: 
                            $class_desc = $rate->class_title.' - ';
                        endif;
                        $class_desc .= $rate->session_duration.' Session(s)'; 
                    endif;
                    echo $class_desc;
                  ?></td>
                <td><?php echo $rate->price ?></td>
                
                <td data-encode='<?php echo json_encode($rate)?>'>
                    <?php 
                    if( $access == 1 ):
                    ?>
                    <button class="btn btn-primary btn-sm action-btn hide" data-action='add'><i class="fa fa-plus"></i></button>

                    <a href="<?php echo base_url('membershiptypes/edit/'.$rate->id) ?>">
                	<button class="btn btn-warning btn-sm action-btn" data-action="edit"><i class="fa fa-pencil"></i></button>
                        </a>
                	<button data-id="<?php echo $rate->id ?>" data-table="vouchers" class="btn btn-danger btn-sm action-btn" data-action="delete" ><i class="fa fa-times"></i></button>
                    <?php         
                    endif;
                    ?>
                </td>
            </tr>
           
        <?php endforeach;?>
    </tbody>
	<?php else: ?>
    <tbody>
        <tr>
			<td colspan='4'>No records</td>
		</tr>
    </tbody>
    <?php endif; ?>
	
</table>
</div>