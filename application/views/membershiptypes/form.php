
<?php if(isset($response['result'])): ?>
  <?php if($response['result'] == 1):?>
     <div class="alert alert-success"><?php echo $response['msg']; ?></div>
  <?php else:?>
      <div class="alert alert-danger"><?php echo $response['msg']; ?></div>
  <?php endif;?>
<?php else:?>

<?php endif; ?> 

  <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo !empty($rate->id) ? base_url('membershiptypes/edit/'.$rate->id) :  base_url('Membershiptypes/add');?>">
    <div class="form_result"></div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Class Type:</label>
        <div class="col-lg-8">
            <select name="class_type_id" id="class_type_id" required class="form-control">
              <option value="">Select Class type</option>
              <?php if( !empty($class_types)):?>
                <?php foreach($class_types as $type):?>
                  <option value="<?php echo $type->id ?>" <?php echo !empty($rate->class_type_id) && $rate->class_type_id == $type->id ? "selected" : "" ?>><?php echo $type->class_title ?></option>
                <?php endforeach;?>
              <?php endif;?>
            </select>
           
            <input type="hidden" class="form-control" id="id" name="id" value="<?php echo !empty($rate->id) ? $rate->id : '' ?>">
            <input type="hidden" class="form-control" id="userid" name="userid" value="<?php echo $this->session->userdata['userdata']['userid']; ?>">

        </div>
    </div>
    <!--
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Middle Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="middle" name="middlename" placeholder="Middle Name">
        </div>
    </div>
    -->
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Monthly Duration:</label>
        <div class="col-lg-8">
          <select name="duration_id" id="duration_id" class="form-control">
              <option value="">Select Monthly Duration </option>
              <?php if( !empty($months)):?>
                <?php foreach($months as $month):?>
                  <option value="<?php echo $month->id ?>" <?php echo !empty($rate->duration_id) && $rate->duration_id == $month->id ? "selected" : "" ?>><?php echo $month->durations.' ('.$month->duration_type.' )' ?></option>
                <?php endforeach;?>
              <?php endif;?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Session Duration:</label>
        <div class="col-lg-8">
          <select name="session_duration_id" id="session_duration_id" class="form-control">
              <option value="">Select Session Duration</option>
              <?php if( !empty($sessions)):?>
                <?php foreach($sessions as $session):?>
                  <option value="<?php echo $session->id ?>" <?php echo !empty($rate->session_duration_id) && $rate->session_duration_id == $session->id ? "selected" : "" ?>><?php echo $session->durations.' '.$session->duration_type ?></option>
                <?php endforeach;?>
              <?php endif;?>
            </select>
        </div>
    </div>
    
   
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Price:</label>
        <div class="col-lg-8">
            <input type="number" class="form-control" id="price"  required name="price" placeholder="amount" value="<?php echo !empty($rate->price) ? $rate->price : '' ?>">
        </div>
    </div>
  
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
</form>
