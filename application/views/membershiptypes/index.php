<div class="row">
	<div class="col-md-12">
		<div class="panel">
            <ul class="nav nav-tabs">
              <li role="presentation" ><a href="<?php echo base_url().'classes/'?>">Classes</a></li>
              <li role="presentation" ><a href="<?php echo base_url().'classdurations/'?>">Durations</a></li>
              <li role="presentation" class="active"><a >Class Subscription Rates</a></li>
              <li role="presentation" ><a  href="<?php echo base_url().'walkins/'?>">walkin Rates</a></li>
            </ul>
            <header class="panel-heading">
                Class Rates List
            </header>
            
            <div class="panel-body">
                <div class="box-tools">
                	<a href="<?php echo base_url().'Membershiptypes/add'?>"><button class='btn btn-success btn-sm' data-toggle="modal" data-target="#metermodal">Add Class Rates</button></a>
                    <!--
                    <ul class="pagination pagination-sm m-b-10 m-t-10 pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                	-->
                </div>
                <div class="form-horizontal">
                <div class="row hide">
                    <div class="col-sm-4">
                        <div class="form-group ">
                            <div class="col-sm-8">
                            <label>Filter Start Date</label>
                            
                            <input type="text" class="form-control datepick" id="date" name="date" value="<?php echo date('m/d/Y'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                           
                            <div class="col-sm-4">
                                <label>Filter By Date Range </label>
                                <input type="text" name="daterange" class="form-control" value="" />
                            
                           </div>
                        </div>
                    </div>
                </div>
                </div>
                <div id="module_list">
                	<?php $this->load->view("membershiptypes/list"); ?>
                </div>
            </div><!-- /.panel-body -->
        </div>
	</div>
</div>

