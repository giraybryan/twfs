<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Bootstrap Login Form</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo base_url();?>assets/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>/assets/css/login.css" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url();?>assets/img/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url();?>assets/img/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>assets/img/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();?>assets/img/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>assets/img/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url();?>assets/img/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url();?>assets/img/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url();?>assets/img/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>assets/img/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url();?>assets/img/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>assets/img/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>assets/img/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/img/fav/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url();?>assets/img/fav/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url();?>assets/img/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
		
    <link href="css/styles.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/js/plugins/alertify/css/alertify.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url();?>/assets/js/plugins/alertify/css/themes/default.css" rel="stylesheet"/>
	</head>
<body style="background: #eee">
    
    <div class="container" style="padding: 10px 15px;">
    <div class="row">
        <div class="col-sm-4">
            <h5>PROMO / ANNOUNCEMENTS</h5>
            <div id="promo">

                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                    <div class="item active">
                      <img src="<?php echo base_url('uploads/announcements/voucher promo.jpg'); ?>" alt="promo">
                    </div>

                    <div class="item">
                      <img src="<?php echo base_url('uploads/announcements/ashtaga class.jpg'); ?>" alt="Ashtaga yoga">
                    </div>

                    <div class="item">
                      <img src="<?php echo base_url('uploads/announcements/aerial yoga.jpg'); ?>" alt="Aerial yoga">
                    </div>
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
                <img src="" class="img-responsive">
            </div>
        </div>
        <div class="col-sm-5">
            <div style="" class="loginpage">
                <form class="form-horizontal">
                    <img class="img-responsive" style="max-width: 300px; display: block; margin: auto;" src="<?php echo $logo_img ?>"/>  
                    <h2>MEMBER LOGIN</h2>
                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                      <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#registration">Registration Form</button>
                      </div>
                      <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#class_rates">Class Rates</button>
                       
                      </div>
                      <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#schedule">Schedule</button>
                      </div>
                      
                    </div>
                    <br/>
                    <div class="input-group input-group-lg">
                    
                    <input type="text" class="form-control" placeholder="Member ID No" name="memberid_no" id="memberid_no">
                        <span class="input-group-addon" id="search">Search</span>
                    </div>

                </form>
                <div id="result"></div>
            </div>
        </div>
        <div class="col-sm-3">
            <h5>ATTENDANCE RECORD</h5>
            <div id="attedance_wrapper" style="background: #fff; padding: 5px; max-height: 500px; overflow: auto;">
                <?php
                $this->load->view('attendance/daily_list');
                ?>
            </div>
        </div>

    </div>
    </div>
    

    <div class="modal fade" id="registration" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Registration</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" id="regform" action="<?php echo base_url('members/new')?>" method="post">
                <div class="form-group">
                    <label class="col-sm-3 control-label">First Name</label>
                    <div class="col-sm-9">
                        <input type="text" required id="firstname" name="firstname" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Last Name</label>
                    <div class="col-sm-9">
                        <input type="text" required id="lastname" name="lastname" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-3 col-sm-3 control-label">Address:</label>
                    <div class="col-lg-9">
                        <textarea name="address" placeholder="Address"></textarea>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="inputEmail1" class="col-lg-3 col-sm-3 control-label">Birth Date:</label>
                    <div class="col-lg-9">
                       <input type="text" class="form-control datepick" required name="bd" id="bd" value="<?php echo !empty($member->bd) ? date('m/d/Y',strtotime($member->bd)) : '' ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-3 col-sm-3 control-label">Email:</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo !empty($member->email) ? $member->email : '' ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-3 col-sm-3 control-label">Number:</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" id="mobile" required name="mobile" placeholder="Mobile Number" value="<?php echo !empty($member->mobile) ? $member->mobile : '' ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-3 col-sm-3 control-label">Gender:</label>
                    <div class="col-lg-9">
                        <select name="gender" required class="form-control" >
                          <option value="" >Select Gender</option>
                          <option value="m">Male</option>
                          <option value="f">Female</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <button type="button" class="btn btn-default btn-lg" style="margin-right: 25px" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    </div>
                </div>
            </form>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="class_rates" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Class Rates</h4>
          </div>
          <div class="modal-body">
            <img src="<?php echo base_url('assets/img/rate.jpg') ?>" class="img-responsive"/ >
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="schedule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Class Schedules</h4>
          </div>
          <div class="modal-body">
            <img src="<?php echo base_url('assets/img/schedule.jpg') ?>" class="img-responsive"/ >
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

          </div>
        </div>
      </div>
    </div>
    <style>
    .loginpage {
        max-width: 600px; margin: 30px auto; border: 1px solid #ddd; background: #fff;padding: 10px 15px; border-radius: 8px;
    }
    .loginpage h2 { text-align: center; }
    .loginpage img { max-width: 100%; width: auto; max-height: 200px;}
    @media(min-width: 766px){
        .loginpage { padding: 50px; }
    }
    @media(max-width: 766px){
        .loginpage img { width: 100%;}
    }
    .loginpage h2:before {
    background: #7e7e7e;
    background: linear-gradient(right,#7e7e7e 0,#fff 100%);
    left: 0;
    }
    .loginpage h2{
        font: 400 25px Helvetica,Arial,sans-serif;
    letter-spacing: -.05em;
    line-height: 20px;
    margin: 10px 0 30px;
}
    }
    </style>
    <input type="hidden" value="<?php echo base_url(); ?>" id="baseurl"/>
    <script src=""></script>
    <script src="<?php echo base_url();?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>
    <script src="<?=base_url().'assets/'?>js/plugins/alertify/alertify.min.js"></script>
    <?php foreach($js as $script): ?>
        <script src="<?php echo base_url('assets/js/'.$script.'?date='.strtotime(date('Y-m-d H:s:i')))?>"></script>
    <?php endforeach; ?>
    <script type="text/javascript">
        var latest_card = "<?php echo $latest_card->process == "attendance" ? $latest_card->description : "" ?>";
        var unix = "<?php echo $latest_card->unix ?>";  
        var time_in = "";
        var timeloop = "";

        function clear_display(){
            timeloop = setInterval(function(){
                if( time_in != ""){
                    after_five = new Date(time_in.getTime() + 1 * 60000)
                    currentIme = new Date();
                    if( currentIme > after_five ){
                        $('#memberid_no').val('');
                        $('#result').html('')
                        time_in = "";
                    }
                }
               
                
            },1000);
        }
        setTimeout(function(){
            clear_display();
        },1500)
        
        
        $(document).ready(function(){
            currentID = 0;
            setInterval(function(){
                $.ajax({
                    type : "POST",
                    url : "<?php echo base_url().'/clientlog/fetch_attendance'?>",
                    data: { latest_card : latest_card, unix : unix },
                    dataType: 'json',
                    success: function(response){
                        if( response.card_id == latest_card ){
                            return false;
                        }
                        if( response.id > 0 && response.id != currentID ){
                            currentID = response.id
                            if( response.result ==  0 ){
                                alertify.error(response.msg);
                                $('#memberid_no').val('');
                                $('#result').html('')
                            } else {
                                
                                time_in = new Date();
                                clearInterval(timeloop);
                                clear_display();
                                $('#memberid_no').val(response.member.id);
                                $('#search').trigger('click')
                            }   
                        }
                        
                    }

                })    
            },800)

            $("#regform").submit(function(e){
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url : $(this).attr('action'),
                    data : $(this).serialize(),
                    dataType: "json",
                    success: function(response){
                        if( response.result == 1){
                           $('#regform input[type="text"],#regform textarea,#regform input[type="email"],#regform input[type="number"], #regform select').val('');
                           alertify.success(response.msg)
                        } else {
                            alertify.alert('Error', response.msg)
                        }
                    }
                })
            })
            
        })
    </script>
  </body>


</html>



