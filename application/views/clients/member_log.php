<div class="row">
    <div class="ccol-sm-12">
        <div class="panel">
            <div class="panel-body">
                <div class="form-horizontal">
                <div class="row">
                    <div class="col-sm-3">
                        <img src="<?php echo !empty($member->image) ? base_url('uploads/members/'.$member->image) : base_url('uploads/placeholder.png'); ?>" class="img-responsive"/>
                    </div>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <label class="col-sm-3">Member : </label>
                            <div class="col-sm-9"><?php echo $member->firstname.' '.$member->lastname ?></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12"></label>
                            <div class="col-sm-12" style="">
                                <?php $this->view('members/subscription_list'); ?>
                                <?php 
                                $points = $this->db->select('points')->where('member_id', $member->id)->limit(1)->order_by('date_created', 'DESC')->get('loyalty_points')->row();

                                if( empty($points )){
                                    $_points = 0;
                                } else {
                                    $_points = $points->points;
                                }

                                echo "<h5> Earned Points : ".$_points."</h5>";
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                    
             
                   
                </div>
              
            </div>
        </div>
    </div>
</div>