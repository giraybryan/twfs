<br/>
<a href="<?php echo base_url().'expenses/add'?>"><button class='btn btn-success btn-sm' data-toggle="modal" data-target="#metermodal">Add expense</button></a>
<br/>
<div class="table-responsive">
<table class="table">
    <thead>
    	<tr>
            <th style="width: 10px">ID</th>
            <th>Date</th>
            <th>Expense Type</th>
            <th>Expense Description</th>
            <th>Cost</th>
            <th>Action</th>
    	</tr>
    </thead>
    
    <?php if( !empty($expenses) ): ?>
    <tbody>
        <?php $total_exp = 0 ?>
        <?php foreach($expenses as $expense):?>
            <tr>
                <td><?php echo $expense->id ?></td>
                <td><?php echo date('M d, Y',strtotime($expense->date_paid)) ?></td>
                <td><?php echo $expense->expense_type ?></td>
                <td><?php echo $expense->description ?></td>
                <td><?php echo $expense->cost ?></td>
                
                <td data-encode='<?php echo json_encode($expense)?>'>
                	
                	<a href="<?php echo base_url('expenses/edit/'.$expense->id);?>"><button class="btn btn-warning btn-sm action-btn" data-action="edit"><i class="fa fa-pencil"></i></button></a>
                	<!-- <button class="btn btn-danger btn-sm action-btn" data-action="delete" ><i class="fa fa-times"></i></button> -->
                </td>
            </tr>
            <?php $total_exp = $total_exp +$expense->cost; ?>
        <?php endforeach;?>
	</tbody>
    <tfoot>
        <tr>
            <td></td>
            <td></td>
           
            <td align=right>Total</td>
            <td><?php echo $total_exp; ?></td>
            <td></td>
            <td></td>
        </tr>
    </tfoot>
    <?php else: ?>
    <tbody>
        <tr>
            <td colspan='4'>No records</td>
        </tr>
    </tbody>
    <?php endif; ?>
</table>
</div>