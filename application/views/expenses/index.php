<?php $this->load->view('templates/filterheader'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel">
            <header class="panel-heading">
                Expenses List
            </header>
            <div class="panel-body">
                <div class="box-tools">
                    <!--
                    <ul class="pagination pagination-sm m-b-10 m-t-10 pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                	-->
                </div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" ><a href="#sales" aria-controls="sales" role="tab" data-toggle="tab">Sales</a></li>
                    <li role="presentation" class="active"><a href="#expense" aria-controls="expense" role="tab" data-toggle="tab">Expense</a></li>
                  </ul>
                 <div class="tab-content">
                    <div role="tabpanel" class="tab-pane " id="sales"><?php $this->load->view("sales/list"); ?></div>
                    <div role="tabpanel" class="tab-pane active" id="expense"><?php $this->load->view("expenses/list"); ?></div>
                </div>
            </div><!-- /.panel-body -->
        </div>
	</div>
</div>

