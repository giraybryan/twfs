
<?php if(isset($response['result'])): ?>
  <?php if($response['result'] == 1):?>
     <div class="alert alert-success"><?php echo $response['msg']; ?></div>
  <?php else:?>
      <div class="alert alert-danger"><?php echo $response['msg']; ?></div>
  <?php endif;?>
<?php else:?>

<?php endif; ?> 

  <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo !empty($expense->id) ? base_url('expenses/edit/'.$expense->id) :  base_url('expenses/add');?>">
    <div class="form_result"></div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Expense Type:</label>
        <div class="col-lg-8">
            <select name="expense_type_id" id="expense_type_id" class="form-control">
              <option value="">Select Expense</option>
              <?php if( !empty($types)):?>
                <?php foreach($types as $type):?>
                  <option value="<?php echo $type->id ?>" <?php echo !empty($expense->expense_type_id) && $expense->expense_type_id == $type->id ? "selected" : "" ?>><?php echo $type->expense_type ?></option>
                <?php endforeach;?>
              <?php endif;?>
            </select>
            <a href="<?php echo base_url().'expenses/add_type'?>">Add Expense Type</a>
            <input type="hidden" class="form-control" id="id" name="id" value="<?php echo !empty($expense->id) ? $expense->id : '' ?>">
            <input type="hidden" class="form-control" id="userid" name="userid" value="<?php echo $this->session->userdata['userdata']['userid']; ?>">

        </div>
    </div>
    <!--
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Middle Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="middle" name="middlename" placeholder="Middle Name">
        </div>
    </div>
    -->
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Description:</label>
        <div class="col-lg-8">
           <textarea class="form-control" name="description"><?php echo !empty($expense->description) ? $expense->description : '' ?></textarea>
        </div>
    </div>
    
   
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Cost:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="cost" required name="cost" placeholder="Cost" value="<?php echo !empty($expense->cost) ? $expense->cost : '' ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Date Paid:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control datepick" id="cost" required name="date_paid" placeholder="date_paid" value="<?php echo !empty($expense->date_paid) ? $expense->date_paid : date('m/d/Y') ?>">
        </div>
    </div>
  
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
</form>