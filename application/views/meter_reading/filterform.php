<div class="row">
  <div class="col-sm-7">
    <section class="panel">
        <header class="panel-heading">
            Filter Form
        </header>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="get" action="meter_reading">
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-3 col-sm-3 control-label">Room/Apartment Name</label>
                    <div class="col-lg-9">
                        <select name="meter_id" class="form-control">
                          <option value="">Select</option>
                          <?php $options =  get_meteroptions(); ?>
                          <?php 
                          foreach ($options as $meter) {
                            echo "<option value='".$meter->id."'";
                            echo $this->input->get('meter_id') == $meter->id ? "selected" : '';
                            echo ">".$meter->meter_room_name."</option>";
                          }
                         
                          ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword1" class="col-lg-3 col-sm-3 control-label">Date Ranger</label>
                    <div class="col-lg-9">
                        <div class="input-daterange" id="datepicker" >
                          <div class="input-group">
                            <input type="text" class="input-small form-control" name="start" value="<?php echo $this->input->get('start')?>" />
                            <span class="input-group-addon">to</span>
                            <input type="text" class="input-small form-control" name="end" value="<?php echo $this->input->get('end')?>" />
                            
                          </div>
                            
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-3 col-sm-3 control-label">Result Per Page</label>
                    <div class="col-lg-9">
                        <input type="number"  name="limit" class="input-small form-control" value="<?php echo $this->input->get('limit')?>" />
                    </div>
                </div>
               
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <button type="submit" class="btn btn-success">Filter</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
  </div>
</div>