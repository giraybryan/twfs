<div class="table-responsive">
<table class="table">
    <thead>
        <tr>
            <th style="width: 60px"><a onclick="<?php column_sort('module_list', base_url('meter_reading/paginate/'.$keywords.'/electricity_log.id/'.$sort_order.'/'.$limit.'/'.$offset).'?'.http_build_query($this->input->get()) )?>"><i class='fa fa-sort'></i>  ID</a></th>
            <th><a onclick="<?php column_sort('module_list', base_url('meter_reading/paginate/'.$keywords.'/meters.meter_room_name/'.$sort_order.'/'.$limit.'/'.$offset).'?'.http_build_query($this->input->get()) )?>"><i class='fa fa-sort'></i>  Room </a></th>
            <th><a onclick="<?php column_sort('module_list', base_url('meter_reading/paginate/'.$keywords.'/electricity_log.date_read/'.$sort_order.'/'.$limit.'/'.$offset).'?'.http_build_query($this->input->get()) )?>"><i class='fa fa-sort'></i>Date Read</a></th>
            <th><a onclick="<?php column_sort('module_list', base_url('meter_reading/paginate/'.$keywords.'/total_overall/'.$sort_order.'/'.$limit.'/'.$offset).'?'.http_build_query($this->input->get()) )?>"><i class='fa fa-sort'></i> Total</a></th>
            <th>
                <a onclick="<?php column_sort('module_list', base_url('meter_reading/paginate/'.$keywords.'/total_overall/'.$sort_order.'/'.$limit.'/'.$offset).'?'.http_build_query($this->input->get()) )?>"><i class='fa fa-sort'></i>
                    Total Cost
                </a>
            </th>
            <th> 
                    Latest Reading
                </a>
            </th>
            <th>
                 Latest Total Cost
            </th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    <?php if( !empty($readings) ): ?>
        <?php foreach($readings as $reading):?>
            <tr>
                <td><?php echo $reading->id ?></td>
                <td><?php echo $reading->meter_room_name ?></td>
                <td><?php echo dateformat($reading->date_read) ?></td>
                <td><?php echo $reading->total_overall  ?></td>
                <td><?php echo format_currency($reading->total_overall * .28) ?></td>
                <td>
                    <?php echo $reading->total_overall - $reading->prev_reading ?>
                </td>
                <td><?php echo format_currency(($reading->total_overall - $reading->prev_reading ) * .28 ) ?></td>
                <td data-encode='<?php echo json_encode($reading)?>'>
                    <button class="btn btn-warning btn-sm read-action-btn" data-action="edit"><i class="fa fa-pencil"></i></button>
                    <button class="btn btn-danger btn-sm read-action-btn" data-action="delete" ><i class="fa fa-times"></i></button>
                </td>
            </tr>
        <?php endforeach;?>
        
    <?php else: ?>
        <tr>
            <td colspan='5'>No records</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
</div>
<?php echo $pagination_link; ?>
