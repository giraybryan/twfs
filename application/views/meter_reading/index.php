<?php $this->load->view('meter_reading/filterform'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel">
            <header class="panel-heading">
                Meter Reading
            </header>
            <div class="panel-body">
                <div class="box-tools">
                	
                    <!--
                    <ul class="pagination pagination-sm m-b-10 m-t-10 pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                	-->
                </div>
                <div id="module_list">
                	<?php $this->load->view("meter_reading/list"); ?>
                </div>
            </div><!-- /.panel-body -->
        </div>
	</div>
</div>

<?php $this->load->view('meter_reading/form') ?>