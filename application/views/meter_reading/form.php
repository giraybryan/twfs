<!-- Modal -->
<div class="modal fade modalform" id="readingmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Meter Reading Form</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal targetform"  role="form" id="readingform" method="post" name="readingform" action="<?php echo base_url('meter_reading/add');?>">
            <div class="form_result"></div>
            <div class="form-group">
                <label for="meter_id" class="col-lg-4 col-sm-3 control-label">Meter Room No.</label>
                <div class="col-lg-8">
                    <select name="meter_id" required id="meter_id" class="form-control">
                        <option value="">Select</option>
                          <?php $options =  get_meteroptions(); ?>
                          <?php 
                          foreach ($options as $meter) {
                            echo "<option value='".$meter->id."'";
                            echo $this->input->get('meter_id') == $meter->id ? "selected" : '';
                            echo ">".$meter->meter_room_name."</option>";
                          }
                         
                          ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="date_read" class="col-lg-4 col-sm-3 control-label">Date Read</label>
                <div class="col-lg-8">
                    <input type="text" placeholder="Date Read" required name="date_read" id="date_read" class="form-control datepick"/>
                    <input type="hidden" placeholder="" required name="id" id="id" class="form-control datepick"/>
                </div>
            </div>
            <div class="form-group">
                <label for="date_read" class="col-lg-4 col-sm-3 control-label">Reading</label>
                <div class="col-lg-8">
                    <input type="number" placeholder="Reading" onkeypress="return isNumberKey(event)" required name="reading" id="reading" class="form-control"/>
                </div>
            </div>
            <div class="form-group hide">
                <div class="col-lg-offset-4 col-lg-8">
                    <button type="submit" class="btn btn-danger ">Save</button>
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-sm modalsubmit">Save changes</button>
      </div>
    </div>
  </div>
</div>