<?php $this->load->view('templates/filterheader'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel">
            <header class="panel-heading">
                Sales List
            </header>
            <div class="panel-body">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#sales" aria-controls="sales" role="tab" data-toggle="tab">Sales</a></li>
                    <li role="presentation"><a href="#expense" aria-controls="expense" role="tab" data-toggle="tab">Expense</a></li>
                  </ul>
                 <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="sales"><?php $this->load->view("sales/list"); ?></div>
                    <div role="tabpanel" class="tab-pane" id="expense"><?php $this->load->view("expenses/list"); ?></div>
                </div>
                <div class="box-tools">
                    <!--
                    <ul class="pagination pagination-sm m-b-10 m-t-10 pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                	-->
                </div>
            </div><!-- /.panel-body -->
        </div>
	</div>
</div>

<script type="text/javascript">
   setTimeout(function(){
    $(document).ready(function(){
        $(document).on('click','.issue_or',function(){
            btn = $(this)
            sales_id = $(this).attr('data-id')
            amount = $(this).attr('data-amount')
            conf = confirm('Are you sure you want to issue?')
            if( conf == true ){
                $.ajax({
                    type: "POST",
                    url : $('#baseurl').val()+'sales/ajax_sales_or',
                    data: { sales_id : sales_id, total : amount},
                    success: function(response){
                        if( response == "success"){
                            alert('OR issued!');
                            $(btn).remove();
                        }
                    }
                })
            } 
        })
    })
},2000) 
</script>