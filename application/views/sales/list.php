<br/>
<a href="<?php echo base_url().'sales/add'?>"><button class='btn btn-success btn-sm' data-toggle="modal" data-target="#metermodal">Add sales</button></a>
<br/>
<div class="table-responsive">
<table class="table">
    <thead>
    	<tr>
            <th style="width: 10px">ID</th>
            <th>Date</th>
            <th>Sales Type</th>
            <th>Sales Details</th>
            <th>Promo</th>
            <th>Sales Amount</th>
            
            <th>Action</th>
    	</tr>
    </thead>
    <?php $total_or = 0; ?>
    <?php if( !empty($sales) ): ?>
        <tbody>
        <?php $total_sales = 0; ?>
        <?php foreach($sales as $_sales):?>
            <tr>
                <td><?php echo $_sales->id ?></td>
                <td><?php echo date('M d, Y',strtotime($_sales->date)) ?></td>
                <td><?php echo $_sales->sales_type ?></td>
                <td>
                    <?php 
                    if($_sales->member_id){
                        echo $_sales->firstname.' '.$_sales->lastname;
                        echo "<br/>".$_sales->class_title.' - '.$_sales->durations;
                        if($_sales->duration_type == "monthly"){
                            echo  $_sales->durations == 1 ? "month" : "months";
                        } else{
                            echo $_sales->duration_type;
                        }
                    } elseif($_sales->class_title){
                        echo $_sales->class_title;
                    }
                    if($_sales->description){
                        echo $_sales->description;
                    }
                    ?>
                </td>

                <td><?php echo $_sales->promo_title ?></td>
                <td><?php echo $_sales->amount?></td>
                
                
                <td data-encode='<?php echo json_encode($_sales)?>'>
                   
                    <button class="btn btn-primary btn-sm action-btn hide" data-action='add'><i class="fa fa-plus"></i></button>

                    
                     <?php if(!empty($_sales->member_id)):?>
                        <a href="<?php echo base_url('memberships/edit/'.$_sales->membership_log_id.'/'.$_sales->member_id) ?>">
                    <?php else: ?>
                        <a href="<?php echo base_url('sales/edit/'.$_sales->id); ?>">
                    <?php endif; ?>
                	<button class="btn btn-warning btn-sm action-btn" data-action="edit"><i class="fa fa-pencil"></i></button>
                        </a>
                	<button class="btn btn-danger btn-sm action-btn hide" data-action="delete" ><i class="fa fa-times"></i></button>
                    <?php
                    if( empty($_sales->orid) ){

                        ?>
                        <span class="btn btn-info btn-sm issue_or" data-id="<?php echo $_sales->id ?>" data-amount="<?php echo $_sales->amount ?>">Issue OR</span>
                        <?php
                    }
                    ?>
                </td>
            </tr>
            <?php $total_sales = $total_sales + $_sales->amount; ?>
            <?php 
                if( !empty($_sales->orid) ){
                    $total_or = $total_or + $_sales->amount;
                }
                
            ?>
        <?php endforeach;?>
    </tbody>
    <tfoot>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>TOTAL SALES</td>
            <td><?php echo $total_sales; ?></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>TOTAL OR</td>
            <td><?php echo $total_or; ?></td>
            <td></td>
            <td></td>
        </tr>
    </tfoot>
	<?php else: ?>
    <tbody>
        <tr>
			<td colspan='4'>No records</td>
		</tr>
    </tbody>
    <?php endif; ?>
	
</table>
<div><strong>COH</strong> : <?php echo $coh ?></div>
</div>

