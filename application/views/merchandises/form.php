
<?php if(isset($response['result'])): ?>
  <?php if($response['result'] == 1):?>
     <div class="alert alert-success"><?php echo $response['msg']; ?></div>
  <?php else:?>
    sdfsadf
      <div class="alert alert-danger"><?php echo $response['msg']; ?></div>
  <?php endif;?>
<?php else:?>

<?php endif; ?> 
  <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo !empty($merchandise->id) ? base_url('merchandises/edit/'.$merchandise->id) :  base_url('merchandises/add');?>">
    <div class="form_result"></div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Item Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="item" name="item" placeholder="Item Name" value="<?php echo !empty($merchandise->item) ? $merchandise->item : '' ?>">
            <input type="hidden" class="form-control" id="id" name="id" value="<?php echo !empty($merchandise->id) ? $merchandise->id : '' ?>">
        </div>
    </div>
    <!--
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Middle Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="middle" name="middlename" placeholder="Middle Name">
        </div>
    </div>
    -->
    <?php if( empty($merchandise->id)): ?>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Has Variant?:</label>
        <div class="col-lg-8">
            <input type="checkbox" class="" id="has_variant" name="has_variant" placeholder="Cost" value="1">
        </div>
    </div>
    <?php 
        $has_variant = 0;
        else: 
        $has_variant = $merchandise->has_variant;
        ?>
    <?php endif; ?>

<div id="variant_wrapper">
     <?php $this->load->view('merchandises/form_variants', array('has_variant' =>$has_variant,'variants' => $variants )); ?>
</div>
    
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger btn-large " style="font-size: 20px; min-width: 150px;">Save</button>
        </div>
    </div>
</form>