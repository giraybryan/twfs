<div class="table-responsive">
<table class="table">
    <thead>
    	<tr>
            <th style="width: 10px">ID</th>
            <th>Merchandise</th>
            <th>Stock</th>
            <th>Cost</th>
            <th>Price</th>
            <th>Action</th>
    	</tr>
    </thead>
    <tbody>
    <?php if( !empty($merchandises) ): ?>
        <?php foreach($merchandises as $merchandise):?>
            <tr>
                <td><?php echo $merchandise->id ?></td>
                <td><?php echo $merchandise->item; ?></td>
                <td class="<?php echo $merchandise->stock_var <= 0 ? 'alert alert-danger' : '' ?>"><?php echo $merchandise->stock_var; ?></td>
                <td >
                    <?php echo $merchandise->cost_var;   ?>
                        
                </td>
                <td><?php echo $merchandise->price_var  ?></td>
                <td>

                   
                
                    
                    <a href="<?php echo base_url('merchandises/add_inventory/'.$merchandise->id)?>" class="btn btn-primary">Add Inventory</a>
                 
                    
                	<a href="<?php echo base_url('merchandises/edit/'.$merchandise->id);?>"><button class="btn btn-warning btn-sm action-btn" data-action="edit"><i class="fa fa-pencil"></i></button></a>
                	<button class="btn btn-danger btn-sm action-btn" data-id="<?php echo $merchandise->id ?>" data-action="delete" ><i class="fa fa-times"></i></button>
                </td>
            </tr>
        <?php endforeach;?>
	<?php else: ?>
		<tr>
			<td colspan='4'>No records</td>
		</tr>
    <?php endif; ?>
	</tbody>
</table>
</div>