<h4>Inventory Form</h4>
<form class="form-horizontal" action="<?php echo base_url('merchandises/add_inventory') ?>" method="post">
<input type="hidden" id="id" name="id" value="<?php echo $merchandise->id?>">
<?php if($merchandise->has_variant == 0): ?>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Item:</label>
        <div class="col-lg-8">
            <label style="margin-top: 8px;">
                <?php echo $merchandise->item; ?>
            </label>
            
        </div>
    </div>
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Stock:</label>
        <div class="col-lg-8">
            <input type="hidden" class="form-control" id="cost" name="variant[0][id]" placeholder="id" value="<?php echo !empty($merchandise->variant_id) ? $merchandise->variant_id : '' ?>">
           <input type="number" class="form-control stockfield" name="variant[0][stock]" id="" value="">
        </div>
    </div>
<?php else: ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Item Variant</th>
                <th>Stock</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($variants)): ?>
                <?php foreach($variants as $key => $value): ?>
            <tr>
                <td><input type="text" value="<?php echo $variant->variant ?>" name="variant[<?php echo $key?>][variant]" class="form-control"/><input type="text" value="<?php echo $variant->id ?>" name="variant[<?php echo $key?>][id]" class="form-control"/></td>

                <td><input type="number" value="<?php echo $variant->stock ?>" name="variant[<?php echo $key?>][stock]" class="form-control"/></td>
                <td><button class="btn-remove-tr btn btn-danger"><i class="fa fa-close"></i></button></td>
            </tr>  
                <?php endforeach; ?>
          
            <?php endif;  ?>
        </tbody>
        <tfoot>
            
        </tfoot>
    </table>
<?php endif; ?>
     <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger btn-large " style="font-size: 20px; min-width: 150px;">Save</button>
        </div>
    </div>
</form>

  