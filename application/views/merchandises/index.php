<div class="row">
	<div class="col-md-12">
		<div class="panel">
            <header class="panel-heading">
                Merchandises List
            </header>
            <div class="panel-body">
                <div class="box-tools">
                    <div class="row">
                        <div class="col-xs-4">
                	       <a href="<?php echo base_url().'merchandises/add'?>"><button class='btn btn-success btn-sm' data-toggle="modal" data-target="#metermodal">Add Merchandise</button></a>
                        </div>
                        <div class="col-xs-4">

                            

                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-sm-3">
                            <select class="form-control" name="filter_class" id="filter_class">
                                <option value="">Select By Class</option>
                                <?php foreach($classes as $class): ?>
                                    <option value="<?php echo $class->id?>"><?php echo $class->class_title ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <select class="form-control">
                                <option value="">Select By Duration</option>
                                <?php //foreach()?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">

                            <input type="text" class="form-control" id="membersearch" name="member_namesearch" placeholder="Member Name Search">
                            <span class="input-group-addon btn btn-default" id="simple_go" id="basic-addon1">Go</span>
                            </div>
                        </div>
                    </div> -->
                    <!--
                    <ul class="pagination pagination-sm m-b-10 m-t-10 pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                	-->
                </div>
                <div id="module_list">
                	<?php $this->load->view("Merchandises/list"); ?>
                </div>
            </div><!-- /.panel-body -->
        </div>
	</div>
</div>

