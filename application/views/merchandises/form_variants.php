
<?php if($has_variant == 0): ?>
    
  
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Cost:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="cost" name="variant[0][cost]" placeholder="Cost" value="<?php echo !empty($merchandise->cost_var) ? $merchandise->cost_var : '' ?>">
            <input type="hidden" class="form-control" id="cost" name="variant[0][id]" placeholder="id" value="<?php echo !empty($merchandise->variant_id) ? $merchandise->variant_id : '' ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Price:</label>
        <div class="col-lg-8">
             <input type="text" class="form-control pricefield" id="cost" name="variant[0][price]" placeholder="Price" value="<?php echo !empty($merchandise->price_var) ? $merchandise->price_var : '' ?>">
        </div>
    </div>
     <div class="form-group hide">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Stock:</label>
        <div class="col-lg-8">
           <input type="number" class="form-control stockfield" name="variant[0][stock]" id="" value="<?php echo !empty($merchandise->stock_var) ? $merchandise->stock_var : 0 ?>">
        </div>
    </div>
<?php else: ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Item Variant</th>
                <th> Cost</th>
                <th> Price</th>
                <th> </th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($variants)): ?>
                <?php foreach($variants as $variant): ?>
            <tr>
                <td><input type="text" value="<?php echo $variant->variant ?>" name="variant[0][variant]" class="form-control"/><input type="text" value="<?php echo $variant->id ?>" name="variant[0][id]" class="form-control"/></td>
                <td><input type="number" value="<?php echo $variant->cost ?>" name="variant[0][cost]" class="form-control"/></td>
                <td><input type="number" value="<?php echo $variant->price ?>" name="variant[0][price]" class="form-control"/></td>
                <td><button class="btn-remove-tr btn btn-danger"><i class="fa fa-close"></i></button></td>
            </tr>  
                <?php endforeach; ?>
            <?php else: ?>
            <tr>
                <td><input type="text" value="" name="variant[0][variant]" class="form-control"/></td>
                <td><input type="number" value="0" name="variant[0][cost]" class="form-control"/></td>
                <td><input type="number" value="0" name="variant[0][price]" class="form-control"/></td>
                <td><button class="btn-remove-tr btn btn-danger"><i class="fa fa-close"></i></button></td>
            </tr>
            <?php endif;  ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4"></td>
                <td>
                    <button type="submit" class="btn btn-warning addvariant">Add Variant</button>
                </td>
            </tr>
        </tfoot>
    </table>
<?php endif; ?>

  