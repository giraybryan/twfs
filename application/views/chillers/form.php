<a href="<?php echo base_url('chillers'); ?>" class="btn btn-warning">Back to Chillers</a>
  <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo base_url('chillers/'.$formaction);?>">

    <input type="hidden"  value="<?php echo !empty($item->id) ? $item->id : ''?>" name="id"  />
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Item Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" value="<?php echo !empty($item->name) ? $item->name : ''?>" name="name" placeholder="Item Name">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Capital Price:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control"  value="<?php echo !empty($item->capital_price) ? $item->capital_price : ''?>" id="capital_price" name="capital_price" placeholder="Capital Price">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Selling Price:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="selling_price" value="<?php echo !empty($item->selling_price) ? $item->selling_price : ''?>" name="selling_price" placeholder="Selling Price">
        </div>
    </div>
    

    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
</form>