<div class="table-responsive">
<table class="table">
    <thead>
    	<tr>
            <th style="width: 10px">ID</th>
            <th>Item</th>
            <th>Current Stock</th>
            <th>Capital Price</th>
            <th>Selling Price</th>
            <th>Action</th>
    	</tr>
    </thead>
    <tbody>
    <?php if( !empty($chillers) ): ?>
        <?php foreach($chillers as $chiller):?>
            <tr>
                <td><?php echo $chiller->id ?></td>
                <td><?php echo $chiller->name; ?></td>
                <td><?php echo $chiller->stock ?></td>
                <td><?php echo $chiller->capital_price  ?></td>
                <td><?php echo $chiller->selling_price?></td>
                <td data-encode='<?php echo json_encode($chiller)?>'>
                	<a href="<?php echo base_url('chillers/add_inventory/'.$chiller->id)?>"><button class="btn btn-success"> <i class="fa fa-plus"></i> inventory</button></a>
                	<a href="<?php echo base_url('chillers/edit/'.$chiller->id);?>"><button class="btn btn-warning btn-sm action-btn" data-action='add'><i class="fa fa-pencil"></i> Edit</button></a>
                	<button class="btn btn-danger btn-sm action-btn" data-action="delete"  data-id="<?php echo $chiller->id; ?>"><i class="fa fa-times"></i></button>
                </td>
            </tr>
        <?php endforeach;?>
	<?php else: ?>
		<tr>
			<td colspan='4'>No records</td>
		</tr>
    <?php endif; ?>
	</tbody>
</table>
</div>