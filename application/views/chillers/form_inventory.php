<a href="<?php echo base_url('chillers'); ?>" class="btn btn-warning">Back to Chillers</a>
  <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo base_url('chillers/'.$formaction);?>">

    <input type="hidden"  value="<?php echo !empty($item->id) ? $item->id : ''?>" name="chiller_id"  />
    <input type="hidden"  value="<?php echo !empty($inventory->id) ? $inventory->id : ''?>" name="id"  />
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Item Name:</label>
        <div class="col-lg-8">
            <input type="text" readonly class="form-control" value="<?php echo !empty($item->name) ? $item->name : ''?>"/>
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Stock Quantity:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control"  value="<?php echo !empty($inventory->qty) ? $inventory->qty : ''?>" id="qty" name="qty" placeholder="Stock Quantity">
        </div>
    </div>
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Date Added:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control datepick"  value="<?php echo date('m/d/Y')?>" id="date_added" name="date_added" placeholder="Stock Quantity">
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
</form>