<a href="<?php echo base_url('chillers/sales'); ?>" class="btn btn-warning">Back to Sales</a>
  <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo base_url('chillers/'.$formaction);?>">

    <input type="hidden"  value="<?php echo !empty($sales->id) ? $sales->id : ''?>" name="id"  />
   	<div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Date:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control datepick"  value="<?php echo !empty($sales->date) ? date('m/d/Y', strtotime($sales->date)) : date('m/d/Y')?>" required id="date" name="date" placeholder="Date">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Item:</label>
        <div class="col-lg-8">
           <select required name="chiller_id" class="form-control"> 
           	<option value="">Select Item</option>
           	<?php if( !empty($items)): ?>
	           	<?php foreach( $items as $item): ?>
	           		<option value="<?php echo $item->id;?>" <?php echo  !empty($sales->chiller_id) && $sales->chiller_id == $item->id ? " selected ": ""; ?>><?php echo $item->name;?></option>
	           	<?php endforeach; ?>
           	<?php endif; ?>
           </select>
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Quantity:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control"  value="<?php echo !empty($sales->qty) ? $sales->qty : ""; ?>" required id="qty" name="qty" placeholder="Quantity">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Note:</label>
        <div class="col-lg-8">
            <textarea name="note" id="note"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Member:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="membersearch" placeholder="Member Name Search" value="<?php echo !empty($sales->firstname) ? $sales->firstname.' '.$sales->lastname : ""?>">
            <div id="member_searchresult" class="" style="background: #fff;">
              
            </div>
            <br/>
            <div id="memberselected"></div>
            <input type="hidden" value="<?php echo !empty($sales->member_id)  ? $sales->member_id : "" ?>" name="member_id" id="member_id" />

        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label"></label>
        <div class="col-lg-8">
           <label><input type="checkbox" name="paid" value="1" <?php echo (!empty($sales->paid) ? "checked": '') ?>> Paid:</label>
        </div>
    </div>

    

    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
</form>