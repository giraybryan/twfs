<div class="table-responsive">
<table class="table">
    <thead>
    	<tr>
            <th style="width: 10px">ID</th>
            <th>Item</th>
            <th>Quantity</th>
            <th>Note</th>
            <th>Member</th>
            <th>Status</th>
            <th>Action</th>
    	</tr>
    </thead>
    <tbody>
    <?php if( !empty($saleslog) ): ?>
        <?php foreach($saleslog as $log):?>
            <tr>
                <td><?php echo $log->id ?></td>
                <td><?php echo $log->name; ?></td>
                <td><?php echo $log->qty ?></td>
                <td><?php echo $log->note ?></td>
                <td><?php echo !empty($log->member_id) ? $log->firstname.' '.$log->lastname : '' ?></td>
                <td><?php echo $log->paid == 1 ? "PAID" : '' ?></td>
                <td data-encode='<?php echo json_encode($chiller)?>'>
                	<?php if(!$log->paid):?>
                	<span class='markpaid' data-id='<?php echo $log->id ?>'><button class="btn btn-success">Mark As Paid</button> </span>
                	<?php endif; ?>
                	<a href="<?php echo base_url('chillers/addsales/'.$log->id);?>"><button class="btn btn-warning btn-sm action-btn" data-action='add'><i class="fa fa-pencil"></i> Edit</button></a>
                	<button class="btn btn-danger btn-sm action-btn" data-action="delete"  data-id="<?php echo $log->id; ?>"><i class="fa fa-times"></i></button>
                </td>
            </tr>
        <?php endforeach;?>
	<?php else: ?>
		<tr>
			<td colspan='4'>No records</td>
		</tr>
    <?php endif; ?>
	</tbody>
</table>
</div>