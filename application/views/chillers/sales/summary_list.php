<div class="table-responsive">
<table class="table">
    <thead>
    	<tr>
            <th>Item</th>
            <th>Capital</th>
            <th>Amount</th>
            <th>Earnings</th>
            <th>Additional</th>
            <th>Sold</th>
            <th>Left</th>
    	</tr>
    </thead>
    <tbody>
    <?php if( !empty($summary) ): ?>
    	<?php 
    	$sumtotal_capital  = 0;
    	$sumtotal_amount   = 0;
    	$sumtotal_earnings = 0;
    	?>
        <?php foreach($summary as $summ):?>
            <tr>
            
                <td><?php echo $summ->name; ?></td>
                <td><?php echo $totalcapital = !empty($summ->sold) ? $capitaltotal = $summ->sold * $summ->capital_price : 0; ?></td>
                <td><?php echo $totalamount = !empty($summ->sold) ? $summ->sold * $summ->selling_price : 0; ?></td>
                <td><?php echo $totalearnings = $totalamount - $totalcapital ?></td>
                <td><?php echo $summ->additional ?></td>
                <td><?php echo $summ->sold ?></td>
                <td><?php echo $summ->remaining?></td> 
                <?php 
                $sumtotal_capital += $totalcapital; 
                $sumtotal_amount += $totalamount; 
                $sumtotal_earnings += $totalearnings; 
                ?>
            </tr>
        <?php endforeach;?>
        <tr> 
			<td> </td>
			<td><?php echo $sumtotal_capital ?><input type="hidden" id="capital" value="<?php echo $sumtotal_capital?>" name="capital"/></td>
			<td><?php echo $sumtotal_amount ?></td>
			<td><?php echo $sumtotal_earnings ?><input type="hidden" id="profit" value="<?php echo $sumtotal_earnings?>" name="profit"/></td>
			<td><button class="btn btn-default" onclick="return false" id="finalized">Finalize</button></td>
			<td></td>
			<td></td>
		</tr>
	<?php else: ?>
		
		<tr>
			<td colspan='4'>No records</td>
		</tr>
    <?php endif; ?>
	</tbody>

</table>
<br/>
<div class="col-sm-4">
<table class="table table-bordered">
    <thead>
        <tr>
            <th colspan="2">Yesterday (<?php echo date('m/d/Y',strtotime($capital->lastday_date))?>)</th>
        </tr>
        <tr>
            <th>Box Capital</th>
            <th>Box Profit</th>
        </tr>
    </thead>
    <tbody>
        <td><?php echo $capital->amount?><input type="hidden" id="capitalbox" name=""></td>
        <td><?php echo $profit->amount?><input type="hidden" id="profitbox" name=""></td>
    </tbody>
</table>
</div>

</div>