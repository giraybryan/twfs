<div class="row">
	<div class="col-md-12">
		<div class="panel">
            <header class="panel-heading">
                Chiller List
            </header>
            <div class="panel-body">
                <div class="box-tools">
                	<?php if(!empty($this->session->userdata['msg'])): ?>
                		<div class="alert alert-info"><?php echo $this->session->userdata['msg']; ?></div>
                		<?php $this->session->msg = "";  ?>
                	<?php endif; ?>
                	<a href="<?php echo base_url().'chillers/add'?>"><button class='btn btn-success btn-sm' data-toggle="modal" data-target="#metermodal">Add Chiller Item</button></a>
                    <a href="<?php echo base_url().'chillers/sales'?>" class="btn btn-info btn-sm">Sales</a>
                    <!--
                    <ul class="pagination pagination-sm m-b-10 m-t-10 pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                	-->
                </div>
                <div id="module_list">
                	<?php $this->load->view("chillers/list"); ?>
                </div>
            </div><!-- /.panel-body -->
        </div>
	</div>
</div>

