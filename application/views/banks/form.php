
<?php if(isset($response['result'])): ?>
  <?php if($response['result'] == 1):?>
     <div class="alert alert-success"><?php echo $response['msg']; ?></div>
  <?php else:?>
      <div class="alert alert-danger"><?php echo $response['msg']; ?></div>
  <?php endif;?>
<?php else:?>

<?php endif; ?> 

  <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo !empty($sales->id) ? base_url('banks/edit/'.$sales->id) :  base_url('banks/add');?>">
    <div class="form_result"></div>
    <!--
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Middle Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="middle" name="middlename" placeholder="Middle Name">
        </div>
    </div>
    -->
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Bank Name:</label>
        <div class="col-lg-8">
          <input type="text" required name="bankname" id="bankname" placeholder="Bank Name" value="<?php echo !empty($bank->bankname) ? $bank->bankname : '' ?>" class="form-control">
        </div>
    </div>
    
   
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Initial Amount:</label>
        <div class="col-lg-8">
            <input type="number" class="form-control" id="amount"  required name="amount" placeholder="amount" value="<?php echo !empty($bank->amount) ? $bank->amount : '' ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Date Opened:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control datepick" id="date_update" required name="date_update" placeholder="Date" value="<?php echo !empty($bank->date_update) ? date('m/d/Y',strtotime($bank->date_update)) : date('m/d/Y') ?>">
        </div>
    </div>
  
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
</form>