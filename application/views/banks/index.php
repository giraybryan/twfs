<ul class="nav nav-tabs">
  <li class="active"><a href="#">Banks</a></li>
  <li><a href="<?php echo base_url('banktransacs') ?>">Bank Transactions</a></li>
</ul>
<div class="row">
	<div class="col-md-12">
		<div class="panel">
            <header class="panel-heading">
                Bank List
            </header>
            <div class="panel-body">
                <div class="box-tools">
                	<a href="<?php echo base_url().'banks/add'?>"><button class='btn btn-success btn-sm' data-toggle="modal" data-target="#metermodal">Add Bank</button></a>
                    <!--
                    <ul class="pagination pagination-sm m-b-10 m-t-10 pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                	-->
                </div>
                <div class="form-horizontal">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group ">
                            <div class="col-sm-8">
                            <label>Filter Date</label>
                            
                            <input type="text" class="form-control datepick" id="date" name="date" value="<?php echo date('m/d/Y'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                           
                            <div class="col-sm-4">
                                <label>Filter By Date Range </label>
                                <input type="text" name="daterange" class="form-control" value="" />
                            
                           </div>
                        </div>
                    </div>
                </div>
                </div>
                <div id="module_list">
                	<?php $this->load->view("banks/list"); ?>
                </div>
            </div><!-- /.panel-body -->
        </div>
	</div>
</div>

