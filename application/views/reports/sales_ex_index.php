<style type="text/css">
    .chart_wrapper { min-height: 420px; background: #000; }
</style>
<link rel="stylesheet" href="<?php echo base_url('assets/js/amchart/v3')?>/export.css" type="text/css" media="all" />
<script src="<?php echo base_url('assets/js/amchart/v3')?>/charts.js"></script>
<script src="<?php echo base_url('assets/js/amchart/v3')?>/serial.js"></script>
<script src="<?php echo base_url('assets/js/amchart/v3')?>/export.min.js"></script>
<script src="<?php echo base_url('assets/js/amchart/v3')?>/black.js"></script>
<script src="<?php echo base_url('assets/js/amchart/v3')?>/black.js"></script>

<div class="container">
			<div class="form-horizontal">
				<div class="form-group">      
					<div class="col-sm-4">
						<label>Filter By Date Range </label>
						<input type="text" name="daterange" value="<?php echo $date_range; ?>" class="form-control" value="" />
					</div>
				</div>
			</div>
		</div>

<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Sales Vs Expenses</a></li>
    <!-- <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Sales Breakdown</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Expense Breakdown</a></li> -->
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
    	<br/>
		
    	<div id="sales_ex_compare">
		  <?php $this->load->view("reports/sales_expensebar"); ?>
		</div>

    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
    	<div id="sales_brekadown">
    		
		  <?php $this->load->view("reports/sales_breakdown"); ?>
		</div>
    </div>
    <div role="tabpanel" class="tab-pane" id="messages">
    	<div id="expense_brekadown">
		  <?php $this->load->view("reports/expense_breakdown"); ?>
		</div>
    </div>
  </div>

</div>



