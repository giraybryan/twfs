<div class="row"></div>


<script type="text/javascript">
var chart = AmCharts.makeChart("chartdiv", {
    "theme": "black",
    "type": "serial",
    "dataProvider": [<?php echo $sales_expense_string ?>],
    "valueAxes": [{
        "unit": " PHP",
        "position": "left",
        "title": "Sales VS Expenses",
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "[[category]] (Expense): <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "expense",
        "type": "column",
        "valueField": "expense"
    }, {
        "balloonText": "[[category]] (Sales): <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "sales",
        "type": "column",
        "clustered":false,
        "columnWidth":0.5,
        "valueField": "sales"
    }],
    "plotAreaFillAlphas": 0.1,
    "categoryField": "Month",
    "categoryAxis": {
        "gridPosition": "start"
    },
    "export": {
        "enabled": true
     }

});   

</script>
<!-- HTML -->



<div class="panel">
    <div class="panel-heading"><h4>Sales VS Expense</h4></div>
    <div class="panel-body">
        <div id="chartdiv" class="chart_wrapper"></div>
        <br/>
       <div class="row">
            <div class="col-sm-6">
                <div style="margin-bottom: 10px;">
                    <label>Highest Sales: </label> <br/><span> <?php echo $highest_sales['month'].' : '. number_format($highest_sales['total'],2) ?> </span>
                </div>
                <div>
                     <label>Lowsest Sales: </label> <br/><span> <?php echo $lowest_sales['month'].' : '. number_format($lowest_sales['total'],2) ?> </span>
                </div>
            </div>
            <div class="col-sm-6">
                <div style="margin-bottom: 10px;">
                    <label>Highest Expense: </label> <br/><span> <?php echo $highest_expense['month'].' : '. number_format($highest_expense['total'],2) ?> </span>
                </div>
                <div>
                     <label>Lowest Expense: </label> <br/><span> <?php echo $lowest_expense['month'].' : '. number_format($lowest_expense['total'],2) ?> </span>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .panel-heading h4 { margin-bottom: 0px; }
</style>

<!-- Chart code -->
<div class="row">
    <div class="col-sm-6">
    <?php 
        $sales_category = array();
        $sales_data = array();
        $maxtotalsales = 0;
        $expense_category = array();
        $expense_data = array();
        $maxtotalexpense = 0;
        foreach ($month_category as $monthkey =>  $month_c) {
            $row_data = array( 'month' => $monthkey);
            $expense_row = $row_data;
            foreach($month_c as $yearkey => $year){
                //FOR SALES
                if( !in_array($yearkey, $sales_category))
                    array_push($sales_category, (string) $yearkey);

                if( $year['sales'] > $maxtotalsales )
                    $maxtotalsales = $year['sales'];
                $row_data[$yearkey] = $year['sales'];

                //FOR EXPENSE
                if( !in_array($yearkey, $expense_category))
                    array_push($expense_category, (string) $yearkey);

                if( $year['expense'] > $maxtotalexpense )
                    $maxtotalexpense = $year['expense'];
                $expense_row[$yearkey] = $year['expense'];
            }
            array_push($sales_data, $row_data);
            array_push($expense_data, $expense_row);
        }

    ?>
<script type="text/javascript">
    
    maxtotalsales = parseInt('<?php echo $maxtotalsales; ?>') ;
    years = JSON.parse('<?php echo json_encode($sales_category) ?>')
    //years = ["2019","2018"]
   
    graphjson = JSON.parse('<?php echo json_encode($sales_data); ?>');
    categoryjson = [];
    for(var key in years) {
      if ( typeof years[key] === 'string'){
        var rowcat = {
            "balloonText": "Sales in  [[category]] "+years[key]+" : [[value]]",
            "bullet": "round",
            /*"hidden": true,*/
            "title": ""+years[key]+"",
            "valueField": ""+years[key]+"",
            "fillAlphas": 0
        }
        categoryjson.push(rowcat);
      }
       
    }

</script>

<script type="text/javascript">

    var linechart = AmCharts.makeChart("linechart", {
        "type": "serial",
        "theme": "black",
        "legend": {
            "useGraphSettings": true
        },
        "dataProvider": graphjson,
        "valueAxes": [{
            "integersOnly": true,
            "maximum": maxtotalsales,
            "minimum": 0,
            "reversed": false,
            "axisAlpha": 0,
            "dashLength": 5,
            "gridCount": 10,
            "position": "left",
            "title": "Month Year Sales"
        }],
        "startDuration": 0.5,
        "graphs": categoryjson,
        "chartCursor": {
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "month",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "fillAlpha": 0.05,
            "fillColor": "#000000",
            "gridAlpha": 0,
            "position": "bottom"
        },
        "export": {
            "enabled": true,
            "position": "bottom-right"
         }
    });
    </script>
    <style>
    #linechart {
        width   : 100%;
        height  : 420px;
    }                   
    </style>
    <div class="panel">
        <div class="panel-heading"><h4>Sales Comparison (Line Chart)</h4></div>
        <div class="panel-body">

            <div id="linechart" class="chart_wrapper"></div>  
        </div>
    </div>
<!-- HTML -->

              <script type="text/javascript">
                setTimeout(function(){
            
                  var chart_doughnut_settings = {
                    type: 'doughnut',
                    tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                    data: {
                      labels:<?php echo json_encode($brkdownsales_category) ?>,
                      datasets: [{
                        data: <?php echo json_encode($brkdownsales_category_data)?>,
                        backgroundColor:<?php echo json_encode($colorbrkdown) ?>,
                        hoverBackgroundColor: <?php echo json_encode($colorbrkdown) ?>
                      }]
                    },
                    options: { 
                      legend: false, 
                      responsive: false 
                    }
                  }
                
                  $('.sales_canvasDoughnut').each(function(){
                    
                    var chart_element = $(this);
                    var chart_doughnut = new Chart( chart_element, chart_doughnut_settings);
                    
                  });     
                },1000)
              </script>
              <div class="x_panel tile overflow_hidden">
                <div class="x_title">
                  <h2>Sales BreakDown</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <!--<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>-->
          
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table class="" style="width:100%">
                    <tr>
                      <th style="width:37%;">
                        
                      </th>
                      <th>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                          <p class=""></p>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                         
                        </div>
                      </th>
                    </tr>
                    <tr>
                      <td>
                        <canvas class="sales_canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                      </td>
                      <td>
                        <table class="tile_info">
                         <?php foreach( $brkdownsales_category as $key => $_sales): ?>
                         <tr>
                            <td>
                              <p><i class="fa fa-square" style="color: <?php echo $colorbrkdown[$key] ?>"></i><?php echo $brkdownsales_category[$key] ?> </p>
                            </td>
                            <td><?php echo number_format($brkdownsales_category_data[$key],2) ?></td>
                          </tr>
                         <?php endforeach; ?>
                          
                          
                        
                        </table>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>

    </div>
    <div class="col-sm-6">
        
        <script type="text/javascript">
    
    maxtotalexpense = parseInt('<?php echo $maxtotalexpense; ?>') ;
    ex_years = JSON.parse('<?php echo json_encode($expense_category) ?>')
    //years = ["2019","2018"]
   
    graphjson = JSON.parse('<?php echo json_encode($expense_data); ?>');
    categoryjson = [];
    for(var key in ex_years) {
      if ( typeof ex_years[key] === 'string'){
        var rowcat = {
            "balloonText": "Expenses in  [[category]] "+ex_years[key]+" : [[value]]",
            "bullet": "round",
            /*"hidden": true,*/
            "title": ""+ex_years[key]+"",
            "valueField": ""+ex_years[key]+"",
            "fillAlphas": 0
        }
        categoryjson.push(rowcat);
      }
       
    }

</script>

<script type="text/javascript">

    var linechart = AmCharts.makeChart("expense_linechart", {
        "type": "serial",
        "theme": "black",
        "legend": {
            "useGraphSettings": true
        },
        "dataProvider": graphjson,
        "valueAxes": [{
            "integersOnly": true,
            "maximum": maxtotalexpense,
            "minimum": 0,
            "reversed": false,
            "axisAlpha": 0,
            "dashLength": 5,
            "gridCount": 10,
            "position": "left",
            "title": "Month Year expenses"
        }],
        "startDuration": 0.5,
        "graphs": categoryjson,
        "chartCursor": {
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "month",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "fillAlpha": 0.05,
            "fillColor": "#000000",
            "gridAlpha": 0,
            "position": "bottom"
        },
        "export": {
            "enabled": true,
            "position": "bottom-right"
         }
    });
    </script>
    <style>
    #linechart {
        width   : 100%;
        height  : 420px;
    }                   
    </style>
    <div class="panel">
        <div class="panel-heading"><h4>Expenses Comparison (Line Chart)</h4></div>
        <div class="panel-body">

            <div id="expense_linechart" class="chart_wrapper"></div>  
        </div>
    </div>
<!-- HTML -->



        <script type="text/javascript">
                setTimeout(function(){
            
                  var chart_doughnut_settings = {
                    type: 'doughnut',
                    tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                    data: {
                      labels:<?php echo json_encode($brkdownexpense_category) ?>,
                      datasets: [{
                        data: <?php echo json_encode($brkdownexpense_category_data)?>,
                        backgroundColor:<?php echo json_encode($colorbrkdown) ?>,
                        hoverBackgroundColor: <?php echo json_encode($colorbrkdown) ?>
                      }]
                    },
                    options: { 
                      legend: false, 
                      responsive: false 
                    }
                  }
                
                  $('.expense_canvasDoughnut').each(function(){
                    
                    var chart_element = $(this);
                    var chart_doughnut = new Chart( chart_element, chart_doughnut_settings);
                    
                  });     
                },1000)
              </script>
              <div class="x_panel tile overflow_hidden">
                <div class="x_title">
                  <h2>Expense BreakDown</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <!--<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>-->
          
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table class="" style="width:100%">
                    <tr>
                      <th style="width:37%;">
                        
                      </th>
                      <th>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                          <p class=""></p>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                         
                        </div>
                      </th>
                    </tr>
                    <tr>
                      <td>
                        <canvas class="expense_canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                      </td>
                      <td>
                        <table class="tile_info">
                         <?php foreach( $brkdownexpense_category as $key => $_exp): ?>
                         <tr>
                            <td>
                              <p><i class="fa fa-square" style="color: <?php echo $colorbrkdown[$key] ?>"></i><?php echo $brkdownexpense_category[$key] ?> </p>
                            </td>
                            <td><?php echo number_format($brkdownexpense_category_data[$key],2) ?></td>
                          </tr>
                         <?php endforeach; ?>
                          
                          
                        
                        </table>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>

    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <script>
        var year_gross = AmCharts.makeChart("year_gross", {
            "theme": "black",
            "type": "serial",
            "dataProvider": <?php echo $annual_gross ?>,
            "valueAxes": [{
                "title": "Gross Income, PHP"
            }],
            "graphs": [{
                "balloonText": "Income in [[category]]:[[value]]",
                "fillAlphas": 1,
                "lineAlpha": 0.2,
                "title": "Income",
                "type": "column",
                "valueField": "income"
            }],
            "depth3D": 20,
            "angle": 30,
            "rotate": true,
            "categoryField": "year",
            "categoryAxis": {
                "gridPosition": "start",
                "fillAlpha": 0.05,
                "position": "left"
            },
            "export": {
                "enabled": true
             }
        });
        </script>
        <div class="panel">
            <div class="panel-heading">Annual Gross</div>
            <div class="panel-body">
                <div id="year_gross" class="chart_wrapper"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <script>
        var year_net = AmCharts.makeChart("year_net", {
            "theme": "black",
            "type": "serial",
            "dataProvider": <?php echo $annual_net ?>,
            "valueAxes": [{
                "title": "Net Income, PHP"
            }],
            "graphs": [{
                "balloonText": "Net Income in [[category]]:[[value]]",
                "fillAlphas": 1,
                "lineAlpha": 0.2,
                "title": "Income",
                "type": "column",
                "valueField": "income"
            }],
            "depth3D": 20,
            "angle": 30,
            "rotate": true,
            "categoryField": "year",
            "categoryAxis": {
                "gridPosition": "start",
                "fillAlpha": 0.05,
                "position": "left"
            },
            "export": {
                "enabled": true
             }
        });
        </script>
        <div class="panel">
            <div class="panel-heading">Annual Net</div>
            <div class="panel-body">
                <div id="year_net" class="chart_wrapper"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <script>
        var year_net = AmCharts.makeChart("year_expenses", {
            "theme": "black",
            "type": "serial",
            "dataProvider": <?php echo $annual_gross ?>,
            "valueAxes": [{
                "title": "Annual Expenses, PHP"
            }],
            "graphs": [{
                "balloonText": "Annual Expense in [[category]]:[[value]]",
                "fillAlphas": 1,
                "lineAlpha": 0.2,
                "title": "Expense",
                "type": "column",
                "valueField": "expense"
            }],
            "depth3D": 20,
            "angle": 30,
            "rotate": true,
            "categoryField": "year",
            "categoryAxis": {
                "gridPosition": "start",
                "fillAlpha": 0.05,
                "position": "left"
            },
            "export": {
                "enabled": true
             }
        });
        </script>
        <div class="panel">
            <div class="panel-heading">Annual Expenses</div>
            <div class="panel-body">
                <div id="year_expenses" class="chart_wrapper"></div>
            </div>
        </div>
    </div>
</div>