<div class="row"></div>
<style type="text/css">
    .chart_wrapper { min-height: 350px; background: #000; }
</style>
<link rel="stylesheet" href="<?php echo base_url('assets/js/amchart/v3')?>/export.css" type="text/css" media="all" />
<script src="<?php echo base_url('assets/js/amchart/v3')?>/charts.js"></script>
<script src="<?php echo base_url('assets/js/amchart/v3')?>/serial.js"></script>
<script src="<?php echo base_url('assets/js/amchart/v3')?>/export.min.js"></script>
<script src="<?php echo base_url('assets/js/amchart/v3')?>/black.js"></script>
<script src="<?php echo base_url('assets/js/amchart/v3')?>/black.js"></script>

<script type="text/javascript">
var chart = AmCharts.makeChart("chartdiv", {
    "theme": "black",
    "type": "serial",
    "dataProvider": <?php echo $new_renew_string ?>,
    "valueAxes": [{
        "unit": "",
        "position": "left",
        "title": "New Members VS Renewal",
    }],
    "startDuration": 1,
    "graphs": [{
        "balloonText": "[[category]] (New Members): <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "New",
        "type": "column",
        "valueField": "new"
    }, {
        "balloonText": "[[category]] (Renewal): <b>[[value]]</b>",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "Renew",
        "type": "column",
        "clustered":false,
        "columnWidth":0.5,
        "valueField": "renew"
    }],
    "plotAreaFillAlphas": 0.1,
    "categoryField": "Month",
    "categoryAxis": {
        "gridPosition": "start"
    },
    "export": {
        "enabled": true
     }

});   

</script>
<!-- HTML -->
<div class="panel">
    <div class="panel-heading"><h4>New Members Vs Renewal</h4></div>
    <div class="panel-body">
        <div id="chartdiv" class="chart_wrapper"></div>


        <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total New Members</span>
                <div class="count"><?php echo $total_new; ?></div>
                
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Total Renew Members</span>
                <div class="count"><?php echo $total_renew; ?></div>
                
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .panel-heading h4 { margin-bottom: 0px; }
    .pink { color: #f64f8a; }
</style>

<!-- Chart code -->
<div class="row">
    <div class="col-sm-6">
              <script type="text/javascript">
                setTimeout(function(){
            
                  var chart_doughnut_settings = {
                    type: 'doughnut',
                    tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                    data: {
                      labels: [
                        "Female",
                        "Male"
                      ],
                      datasets: [{
                        data: [<?php echo $genders['female'] ?>, <?php echo $genders['male'] ?>],
                        backgroundColor: [
                          "#f64f8a",
                          "#1a67df",
                  
                        ],
                        hoverBackgroundColor: [
                          "#ff85b0",
                          "#75abff",
                          
                        ]
                      }]
                    },
                    options: { 
                      legend: false, 
                      responsive: false 
                    }
                  }
                
                  $('.canvasDoughnut').each(function(){
                    
                    var chart_element = $(this);
                    var chart_doughnut = new Chart( chart_element, chart_doughnut_settings);
                    
                  });     
                },1000)
              </script>
              <div class="x_panel tile overflow_hidden">
                <div class="x_title">
                  <h2>New Registration Population</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <!--<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>-->
          
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table class="" style="width:100%">
                    <tr>
                      <th style="width:37%;">
                        
                      </th>
                      <th>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                          <p class=""></p>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                         
                        </div>
                      </th>
                    </tr>
                    <tr>
                      <td>
                        <canvas class="canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                      </td>
                      <td>
                        <table class="tile_info">
                          <tr>
                            <td>
                              <p><i class="fa fa-square blue"></i>Male </p>
                            </td>
                            <td><?php echo $genders['male']?></td>
                          </tr>
                          <tr>
                            <td>
                              <p><i class="fa fa-square pink"></i>Female </p>
                            </td>
                            <td><?php echo $genders['female']?></td>
                          </tr>
                          
                        
                        </table>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
    

    </div>
    <div class="col-sm-6">
    
      <script type="text/javascript">
                setTimeout(function(){
            
                  var chart_doughnut_settings = {
                    type: 'doughnut',
                    tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                    data: {
                      labels: [
                        "Female",
                        "Male"
                      ],
                      datasets: [{
                        data: [<?php echo $renew_gender['female'] ?>, <?php echo $renew_gender['male'] ?>],
                        backgroundColor: [
                          "#f64f8a",
                          "#1a67df",
                  
                        ],
                        hoverBackgroundColor: [
                          "#ff85b0",
                          "#75abff",
                          
                        ]
                      }]
                    },
                    options: { 
                      legend: false, 
                      responsive: false 
                    }
                  }
                
                  $('.renew_canvasDoughnut').each(function(){
                    
                    var chart_element = $(this);
                    var chart_doughnut = new Chart( chart_element, chart_doughnut_settings);
                    
                  });     
                },1000)
              </script>
              <div class="x_panel tile overflow_hidden">
                <div class="x_title">
                  <h2>Renewal Population</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <!--<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>-->
          
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table class="" style="width:100%">
                    <tr>
                      <th style="width:37%;">
                        
                      </th>
                      <th>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                          <p class=""></p>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                         
                        </div>
                      </th>
                    </tr>
                    <tr>
                      <td>
                        <canvas class="renew_canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                      </td>
                      <td>
                        <table class="tile_info">
                          <tr>
                            <td>
                              <p><i class="fa fa-square blue"></i>Male </p>
                            </td>
                            <td><?php echo $renew_gender['male']?></td>
                          </tr>
                          <tr>
                            <td>
                              <p><i class="fa fa-square pink"></i>Female </p>
                            </td>
                            <td><?php echo $renew_gender['female']?></td>
                          </tr>
                          
                        
                        </table>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
    </div>
</div>