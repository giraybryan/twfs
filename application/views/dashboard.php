<!-- top tiles -->
<div class="row tile_count">
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
  <span class="count_top"><i class="fa fa-user"></i> Total Members</span>
  <div class="count"><?php echo $total_members?></div>
  <span class="count_bottom">
    <?php if( $percent_result >= 0 ): ?>
    <i class="green"><i class="fa fa-sort-asc"></i><?php echo format_percent($percent_result) ?>% </i> 
    <?php else: ?>
    <i class="red"><i class="fa fa-sort-desc"></i><?php echo format_percent($percent_result) ?>% </i>     
    <?php endif; ?>
        From last Month
    </span>
</div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
  <span class="count_top"><i class="fa fa-user"></i> Total Monthly Sales</span>
  <div class="count"><?php echo $sales_month ?></div>
  <span class="count_bottom">
        <?php if( $walkin_result >= 0 ): ?>
        <i class="green"><i class="fa fa-sort-asc"></i><?php echo $sales_month; ?>% </i> 
        <?php else: ?>
        <i class="red"><i class="fa fa-sort-desc"></i><?php echo $sales_month ?>% </i>     
        <?php endif; ?>
            From last Month
        </span>
  </span>
</div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
  <span class="count_top"><i class="fa fa-clock-o"></i> Daily Sales</span>
  <div class="count"><?php echo $sales_today; ?></div>
  <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span>
</div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
  <span class="count_top"><i class="fa fa-user"></i> Monthly Expenses</span>
  <div class="count"><?php echo $expenses_month; ?></div>
  <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
</div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
  <span class="count_top"><i class="fa fa-user"></i> Daily Expenses</span>
  <div class="count"><?php echo $expenses_today; ?></div>
  <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
</div>
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
  <span class="count_top"><i class="fa fa-dollar"></i> COH</span>
  <div class="count"><?php echo number_format($coh,2); ?></div>
</div>

</div>
<div class="row tile_count">
<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
  <span class="count_top"><i class="fa fa-dollar"></i> Bank Balance</span>
  <?php 
    $total_balance = 0;
    foreach($banks as $bank): 
      $total_balance = $total_balance + $bank->amount;
    endforeach;
  ?>
  <div class="count"><?php echo number_format($total_balance,2); ?></div>
  <div class="count_bottom"><a style="color: #4f77fa" href="<?php echo base_url('banks') ?>">Go to Banks</a></div>
</div>
</div>

<!-- /top tiles -->


<div class="row">

    <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320 overflow_hidden">
                <div class="x_title">
                  <h2>Monthly Enrollees</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <!-- <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>-->
                    
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <th><?php echo date('F').' Total New Enrollees'; ?></th>
                        <td><?php echo $month_newenroll->total?></td>
                      </tr>
                      <tr>
                        <th><?php echo date('F').' Total Renewal' ?></th>
                        <td><?php echo $month_renewal->total?></td>
                      </tr>
                    </tbody>
                  </table>
                  <table class="table table-bordered">
                    <tr>
                      <tr>
                        <th><?php echo date('F', strtotime('- 1 months')).' Total New Enrollees'; ?></th>
                        <td><?php echo $lastmonth_newenroll->total?></td>
                      </tr>
                      <tr>
                        <th><?php echo date('F', strtotime('- 1 months')).' Total Renewal' ?></th>
                        <td><?php echo $lastmonth_renewal->total?></td>
                      </tr>
                    </tr>
                  </table>
                  
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="x_panel tile fixed_height_320 overflow_hidden">
                <div class="x_title">
                <h2>Daily Attendees</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <!--<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>-->
          
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content tile_count">
                  <div class="form-horizontal">
                    <div class="form-group">
                      <label class="col-sm-2" style="padding-top:5px;font-size: 15px;">Date:</label>
                      <div class="col-sm-10">
                        <input type="text" class="datepick form-control" id="attendance_date" value="<?php echo date('m/d/Y', strtotime($date));?>"/>
                      </div>
                    </div>
                  </div>
                  <div class="container tile_stats_count">
                    <h4>Total Attendees :  </h4>
                      <div class="count" id="total_attendees"><?php echo $attendance->total_attendees; ?></div>
                  </div>

                </div>
              </div>
            </div>
        <div class="col-md-4 col-sm-4 col-xs-12 hide">
              <div class="x_panel tile fixed_height_320 overflow_hidden">
                <div class="x_title">
                  <h2>Class Enrollies</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <!--<li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>-->
          
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <table class="" style="width:100%">
                    <tr>
                      <th style="width:37%;">
                        <p>Top 5</p>
                      </th>
                      <th>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                          <p class="">Classes</p>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                          <p class="">Total</p>
                        </div>
                      </th>
                    </tr>
                    <tr>
                      <td>
                        <canvas class="canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
                      </td>
                      <td>
                        <table class="tile_info">
                          <tr>
                            <td>
                              <p><i class="fa fa-square blue"></i>Circuit </p>
                            </td>
                            <td><?php echo $class1_total?></td>
                          </tr>
                          <tr>
                            <td>
                              <p><i class="fa fa-square green"></i>Baby Ballet </p>
                            </td>
                            <td><?php echo $class2_total?></td>
                          </tr>
                          
                        
                        </table>
                      </td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>


</div>
<div class="row">
  <div class="col-sm-12">
     <div class="x_panel tile overflow_hidden">
        <div class="x_title">
          <h2>Will Expire soon</h2>
           <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Mobile</th>
              <th>Membership</th>
              <th>End Date</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($willexpire as $mem): ?>
            <tr>
              <td><?php echo $mem->firstname.' '.$mem->lastname ?></td>
              <td><?php echo $mem->mobile ?></td>
              <td><?php echo $mem->class_title.' ( '.$mem->membership_type.' )' ?></td>
              <td><?php echo date('Y-m-d', strtotime($mem->date_end)) ?></td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
        
      </div>
      <div class="clearfix"></div>
      </div>

  </div>
</div>


