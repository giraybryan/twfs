<div class="table-responsive">
<table class="table">
    <thead>
    	<tr>
            <th>Duration</th>
            <th>Action</th>
    	</tr>
    </thead>
   
    <?php if( !empty($durations) ): ?>
        <tbody>
        <?php $total_rates = 0; ?>
        <?php foreach($durations as $duration):?>
            <tr>
                <td><?php echo $duration->durations.' ('.$duration->duration_type.')' ?></td>           
                <td data-encode='<?php echo json_encode($duration)?>'>
                   
                    <button class="btn btn-primary btn-sm action-btn hide" data-action='add'><i class="fa fa-plus"></i></button>

                    <a href="<?php echo base_url('classdurations/edit/'.$duration->id) ?>">
                	<button class="btn btn-warning btn-sm action-btn" data-action="edit"><i class="fa fa-pencil"></i></button>
                        </a>
                	<button data-id="<?php echo $duration->id ?>" data-table="vouchers" class="btn btn-danger btn-sm action-btn" data-action="delete" ><i class="fa fa-times"></i></button>
                </td>
            </tr>
           
        <?php endforeach;?>
    </tbody>
	<?php else: ?>
    <tbody>
        <tr>
			<td colspan='4'>No records</td>
		</tr>
    </tbody>
    <?php endif; ?>
	
</table>
</div>