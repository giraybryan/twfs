
<?php if(isset($response['result'])): ?>
  <?php if($response['result'] == 1):?>
     <div class="alert alert-success"><?php echo $response['msg']; ?></div>
  <?php else:?>
      <div class="alert alert-danger"><?php echo $response['msg']; ?></div>
  <?php endif;?>
<?php else:?>

<?php endif; ?> 
  <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo !empty($rate->id) ? base_url('classdurations/edit/'.$rate->id) :  base_url('classdurations/add');?>">
    <div class="form_result"></div>
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Duration Type:</label>
        <div class="col-lg-8">
          <select name="duration_type" id="duration_type" required class="form-control">
              <option value="">Select Duration type</option>
              <option value="monthly" <?php  echo !empty($duration->duration_type) && $duration->duration_type == 'monthly' ? 'selected' : ''  ?>>Monthly</option>
              <option value="session" <?php  echo !empty($duration->duration_type) && $duration->duration_type == 'session' ? 'selected' : ''  ?>>Session</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Duration:</label>
        <div class="col-lg-8">
            <input type="number" name="durations" required value="<?php echo !empty($duration->durations) ? $duration->durations : '' ?>" class='form-control'/>
           
            <input type="hidden" class="form-control" id="id" name="id" value="<?php echo !empty($rate->id) ? $rate->id : '' ?>">

        </div>
    </div>
    <!--
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Middle Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="middle" name="middlename" placeholder="Middle Name">
        </div>
    </div>
    -->
    
  
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
</form>