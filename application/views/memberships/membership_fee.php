  
  <?php if(isset($response['result'])): ?>
      <?php if($response['result'] == 1):?>
         <div class="alert alert-success"><?php echo $response['msg']; ?></div>
         
      <?php else:?>
          <div class="alert alert-danger"><?php echo $response['msg']; ?></div>
      <?php endif;?>

   <?php endif; ?> 
  <form class="form-horizontal targetform"  role="form" id="membershipform" method="post" name="membershipform" action="<?php echo base_url('memberships/membership_fee/'.$member->id);?><?php echo ( !empty($membership->id) ? "/".$membership->id : "" )?>">
    <div class="form_result"></div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Member Name:</label>
        <div class="col-lg-8" style="margin-top: 8px;">
            <label> <?php echo $member->firstname.' '.$member->lastname ?></label>
            <input type="hidden" class="form-control" value="<?php echo $member->id ?>" id="member_id" name="member_id">
            <input type="hidden" class="form-control" value="<?php echo (!empty($membership->id) ? $membership->id : "") ?>" id="id" name="id">
           
        </div>
    </div>
    <!--
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Middle Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="middle" name="middlename" placeholder="Middle Name">
        </div>
    </div>
    -->
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Amount:</label>
        <div class="col-lg-8">
           <input type="text" class="form-control" required readonly name="amount" id="amount" value="<?php echo $this->config_model->get_valueby_code('joiningfee') ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Date Registered:</label>
        <div class="col-lg-8">
           <input type="text" class="form-control datepick" name="date_register" id="date_register" value="<?php echo date('m/d/Y') ?>">
        </div>
    </div>

    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">WITH O.R:</label>
        <div class="col-lg-8">
          <label> 
          <input type="checkbox" checked name="or" value="1"> Yes:
          </label>
            
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
    
</form>