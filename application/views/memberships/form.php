  
  <?php if(isset($response['result'])): ?>
      <?php if($response['result'] == 1):?>
         <div class="alert alert-success"><?php echo $response['msg']; ?></div>
         <div class="">
           <a href="<?php echo base_url("members/add")?>" class="btn btn-primary btn-lg">Add New Member</a>
         </div>
      <?php else:?>
          <div class="alert alert-danger"><?php echo $response['msg']; ?></div>
      <?php endif;?>

   <?php endif; ?> 
  <form class="form-horizontal targetform"  role="form" id="membershipform" method="post" name="membershipform" action="<?php echo base_url('memberships/add/'.$member->id);?><?php echo ( !empty($membership->id) ? "/".$membership->id : "" )?>">
    <div class="form_result"></div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Member Name:</label>
        <div class="col-lg-8">
            <label> <?php echo $member->firstname.' '.$member->lastname ?></label>
            <input type="hidden" class="form-control" value="<?php echo $member->id ?>" id="member_id" name="member_id">
            <input type="hidden" class="form-control" value="<?php echo !empty($_GET['new']) ?  $_GET['new']: "" ?>" id="newmember" name="newmember">
            <input type="hidden" class="form-control" value="<?php echo (!empty($membership->id) ? $membership->id : "") ?>" id="id" name="id">
        </div>
    </div>
    <!--
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Middle Name:</label>
        <div class="col-lg-8">
            <input type="text" class="form-control" id="middle" name="middlename" placeholder="Middle Name">
        </div>
    </div>
    -->
     
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Class Type:</label>
        <div class="col-lg-8">
            <select name="class_type_id" class="form-control" required>
              <option value="">Select Class Type</option>
              <?php if(!empty($class_types)): ?>
                <?php foreach($class_types as $class_type):?>
                    <option value="<?php echo $class_type->id ?>"  <?php echo ( !empty($membership->class_type_id) && $membership->class_type_id == $class_type->id ? 'selected' : '').''; ?>><?php echo $class_type->class_title?></option>
                <?php endforeach;?>
              <?php endif; ?>
            </select>
        </div>
    </div>
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Duration:</label>
        <div class="col-lg-8">
            <select name="membership_type_id" class="form-control" required >
              <option value="">Duration</option>
              <?php if(!empty($membership_types)): ?>
                <?php foreach($membership_types as $membership_type):?>
                    <option data-class="<?php echo $membership_type->class_type_id?>" data-amount="<?php echo $membership_type->price ?>" value="<?php echo $membership_type->id ?>" <?php echo ( !empty($membership->membership_type_id) && $membership->membership_type_id == $membership_type->id ? 'selected' : ''); ?>><?php echo $membership_type->title?></option>
                <?php endforeach;?>
              <?php endif; ?>
            </select>
        </div>
    </div>
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Promo Type:</label>
        <div class="col-lg-8">
            <select name="promo_id" class="form-control" >
              <option value="">Select Class Type</option>
              <?php if(!empty($promos)): ?>
                <?php foreach($promos as $promo):?>
                    <option value="<?php echo $promo->id ?>" <?php echo ( $promo->default_promo == 1 ? 'selected' : ''); ?>  <?php echo (empty($membership->promo_id) &&  $promo->id == 6 ? 'selected' : ''); ?>><?php echo $promo->promo_title?> </option>
                <?php endforeach;?>
              <?php endif; ?>
            </select>
            <p>Remove Promo / click "Select Promo Type" if the amount is empty </p>
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Amount:</label>
        <div class="col-lg-8">
           <input type="text" class="form-control" required readonly name="amount" id="amount">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Date Registered:</label>
        <div class="col-lg-8">
           <input type="text" class="form-control datepick" name="date_register" id="date_register" value="<?php echo date('m/d/Y') ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Paid:</label>
        <div class="col-lg-8">
          <label> 
          <input type="checkbox" name="paid" value="1" checked > Paid:
          </label>
            
        </div>
    </div>
   
    <div class="form-group">
      <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Date Start:</label>
        <div class="col-lg-8">
            <input type="text" name="date_start" class="datepick form-control" value="<?php echo date('m/d/Y') ?>"  />
        </div>
    </div>
     <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Note:</label>
        <div class="col-lg-8">
            <textarea name="note" class="form-control"><?php echo (!empty($membership->note) ? $membership->note: '') ?></textarea>
        </div>
    </div>
    <?php if( !empty($_GET['new']) ): ?>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label"></label>
        <div class="col-lg-8">
            <div><label> <input type="checkbox" value="1" name="joining" /> Include Membership Fee (<?php echo $this->config_model->get_valueby_code('joiningfee') ?> )</label> </div>
        </div>
    </div>
    <?php endif;?>
    <div class="form-group">
        <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">WITH O.R:</label>
        <div class="col-lg-8">
          <label> 
          <input type="checkbox" name="or" value="1"> Yes:
          </label>
            
        </div>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-8">
            <button type="submit" class="btn btn-danger ">Save</button>
        </div>
    </div>
    
</form>