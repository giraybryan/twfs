<div class="table-responsive">
<table class="table">
    <thead>
    	<tr>
            <th style="width: 10px">ID</th>
            <th>Member Name</th>
            <th>Class Enrolled</th>
            <th>Membership Type</th>
            <th>Membership Status</th>
            <th>Payment Status</th>
            <th>Action</th>
    	</tr>
    </thead>
    <tbody>
    <?php if( !empty($members) ): ?>
        <?php foreach($members as $member):?>
            <tr>
                <td><?php echo $member->id ?></td>
                <td><?php echo $member->firstname.' '.$member->lastname; ?></td>
                <td><?php echo $member->class_title ?></td>
                <td><?php echo $member->membership_type  ?></td>
                <td><?php echo $member->mermbership_status?></td>
                <td><?php echo $member->payment_status == 1 ?  "paid" : "not yet paid";  ?></td>
                <td data-encode='<?php echo json_encode($meter)?>'>
                	<button class="btn btn-primary btn-sm action-btn" data-action='add'><i class="fa fa-plus"></i></button>
                	<button class="btn btn-warning btn-sm action-btn" data-action="edit"><i class="fa fa-pencil"></i></button>
                	<button class="btn btn-danger btn-sm action-btn" data-action="delete" ><i class="fa fa-times"></i></button>
                </td>
            </tr>
        <?php endforeach;?>
	<?php else: ?>
		<tr>
			<td colspan='4'>No records</td>
		</tr>
    <?php endif; ?>
	</tbody>
</table>
</div>