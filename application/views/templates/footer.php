
                

                

                    <!--
                    <div class="row" style="margin-bottom:5px;">


                        <div class="col-md-3">
                            <div class="sm-st clearfix">
                                <span class="sm-st-icon st-red"><i class="fa fa-check-square-o"></i></span>
                                <div class="sm-st-info">
                                    <span>3200</span>
                                    Total Tasks
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="sm-st clearfix">
                                <span class="sm-st-icon st-violet"><i class="fa fa-envelope-o"></i></span>
                                <div class="sm-st-info">
                                    <span>2200</span>
                                    Total Messages
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="sm-st clearfix">
                                <span class="sm-st-icon st-blue"><i class="fa fa-dollar"></i></span>
                                <div class="sm-st-info">
                                    <span>100,320</span>
                                    Total Profit
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="sm-st clearfix">
                                <span class="sm-st-icon st-green"><i class="fa fa-paperclip"></i></span>
                                <div class="sm-st-info">
                                    <span>4567</span>
                                    Total Documents
                                </div>
                            </div>
                        </div>
                    </div>
                    -->

                    <!-- Main row -->
                    <!--
                    <div class="row">

                        <div class="col-md-8">
                            
                            <section class="panel">
                                <header class="panel-heading">
                                    Earning Graph
                                </header>
                                <div class="panel-body">
                                    <canvas id="linechart" width="600" height="330"></canvas>
                                </div>
                            </section>
                            

                        </div>
                        <div class="col-lg-4">
                            <section class="panel">
                                <header class="panel-heading">
                                    Notifications
                                </header>
                                    <div class="panel-body" id="noti-box">

                                        <div class="alert alert-block alert-danger">
                                            <button data-dismiss="alert" class="close close-sm" type="button">
                                                <i class="fa fa-times"></i>
                                            </button>
                                            <strong>Oh snap!</strong> Change a few things up and try submitting again.
                                        </div>
                                        <div class="alert alert-success">
                                            <button data-dismiss="alert" class="close close-sm" type="button">
                                                <i class="fa fa-times"></i>
                                            </button>
                                            <strong>Well done!</strong> You successfully read this important alert message.
                                        </div>
                                        <div class="alert alert-info">
                                            <button data-dismiss="alert" class="close close-sm" type="button">
                                                <i class="fa fa-times"></i>
                                            </button>
                                            <strong>Heads up!</strong> This alert needs your attention, but it's not super important.
                                        </div>
                                        <div class="alert alert-warning">
                                            <button data-dismiss="alert" class="close close-sm" type="button">
                                                <i class="fa fa-times"></i>
                                            </button>
                                            <strong>Warning!</strong> Best check yo self, you're not looking too good.
                                        </div>


                                        <div class="alert alert-block alert-danger">
                                            <button data-dismiss="alert" class="close close-sm" type="button">
                                                <i class="fa fa-times"></i>
                                            </button>
                                            <strong>Oh snap!</strong> Change a few things up and try submitting again.
                                        </div>
                                        <div class="alert alert-success">
                                            <button data-dismiss="alert" class="close close-sm" type="button">
                                                <i class="fa fa-times"></i>
                                            </button>
                                            <strong>Well done!</strong> You successfully read this important alert message.
                                        </div>
                                        <div class="alert alert-info">
                                            <button data-dismiss="alert" class="close close-sm" type="button">
                                                <i class="fa fa-times"></i>
                                            </button>
                                            <strong>Heads up!</strong> This alert needs your attention, but it's not super important.
                                        </div>
                                        <div class="alert alert-warning">
                                            <button data-dismiss="alert" class="close close-sm" type="button">
                                                <i class="fa fa-times"></i>
                                            </button>
                                            <strong>Warning!</strong> Best check yo self, you're not looking too good.
                                        </div>



                                    </div>
                            </section>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-8">
                            <section class="panel">
                                <header class="panel-heading">
                                    Work Progress
                                </header>
                                <div class="panel-body table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                              <th>#</th>
                                              <th>Project</th>
                                              <th>Manager</th>
                                              
                                              <th>Deadline</th>
                                              
                                              <th>Status</th>
                                              <th>Progress</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                              <td>1</td>
                                              <td>Facebook</td>
                                              <td>Mark</td>
                                              
                                              <td>10/10/2014</td>
                                              
                                              <td><span class="label label-danger">in progress</span></td>
                                              <td><span class="badge badge-info">50%</span></td>
                                          </tr>
                                          <tr>
                                              <td>2</td>
                                              <td>Twitter</td>
                                              <td>Evan</td>
                                              
                                              <td>10/8/2014</td>
                                              
                                              <td><span class="label label-success">completed</span></td>
                                              <td><span class="badge badge-success">100%</span></td>
                                          </tr>
                                          <tr>
                                              <td>3</td>
                                              <td>Google</td>
                                              <td>Larry</td>
                                              
                                              <td>10/12/2014</td>
                                             
                                              <td><span class="label label-warning">in progress</span></td>
                                              <td><span class="badge badge-warning">75%</span></td>
                                          </tr>
                                          <tr>
                                              <td>4</td>
                                              <td>LinkedIn</td>
                                              <td>Allen</td>
                                             
                                              <td>10/01/2015</td>
                                              
                                              <td><span class="label label-info">in progress</span></td>
                                              <td><span class="badge badge-info">65%</span></td>
                                          </tr>
                                          <tr>
                                              <td>5</td>
                                              <td>Tumblr</td>
                                              <td>David</td>
                                             
                                              <td>01/11/2014</td>
                                              
                                              <td><span class="label label-warning">in progress</span></td>
                                              <td><span class="badge badge-danger">95%</span></td>
                                          </tr>
                                          <tr>
                                              <td>6</td>
                                              <td>Tesla</td>
                                              <td>Musk</td>
                                              
                                              <td>01/11/2014</td>
                                             
                                              <td><span class="label label-info">in progress</span></td>
                                              <td><span class="badge badge-success">95%</span></td>
                                          </tr>
                                          <tr>
                                              <td>7</td>
                                              <td>Ghost</td>
                                              <td>XXX</td>
                                              
                                              <td>01/11/2014</td>
                                             
                                              <td><span class="label label-info">in progress</span></td>
                                              <td><span class="badge badge-success">95%</span></td>
                                          </tr>
                                        </tbody>
                                    </table>
                                </div>  
                            </section>
                        </div>
                        <div class="col-md-4">
                            <section class="panel">
                                <header class="panel-heading">
                                    Twitter Feed
                                </header>
                                <div class="panel-body">
                                    <div class="twt-area">
                                        <form action="#" method="post">
                                            <textarea class="form-control" name="profile-tweet" placeholder="Share something on Twitter.." rows="3"></textarea>

                                            <div class="clearfix">
                                                <button class="btn btn-sm btn-primary pull-right" type="submit">
                                                    <i class="fa fa-twitter"></i>
                                                    Tweet
                                                </button>
                                                <a class="btn btn-link btn-icon fa fa-location-arrow" data-original-title="Add Location" data-placement="bottom" data-toggle="tooltip" href=
                                                "#" style="text-decoration:none;" title=""></a>
                                                <a class="btn btn-link btn-icon fa fa-camera" data-original-title="Add Photo" data-placement="bottom" data-toggle="tooltip" href="#"
                                                style="text-decoration:none;" title=""></a>
                                            </div>
                                        </form>
                                    </div>
                                    <ul class="media-list">
                                        <li class="media">
                                            <a href="#" class="pull-left">
                                                <img src="img/26115.jpg" alt="Avatar" class="img-circle" width="64" height="64">
                                            </a>
                                            <div class="media-body">
                                                <span class="text-muted pull-right">
                                                    <small><em>30 min ago</em></small>
                                                </span>
                                                <a href="page_ready_user_profile.php">
                                                    <strong>John Doe</strong>
                                                </a>
                                                <p>
                                                    In hac <a href="#">habitasse</a> platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend.
                                                    <a href="#" class="text-danger">
                                                        <strong>#dev</strong>
                                                    </a>
                                                </p>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <a href="#" class="pull-left">
                                                <img src="img/26115.jpg" alt="Avatar" class="img-circle" width="64" height="64">
                                            </a>
                                            <div class="media-body">
                                                <span class="text-muted pull-right">
                                                    <small><em>30 min ago</em></small>
                                                </span>
                                                <a href="page_ready_user_profile.php">
                                                    <strong>John Doe</strong>
                                                </a>
                                                <p>
                                                    In hac <a href="#">habitasse</a> platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend.
                                                    <a href="#" class="text-danger">
                                                        <strong>#design</strong>
                                                    </a>
                                                </p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="panel">
                                <header class="panel-heading">
                                    Teammates
                                </header>

                                <ul class="list-group teammates">
                                    <li class="list-group-item">
                                        <a href=""><img src="img/26115.jpg" width="50" height="50"></a>
                                        <span class="pull-right label label-danger inline m-t-15">Admin</span>
                                        <a href="">Damon Parker</a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href=""><img src="img/26115.jpg"  width="50" height="50"></a>
                                        <span class="pull-right label label-info inline m-t-15">Member</span>
                                        <a href="">Joe Waston</a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href=""><img src="img/26115.jpg"  width="50" height="50"></a>
                                        <span class="pull-right label label-warning inline m-t-15">Editor</span>
                                        <a href="">Jannie Dvis</a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href=""><img src="img/26115.jpg"  width="50" height="50"></a>
                                        <span class="pull-right label label-warning inline m-t-15">Editor</span>
                                        <a href="">Emma Welson</a>
                                    </li>
                                    <li class="list-group-item">
                                        <a href=""><img src="img/26115.jpg"  width="50" height="50"></a>
                                        <span class="pull-right label label-success inline m-t-15">Subscriber</span>
                                        <a href="">Emma Welson</a>
                                    </li>
                                </ul>
                                <div class="panel-footer bg-white">
                                  
                                    <button class="btn btn-primary btn-addon btn-sm">
                                        <i class="fa fa-plus"></i>
                                        Add Teammate
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <section class="panel tasks-widget">
                                <header class="panel-heading">
                                  Todo list
                                </header>
                                <div class="panel-body">

                                  <div class="task-content">

                                      <ul class="task-list">
                                          <li>
                                              <div class="task-checkbox">
                                                 
                                                  <input type="checkbox" class="flat-grey list-child"/>
                                                  
                                              </div>
                                              <div class="task-title">
                                                  <span class="task-title-sp">Director is Modern Dashboard</span>
                                                  <span class="label label-success">2 Days</span>
                                                  <div class="pull-right hidden-phone">
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-check"></i></button>
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button>
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-times"></i></button>
                                                  </div>
                                              </div>
                                          </li>
                                          <li>
                                              <div class="task-checkbox">
                                             
                                                  <input type="checkbox" class="flat-grey"/>
                                              </div>
                                              <div class="task-title">
                                                  <span class="task-title-sp">Fully Responsive & Bootstrap 3.0.2 Compatible</span>
                                                  <span class="label label-danger">Done</span>
                                                  <div class="pull-right hidden-phone">
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-check"></i></button>
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button>
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-times"></i></button>
                                                  </div>
                                              </div>
                                          </li>
                                          <li>
                                              <div class="task-checkbox">
                                                 
                                                  <input type="checkbox" class="flat-grey"/>
                                              </div>
                                              <div class="task-title">
                                                  <span class="task-title-sp">Latest Design Concept</span>
                                                  <span class="label label-warning">Company</span>
                                                  <div class="pull-right hidden-phone">
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-check"></i></button>
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button>
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-times"></i></button>
                                                  </div>
                                              </div>
                                          </li>
                                          <li>
                                              <div class="task-checkbox">
                                                  
                                                  <input type="checkbox" class="flat-grey"/>
                                              </div>
                                              <div class="task-title">
                                                  <span class="task-title-sp">Write well documentation for this theme</span>
                                                  <span class="label label-primary">3 Days</span>
                                                  <div class="pull-right hidden-phone">
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-check"></i></button>
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button>
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-times"></i></button>
                                                  </div>
                                              </div>
                                          </li>
                                          <li>
                                              <div class="task-checkbox">
                                                 
                                                  <input type="checkbox" class="flat-grey"/>
                                              </div>
                                              <div class="task-title">
                                                  <span class="task-title-sp">Don't bother to download this Dashbord</span>
                                                  <span class="label label-inverse">Now</span>
                                                  <div class="pull-right">
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-check"></i></button>
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button>
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-times"></i></button>
                                                  </div>
                                              </div>
                                          </li>
                                          <li>
                                              <div class="task-checkbox">

                                                  <input type="checkbox" class="flat-grey"/>
                                              </div>
                                              <div class="task-title">
                                                  <span class="task-title-sp">Give feedback for the template</span>
                                                  <span class="label label-success">2 Days</span>
                                                  <div class="pull-right hidden-phone">
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-check"></i></button>
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button>
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-times"></i></button>
                                                  </div>
                                              </div>
                                          </li>
                                          <li>
                                              <div class="task-checkbox">
                                                 
                                                  <input type="checkbox" class="flat-grey"/>
                                              </div>
                                              <div class="task-title">
                                                  <span class="task-title-sp">Tell your friends about this admin template</span>
                                                  <span class="label label-danger">Now</span>
                                                  <div class="pull-right hidden-phone">
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-check"></i></button>
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></button>
                                                      <button class="btn btn-default btn-xs"><i class="fa fa-times"></i></button>
                                                  </div>
                                              </div>
                                          </li>

                                      </ul>
                                  </div>

                                  <div class=" add-task-row">
                                      <a class="btn btn-success btn-sm pull-left" href="#">Add New Tasks</a>
                                      <a class="btn btn-default btn-sm pull-right" href="#">See All Tasks</a>
                                  </div>
                              </div>
                            </section>
                        </div>
                    </div>
                    -->
                </div>
        <!-- /page content -->

        </div>
    </div>
          <input type="hidden" name="baseurl" value="<?php echo base_url(); ?>" id="baseurl"/>
          <input type="hidden" name="sess_validate" value="<?php echo $session_validate; ?>" id="sess_validate"/>
        <!-- jQuery 2.0.2 -->
        <script src="<?php echo base_url().'assets/js/jquery.min.js'?>"></script>
        
        <!-- jQuery UI 1.10.3 -->
        <script src="<?=base_url().'assets/'?>js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="<?=base_url().'assets/'?>js/bootstrap.min.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <link rel="stylesheet" type="text/css" href="<?=base_url().'assets/'?>js/plugins/daterange/daterangepicker.css">
        <script src="<?=base_url().'assets/'?>js/plugins/daterange/daterangepicker.js" type="text/javascript"></script>
        <script src="<?=base_url().'assets/'?>js/plugins/alertify/alertify.min.js"></script>


        <script src="<?=base_url().'assets/'?>js/plugins/chart.js" type="text/javascript"></script>

        <!-- datepicker -->
       <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap-datepicker.css'?>"/>
      <script type="text/javascript" src="<?=base_url().'assets/'?>js/bootstrap-datepicker.js"></script>

        <!-- Bootstrap WYSIHTML5
        <script src="js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>-->
        <!-- iCheck -->
        <script src="<?=base_url().'assets/'?>js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- calendar -->
        <script src="<?=base_url().'assets/'?>js/plugins/fullcalendar/fullcalendar.js" type="text/javascript"></script>

        <!-- Director App -->
        <script src="<?=base_url().'assets/'?>js/Director/app.js" type="text/javascript"></script>

        <!-- Director dashboard demo (This is only for demo purposes) -->
        <script src="<?=base_url().'assets/'?>js/Director/dashboard.js" type="text/javascript"></script>



         <script src="<?=base_url().'assets/'?>js/fastclick/lib/fastclick.js"></script>
        <!-- NProgress -->
        <script src="<?=base_url().'assets/'?>js/nprogress/nprogress.js"></script>
        <!-- Chart.js -->
        <script src="<?=base_url().'assets/'?>js/Chart.js/dist/Chart.min.js"></script>
        <!-- gauge.js -->
        <script src="<?=base_url().'assets/'?>js/gauge.js/dist/gauge.min.js"></script>
        <!-- bootstrap-progressbar -->
        <script src="<?=base_url().'assets/'?>js/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <!-- iCheck -->

        <script src="<?=base_url().'assets/'?>js/iCheck/icheck.min.js"></script>
        <!-- Skycons -->
        <script src="<?=base_url().'assets/'?>js/skycons/skycons.js"></script>
        <!-- Flot -->
        <script src="<?=base_url().'assets/'?>js/Flot/jquery.flot.js"></script>
        <script src="<?=base_url().'assets/'?>js/Flot/jquery.flot.pie.js"></script>
        <script src="<?=base_url().'assets/'?>js/Flot/jquery.flot.time.js"></script>
        <script src="<?=base_url().'assets/'?>js/Flot/jquery.flot.stack.js"></script>
        <script src="<?=base_url().'assets/'?>js/Flot/jquery.flot.resize.js"></script>
        <!-- Flot plugins -->
        <script src="<?=base_url().'assets/'?>js/flot.orderbars/js/jquery.flot.orderBars.js"></script>
        <script src="<?=base_url().'assets/'?>js/flot-spline/js/jquery.flot.spline.min.js"></script>
        <script src="<?=base_url().'assets/'?>js/flot.curvedlines/curvedLines.js"></script>
        <!-- DateJS -->
        <script src="<?=base_url().'assets/'?>js/DateJS/build/date.js"></script>
        <!-- JQVMap -->
        <script src="<?=base_url().'assets/'?>js/jqvmap/dist/jquery.vmap.js"></script>
        <script src="<?=base_url().'assets/'?>js/jqvmap/dist/maps/jquery.vmap.world.js"></script>
        <script src="<?=base_url().'assets/'?>js/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
        <!-- bootstrap-daterangepicker -->
        <script src="<?=base_url().'assets/'?>js/moment/min/moment.min.js"></script>
        <script src="<?=base_url().'assets/'?>js/bootstrap-daterangepicker/daterangepicker.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="<?=base_url().'assets/'?>js/admin_theme/custom.min.js"></script>

        <!-- Director for demo purposes -->
        <script type="text/javascript">
            $('input').on('ifChecked', function(event) {
                // var element = $(this).parent().find('input:checkbox:first');
                // element.parent().parent().parent().addClass('highlight');
                $(this).parents('li').addClass("task-done");
                console.log('ok');
            });
            $('input').on('ifUnchecked', function(event) {
                // var element = $(this).parent().find('input:checkbox:first');
                // element.parent().parent().parent().removeClass('highlight');
                $(this).parents('li').removeClass("task-done");
                console.log('not');
            });

        </script>
        <script>
            $('#noti-box').slimScroll({
                height: '400px',
                size: '5px',
                BorderRadius: '5px'
            });

            $('input[type="checkbox"].flat-grey, input[type="radio"].flat-grey').iCheck({
                checkboxClass: 'icheckbox_flat-grey',
                radioClass: 'iradio_flat-grey'
            });
</script>

<?php if( !empty($controller)): ?>
  <script>
var _controller = '<?php echo $controller; ?>'
</script>
<?php endif; ?>

<script type="text/javascript">
  ajax_loader = '<div class="spinner"></div>'
  $(document).ready(function(){
    
    $('.modalform .modalsubmit').click(function(){
      $(this).closest('.modalform').find('.targetform').trigger('submit');
    });
    $('.datepick').datepicker();
    $('.input-daterange').datepicker({
        todayBtn: "linked"
    });
  })
  function load_pageloader(target, url){
    $('#'+target).html(ajax_loader);

    $('#'+target).load(url ,function(data){
        $('#'+target).fadeIn();
    });
  }

  function isNumberKey(evt)
  {
     var charCode = (evt.which) ? evt.which : event.keyCode
     if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

     return true;
  }

</script>
<?php 
   
    if( !empty($js) ):
        foreach($js as $js_file):
            echo "<script type='text/javascript' src='".base_url('assets/js/'.$js_file.'?date='.strtotime(date('Y-m-d H:s:i')))."'></script>";
        endforeach;
    endif;
?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.7/js/bootstrap-dialog.min.js"></script>
</body>
</html>




