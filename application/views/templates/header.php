<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Director | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="description" content="Developed By M Abdur Rokib Promy">
    <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
    <!-- bootstrap 3.0.2 -->
    <link href="<?php echo base_url().'assets/css/bootstrap.css'?>" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="<?php echo base_url().'assets/css/fontawesome.css'?>" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="<?=base_url().'assets/'?>css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url().'assets/'?>css/admin_theme/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="<?=base_url().'assets/'?>css/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="<?=base_url().'assets/'?>css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    
    <!-- fullCalendar -->
    <!-- <link href="css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" /> -->
    <!-- Daterange picker -->
    <link href="<?=base_url().'assets/'?>css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- iCheck for checkboxes and radio inputs -->
    <link href="<?=base_url().'assets/'?>css/iCheck/all.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <!-- <link href="css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" /> -->
   
    <!-- Theme style -->
    <link href="<?php echo base_url();?>/assets/css/login.css" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url();?>assets/img/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url();?>assets/img/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>assets/img/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();?>assets/img/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>assets/img/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url();?>assets/img/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url();?>assets/img/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url();?>assets/img/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>assets/img/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url();?>assets/img/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>assets/img/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>assets/img/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/img/fav/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url();?>assets/img/fav/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url();?>assets/img/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
          <![endif]-->

    
</head>
<body class="skin-black nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
              <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                  <a href="index.html" class="site_title"><span><img src="<?php echo base_url().'assets/img/';?>warehouse logo transparent.png" style="width: 204px;"></span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                  <div class="profile_pic">
                    <i class="fa fa-user" style="color: #fff;
    font-size: 37px;"></i>
                  </div>
                  <div class="profile_info" style="padding: 0px;">
                    <span>Welcome,</span>
                    <h2><?php echo $userdata['username']?></h2>
                  </div>
                </div>
                <!-- /menu profile quick info -->f

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                  <div class="menu_section">
                    <h3>General</h3>
                    <ul class="nav side-menu">
                      <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                       
                      </li>
                      <li><a><i class="fa fa-edit"></i> Forms <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="<?php echo base_url().'members/add'?>">Membership Form (NEW) </a></li>
                          <li><a href="<?php echo base_url().'attendance/add'?>">Attendance Form</a></li>
                          <li><a href="<?php echo base_url().'sales/add'?>">Sales Form</a></li>
                          <li><a href="<?php echo base_url().'chillers/addsales'?>">Chiller Sales Form</a></li>
                          <li><a href="<?php echo base_url().'expenses/add' ?>">Bill / Expense Form</a></li>
                          <li style="display: none;"><a href="<?php echo base_url().'salaries/add' ?>">Salary Form</a></li>
                          <li><a href="<?php echo base_url().'equities/add' ?>">Equity Form</a></li>
                          <li><a href="<?php echo base_url().'banktransacs/add' ?>">Bank Transaction</a></li>
                        </ul>
                      </li>
                       <li><a href="<?php echo base_url('members')?>"> <i class="fa fa-user"></i> Members</a>
                          <ul class="nav child_menu">
                          <li><a href="<?php echo base_url().'members'?>">Members List </a></li>
                          <li><a href="<?php echo base_url().'members/add'?>">Member Form</a></li>
                          <li><a href="<?php echo base_url().'clientlog'?>">Client Login</a></li>
                        </ul>
                      </li>
                      <li><a><i class="fa fa-list-ul"></i> Classes <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="<?php echo base_url('classes')?>">Class List</a></li>
                          <li><a href="<?php echo base_url('Classdurations')?>">Durations</a></li>
                          <li><a href="<?php echo base_url('Membershiptypes')?>">Class Rates</a></li>
                        </ul>
                      </li>
                      <li><a href="<?php echo base_url('chillers')?>"><i class="fa fa-table"></i> Chillers</a>
                      <li><a href="<?php echo base_url('promotions')?>"><i class="fa fa-table"></i> Promotions</a>
                      <li><a href="<?php echo base_url('schedules')?>"><i class="fa fa-table"></i> Schedules</a>
                        
                      </li>
                      <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="<?php echo base_url('reports/sales_expensechart')?>">Sales Vs Expense</a></li>
                          <li><a href="<?php echo base_url('reports/financial_report'); ?>">Financial Report</a></li>
                          <li><a href="<?php echo base_url('reports/memberschart')?>">Members Chart</a></li>
                        </ul>
                      </li>
                    
                    </ul>
                  </div>
                  <div class="menu_section">
                    <h3>REVENUE DATA</h3>
                    <ul class="nav side-menu">
                      <li><a><i class="fa fa-bug"></i> Assets <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                          <li><a href="<?php echo base_url('sales');?>">Sales</a></li>
                          <li><a href="<?php echo base_url('')?>">Merchandise</a></li>
                          <li><a href="<?php echo base_url('equities')?>">Equity</a></li>
                          <li><a href="<?php echo base_url('')?>">Bank</a></li>
                          
                        </ul>
                      </li>
                     <li><a href="<?php echo base_url('expenses')?>">Expenses</a></li>
                               
                      <li style="display: none;"><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
                    </ul>
                  </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                  <a data-toggle="tooltip" data-placement="top" title="Settings">
                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                  </a>
                  <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                    <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                  </a>
                  <a data-toggle="tooltip" data-placement="top" title="Lock">
                    <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                  </a>
                  <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo base_url().'non_users/logout' ?>">
                    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                  </a>
                </div>
                <!-- /menu footer buttons -->
              </div>
            </div>
            <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                   <?php echo $userdata['username']?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo base_url().'user' ?>"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="<?php echo base_url().'non_users/logout' ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
                <li role="presentation" class="dropdowns">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    Quick Access
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a href="#" class="" data-toggle="modal" data-target="#classrate_modal">CLASS RATES</a>
                    </li>
                    <li>
                      <a class="" href="<?php echo base_url('walkins/entry'); ?>">WAlK-IN Form</a>
                    </li>
                  </ul>
                </li>
                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          

          
        
         

          
        
       
       
          