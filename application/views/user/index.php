<br/>
<div class="col-sm-12">
	<div class="panel">

		<div class="panel-body" >

			<div class="form-horizontal " style="max-width: 500px; margin: auto">
				<div class="form-group">
					<label class="col-sm-2">Username </label>
					<div class="col-sm-10"><input type="text"  class="form-control" readonly value="<?php echo $info->username; ?>" ></div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Userlevel </label>
					<div class="col-sm-10"><input type="text"  class="form-control" readonly value="<?php echo $info->superadmin == 1 ? 'Super Admin' : 'Moderator' ?>" ></div>
				</div>
				<div class="container">
					<br/>
					<h4> Change Password</h4>
				</div>
				<?php if(!empty($error_msg)) : ?>
					<div class="alert alert-danger"><?php echo $error_msg?></div>
				<?php endif; ?>
				<?php if(!empty($success_msg)) : ?>
					<div class="alert alert-success"><?php echo $success_msg?></div>
				<?php endif; ?>
				<form method="post" class >
				<div class="form-group">
					<label class="col-sm-2">Password </label>
					<div class="col-sm-10"><input type="password" required name="password" class="form-control"  value="" ></div>
				</div>
				<div class="form-group">
					<label class="col-sm-2">Confirm </label>
					<div class="col-sm-10"><input type="password" required name="confirm" class="form-control"  value="" ></div>
				</div>
				<div class="form-group">
					<div class="col-sm-10">
						<input type="submit" value="Save" class="btn btn-primary" name="">
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>