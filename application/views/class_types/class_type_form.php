
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Class Form</h4>
  </div>
  <div class="modal-body">
    <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo !empty($class) ? base_url('classes/edit/'.$class->id) : base_url('classes/add');?>">
        <div class="form_result"></div>
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Class Name</label>
            <div class="col-lg-8">
                <input type="text" class="form-control" id="class_title" value="<?php echo !empty($class) ? $class->class_title: '' ?>" name="class_title" placeholder="Class Type Name">
                <input type="hidden" class="form-control" id="id" value="<?php echo !empty($class) ? $class->id: '' ?>" name="id" placeholder="id">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Class Category </label>
            <div class="col-lg-8">
                <select class="form-control" name="class_category_id">
                  <option value="">Select</option>
                  <?php if( !empty($categories) ):?>
                  <?php foreach($categories as $category): ?>
                      <option value="<?php echo $category->id ?>" <?php echo !empty($class) && $class->class_category_id == $category->id ? ' selected ' : '' ?>><?php echo $category->category_title?></option>
                  <?php endforeach; ?>
                  <?php endif;?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-4 col-lg-8">
                <button type="submit" class="btn btn-danger ">Save</button>
            </div>
        </div>
    </form>
  </div>
  