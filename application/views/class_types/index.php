<div class="row">
	<div class="col-md-12">
		<div class="panel">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="#">Classes</a></li>
            <li role="presentation"><a href="<?php echo base_url().'classdurations' ?>">Durations</a></li>
            <li role="presentation" ><a  href="<?php echo base_url().'membershiptypes/'?>">Class Subscription Rates</a></li>
            <li role="presentation" ><a  href="<?php echo base_url().'walkins/'?>">walkin Rates</a></li>
        </ul>
            <header class="panel-heading">
                Class List
            </header>
            <div class="panel-body">
                <div class="box-tools">
                	<a href="<?php echo base_url().'classes/add'?>"><button class='btn btn-success btn-sm' data-toggle="modal" data-target="#metermodal">Add Class</button></a>
                    <!--
                    <ul class="pagination pagination-sm m-b-10 m-t-10 pull-right">
                        <li><a href="#">«</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">»</a></li>
                    </ul>
                	-->
                </div>
                <div id="module_list">
                	<?php $this->load->view("class_types/list"); ?>
                </div>
            </div><!-- /.panel-body -->
        </div>
	</div>
</div>

