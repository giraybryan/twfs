<!-- Modal -->
<div class="modal fade modalform" id="metermodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Meter Form</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal targetform"  role="form" id="meterform" method="post" name="meterform" action="<?php echo base_url('meters/add');?>">
            <div class="form_result"></div>
            <div class="form-group">
                <label for="inputEmail1" class="col-lg-4 col-sm-3 control-label">Meter Room No.</label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" id="meter_room_name" name="meter_room_name" placeholder="Meter Room No.">
                    <input type="hidden" class="form-control" id="id" name="id" placeholder="meterid">
                </div>
            </div>
            <div class="form-group hide">
                <div class="col-lg-offset-4 col-lg-8">
                    <button type="submit" class="btn btn-danger ">Save</button>
                </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-sm modalsubmit">Save changes</button>
      </div>
    </div>
  </div>
</div>