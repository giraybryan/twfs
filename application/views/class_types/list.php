<div class="table-responsive">
<table class="table">
    <thead>
    	<tr>
            <th style="width: 10px">ID</th>
            <th>Class Name</th>
            <th>Class Category</th>
            <th>Action</th>
    	</tr>
    </thead>
    <tbody>
    <?php if( !empty($classes) ): ?>
        <?php foreach($classes as $class):?>
            <tr>
                <td><?php echo $class->id ?></td>
                <td><?php echo $class->class_title ?></td>
                <td><?php echo $class->class_type ?></td>
                
                <td>
                	<a  href="<?php echo base_url('classes/edit/'.$class->id)?>"  class="btn btn-warning btn-sm action-btn" data-action="edit"><i class="fa fa-pencil"></i></a>
                	<button data-id="<?php echo $class->id ?>"  class="btn btn-danger btn-sm action-btn" data-action="delete" ><i class="fa fa-times"></i></button>
                </td>
            </tr>
        <?php endforeach;?>
	<?php else: ?>
		<tr>
			<td colspan='4'>No records</td>
		</tr>
    <?php endif; ?>
	</tbody>
</table>
</div>