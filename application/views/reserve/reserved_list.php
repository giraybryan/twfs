<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
			<th>Time</th>
			<th>Class</th>
		</tr>
	</thead>
	<tbody>
		<?php if(!empty($reservations)): ?>
			<?php foreach($reservations as $reserve): ?>
			<tr>
				<td><?php echo $reserve->firstname.' '.$reserve->lastname; ?></td>
				<td><?php echo date('g:i a', strtotime($reserve->date)); ?></td>
				<td><?php echo $reserve->class_title; ?></td>
			</tr>
			<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="3"><div class="alert alert-warning">No Reservation</div></td>
			</tr>
		<?php endif; ?>
	</tbody>
</table>