<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>The Warehouse Fitness Studio | Reservation Form</title>
		<meta name="generator" content="Bootply" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url();?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url();?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo base_url();?>assets/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url();?>/assets/css/login.css" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url();?>assets/img/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url();?>assets/img/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>assets/img/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();?>assets/img/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>assets/img/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url();?>assets/img/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url();?>assets/img/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url();?>assets/img/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>assets/img/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url();?>assets/img/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>assets/img/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>assets/img/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/img/fav/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url();?>assets/img/fav/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url();?>assets/img/fav/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
		
    <link href="css/styles.css" rel="stylesheet">
    <link href="<?php echo base_url();?>/assets/js/plugins/alertify/css/alertify.min.css" rel="stylesheet"/>
    <link href="<?php echo base_url();?>/assets/js/plugins/alertify/css/themes/default.css" rel="stylesheet"/>
	</head>
<body style="background: #eee">
    
    <div class="container" style="padding: 10px 15px;">
    <div class="row">
        <!-- <div class="col-sm-5 col-sm-push-4"> -->
        <div class="col-sm-8">
            <div style="" class="loginpage">
                <form class="form-horizontal">
                    <img class="img-responsive" style="max-width: 300px; display: block; margin: auto;" src="<?php echo $logo_img ?>"/>  
                    <h2>RESERVATION AND INQUIRY</h2>
                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                      <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#registration">Booking Form</button>
                      </div>
                      <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#class_rates">Class Rates</button>
                       
                      </div>
                      <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#schedule">Schedule</button>
                      </div>
                      
                    </div>
                    <br/>
                    <div class="input-group input-group-lg"></div>
                </form>
                <div id="result"></div>
            </div>
        </div>
        <div class="col-sm-4 col-sm-pull-5 hide">
            <h5>PROMO / ANNOUNCEMENTS</h5>
            <div id="promo">

                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                    <div class="item active">
                      <img src="<?php echo base_url('uploads/announcements/voucher promo.jpg'); ?>" alt="promo">
                    </div>

                    <div class="item">
                      <img src="<?php echo base_url('uploads/announcements/ashtaga class.jpg'); ?>" alt="Ashtaga yoga">
                    </div>

                    <div class="item">
                      <img src="<?php echo base_url('uploads/announcements/aerial yoga.jpg'); ?>" alt="Aerial yoga">
                    </div>
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
                <img src="" class="img-responsive">
            </div>
        </div>
        
        <div class="col-sm-4">
            <div class="row">
                <br/>
                <div class="col-sm-4"><h5>Reservation</h5></div>
                <div class="col-sm-8"><input type="text" id="reserve_datepick"  value="<?php echo date('m/d/Y');?>" autocomplete="off" class="datepick form-control" style="display: inline-block;" /></div>
                
            </div>
            

            <div id="reservation_wrapper" style="background: #fff; padding: 5px; max-height: 500px; overflow: auto;">
                <?php
                $this->load->view('reserve/reserved_list');
                ?>
            </div>
        </div>

    </div>
    </div>
    
    <style type="text/css">
        .searchresult {
            position: absolute;    position: absolute;
            z-index: 100;
            background: #fff;
            border: 2px solid #000;
            width: 96%;
            display: none;
        }
        .searchresult .result {
                padding: 8px 15px;
    border: 1px solid #ddd;
        }
        @media ( max-width: 768px ){
            .col-sm-3, .col-sm-9 { width: 100%; }
        }
    </style>
    <div class="modal fade" id="registration" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Booking Form</h4>
          </div>
          <div class="modal-body">

            <form class="form-horizontal" id="resform" action="<?php echo base_url('reserve/add')?>" method="post">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Client Search</label>
                    <div class="col-sm-9" style="position: relative;">
                        <input type="text" id="clientsearch" autocomplete="off" placeholder="Membership Card No / LastName"  class="form-control">
                        <div class="searchresult"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">First Name</label>
                    <div class="col-sm-9">
                        <input type="text" required id="firstname" name="firstname" class="form-control">
                        <input type="hidden" id="member_id" name="member_id" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Last Name</label>
                    <div class="col-sm-9">
                        <input type="text" required id="lastname" name="lastname" class="form-control">
                    </div>
                </div>
                
                 <div class="form-group">
                    <label for="inputEmail1" class="col-lg-3 col-sm-3 control-label">Date:</label>
                    <div class="col-lg-9">
                       <input type="text" class="form-control datepick" value="<?php echo date('m/d/Y'); ?>" autocomplete="off" required name="date" id="date" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail1" class="col-sm-3 control-label">Class Type:</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="class_type_id" id="class_type_id" required="">
                            <option value="1">CBH</option>
                            <option value="20">Boxing</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="inputEmail1" class="col-sm-3 control-label">Time Slot:</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="timeslot" id="timeslot" required="">
                            <?php $this->load->view('reserve/timeslot') ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-lg-9">
                        <button type="button" class="btn btn-default btn-lg" style="margin-right: 25px" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    </div>
                </div>
            </form>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" id="class_rates" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Class Rates</h4>
          </div>
          <div class="modal-body">
            <img src="<?php echo base_url('assets/img/rate.jpg') ?>" class="img-responsive"/ >
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade" id="schedule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Class Schedules</h4>
          </div>
          <div class="modal-body">
            <img src="<?php echo base_url('assets/img/schedule.jpg') ?>" class="img-responsive"/ >
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

          </div>
        </div>
      </div>
    </div>
    <style>
    .loginpage {
        max-width: 600px; margin: 30px auto; border: 1px solid #ddd; background: #fff;padding: 10px 15px; border-radius: 8px;
    }
    .loginpage h2 { text-align: center; }
    .loginpage img { max-width: 100%; width: auto; max-height: 200px;}
    @media(min-width: 766px){
        .loginpage { padding: 50px; }
    }
    @media(max-width: 766px){
        .loginpage img { width: 100%;}
    }
    .loginpage h2:before {
    background: #7e7e7e;
    background: linear-gradient(right,#7e7e7e 0,#fff 100%);
    left: 0;
    }
    .loginpage h2{
        font: 400 25px Helvetica,Arial,sans-serif;
    letter-spacing: -.05em;
    line-height: 20px;
    margin: 10px 0 30px;
}
    .btn-group-justified>.btn-group .btn { font-size: 14px; }
    option[disabled] {
        background: #ddd;
    }
    }
    </style>
    <input type="hidden" value="<?php echo base_url(); ?>" id="baseurl"/>
    <script src=""></script>
    <script src="<?php echo base_url();?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>
    <script src="<?=base_url().'assets/'?>js/plugins/alertify/alertify.min.js"></script>
    <?php foreach($js as $script): ?>
        <script src="<?php echo base_url('assets/js/'.$script.'?date='.strtotime(date('Y-m-d H:s:i')))?>"></script>
    <?php endforeach; ?>
    <script type="text/javascript">
        
    </script>

     <!-- datepicker -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap-datepicker.css'?>"/>
    <script type="text/javascript" src="<?=base_url().'assets/'?>js/bootstrap-datepicker.js"></script>
    <!-- JavaScript -->
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
    <!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.datepick').datepicker();
            $('#clientsearch').keyup(function(){
                if( $(this).val().length > 2){
                    search_member($(this).val());
                }
            })
            $(this).on('click','.searchresult .result .btn', function(e){
                e.preventDefault();
                dataid = $(this).attr('data-key')
                firstname = $(this).attr('data-firstname')
                lastname = $(this).attr('data-lastname')
                $('#firstname').val(firstname)
                $('#member_id').val(dataid)
                $('#lastname').val(lastname)
                $('.searchresult').hide();
                $('#clientsearch').val('')
            })  

            $('#resform').submit(function(e){
                e.preventDefault();
                confirmsg = '<div class=""><label>First Name</label> '+$('#firstname').val()+'</div>';
                confirmsg = confirmsg+ '<div class=""><label>Last Name</label> '+$('#lastname').val()+'</div>';
                confirmsg = confirmsg+ '<div class=""><label>Date Selected</label> '+$('#date').val()+'</div>';
                confirmsg = confirmsg+ '<div class=""><label>Time Slot</label> '+$('#timeslot option[value="'+$('#timeslot').val()+'"]').html()+'</div>';
                alertify.confirm('Confirm Reservation', confirmsg, function(){ 
                    $.ajax({
                        type: "POST",
                        url : $('#baseurl').val()+"reserve/add",
                        data: $('#resform').serialize(),
                        dataType: 'json',
                        success: function(respose){
                            if( respose.result == 1 ){
                                $('#reserve_datepick').trigger('change')
                                refresh_slot();
                                alertify.success(respose.msg)
                                $('#resform input[name="firstname"],#resform input[name="lastname"]').val('')
                                $('#resform input[type="hidden"]').val('')
                            } else {
                                alertify.error(respose.msg)
                            }
                            
                        }
                    })
                }
                , function(){ 
                    alertify.error('Cancel')

                });

                
            })  
            $('#resform #date').change(function(){
                refresh_slot();
            })
            $('#reserve_datepick').change(function(){
                $.ajax({
                    type : "post",
                    url  : $('#baseurl').val()+"reserve/reservation_list",
                    data : { date : $(this).val() },
                    success: function( respose ){
                        $('#reservation_wrapper').html(respose)
                    }
                })
            })        
        })
        var search_var;
        function refresh_slot(){
            date = $('#resform #date').val()
            if( date != "" ){
                $.ajax({
                    type: "post",
                    url : $('#baseurl').val()+'reserve/get_available_slot',
                    data: {date : date},
                    success: function( respose ){
                        $('#timeslot').html(respose);
                    }
                })   
            }
            
        }

        function search_member(searchkey){
            if (search_var != null){ 
                search_var.abort();
                search_var = null;
            }
            search_var = $.ajax({
                            type : "POST", //TODO: Must be changed to POST
                            url : $('#baseurl').val()+"reserve/searchmember",
                            data : { searchkey : searchkey },
                            success: function(respose){
                                $('.searchresult').html(respose).show()
                                setTimeout(function(){
                                    $('.searchresult').html('').hide();
                                },8000)
                            }
                            });
        }
    </script>
  </body>


</html>



