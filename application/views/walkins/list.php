<div class="table-responsive">
<table class="table" style="max-width: 500px;">
    <thead>
    	<tr>
            <th>Class Title</th>
            <th>Member's Rate</th>
            <th>Non Meber's Rate</th>
    	</tr>
    </thead>
   
    <?php if( !empty($walkins) ): ?>
        <tbody>
        <?php $total_rates = 0; ?>
        <?php foreach($walkins as $walkin):?>
            <tr>
                <td><?php echo $walkin->class_title ?></td>           
                <td>
                    <input type="number" step="any"  data-field="rate" value="<?php echo $walkin->rate > 0 ? $walkin->rate : 0 ?>" data-id="<?php echo $walkin->class_type_id?>" class="form-control edit-rate" />
                </td> 
                <td>
                    <input type="number" step="any" data-field="nonmem_rate" value="<?php echo $walkin->nonmem_rate > 0 ? $walkin->nonmem_rate : 0 ?>" data-id="<?php echo $walkin->class_type_id?>" class="form-control edit-rate" />
                </td>    
            </tr>
           
        <?php endforeach;?>
    </tbody>
	<?php else: ?>
    <tbody>
        <tr>
			<td colspan='4'>No records</td>
		</tr>
    </tbody>
    <?php endif; ?>
	
</table>
</div>