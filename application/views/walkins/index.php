<div class="row">
	<div class="col-md-12">
		<div class="panel">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="<?php echo base_url().'classes' ?>">Classes</a></li>
            <li role="presentation"><a href="<?php echo base_url().'classdurations' ?>">Durations</a></li>
            <li role="presentation" ><a  href="<?php echo base_url().'membershiptypes/'?>">Class Subscription Rates</a></li>
            <li role="presentation" class="active" ><a  href="#">walkin Rates</a></li>
        </ul>
            <header class="panel-heading">
                Walkin Rate List
            </header>
            <div class="panel-body">
                <div class="box-tools">
                <a href="<?php echo base_url().'classes/add'?>"><button class='btn btn-success btn-sm' data-toggle="modal" data-target="#metermodal">Add Class</button></a>
                </div>
                <div id="module_list">
                	<?php $this->load->view("walkins/list"); ?>
                </div>
            </div><!-- /.panel-body -->
        </div>
	</div>
</div>