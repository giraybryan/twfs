<div class="row">
	<div class="col-md-12">
		<div class="panel">
            <br/>
            <div class="container">
                <form class="form-horizontal" method="post" name="" action="<?php echo base_url('walkins/entry');?>">
                    <div class="form-group">
                        <label class="control-label col-sm-3">
                            Walkin Type
                        </label>
                        <div class="col-sm-8">
                            <div class="col-sm-8">
                                <select required name="walkin_type" id="walkin_type" class="form-control">
                                    <option value="">Select Type</option>
                                    <option value="member">Member</option>
                                    <option value="non">Non Member</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group hide" id="member_search">
                        <label class="control-label col-sm-3">
                            Member Search
                        </label>
                        <div class="col-sm-8">
                            <div class="col-sm-8">
                               <input type="text" class="form-control" id="membersearch">
                               <br/>
                               <div id="member_searchresult"></div>
                               <input type="text" placeholder="Member Selected" id="customername" class="form-control" />
                               <input type="hidden" id="member_id" name="member_id"/>
                               
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">
                            Class to Attend
                        </label>
                        <div class="col-sm-8">
                            <div class="col-sm-8">
                              <select class="form-control" required name="class_type_id">
                                <option value="">Select Class</option>
                                <?php if(!empty($classes)): ?>
                                    <?php foreach($classes as $class): ?>
                                        <option value="<?php echo $class->id ?>"><?php echo $class->class_title ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                              </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">
                            Amount
                        </label>
                        <div class="col-sm-8">
                            <div class="col-sm-8">
                               <input type="text" required readonly class="form-control" name="amount">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">
                            
                        </label>
                        <div class="col-sm-8">
                            <div class="col-sm-8">
                               <button type="submit" id="submit" class="form-control btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>