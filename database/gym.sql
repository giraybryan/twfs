-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2019 at 03:28 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gym`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `table_name` varchar(20) NOT NULL,
  `table_id` int(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `bankname` varchar(80) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `date_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bank_transactions`
--

CREATE TABLE `bank_transactions` (
  `id` int(11) NOT NULL,
  `bankid` int(11) NOT NULL,
  `transaction_type` enum('deposit','withdraw','transfer') NOT NULL DEFAULT 'deposit',
  `amount` float(10,2) NOT NULL,
  `date` date NOT NULL,
  `userid` int(11) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bmi`
--

CREATE TABLE `bmi` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `weight` float(10,2) NOT NULL,
  `body_fat` float(10,2) NOT NULL,
  `visceral` int(11) NOT NULL,
  `metabolic_age` int(11) NOT NULL,
  `muscle_mass` float(10,2) NOT NULL,
  `rmr` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(11) NOT NULL,
  `branch_name` varchar(100) DEFAULT NULL,
  `date_started` date DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cashflow`
--

CREATE TABLE `cashflow` (
  `cashid` int(11) NOT NULL,
  `coh` float(10,2) NOT NULL DEFAULT 0.00,
  `expenses` float(10,2) NOT NULL DEFAULT 0.00,
  `deposit` float(10,2) NOT NULL DEFAULT 0.00,
  `withdraw` float(10,2) NOT NULL DEFAULT 0.00,
  `equity` float(10,2) NOT NULL DEFAULT 0.00,
  `date` date NOT NULL,
  `userid` int(11) NOT NULL DEFAULT 0,
  `dateupdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chillers`
--

CREATE TABLE `chillers` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT 0,
  `selling_price` float(10,2) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT 0,
  `capital_price` float(10,2) NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chillers_log`
--

CREATE TABLE `chillers_log` (
  `id` int(11) NOT NULL,
  `chiller_id` int(11) NOT NULL,
  `qty` int(2) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `note` text NOT NULL,
  `paid` tinyint(1) NOT NULL DEFAULT 0,
  `member_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `walkin_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_reason` text NOT NULL,
  `deleted_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chiller_dailysales_log`
--

CREATE TABLE `chiller_dailysales_log` (
  `id` int(11) NOT NULL,
  `chiller_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `sell` int(11) NOT NULL DEFAULT 0,
  `remaining` int(11) NOT NULL DEFAULT 0,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chiller_daily_cash`
--

CREATE TABLE `chiller_daily_cash` (
  `id` int(11) NOT NULL,
  `box_type` enum('capital','profit') NOT NULL,
  `amount` float(10,2) NOT NULL,
  `date` date NOT NULL,
  `lastday` float(10,2) NOT NULL,
  `lastday_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chiller_daily_cash_log`
--

CREATE TABLE `chiller_daily_cash_log` (
  `id` int(11) NOT NULL,
  `capital` float(10,2) NOT NULL,
  `prev_capital` float(10,2) NOT NULL,
  `profit` float(10,2) NOT NULL,
  `prev_profit` float(10,2) NOT NULL,
  `date` date NOT NULL,
  `prev_date` date NOT NULL,
  `total_expense` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chiller_inventory`
--

CREATE TABLE `chiller_inventory` (
  `id` int(11) NOT NULL,
  `chiller_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `class_duration`
--

CREATE TABLE `class_duration` (
  `id` int(11) NOT NULL,
  `durations` int(11) NOT NULL,
  `duration_type` enum('monthly','session') NOT NULL DEFAULT 'monthly'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `class_price`
--

CREATE TABLE `class_price` (
  `id` int(11) NOT NULL,
  `class_type_id` int(11) NOT NULL,
  `membership_type_id` int(11) NOT NULL,
  `price` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `class_type`
--

CREATE TABLE `class_type` (
  `id` int(11) NOT NULL,
  `class_title` varchar(50) NOT NULL,
  `membership_type` tinyint(1) NOT NULL,
  `class_category_id` int(11) NOT NULL,
  `on_off` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_type`
--

INSERT INTO `class_type` (`id`, `class_title`, `membership_type`, `class_category_id`, `on_off`) VALUES
(1, 'CBH', 1, 1, 1),
(2, 'TRX', 0, 1, 0),
(3, 'Zumba', 0, 1, 0),
(4, 'Baby Ballet', 1, 2, 0),
(5, 'Kids Hiphop', 0, 2, 0),
(8, 'Muay Thai', 0, 3, 0),
(9, 'Boxing', 0, 3, 0),
(10, 'Circuit & Boxing', 0, 0, 0),
(11, 'Circuit & Muay Thai', 0, 0, 0),
(12, 'Circuit & Aerial Silk', 0, 0, 0),
(13, 'Circuit & TRX', 0, 0, 0),
(14, 'Circuit & Dance Class', 0, 0, 0),
(15, 'Aerial Silk', 0, 0, 0),
(16, 'Basic Hiphop', 0, 2, 0),
(17, 'Contemporary Jazz', 0, 2, 0),
(18, 'OCR Training', 0, 1, 1),
(19, 'CBH + OCR', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `class_type_categories`
--

CREATE TABLE `class_type_categories` (
  `id` int(11) NOT NULL,
  `category_title` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coaches`
--

CREATE TABLE `coaches` (
  `id` int(11) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `class` varchar(100) NOT NULL,
  `type` enum('partime','fulltime') NOT NULL DEFAULT 'partime'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `firstname` varchar(20) DEFAULT NULL,
  `lastname` varchar(20) DEFAULT NULL,
  `middlename` varchar(20) DEFAULT NULL,
  `age` int(2) NOT NULL DEFAULT 0,
  `address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `daily_attendance`
--

CREATE TABLE `daily_attendance` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL DEFAULT 0,
  `walkin_id` int(11) NOT NULL DEFAULT 0,
  `class_type_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `paid` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `equities`
--

CREATE TABLE `equities` (
  `eqid` int(11) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `date` date NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(11) NOT NULL,
  `expense_type_id` int(100) NOT NULL,
  `description` text NOT NULL,
  `cost` float(10,2) NOT NULL,
  `image` varchar(100) NOT NULL,
  `datecreated` datetime NOT NULL,
  `paid` tinyint(1) NOT NULL DEFAULT 0,
  `date_paid` datetime NOT NULL,
  `userid` varchar(20) NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expense_type`
--

CREATE TABLE `expense_type` (
  `id` int(11) NOT NULL,
  `expense_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense_type`
--

INSERT INTO `expense_type` (`id`, `expense_type`) VALUES
(1, 'Place Rental'),
(2, 'Electric Bill'),
(3, 'Water Bill'),
(4, 'Supplies'),
(5, 'Payroll'),
(6, 'Internet Bill'),
(7, 'Daily Staff Payment'),
(8, 'Aircon Maintenance'),
(9, 'BIR'),
(10, 'Merchandise'),
(11, 'Maintenance'),
(12, 'Phone Bill'),
(13, 'Promotion Boost : So'),
(14, 'spartan fee');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_equipments`
--

CREATE TABLE `inventory_equipments` (
  `id` int(11) NOT NULL,
  `equipment_name` varchar(20) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `middlename` varchar(20) NOT NULL,
  `bd` date NOT NULL,
  `age` int(2) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` text NOT NULL,
  `gender` enum('m','f') NOT NULL,
  `class_type_id` int(11) NOT NULL,
  `membership_type_id` int(11) NOT NULL,
  `status` enum('active','expired','cancelled') NOT NULL DEFAULT 'active',
  `referral_id` int(11) NOT NULL,
  `date_registered` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `branch_id` int(11) DEFAULT NULL,
  `height` varchar(100) DEFAULT NULL,
  `weight` float(10,2) DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `membership_log`
--

CREATE TABLE `membership_log` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `membership_type_id` int(11) NOT NULL,
  `class_type_id` int(11) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date DEFAULT NULL,
  `additional_days` int(2) DEFAULT NULL,
  `paid` tinyint(1) NOT NULL DEFAULT 0,
  `date_register` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` enum('valid','expired') NOT NULL DEFAULT 'valid',
  `promo_id` int(11) DEFAULT 0,
  `note` text NOT NULL,
  `amount` float(10,2) NOT NULL,
  `renew` tinyint(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `membership_type`
--

CREATE TABLE `membership_type` (
  `id` int(11) NOT NULL,
  `class_type_id` int(11) NOT NULL,
  `duration_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `price` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `merchandise`
--

CREATE TABLE `merchandise` (
  `id` int(11) NOT NULL,
  `item` varchar(50) NOT NULL,
  `price` float(10,2) NOT NULL DEFAULT 0.00,
  `stock` int(11) NOT NULL DEFAULT 0,
  `cost` float(10,2) NOT NULL DEFAULT 0.00,
  `description` text NOT NULL,
  `has_variant` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `merchandise_inventory`
--

CREATE TABLE `merchandise_inventory` (
  `id` int(11) NOT NULL,
  `merchandise_id` int(11) NOT NULL,
  `merchandise_variant_id` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `userid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `merchandise_variants`
--

CREATE TABLE `merchandise_variants` (
  `id` int(11) NOT NULL,
  `merchandise_id` int(11) NOT NULL,
  `variant` varchar(20) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT 0,
  `cost` float(10,2) NOT NULL DEFAULT 0.00,
  `price` float(10,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `or_issue`
--

CREATE TABLE `or_issue` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `total` float(10,2) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `owners`
--

CREATE TABLE `owners` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `cash` float(10,2) NOT NULL,
  `credit_balance` float(10,2) NOT NULL,
  `share` float(10,2) NOT NULL,
  `userid` int(11) NOT NULL,
  `rate` float(10,3) NOT NULL,
  `real_rate` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_earnings`
--

CREATE TABLE `payroll_earnings` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `rate` float(10,2) NOT NULL,
  `days` int(2) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `userid` int(11) NOT NULL,
  `payroll_id` int(11) NOT NULL,
  `earnings_type` int(11) NOT NULL,
  `amount` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_sales_log`
--

CREATE TABLE `product_sales_log` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promos`
--

CREATE TABLE `promos` (
  `id` int(11) NOT NULL,
  `promo_title` varchar(50) NOT NULL,
  `class_type_id` int(11) NOT NULL DEFAULT 1,
  `description` text NOT NULL,
  `price_rate` int(11) NOT NULL,
  `onemonth` float(10,2) NOT NULL,
  `threemonths` float(10,2) NOT NULL,
  `sixmonths` float(10,2) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `suspended` tinyint(1) NOT NULL DEFAULT 0,
  `default_promo` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_log`
--

CREATE TABLE `rental_log` (
  `id` int(11) NOT NULL,
  `rental_user_id` int(11) NOT NULL,
  `rentaldate_start` datetime NOT NULL,
  `total_hours` int(2) NOT NULL,
  `rentaldate_end` datetime NOT NULL,
  `purpose` text NOT NULL,
  `discount` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salaries`
--

CREATE TABLE `salaries` (
  `salaryid` int(11) NOT NULL,
  `coachid` int(11) NOT NULL,
  `rate` float(10,2) NOT NULL DEFAULT 0.00,
  `dayswork` int(11) NOT NULL DEFAULT 0,
  `daysholiday` int(11) NOT NULL DEFAULT 0,
  `holidayrate` float(10,2) NOT NULL DEFAULT 0.00,
  `gross` float(10,2) NOT NULL DEFAULT 0.00,
  `date` date NOT NULL,
  `datecreated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `userid` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salary_deductions`
--

CREATE TABLE `salary_deductions` (
  `salarydedid` int(11) NOT NULL,
  `deductiontype_id` int(11) NOT NULL,
  `deduction_amount` float(10,2) NOT NULL DEFAULT 0.00,
  `salaryid` int(11) NOT NULL,
  `date` date NOT NULL,
  `datecreated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salary_deduction_types`
--

CREATE TABLE `salary_deduction_types` (
  `deduct_id` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `datecreated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `class_type_id` int(11) NOT NULL,
  `membership_type_id` int(11) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `promo_id` int(11) NOT NULL DEFAULT 0,
  `member_id` int(11) NOT NULL,
  `description` text DEFAULT NULL,
  `date` date NOT NULL,
  `membership_log_id` int(11) NOT NULL DEFAULT 0,
  `merchandise_variant_id` int(11) NOT NULL DEFAULT 0,
  `walkin_log_id` int(11) NOT NULL DEFAULT 0,
  `userid` int(11) NOT NULL DEFAULT 0,
  `sales_type_id` int(11) NOT NULL DEFAULT 3,
  `date_created` date DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `branch_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_type`
--

CREATE TABLE `sales_type` (
  `id` int(11) NOT NULL,
  `sales_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `session_counter`
--

CREATE TABLE `session_counter` (
  `id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `membership_log_id` int(11) NOT NULL,
  `total_session` int(11) NOT NULL DEFAULT 0,
  `class_type_id` int(11) NOT NULL,
  `total_allowed` int(11) NOT NULL,
  `finished` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `superadmin` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `superadmin`) VALUES
(1, 'admin', 'admin', 1),
(2, 'bryan', 'warehouse1', 1),
(3, 'andrew', 'warehouse1', 1),
(4, 'jewel', 'warehouse1', 1),
(5, 'james', 'warehouse1', 0),
(6, 'monica', 'warehouse1', 0),
(7, 'bible', 'warehouse1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_session`
--

CREATE TABLE `user_session` (
  `id` int(11) NOT NULL,
  `session` text NOT NULL,
  `active` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_session`
--

INSERT INTO `user_session` (`id`, `session`, `active`) VALUES
(2, '{\"username\":\"bryan\",\"userid\":\"2\",\"logged_in\":true}', 1),
(3, '{\"username\":\"admin\",\"userid\":\"1\",\"logged_in\":true}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(11) NOT NULL,
  `vouchercode` varchar(50) NOT NULL,
  `controlnum` int(11) NOT NULL,
  `membership_type_id` int(11) NOT NULL,
  `expiration` date NOT NULL,
  `used` tinyint(1) NOT NULL DEFAULT 0,
  `membership_log_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `walkin_log`
--

CREATE TABLE `walkin_log` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `class_type_id` int(2) NOT NULL,
  `date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `walkin_rates`
--

CREATE TABLE `walkin_rates` (
  `id` int(11) NOT NULL,
  `class_type_id` int(11) NOT NULL,
  `rate` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `walkin_rates`
--

INSERT INTO `walkin_rates` (`id`, `class_type_id`, `rate`) VALUES
(1, 1, 300.00),
(2, 2, 300.00),
(3, 3, 250.00),
(4, 15, 550.00),
(5, 8, 350.00),
(6, 9, 300.00),
(7, 16, 300.00),
(8, 17, 300.00);

-- --------------------------------------------------------

--
-- Table structure for table `warehouse_cash`
--

CREATE TABLE `warehouse_cash` (
  `id` int(11) NOT NULL,
  `amount` float(10,2) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_transactions`
--
ALTER TABLE `bank_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bmi`
--
ALTER TABLE `bmi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cashflow`
--
ALTER TABLE `cashflow`
  ADD PRIMARY KEY (`cashid`);

--
-- Indexes for table `chillers`
--
ALTER TABLE `chillers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chillers_log`
--
ALTER TABLE `chillers_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chiller_dailysales_log`
--
ALTER TABLE `chiller_dailysales_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chiller_daily_cash`
--
ALTER TABLE `chiller_daily_cash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chiller_daily_cash_log`
--
ALTER TABLE `chiller_daily_cash_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chiller_inventory`
--
ALTER TABLE `chiller_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_duration`
--
ALTER TABLE `class_duration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_price`
--
ALTER TABLE `class_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_type`
--
ALTER TABLE `class_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_type_categories`
--
ALTER TABLE `class_type_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coaches`
--
ALTER TABLE `coaches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_attendance`
--
ALTER TABLE `daily_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equities`
--
ALTER TABLE `equities`
  ADD PRIMARY KEY (`eqid`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense_type`
--
ALTER TABLE `expense_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `membership_log`
--
ALTER TABLE `membership_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `membership_type`
--
ALTER TABLE `membership_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merchandise`
--
ALTER TABLE `merchandise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merchandise_inventory`
--
ALTER TABLE `merchandise_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merchandise_variants`
--
ALTER TABLE `merchandise_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `or_issue`
--
ALTER TABLE `or_issue`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owners`
--
ALTER TABLE `owners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll_earnings`
--
ALTER TABLE `payroll_earnings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_sales_log`
--
ALTER TABLE `product_sales_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promos`
--
ALTER TABLE `promos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rental_log`
--
ALTER TABLE `rental_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salaries`
--
ALTER TABLE `salaries`
  ADD PRIMARY KEY (`salaryid`);

--
-- Indexes for table `salary_deductions`
--
ALTER TABLE `salary_deductions`
  ADD PRIMARY KEY (`salarydedid`);

--
-- Indexes for table `salary_deduction_types`
--
ALTER TABLE `salary_deduction_types`
  ADD PRIMARY KEY (`deduct_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_type`
--
ALTER TABLE `sales_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `session_counter`
--
ALTER TABLE `session_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_session`
--
ALTER TABLE `user_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `walkin_log`
--
ALTER TABLE `walkin_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `walkin_rates`
--
ALTER TABLE `walkin_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warehouse_cash`
--
ALTER TABLE `warehouse_cash`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bank_transactions`
--
ALTER TABLE `bank_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bmi`
--
ALTER TABLE `bmi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cashflow`
--
ALTER TABLE `cashflow`
  MODIFY `cashid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chillers`
--
ALTER TABLE `chillers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chillers_log`
--
ALTER TABLE `chillers_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chiller_dailysales_log`
--
ALTER TABLE `chiller_dailysales_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chiller_daily_cash`
--
ALTER TABLE `chiller_daily_cash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chiller_daily_cash_log`
--
ALTER TABLE `chiller_daily_cash_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chiller_inventory`
--
ALTER TABLE `chiller_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `class_duration`
--
ALTER TABLE `class_duration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `class_price`
--
ALTER TABLE `class_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `class_type`
--
ALTER TABLE `class_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `class_type_categories`
--
ALTER TABLE `class_type_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coaches`
--
ALTER TABLE `coaches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daily_attendance`
--
ALTER TABLE `daily_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `equities`
--
ALTER TABLE `equities`
  MODIFY `eqid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expense_type`
--
ALTER TABLE `expense_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `membership_log`
--
ALTER TABLE `membership_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `membership_type`
--
ALTER TABLE `membership_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `merchandise`
--
ALTER TABLE `merchandise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `merchandise_inventory`
--
ALTER TABLE `merchandise_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `merchandise_variants`
--
ALTER TABLE `merchandise_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `or_issue`
--
ALTER TABLE `or_issue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owners`
--
ALTER TABLE `owners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payroll_earnings`
--
ALTER TABLE `payroll_earnings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_sales_log`
--
ALTER TABLE `product_sales_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `promos`
--
ALTER TABLE `promos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_log`
--
ALTER TABLE `rental_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salaries`
--
ALTER TABLE `salaries`
  MODIFY `salaryid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salary_deductions`
--
ALTER TABLE `salary_deductions`
  MODIFY `salarydedid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salary_deduction_types`
--
ALTER TABLE `salary_deduction_types`
  MODIFY `deduct_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_type`
--
ALTER TABLE `sales_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `session_counter`
--
ALTER TABLE `session_counter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_session`
--
ALTER TABLE `user_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `walkin_log`
--
ALTER TABLE `walkin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `walkin_rates`
--
ALTER TABLE `walkin_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `warehouse_cash`
--
ALTER TABLE `warehouse_cash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
